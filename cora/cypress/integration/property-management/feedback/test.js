// describe("test wrong credentials", () => {
//   it("renders properly", () => {
//     cy.visit("http://localhost:3000/")
//       .get(
//         ".MuiBox-root-88 > .MuiFormControl-root > .MuiInputBase-root > .MuiInputBase-input.login-email-input",
//       )
//       .click()
//       .type("carlo@cora.pro")
//       .get(
//         ".MuiBox-root-90 > .MuiFormControl-root > .MuiInputBase-root > .MuiInputBase-input",
//       )
//       .click()
//       .type("1234567")
//       .get(".MuiBox-root > .MuiButtonBase-root")
//       .click()
//   })
// })

describe("test render", () => {
  it("renders properly", () => {
    cy.visit("http://localhost:3000/")
      .get(".login-email-input")
      .click()
      .type("carlo@cora.pro")
      .get(
        ".MuiBox-root-90 > .MuiFormControl-root > .MuiInputBase-root > .MuiInputBase-input"
      )
      .click()
      .type("123456789")
      .get(".MuiBox-root > .MuiButtonBase-root")
      .click()
  })
})

describe("feedback", () => {
  it("renders feedback page", () => {
    cy.get(".MuiBox-root-448")
      .click()
      .get(".cta-add-btn")
      .click()
      .get(".btn-next")
      .click()
      .get("#mui-component-select-blockUnit")
      .click()
      .get(".ec06ffe6-e987-41bd-a733-3abb9b0e8086")
      .click()
      .get("#mui-component-select-residentName")
      .click()
      .get(".name-dame")
      .click()
      .get(".btn-next")
      .click()
      .get(".btn-next")
      .click()
      .get("#mui-component-select-feedbackType")
      .click()
      .get(".feedbacktype-security")
      .click()
      .get("#mui-component-select-assignTo")
      .click()
      .get(".staff-cleaner")
      .click()
      .get("#description")
      .click()
      .type("this is a test")
      .get(".btn-cancel")
      .click()
      .get(".btn-dialog-cancel")
      .click()
      .get(".btn-next")
      .click()
  })
})
