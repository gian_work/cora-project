module.exports = {
  root: true,
  plugins: ["eslint-plugin-cypress"],
  extends: ["plugin:cypress/recommended"],
  env: { "cypress/globals": true },
  "rules": {
    "cypress/no-assigning-return-values": "warn",
    "cypress/no-unnecessary-waiting": "warn",
    "cypress/assertion-before-screenshot": "warn",
    "cypress/no-force": "warn"
  }
}
