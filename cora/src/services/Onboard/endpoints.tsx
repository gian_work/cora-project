const version = "v1"

const API = {
  // ONBOARD
  LOGIN_USER: `svc-account/${version}/admin-staff/login`,
  LOGIN_STAFF: `svc-account/${version}/staff/login`,
  FORGOT_PASSWORD: `svc-account/${version}/forgot-password`
}

export default API
