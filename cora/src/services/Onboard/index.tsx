import request from "../request"
import API from "./endpoints"

function loginUser(data: Object) {
  return request({
    url: API.LOGIN_USER,
    method: "POST",
    headers: {
      "Content-Type": "text/plain"
    },
    data: data
  })
}

function loginStaff(data: Object) {
  return request({
    url: API.LOGIN_STAFF,
    method: "POST",
    headers: {
      "Content-Type": "text/plain"
    },
    data: data
  })
}

function forgotPassword(email: String) {
  return request({
    url: API.FORGOT_PASSWORD,
    method: "POST",
    data: {
      "email": email,
      "device_id": "web",
      "method_type": "ACCESS_CODE"
    }
  })
}

const OnboardAPI = {
  loginUser,
  forgotPassword,
  loginStaff
}

export default OnboardAPI
