const version = "v1"
const base = "svc-application"

const API = {
  LIST_VEHICLES: `${base}/${version}/vehicles`,
  LIST_APPLICATIONS: `${base}/${version}/applications`,
  APPROVE_APPLICATION: `${base}/${version}/application/approve`,
  REJECT_APPLICATION: `${base}/${version}/application/reject`,
  CREATE_APPLICATION: `${base}/${version}/application`,
  UPDATE_APPLICATION: `${base}/${version}/application`,
  CREATE_VEHICLE: `${base}/${version}/vehicle`,
  UPDATE_VEHICLE: `${base}/${version}/vehicle`,
  DELETE_VEHICLE: `${base}/${version}/vehicle`
}

export default API
