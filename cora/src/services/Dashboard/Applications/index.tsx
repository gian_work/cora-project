import request from "services/request"
import API from "./endpoints"
import Cookies from "js-cookie"

function headers() {
  return {
    Authorization: `Bearer ${Cookies.get("bearer")}`
  }
}

function getApplications(params: Record<string, any>) {
  return request({
    url: API.LIST_APPLICATIONS,
    method: "GET",
    headers: headers(),
    params: params
  })
}

function createApplication(data: Object) {
  return request({
    url: API.CREATE_APPLICATION,
    method: "POST",
    data: data,
    headers: headers()
  })
}

function updateApplication(data: Object, uid: string) {
  return request({
    url: `${API.CREATE_APPLICATION}/${uid}`,
    method: "PUT",
    data: data,
    headers: headers()
  })
}

function approveApplication(data: Object) {
  return request({
    url: API.APPROVE_APPLICATION,
    method: "POST",
    data: data,
    headers: headers()
  })
}

function rejectApplication(data: Object) {
  return request({
    url: API.REJECT_APPLICATION,
    method: "POST",
    data: data,
    headers: headers()
  })
}

function getVehicles(params?: Record<string, any>) {
  return request({
    url: API.LIST_VEHICLES,
    method: "GET",
    headers: headers(),
    params: {
      "condo_uid": Cookies.get("condoUID"),
      ...params
    }
  })
}

function createVehicle(data: Object) {
  return request({
    url: API.CREATE_VEHICLE,
    method: "POST",
    data: data,
    headers: headers()
  })
}

function updateVehicle(data: Object, uid: string) {
  return request({
    url: `${API.UPDATE_VEHICLE}/${uid}`,
    method: "PUT",
    data: data,
    headers: headers()
  })
}

function deleteVehicle(uid: string) {
  return request({
    url: `${API.DELETE_VEHICLE}/${uid}`,
    method: "DELETE",
    headers: headers()
  })
}

const BookkeepingAPI = {
  getApplications,
  approveApplication,
  rejectApplication,
  createApplication,
  updateApplication,
  getVehicles,
  createVehicle,
  updateVehicle,
  deleteVehicle
}

export default BookkeepingAPI
