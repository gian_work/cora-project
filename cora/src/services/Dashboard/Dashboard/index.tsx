import Cookies from "js-cookie"

import request from "services/request"
import API from "./endpoints"

function headers() {
  return {
    "Authorization": `Bearer ${Cookies.get("bearer")}`
  }
}

function getCountAccountRequests(params?: Object) {
  return request({
    url: API.COUNT_ACCOUNT_REQUESTS,
    method: "GET",
    params: {
      "condo_uid": Cookies.get("condoUID"),
      ...params
    },
    headers: headers()
  })
}

function getCountChangeAddress(params?: Object) {
  return request({
    url: API.CHANGE_ADDRESS,
    method: "GET",
    params: {
      "condo_uid": Cookies.get("condoUID"),
      ...params
    },
    headers: headers()
  })
}

function getCountFeedback(params?: Object) {
  return request({
    url: API.FEEDBACK_NO_REPLY,
    method: "GET",
    params: {
      "condo_uid": Cookies.get("condoUID"),
      ...params
    },
    headers: headers()
  })
}

const CommonApi = {
  getCountAccountRequests,
  getCountChangeAddress,
  getCountFeedback
}

export default CommonApi
