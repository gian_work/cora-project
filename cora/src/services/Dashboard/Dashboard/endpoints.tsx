const version = "v1"
const baseFacility = "svc-facility"
const baseFeedback = "svc-feedback"

const API = {
  // ONBOARD
  COUNT_ACCOUNT_REQUESTS: `${baseFacility}/${version}/account-requests-count`,
  CHANGE_ADDRESS: `${baseFacility}/${version}/change-address-requests-count`,
  FEEDBACK_NO_REPLY: `${baseFeedback}/${version}/feedback-no-reply-count`
}

export default API
