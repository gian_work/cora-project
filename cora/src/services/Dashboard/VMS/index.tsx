import request from "services/request"
import API from "./endpoints"
import Cookies from "js-cookie"

function headers() {
  return {
    "Authorization": `Bearer ${Cookies.get("bearer")}`
  }
}

function createVisitor(data: Object) {
  return request({
    url: API.VMS_CREATE_VISITOR,
    method: "POST",
    data: data,
    headers: headers()
  })
}

function createDelivery(data: Object) {
  return request({
    url: API.VMS_CREATE_DELIVERY,
    method: "POST",
    data: data,
    headers: headers()
  })
}

function createServiceProvider(data: Object) {
  return request({
    url: API.VMS_CREATE_SERVICEPROVIDER,
    method: "POST",
    data: data,
    headers: headers()
  })
}

function getVMS(params?: Record<string, number>) {
  return request({
    url: API.VMS_FETCH_ALL,
    method: "GET",
    params: { "condo_uid": Cookies.get("condoUID"), ...params },
    headers: headers()
  })
}

function getUnits(params: any) {
  return request({
    url: API.VMS_FETCH_ALL_UNITS,
    method: "GET",
    params: params,
    headers: headers()
  })
}

function updateStatusVMS(type: number, data: Object) {
  const endpoint = (vmsType: number) => {
    switch (vmsType) {
      case 1:
        return API.VMS_UPDATE_VISITOR
      case 2:
        return API.VMS_UPDATE_DELIVERY
      case 3:
        return API.VMS_UPDATE_SERVICEPROVIDER
      default:
        return
    }
  }

  return request({
    url: endpoint(type),
    method: "POST",
    headers: {
      "Authorization": `Bearer ${Cookies.get("bearer")}`,
      "X-Device-Identifier": "web",
      "Content-Type": "application/json"
    },
    data: data
  })
}

const VmsAPI = {
  createVisitor,
  createDelivery,
  createServiceProvider,
  getVMS,
  getUnits,
  updateStatusVMS
}

export default VmsAPI
