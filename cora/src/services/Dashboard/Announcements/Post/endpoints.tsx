const version = "v1"
const base = "svc-feed"

const API = {
  ANNOUNCEMENT_POSTS: `${base}/${version}/announcements`,
  POST: `${base}/${version}/announcement`
}

export default API
