import request from "services/request"
import API from "./endpoints"
import Cookies from "js-cookie"

function headers() {
  return {
    Authorization: `Bearer ${Cookies.get("bearer")}`
  }
}

function getAnnouncementEvent(params: Record<string, any>) {
  return request({
    url: API.EVENT,
    method: "GET",
    headers: headers(),
    params: params
  })
}

const AnnouncementAPI = {
  getAnnouncementEvent
}

export default AnnouncementAPI
