const version = "v1"
const base = "svc-feed"

const API = {
  EVENT: `${base}/${version}/announcements?announcement_type=3`
}

export default API
