const version = "v1"
const base = "svc-feed"

const API = {
  BANNER: `${base}/${version}/announcements?announcement_type=2`
}

export default API
