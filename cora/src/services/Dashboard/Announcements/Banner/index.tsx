import request from "services/request"
import API from "./endpoints"
import Cookies from "js-cookie"

function headers() {
  return {
    Authorization: `Bearer ${Cookies.get("bearer")}`
  }
}

function getAnnouncementBanner(params: Record<string, any>) {
  return request({
    url: API.BANNER,
    method: "GET",
    headers: headers(),
    params: params
  })
}

const AnnouncementAPI = {
  getAnnouncementBanner
}

export default AnnouncementAPI
