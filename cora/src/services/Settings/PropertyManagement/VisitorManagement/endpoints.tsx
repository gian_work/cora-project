const version = "v1"
const base = "svc-vms"

const API = {
  LIST_VMS: `${base}/${version}/settings`,
  CREATE_VMS: `${base}/${version}/setting`,
  UPDATE_VMS: `${base}/${version}/setting`,
  DELETE_VMS: `${base}/${version}/setting`
}

export default API
