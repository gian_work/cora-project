const version = "v1"
const base = "svc-bookkeeping"

const API = {
  LIST_BOOKKEEPING: `${base}/${version}/settings`,
  UPDATE_BOOKKEEPING: `${base}/${version}/setting`
}

export default API
