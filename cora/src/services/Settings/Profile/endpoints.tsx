const version = "v1"
const base = "svc-account"
const root = `${base}/${version}`
const API = {
  UPDATE_INFO: `${root}/account-info`
}

export default API
