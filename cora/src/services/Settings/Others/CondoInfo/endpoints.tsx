const version = "v1"
const base = "svc-facility"

const API = {
  CONDO_INFO: `${base}/${version}/condo/`,
  CONDO_INFO_UPDATE: `${base}/${version}/condo/`
}

export default API
