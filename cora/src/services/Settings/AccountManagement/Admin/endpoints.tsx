const version = "v1"
const base = "svc-account"

const root = `${base}/${version}`

const API = {
  LIST_ROLES: `${root}/user-roles`,
  CREATE_ROLE: `${root}/user-role`,
  UPDATE_PERMISSION: `${root}/user-role-permission`,
  LIST_ROLE_PERMISSION: `${root}/user-role-permission`,
  LIST_ADMINS: `${root}/admin/users`,
  ADMIN_USER: `${root}/admin-user`,
  UPDATE_USER: `${root}/admin-user`
}

export default API
