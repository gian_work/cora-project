const version = "v1"
const base = "svc-account"

const root = `${base}/${version}`

const API = {
  LIST_ROLES: `${root}/user-roles`,
  CREATE_ROLE: `${root}/user-role`,
  UPDATE_PERMISSION: `${root}/user-role-permission`,
  LIST_ROLE_PERMISSION: `${root}/user-role-permission`,
  LIST_STAFFS: `${root}/staff/users`,
  STAFF_USER: `${root}/staff-user`,
  UPDATE_STAFF: `${root}/staff-user`
}

export default API
