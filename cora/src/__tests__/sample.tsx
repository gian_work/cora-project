import React from "react"
import { render, screen, fireEvent } from "@testing-library/react"
import OwnersTable from "pages/Dashboard/UnitManagement/"

test("show Owners title", () => {
  render(<OwnersTable />)
  const pageTitle = screen.queryByText(/Owners/i)
  expect(pageTitle).toBeTruthy()
})

// test("show download csv", () => {
//   render(<OwnersTable createOwner={() => null} />)
//   const downloadCsv = screen.queryByText(/Download CSV/i)
//   expect(downloadCsv).toBeTruthy()
// })

// test("show add new user", () => {
//   render(<OwnersTable createOwner={() => null} />)
//   const TableAction = screen.queryByText(/Add New User/i)
//   expect(TableAction).toBeTruthy()
// })
