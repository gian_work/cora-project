import React from "react"
import { AuthProvider } from "context/auth-context"

// Page
import AppMain from "./layout/AppMain"
// import AppInit from "./layout/AppMain/init"

const App: React.FC = () => {
  return (
    <div>
      <AuthProvider>
        <AppMain />
      </AuthProvider>
    </div>
  )
}

export default App
