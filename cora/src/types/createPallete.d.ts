// declare module "@material-ui/core/styles/createPalette" {
//   interface PaletteOptions {
//     success?: PaletteColorOptions
//     warning?: PaletteColorOptions
//     gray?: PaletteColorOptions
//     blue?: PaletteColorOptions
//   }
// }

import createPalette from "@material-ui/core/styles"

declare module "@material-ui/core/styles/createPalette" {
  interface IconPaletteColorOptions {
    main?: string
  }
  interface IconPaletteColor {
    main: string
  }

  interface BodyPaletteColorOptions {
    main?: string
    secondary?: string
    dark?: string
    green?: string
    greenLight100?: string
    greenLight?: string
    pink?: string
    orange?: string
    gold?: string
    blue?: string
    brown?: string
    gray?: string
  }
  interface BodyPaletteColor {
    main: string
    secondary: string
    dark?: string
    green?: string
    greenLight100?: string
    greenLight?: string
    pink?: string
    orange?: string
    gold?: string
    blue?: string
    brown?: string
    gray?: string
  }

  interface PaletteOptions {
    success?: PaletteColorOptions
    warning?: PaletteColorOptions
    sidebar?: IconPaletteColorOptions
    body?: BodyPaletteColorOptions
    green?: BodyPaletteColorOptions
    greenLight100?: BodyPaletteColorOptions
    greenLight?: BodyPaletteColorOptions
    pink?: BodyPaletteColorOptions
    orange?: BodyPaletteColorOptions
    gold?: BodyPaletteColorOptions
    blue?: BodyPaletteColorOptions
    brown?: BodyPaletteColorOptions
    gray?: BodyPaletteColorOptions
  }
  interface Palette {
    success: PaletteColor
    warning: PaletteColor
    sidebar: IconPaletteColor
    body: BodyPaletteColor
    green?: BodyPaletteColor
    greenLight100?: BodyPaletteColor
    greenLight?: BodyPaletteColor
    pink?: BodyPaletteColor
    orange?: BodyPaletteColor
    gold?: BodyPaletteColor
    blue?: BodyPaletteColor
    brown?: BodyPaletteColor
    gray?: BodyPaletteColor
  }
}
