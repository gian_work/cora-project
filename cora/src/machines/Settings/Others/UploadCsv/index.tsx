import { Machine } from "xstate"

export const UploadCsvMachine = Machine({
  id: "UploadCsvAdmin",
  initial: "tableView",
  states: {
    tableView: {
      on: {
        SELECT_FILE: "selectFile",
        FILE_UPLOAD: "fileUpload"
      }
    },
    selectFile: {
      on: {
        BACK: "tableView"
      }
    },
    fileUpload: {
      on: {
        BACK: "tableView"
      }
    }
  },
  on: {
    TABLE_VIEW: "tableView",
    SELECT_FILE: "selectFile",
    FILE_UPLOAD: "fileUpload"
  }
})
