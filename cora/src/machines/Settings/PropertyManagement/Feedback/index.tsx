import { Machine } from "xstate"

export const SettingsFeedbackMachine = Machine({
  id: "SettingsFeedback",
  initial: "tableView",
  states: {
    tableView: {
      on: {
        ADD_CATEGORY: "addCategory",
        UPDATE_CATEGORY: "updateCategory"
      }
    },
    addCategory: {
      on: {
        BACK: "tableView"
      }
    },
    updateCategory: {
      on: {
        BACK: "tableView"
      }
    }
  },
  on: {
    TABLE_VIEW: "tableView",
    ADD_CATEGORY: "addCategory",
    UPDATE_CATEGORY: "updateCategory"
  }
})
