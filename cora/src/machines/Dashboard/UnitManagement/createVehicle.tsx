import { Machine } from "xstate"

export const CreateVehicleMachine = Machine({
  id: "createVehicleMachine",
  initial: "personalDetails",
  states: {
    personalDetails: {
      on: {
        NEXT: "vehicleDetails"
      }
    },
    vehicleDetails: {
      on: {
        BACK: "personalDetails"
      }
    }
  }
})
