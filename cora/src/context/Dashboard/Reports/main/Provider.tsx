import React, { useState } from "react"
import moment from "moment"

/** Context */
import { Context } from "./Context"

const Provider: React.FC = ({ children }) => {
  const [activeDate, setActiveDate] = useState<any>(moment())

  return (
    <Context.Provider
      value={{
        activeDate,
        setActiveDate
      }}
    >
      {children}
    </Context.Provider>
  )
}

export default Provider
