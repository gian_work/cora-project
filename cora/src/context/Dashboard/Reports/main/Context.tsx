import React from "react"

export type CtxType = {
  activeDate: any
  setActiveDate: Function
}

export const Context = React.createContext<Partial<CtxType>>({})
