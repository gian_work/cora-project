import React from "react"

export type CtxType = {
  logDetails: any
  activeData: Record<string, any>
  activeDate: string | null
  logStaff: boolean
  showStaff: boolean
  activeTable: number
  activeStatus: number
  currentAction: number
  setactiveTable: Function
  setActiveStatus: Function
  showDrawer: Function
  setActiveDate: Function
  setActiveData: Function
  handleLogStaff: Function
  setLogDetails: Function
  resetForm: Function
  setCurrentAction: Function
}

export const Context = React.createContext({})
