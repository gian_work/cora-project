import React, { useState } from "react"
import moment from "moment"
import Cookies from "js-cookie"
import { toast } from "react-toastify"
import { mutate } from "swr"

/** Context */
import { toUnix } from "utils/date"
import service from "services/Dashboard/Attendances"
import { Context } from "./Context"

/** Service */

interface ActiveDataType {
  category: number
}

const UMProvider: React.FC = (props: any) => {
  const { children } = props
  const [activeTable, setactiveTable] = useState(0)
  const [activeStatus, setActiveStatus] = useState(0)
  const [logStaff, setLogStaff] = useState(false)
  const [showStaff, setShowStaff] = useState(false)
  const [activeDate, setActiveDate] = useState<any>(moment())
  const [activeData, setActiveData] = useState<any>({})
  const [currentAction, setCurrentAction] = useState(1)
  /**
   * CurrentAction
   * 1 - timein
   * 2 - timeout
   * 3 - update timein
   * 4 - view details
   */

  const [logDetails, setLogDetails] = useState({
    attendanceUID: "",
    category: 1,
    accountUID: "",
    name: "",
    condoUID: Cookies.get("condoUID"),
    attendanceType: 1,
    attendanceDateTime: new Date().toString(),
    attendanceDateTimeout: new Date().toString(),
    bluetoothBeaconUid: "",
    remarks: "",
    timeinReferenceUid: ""
  })

  const unixDate = toUnix(activeDate)
  const refrestTable = () => mutate(`fetchAttendances-${unixDate}-${activeTable}`)

  /**
   * Notification
   */
  const notifyCreate = (staffName: string) =>
    toast(`${staffName} successfully timed-in.`, {
      type: toast.TYPE.SUCCESS
    })

  const notifyDelete = () =>
    toast(`Successfully deleted the time log`, {
      type: toast.TYPE.SUCCESS
    })

  /**
   * Methods
   */
  function showDrawer(
    type: number,
    value: boolean,
    dataItems?: Record<string, any>
  ): void {
    if (dataItems !== undefined) {
      const di = dataItems
      setActiveData(dataItems)
      setLogDetails({
        attendanceUID: di?._uid,
        category: di?.user_role_category,
        accountUID: `${di?.account_uid}-${di?.name}`,
        name: di?.name,
        condoUID: di?.condo_uid,
        attendanceType: di?.attendanceType,
        attendanceDateTime: new Date(di?.attendance_date_time).toString(),
        attendanceDateTimeout: new Date().toString(),
        bluetoothBeaconUid: di?.bluetooth_beacon_uid,
        remarks: di?.remarks,
        timeinReferenceUid: di?._uid
      })
    }
    /** Type 1 = add; Type 2 = update; Type 3 = details */
    switch (type) {
      case 1:
        return setLogStaff(value)
      case 2:
        return setShowStaff(value)
      default:
        return setLogStaff(value)
    }
  }

  function resetForm() {
    setActiveData({})
    setLogDetails({
      attendanceUID: "",
      category: 1,
      accountUID: "",
      name: "",
      condoUID: Cookies.get("condoUID"),
      attendanceType: 1,
      attendanceDateTime: new Date().toString(),
      attendanceDateTimeout: new Date().toString(),
      bluetoothBeaconUid: "",
      remarks: "",
      timeinReferenceUid: ""
    })
  }

  async function handleLogStaff(type: number, formType: number) {
    /**
     * type 1 = time-in; type 2 = time-out;
     * formType 1 = create; formType 2 = update;
     */

    const ld = logDetails
    const accUID = ld?.accountUID?.split("-")[0]
    const accName = ld?.accountUID?.split("-")[1]
    const payload = {
      "account_uid": accUID,
      "name": accName,
      "condo_uid": ld?.condoUID,
      "attendance_type": type,
      "attendance_date_time":
        currentAction === 2
          ? toUnix(ld?.attendanceDateTimeout)
          : toUnix(ld?.attendanceDateTime),
      "photo": {
        "key": "",
        "file_name": "",
        "encoding": ""
      },
      "bluetooth_beacon_uid": ld?.bluetoothBeaconUid,
      "remarks": ld?.remarks,
      "time_in_reference_uid": currentAction === 2 ? ld?.timeinReferenceUid : ""
    }

    const endpoint =
      formType === 1
        ? service.createAttendance(payload)
        : service.updateAttendance(payload, ld?.attendanceUID)

    try {
      const response = await endpoint
      showDrawer(1, false)
      notifyCreate(accName)
      refrestTable()
      return response
    } catch (e) {
      return e
    }
  }

  async function handleDeleteLog(logUID: string) {
    try {
      const response = await service.deleteAttendance(logUID)
      notifyDelete()
      refrestTable()
      return response
    } catch (e) {
      return e
    }
  }

  return (
    <Context.Provider
      value={{
        logDetails,
        logStaff,
        showStaff,
        activeTable,
        activeStatus,
        activeData,
        activeDate,
        currentAction,
        setactiveTable,
        setActiveStatus,
        showDrawer,
        setActiveDate,
        setActiveData,
        handleLogStaff,
        handleDeleteLog,
        setLogDetails,
        resetForm,
        setCurrentAction
      }}
    >
      {children}
    </Context.Provider>
  )
}

export default UMProvider
