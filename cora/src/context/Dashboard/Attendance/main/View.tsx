import React from "react"
import Box from "@material-ui/core/Box"
import Card from "@material-ui/core/Card"

/** Components */
import PageHeaderTitle from "components/PageHeaderTitle"
import WithHeader from "layout/WithHeader"
import FilterWithInfo from "components/FilterButtons/FilterWithInfo"
import TabsFilter from "components/Table/components/TabsFilter/withRef"
import TableHeader from "components/TableHeader"
import DatePicker from "components/Forms/DatePicker"
import Table from "pages/Dashboard/Attendance/tables"
import LogStaff from "pages/Dashboard/Attendance/components/LogStaff"
import ViewStaff from "pages/Dashboard/Attendance/components/ViewStaff"

/** Menu */
import { MenuFilter, MenuStatus } from "config/Dashboard/Attendance/"

/** Context */
import { CtxType } from "./Context"
import withContext from "./withContext"

const UMView: React.FC<CtxType> = ({
  activeData,
  activeDate,
  activeTable,
  setactiveTable,
  activeStatus,
  setActiveStatus,
  setActiveDate,
  resetForm,
  showDrawer,
  logStaff,
  showStaff,
  setCurrentAction
}) => {
  const FilterView = (): JSX.Element => {
    return (
      <Box display="flex" flexWrap="wrap" padding="20px 0">
        {MenuFilter.map((item: any) => {
          return (
            <FilterWithInfo
              type={item.type}
              name={item.name}
              info={item.info}
              color={item.color}
              icon={item.icon}
              action={setactiveTable}
            />
          )
        })}
      </Box>
    )
  }

  const ViewLogStaff = (): JSX.Element => {
    return (
      <LogStaff
        openState={logStaff}
        setopenState={(value: boolean) => {
          showDrawer(1, value)
        }}
      />
    )
  }

  const ViewDetailsStaff = (): JSX.Element => {
    return (
      <ViewStaff
        openState={showStaff}
        setopenState={(value: boolean) => {
          showDrawer(2, value)
        }}
        data={activeData}
      />
    )
  }

  const ViewPageTitle = (): JSX.Element => {
    return (
      <Box>
        <Box>
          <PageHeaderTitle
            title=""
            breadcrumbs="property management / Attendance"
            noBottomGap
          />
        </Box>
        <Box display="flex" alignItems="center" minHeight="40px" overflow="hidden">
          <Box fontSize="24px" fontWeight="500" color="#454B57" paddingRight="10px">
            Attendance
          </Box>
          <DatePicker
            label=""
            name="activeDate"
            format="MM/dd/yyyy"
            value={activeDate}
            placeholder="mm/dd/yyyy"
            handleDateChange={(value: string): void => setActiveDate(value)}
            inlineStyles={{
              minHeight: "40px",
              backgroundColor: "#FFFFFF",
              minWidth: "215px",
              width: "215px"
            }}
          />
        </Box>
      </Box>
    )
  }

  const ViewTable = (): JSX.Element => {
    return (
      <Table
        date={activeDate}
        activeStatus={activeStatus}
        activeTable={activeTable}
        showTimeout={(value: boolean, dataItems: Record<string, any>) => {
          showDrawer(1, value, dataItems)
          setCurrentAction(2)
        }}
      />
    )
  }

  const MainView = (): JSX.Element => {
    return (
      <WithHeader>
        <Box>
          <ViewPageTitle />
        </Box>
        <Box>
          <FilterView />
        </Box>

        <Card>
          <Box>
            <TableHeader
              addActionLabel="TIME-IN STAFF"
              title={MenuFilter[activeTable]?.name}
              titleIcon={MenuFilter[activeTable]?.icon}
              addAction={() => {
                showDrawer(1, true)
                setCurrentAction(1)
                resetForm()
              }}
            />
          </Box>
          <Box paddingLeft="90px" borderBottom="1px solid #F2F2F2">
            <TabsFilter
              value={activeStatus}
              handleChange={setActiveStatus}
              options={MenuStatus}
            />
          </Box>
          <Box>{ViewTable()}</Box>
        </Card>
      </WithHeader>
    )
  }

  return (
    <Box paddingBottom="200px">
      {MainView()}
      {ViewLogStaff()}
      {ViewDetailsStaff()}
    </Box>
  )
}

export default withContext(UMView)
