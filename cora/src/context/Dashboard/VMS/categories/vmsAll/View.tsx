import React from "react"
import Box from "@material-ui/core/Box"
import Card from "@material-ui/core/Card"
/** Components */
import Table from "pages/Dashboard/VisitorManagement/tables/vmsAll"
import TableHeader from "components/TableHeader"
/** Context */
import { MenuIcons } from "pages/Dashboard/VisitorManagement/config/Menu"
import withContext from "./withContext"
import { CtxType } from "./Context"

const VMSAllView: React.FC<CtxType> = ({ activeFilter, showAddForm }) => {
  return (
    <Card>
      <Box>
        <TableHeader
          addActionLabel="Add New"
          hasSearch
          addAction={() => showAddForm()}
          title="All VMS"
          titleIcon={MenuIcons[0]}
        />
        <Table activeFilter={activeFilter} />
      </Box>
    </Card>
  )
}

export default withContext(VMSAllView)
