import React from "react"
import Box from "@material-ui/core/Box"
import Card from "@material-ui/core/Card"

/** Components */
import Table from "pages/Dashboard/VisitorManagement/tables/vmsVisitors"
import TableHeader from "components/TableHeader"
import TabsFilter from "components/Table/components/TabsFilter/withRef"
/** Context */
import { MenuIcons } from "pages/Dashboard/VisitorManagement/config/Menu"
import vmsConfig from "config/Dashboard/VMS/"
import withContext from "./withContext"
import { CtxType } from "./Context"

const VMSVisitorsView: React.FC<CtxType> = ({
  activeFilter,
  filterTable,
  showAddForm
}) => {
  return (
    <Card>
      <Box>
        <TableHeader
          addActionLabel="Add New"
          hasSearch
          addAction={() => showAddForm()}
          title="Visitors"
          titleIcon={MenuIcons[0]}
        />
        <Box
          paddingLeft="90px"
          borderBottom="1px solid #F2F2F2"
          className="thisisatest"
        >
          <TabsFilter
            value={activeFilter}
            handleChange={(e: any) => filterTable(e)}
            options={vmsConfig?.status?.visitor}
          />
        </Box>
        <Table activeFilter={activeFilter} />
      </Box>
    </Card>
  )
}

export default withContext(VMSVisitorsView)
