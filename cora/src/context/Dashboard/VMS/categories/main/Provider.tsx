import React from "react"

/** Context */
import { Context } from "./Context"

/** Interface */
export interface VMSMainProviderProps {
  showAddForm: Function
  activeTable: number
  // contextActions: Record<any, any>
}

const VMSMainProvider: React.FC<VMSMainProviderProps> = ({
  children,
  showAddForm,
  activeTable
  // contextActions,
}) => {
  /** State */
  const [activeFilter, setActiveFilter] = React.useState(0)

  /** Methods */
  const filterTable = (value: number) => {
    setActiveFilter(value)
  }

  return (
    <Context.Provider
      value={{
        showAddForm,
        activeFilter,
        setActiveFilter,
        filterTable,
        // contextActions,
        activeTable
      }}
    >
      {children}
    </Context.Provider>
  )
}

export default VMSMainProvider
