import React from "react"

export type CtxType = {
  showAddForm: Function
  setActiveFilter: Function
  filterTable: Function
  activeFilter: number
  activeTable: number
  // contextActions: Record<any, any>
  initData: Record<any, any>
}

export const Context = React.createContext<Partial<CtxType>>({})
