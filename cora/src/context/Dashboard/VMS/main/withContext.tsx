import * as React from "react"
import { VMSContext } from "./Context"

function withContext(Component: any) {
  return function contextComponent(props: any) {
    return (
      <VMSContext.Consumer>
        {(contexts) => <Component {...props} {...contexts} />}
      </VMSContext.Consumer>
    )
  }
}

export default withContext
