import React from "react"

export type CtxType = {
  openDetails: boolean
  addVisitorsActive: boolean
  tableData: Record<any, any>
  filteredData: Record<any, any>
  activeTable: number
  tabsFilter: number
  activeTitle: string
  setopenDetails: Function
  settableData: Function
  setFilteredData: Function
  setactiveTable: Function
  setTabsFilter: Function
  setactiveTitle: Function
  setAddVisitorsActive: Function
  fecthVms: Function
  filterData: Function
  filterActiveTable: Function
  fetchVms: Function
  tabChange: Function
}

export const VMSContext = React.createContext<Partial<CtxType>>({})
