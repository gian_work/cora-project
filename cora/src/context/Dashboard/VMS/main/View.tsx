import React from "react"
import Box from "@material-ui/core/Box"
import { useMachine } from "@xstate/react"
/** Components */
import PageHeaderTitle from "components/PageHeaderTitle"
import FilterWithInfo from "components/FilterButtons/FilterWithInfo"
import LinkButton from "components/LinkButton"
import WithHeader from "layout/WithHeader"
import VMSMain from "pages/Dashboard/VisitorManagement/categories/main"
/** Categories */
import AddVisitor from "pages/Dashboard/VisitorManagement/components/AddVisitor"
/** Config */
import { MenuIcons } from "pages/Dashboard/VisitorManagement/config/Menu"
/** Machine */
import { VMSMachine } from "machines/Dashboard/VMS"
/** Context */
import withContext from "./withContext"

const FilterMenu = [
  {
    name: "All VMS",
    type: 0,
    info: "10 new",
    color: "#004E8B",
    icon: MenuIcons[0]
  },
  {
    name: "Visitors",
    type: 1,
    info: "10 new",
    color: "#007B83",
    icon: MenuIcons[1]
  },
  {
    name: "Deliveries / Pickup",
    type: 2,
    info: "10 new",
    color: "#D8B469",
    icon: MenuIcons[2]
  },
  {
    name: "Service Providers",
    type: 3,
    info: "10 new",
    color: "#D05611",
    icon: MenuIcons[3]
  }
]

const VMSView: React.FC = (props: any) => {
  const { filterData, activeTable } = props
  const [current, send] = useMachine(VMSMachine)
  const xValue = current.value

  const ViewPageTitle = (): JSX.Element => {
    if (xValue === "addVisitor") {
      return (
        <PageHeaderTitle
          title="Add New VMS"
          breadcrumbs="property management / visitor management / add new vms"
          backAction={() => send("TABLE_VIEW")}
        />
      )
    }

    return (
      <>
        <PageHeaderTitle
          title="Visitor Management"
          breadcrumbs="property management / visitor management"
        />
        <Box>
          <LinkButton name="VISITOR MANAGEMENT SETTINGS" action={() => null} />
        </Box>
      </>
    )
  }

  const ViewFilter = (): JSX.Element => {
    if (xValue !== "tableView") {
      return <div />
    }

    return (
      <Box display="flex" flexWrap="wrap" padding="20px 0">
        {FilterMenu.map((item: any) => {
          return (
            <FilterWithInfo
              type={item.type}
              name={item.name}
              info={item.info}
              color={item.color}
              icon={item.icon}
              action={filterData}
              hasViewAll
            />
          )
        })}
      </Box>
    )
  }

  const ViewTable = (): JSX.Element => {
    if (xValue !== "tableView") {
      return <div />
    }

    return (
      <VMSMain showAddForm={() => send("ADD_VISITOR")} activeTable={activeTable} />
    )
  }

  const AddVisitorView = (): JSX.Element => {
    if (xValue !== "addVisitor") {
      return <div />
    }

    return <AddVisitor showform={() => send("TABLE_VIEW")} />
  }

  const MainView = (): JSX.Element => {
    return (
      <WithHeader>
        <Box display="flex" justifyContent="space-between">
          <ViewPageTitle />
        </Box>
        <Box>
          <ViewFilter />
        </Box>
        <Box>
          <ViewTable />
          <AddVisitorView />
        </Box>
      </WithHeader>
    )
  }

  return (
    <>
      <MainView />
    </>
  )
}

export default withContext(VMSView)
