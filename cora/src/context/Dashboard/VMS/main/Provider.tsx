import React, { useState, useEffect } from "react"
import { useDispatch } from "react-redux"

/** Service */
import vmsAPI from "services/Dashboard/VMS"

/** Context */
import { setVMSTable } from "redux/dashboard/vms/actions"
import { VMSContext } from "./Context"

export const RefAll = React.createRef<HTMLButtonElement>()
export const RefPending = React.createRef<HTMLButtonElement>()
export const RefArrived = React.createRef<HTMLButtonElement>()
export const RefClosed = React.createRef<HTMLButtonElement>()
export const RefCancelled = React.createRef<HTMLButtonElement>()

export const RefDeliveryAll = React.createRef<HTMLButtonElement>()
export const RefDeliveryPending = React.createRef<HTMLButtonElement>()
export const RefDeliveryArrived = React.createRef<HTMLButtonElement>()
export const RefDeliveryClosed = React.createRef<HTMLButtonElement>()
export const RefDeliveryCancelled = React.createRef<HTMLButtonElement>()
export const RefDeliveryReceived = React.createRef<HTMLButtonElement>()
export const RefDeliveryCollected = React.createRef<HTMLButtonElement>()

export const StatusRefs: {
  [index: string]: any
} = {
  1: RefAll,
  2: RefPending,
  3: RefArrived,
  4: RefClosed,
  5: RefCancelled
}

export const StatusDeliveryRefs: {
  [index: string]: any
} = {
  1: RefDeliveryAll,
  2: RefDeliveryPending,
  3: RefDeliveryArrived,
  4: RefDeliveryClosed,
  5: RefDeliveryCancelled,
  6: RefDeliveryReceived,
  7: RefDeliveryCollected
}

const VMSProvider: React.FC = ({ children }: any) => {
  const [openDetails, setopenDetails] = useState(false)
  const [tableData, settableData] = useState([])
  const [filteredData, setFilteredData] = useState([])
  const [activeTable, setactiveTable] = useState(0)
  const [tabsFilter, setTabsFilter] = useState(0)
  const [activeTitle, setactiveTitle] = useState("All VMS")
  const [addVisitorsActive, setAddVisitorsActive] = useState(false)
  const dispatch = useDispatch()

  /** Methods */
  const fetchVms = async (params?: Record<string, number>) => {
    settableData([])
    try {
      const response = await vmsAPI.getVMS(params)
      if (response.data._data !== null) {
        settableData(response.data._data)
        setFilteredData(response.data._data)
        dispatch(setVMSTable(response.data._data))
        // setactiveTable(Number(type))
      } else {
        settableData([])
        setFilteredData([])
      }
      return response.data._data
    } catch (e) {
      return e
    }
  }

  const filterData = (value: number, label: string) => {
    setactiveTable(value)
    setactiveTitle(label)
    setTabsFilter(0)
  }

  const filterActiveTable = (value: number) => {
    setTabsFilter(value)
  }

  const tabChange = (event: any, newValue: any) => {
    setTabsFilter(newValue)
    filterActiveTable(newValue)
  }

  useEffect(() => {
    // fetchVms()
    window.scrollTo(0, 0)
  }, [])

  return (
    <VMSContext.Provider
      value={{
        openDetails,
        setopenDetails,
        tableData,
        settableData,
        filteredData,
        setFilteredData,
        activeTable,
        setactiveTable,
        tabsFilter,
        setTabsFilter,
        activeTitle,
        setactiveTitle,
        addVisitorsActive,
        setAddVisitorsActive,
        fetchVms,
        filterData,
        filterActiveTable,
        tabChange
      }}
    >
      {children}
    </VMSContext.Provider>
  )
}

export default VMSProvider
