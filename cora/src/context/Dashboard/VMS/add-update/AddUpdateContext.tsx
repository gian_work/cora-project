import React from "react"

export type AddUpdateCtxType = {
  activeStep: number
  openDialog: boolean
  visitorDetails: Record<string, any>
  setActiveStep: Function
  setOpenDialog: Function
  setVisitorDetails: Function
  steps: Function
  handleAddVMS: Function
  showform: Function
}

export const AddUpdateContext = React.createContext<Partial<AddUpdateCtxType>>({})
