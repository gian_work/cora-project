import * as React from "react"
import { AddUpdateContext } from "./AddUpdateContext"

function withContext(Component: any) {
  return function contextComponent(props: any) {
    return (
      <AddUpdateContext.Consumer>
        {(contexts) => <Component {...props} {...contexts} />}
      </AddUpdateContext.Consumer>
    )
  }
}

export default withContext
