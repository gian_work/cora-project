import React, { useState } from "react"
import { toast } from "react-toastify"

/** Service */
import vmsAPI from "services/Dashboard/VMS"

/** Config */
import { toUnix } from "utils/date"

/** Context */
import { AddUpdateContext } from "./AddUpdateContext"

export interface AddVisitorProps {
  showform: Function
}

const AddUpdate: React.FC<AddVisitorProps> = (props: any) => {
  const { children, showform } = props
  const [activeStep, setActiveStep] = useState(1)
  const [openDialog, setOpenDialog] = useState(false)
  const [visitorDetails, setVisitorDetails] = useState({
    allowCondo: false,
    vmsType: "1",
    name: "",
    email: "",
    mobile: "",
    remarks: "",
    purpose: "",
    unitUid: "",
    startDate: "",
    endDate: "",
    date: "",
    time: "",
    eta: "",
    multiplePersons: "1",
    numberOfPersons: 1,
    contactPerson: "",
    byAdmin: false
  })

  /** Methods */

  const notifyError = () =>
    toast("Error creating VMS, please check all the fields.", {
      type: toast.TYPE.ERROR
    })

  const notifySuccess = () =>
    toast("Successfully created VMS.", {
      type: toast.TYPE.SUCCESS
    })

  const handleAddVMS = async () => {
    const {
      allowCondo,
      name,
      mobile,
      email,
      eta,
      remarks,
      unitUid,
      contactPerson,
      multiplePersons,
      numberOfPersons,
      startDate,
      endDate,
      purpose,
      vmsType,
      byAdmin
    } = visitorDetails

    const dataVisitor = {
      "name": name,
      "multiple_persons": multiplePersons !== "1",
      "eta_ms": toUnix(eta),
      "mobile": mobile,
      "email": email,
      "no_of_persons": numberOfPersons,
      "remarks": remarks,
      "unit_uid": unitUid,
      "contact_person": contactPerson,
      "for_admin": byAdmin
    }

    const dataDelivery = {
      "name": name,
      "multiple_persons": multiplePersons !== "1",
      "mobile": mobile,
      "email": email,
      "remarks": remarks,
      "unit_uid": unitUid,
      "contact_person": contactPerson,
      "allow_condo_to_receive": allowCondo,
      "start_date_ms": toUnix(startDate),
      "end_date_ms": toUnix(endDate)
    }

    const dataServiceProvider = {
      "name": name,
      "multiple_persons": multiplePersons !== "1",
      "eta_ms": toUnix(eta),
      "mobile": mobile,
      "email": email,
      "no_of_persons": numberOfPersons,
      "remarks": remarks,
      "unit_uid": unitUid,
      "contact_person": contactPerson,
      "purpose": purpose
    }

    const endpoint = (type: string) => {
      switch (type) {
        case "1":
          return vmsAPI.createVisitor(dataVisitor)
        case "2":
          return vmsAPI.createDelivery(dataDelivery)
        case "3":
          return vmsAPI.createServiceProvider(dataServiceProvider)
        default:
          return null
      }
    }

    try {
      const response = await endpoint(vmsType)
      if (response._statusCode === -128) {
        return notifyError()
      }
      notifySuccess()

      return response
    } catch (e) {
      return notifyError()
    }
  }

  const getSteps = () => {
    return ["select vms type", "visitor's information", "requester's information"]
  }

  return (
    <AddUpdateContext.Provider
      value={{
        activeStep,
        openDialog,
        visitorDetails,
        showform,
        setActiveStep,
        setOpenDialog,
        setVisitorDetails,
        steps: getSteps,
        handleAddVMS
      }}
    >
      {children}
    </AddUpdateContext.Provider>
  )
}

export default AddUpdate
