import * as React from "react"
import { FeedbackContext } from "./FeedbackContext"

function withContext(Component: any) {
  return function contextComponent(props: any) {
    return (
      <FeedbackContext.Consumer>
        {(contexts) => <Component {...props} {...contexts} />}
      </FeedbackContext.Consumer>
    )
  }
}

export default withContext
