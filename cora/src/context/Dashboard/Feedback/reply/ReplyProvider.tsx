import React from "react"
import axios from "axios"
import { toast } from "react-toastify"

/* Service */
import CommonAPI from "services/Dashboard/Common"
import FeedbackAPI from "services/Dashboard/Feedback"

/* Context */
import { ReplyContext } from "./ReplyContext"

export interface ReplyProviderProps {
  showForm: Function
  isCreate: boolean
  data: Record<string, any>
}

const ReplyProvider: React.FC<ReplyProviderProps> = ({
  showForm,
  isCreate,
  data,
  children
}) => {
  const [openDialog, setOpenDialog] = React.useState(false)
  const [photos, setPhotos] = React.useState(Array)
  const [attaching, setAttaching] = React.useState(false)
  const [replyDetails, setReplyDetails] = React.useState({
    message: "",
    status: "",
    isUrgent: "",
    staffName: "",
    role: ""
  })
  const [activeFeedback, setActiveFeedback] = React.useState([])
  const [messages] = React.useState([])

  /** For editing feedback reply */
  const [isEditing, setIsEditing] = React.useState(false)
  const [activeReply, setActiveReply] = React.useState({
    uid: "",
    reply: "",
    photos: []
  })

  /** Notification */
  const notify = () =>
    toast("Successfully replied to the feedback.", {
      type: toast.TYPE.SUCCESS
    })

  const getFeedback = () => {
    FeedbackAPI.getFeedback(data?._uid)
      .then((response) => {
        const responseData = response.data._data
        setActiveFeedback(responseData)
      })
      .catch((error) => error)
  }

  const handleUpdateStatus = async (uid: string, status: string) => {
    if (replyDetails.status === "") {
      return null
    }

    const payload = {
      "feedback_uid": uid,
      "status": parseInt(status, 0)
    }

    try {
      const response = await FeedbackAPI.updateStatus(payload)
      return response
    } catch (e) {
      return e
    }
  }

  const handleIsUrgent = async (uid: string, status: boolean) => {
    const payload = {
      "feedback_uid": uid,
      "is_urgent": status
    }

    try {
      const response = await FeedbackAPI.setUrgency(payload)
      window.scrollTo(0, 0)
      return response
    } catch (e) {
      return e
    }
  }

  const handleAssignToStaff = async (uid: string, accountId: string) => {
    if (replyDetails?.staffName === "") {
      return
    }

    const payload = {
      "feedback_uid": uid,
      "assigned_to_staff_account_uid": accountId
    }

    try {
      await FeedbackAPI.assignToStaff(payload)
    } catch (error) {
      throw Error(error)
    }
  }

  const handleImageUpload = (event: any) => {
    const fileToUpload = event[0]
    const items: any = []
    setAttaching(true)
    CommonAPI.getPresigned()
      .then((res: any) => {
        const { key } = res.data._data

        const dataItem = {
          "key": key,
          "file_name": fileToUpload.name,
          "encoding": fileToUpload.type
        }
        axios
          .put(res.data._data.presigned_url, fileToUpload)
          .then(() => {
            items.push(dataItem)
            setPhotos([...photos, ...items])
            setAttaching(false)
          })
          .catch(() => setAttaching(false))
      })
      .catch(() => setAttaching(false))
  }

  const handleFeedbackReply = async () => {
    const payload = {
      "feedback_uid": data?._uid,
      "reply": replyDetails.message,
      "feedback_reply_photo": {
        "photos": photos
      }
    }

    try {
      const response = await FeedbackAPI.createFeedbackReply(payload)
      setReplyDetails({
        ...replyDetails,
        message: ""
      })
      setPhotos([])
      handleAssignToStaff(data?._uid, replyDetails?.staffName)
      handleUpdateStatus(data?._uid, replyDetails?.status)
      handleIsUrgent(data?._uid, Boolean(replyDetails?.isUrgent))

      notify()
      return response
    } catch (e) {
      return e
    }
  }

  const handleFeedbackReplyUpdate = async () => {
    const payload = {
      "reply": activeReply?.reply,
      "feedback_reply_photo": {
        "photos": photos
      }
    }

    try {
      const response = await FeedbackAPI.updateReply(payload, activeReply?.uid)
      setActiveReply({
        ...activeReply,
        reply: "",
        uid: ""
      })
      setIsEditing(false)
      setPhotos([])
      getFeedback()
      notify()
      return response
    } catch (e) {
      return e
    }
  }

  const handleActiveMessage = (message: any) => {
    if (isEditing) {
      setIsEditing(false)
      setActiveReply({
        uid: "",
        reply: "",
        photos: []
      })
      setPhotos([])
    } else {
      setIsEditing(true)
      setActiveReply({
        ...activeReply,
        uid: message._uid,
        reply: message.reply
      })
      setPhotos(
        message.feedback_reply_photo.photos !== null
          ? message.feedback_reply_photo.photos
          : []
      )
    }
  }

  const removePhoto = (value: number) => {
    const photoFiltered = photos.filter((item: any, index: number) => {
      return index !== value
    })
    setPhotos(photoFiltered)
  }

  return (
    <>
      <ReplyContext.Provider
        value={{
          data,
          showForm,
          isCreate,
          activeFeedback,
          openDialog,
          attaching,
          photos,
          replyDetails,
          messages,
          isEditing,
          activeReply,
          setActiveReply,
          handleActiveMessage,
          handleFeedbackReplyUpdate,
          handleImageUpload,
          handleFeedbackReply,
          removePhoto,
          getFeedback,
          setOpenDialog,
          setPhotos,
          setReplyDetails,
          setIsEditing
        }}
      >
        {children}
      </ReplyContext.Provider>
    </>
  )
}

export default ReplyProvider
