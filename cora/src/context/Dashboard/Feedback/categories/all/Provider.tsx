import React from "react"

import { useDispatch } from "react-redux"

/** Redux */
import { setTabStatus } from "redux/dashboard/feedback/actions"

/** Context */
import { Context } from "./Context"

const FeedbackAllProvider: React.FC = ({ children }) => {
  const dispatch = useDispatch()

  React.useEffect(() => {
    return () => {
      dispatch(setTabStatus(0))
    }
  }, [])

  return <Context.Provider value={{}}>{children}</Context.Provider>
}

export default FeedbackAllProvider
