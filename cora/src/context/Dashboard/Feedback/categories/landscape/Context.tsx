import React from "react"

export type CtxType = {
  contextActions: Record<any, any>
}

export const Context = React.createContext<Partial<CtxType>>({})
