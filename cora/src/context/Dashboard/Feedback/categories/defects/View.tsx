import React from "react"
import Box from "@material-ui/core/Box"
import Card from "@material-ui/core/Card"
/** Components */
import Table from "pages/Dashboard/Feedback/tables/defects"
import TableHeader from "components/TableHeader"
import TabsFilter from "components/Table/components/TabsFilter/withRef"
/** Context */
import { TableIcons } from "pages/Dashboard/Feedback/config/Menu"
import withMainContext from "context/Dashboard/Feedback/categories/main/withContext"
import { CtxType } from "context/Dashboard/Feedback/categories/main/Context"
/** Config */
import { feedbackConfig } from "config/Dashboard/Feedback"

const FeedbackAllView: React.FC<CtxType> = ({
  activeFilter,
  showAddForm,
  filterTable
}) => {
  return (
    <Card>
      <Box>
        <TableHeader
          addActionLabel="Add New"
          hasSearch
          addAction={() => showAddForm()}
          title="Defects"
          titleIcon={TableIcons[3]}
        />
        <Box paddingLeft="90px" borderBottom="1px solid #F2F2F2">
          <TabsFilter
            value={activeFilter}
            handleChange={filterTable}
            options={feedbackConfig?.status}
          />
        </Box>
        <Table />
      </Box>
    </Card>
  )
}

export default withMainContext(FeedbackAllView)
