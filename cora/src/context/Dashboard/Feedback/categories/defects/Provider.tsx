import React from "react"

/** Context */
import { Context } from "./Context"

const FeedbackDefectsProvider: React.FC = ({ children }) => {
  return <Context.Provider value={{}}>{children}</Context.Provider>
}

export default FeedbackDefectsProvider
