import React from "react"
import Cookie from "js-cookie"
import useSWR from "swr"

/** Context */
import feedbackApi from "services/Dashboard/Feedback"
import { Context } from "./Context"

/** Service */

/** Interface */
export interface FeedbackMainProviderProps {
  showAddForm: Function
  showEditForm: Function
  showReplyForm: Function
  showDetails: Function
  setActiveStatus: Function
  activeStatus: number
  activeTable: number
  tabStatus: number
  filterTabStatus: Function
  // contextActions: Record<any, any>
}

const FeedbackMainProvider: React.FC<FeedbackMainProviderProps> = ({
  children,
  showAddForm,
  showEditForm,
  showReplyForm,
  showDetails,
  activeTable,
  setActiveStatus,
  activeStatus,
  tabStatus,
  filterTabStatus
}) => {
  /** State */
  const [activeFilter, setActiveFilter] = React.useState(0)

  const fetchAllFeedback = async () =>
    feedbackApi.getFeedbacks({
      "condo_uid": Cookie.get("condoUID")
    })

  const { data, isValidating } = useSWR(`fetchAllFeedback`, fetchAllFeedback)

  /** Methods */
  const filterTable = (value: number) => {
    setActiveFilter(value)
  }

  return (
    <Context.Provider
      value={{
        showAddForm,
        showEditForm,
        showDetails,
        showReplyForm,
        activeFilter,
        setActiveFilter,
        filterTable,
        activeTable,
        setActiveStatus,
        activeStatus,
        tabStatus,
        filterTabStatus,
        data,
        isValidating
      }}
    >
      {children}
    </Context.Provider>
  )
}

export default FeedbackMainProvider
