import { makeStyles, Theme } from "@material-ui/core/styles"

const styles = makeStyles((theme: Theme) => ({
  messageBox: {},
  noConvo: {
    padding: "30px 0",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    "& .description": {
      marginTop: "10px",
      color: theme.palette.primary.main,
      fontSize: "24px",
      fontWeight: "500"
    }
  }
}))

export default styles
