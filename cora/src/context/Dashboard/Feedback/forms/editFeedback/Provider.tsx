import React, { useState } from "react"
import { toast } from "react-toastify"
import axios from "axios"
import Cookies from "js-cookie"

/** API */
import FeedbackAPI from "services/Dashboard/Feedback"
import CommonAPI from "services/Dashboard/Common"

import { AddFeedbackContext } from "./Context"

export interface EditFeedbackProviderProps {
  showForm: Function
  activeItem: Record<string, any>
}

const EditFeedbackProvider: React.FC<EditFeedbackProviderProps> = ({
  children,
  showForm,
  activeItem
}) => {
  const [activeFeedback] = React.useState([])

  /* Notification */
  const notifyCreate = () =>
    toast("Successfully updated a the Feedback.", {
      type: toast.TYPE.SUCCESS
    })

  const [openDialog, setOpenDialog] = useState(false)
  const [feedbackDetails, setFeedbackDetails] = useState({
    id: activeItem?._uid || "",
    unitUid: activeItem?.feedback_unit_uid || "",
    byAdmin: activeItem?.by_admin || false,
    type: activeItem?.feedback_type || "",
    description: activeItem?.remarks || "",
    isUrgent: activeItem?.is_urgent || false,
    staffName: activeItem?.assigned_to_staff_account_uid || "",
    photo: activeItem?.feedback_photo?.photos || [],
    status: activeItem?.status || ""
  })
  const [photos, setPhotos] = useState(activeItem?.feedback_photo?.photos || [])
  const [attaching, setAttaching] = useState(false)

  /** Methods */
  const removePhoto = (value: number) => {
    const photoFiltered = photos.filter((item: any, index: number) => {
      return index !== value
    })
    setPhotos(photoFiltered)
  }

  const handleImageUpload = (event: any) => {
    const fileToUpload = event[0]
    const items: any = []
    setAttaching(true)
    CommonAPI.getPresigned()
      .then((res: any) => {
        const { key } = res.data._data
        const dataItem = {
          "key": key,
          "file_name": fileToUpload.name,
          "encoding": fileToUpload.type
        }
        axios
          .put(res.data._data.presigned_url, fileToUpload)
          .then(() => {
            items.push(dataItem)
            setPhotos([...photos, ...items])
            setAttaching(false)
          })
          .catch(() => setAttaching(false))
      })
      .catch(() => setAttaching(false))
  }

  const handleAddFeedback = async () => {
    const data = {
      "feedback_type": feedbackDetails.type,
      "remarks": feedbackDetails.description,
      "feedback_photo": { "photos": photos },
      "feedback_unit_uid": feedbackDetails.unitUid,
      "is_urgent": feedbackDetails.isUrgent,
      "by_admin": feedbackDetails.byAdmin,
      "assigned_to_staff_account_uid": feedbackDetails.staffName,
      "feedback_condo_uid": Cookies.get("condoUID"),
      "status": feedbackDetails.status
    }

    if (feedbackDetails.staffName === "") {
      delete data.assigned_to_staff_account_uid
    }

    if (feedbackDetails.byAdmin) {
      if (feedbackDetails.staffName === "") {
        delete data.assigned_to_staff_account_uid
      }
    }

    try {
      const response = await FeedbackAPI.updateFeedback(data, feedbackDetails.id)
      notifyCreate()
      return response
    } catch (e) {
      return e
    }
  }

  return (
    <AddFeedbackContext.Provider
      value={{
        feedbackDetails,
        photos,
        attaching,
        openDialog,
        activeFeedback,
        showForm,
        activeItem,
        setFeedbackDetails,
        handleAddFeedback,
        setOpenDialog,
        handleImageUpload,
        removePhoto
      }}
    >
      {children}
    </AddFeedbackContext.Provider>
  )
}

export default EditFeedbackProvider
