import { makeStyles, Theme } from "@material-ui/core/styles"

const styles = makeStyles((theme: Theme) => ({
  tabContainer: {
    "& .active": {
      color: theme.palette.secondary.main
    },
    "& button": {
      padding: "25px 20px !important",
      overflow: "hidden",
      position: "relative",
      fontSize: "0.875rem",
      maxWidth: "264px",
      minWidth: "auto !important",
      boxSizing: "border-box",
      minHeight: "48px",
      textAlign: "center",
      flexShrink: 0,
      lineHeight: "1.75",
      whitespace: "normal"
    },
    "& span": {
      fontWeight: "800",
      textTransform: "uppercase"
    }
  }
}))

export default styles
