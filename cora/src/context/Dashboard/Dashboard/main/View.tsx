import React from "react"

/** Components */
import ViewDashboard from "pages/Dashboard/Dashboards/view"
/** Context */
import WithHeader from "layout/WithHeader"
import { CtxType } from "./Context"
import withContext from "./withContext"

const View: React.FC<CtxType> = () => {
  React.useEffect(() => {
    window.scrollTo(0, 0)
  }, [])

  const MainView = () => {
    return <ViewDashboard />
  }

  return (
    <>
      <div>
        <WithHeader>{MainView()}</WithHeader>
      </div>
    </>
  )
}

export default withContext(View)
