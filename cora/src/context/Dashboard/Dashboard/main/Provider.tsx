import React, { useState } from "react"

/** Context */
import { displayDate, toUnix } from "utils/date"
import { Context } from "./Context"
/** Utils */

const Provider: React.FC = ({ children }) => {
  const [viewSettings, setViewSettings] = useState({
    applications: true,
    vms: true,
    newAccounts: true,
    paymentsToday: true,
    newFeedback: true,
    attendance: true
  })

  const [dateRange] = useState({
    startDate: toUnix(displayDate(new Date(), "YYYY-MM-DD 00:00:00")),
    endDate: toUnix(displayDate(new Date(), "YYYY-MM-DD 23:59:59"))
  })

  const [currentDate] = useState(displayDate(new Date(), "MMMM DD, YYYY"))

  return (
    <Context.Provider
      value={{
        dateRange,
        currentDate,
        viewSettings,
        setViewSettings
      }}
    >
      {children}
    </Context.Provider>
  )
}

export default Provider
