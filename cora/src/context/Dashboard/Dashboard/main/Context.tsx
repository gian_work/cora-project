import React from "react"

export type CtxType = {
  viewSettings: any
  dateRange: any
  currentDate: string
  setViewSettings: Function
}

export const Context = React.createContext<Partial<CtxType>>({})
