import React from "react"

/** Context */
import { Context } from "./Context"

/** Utils */

const Provider: React.FC = ({ children }) => {
  /** States */
  const [activeTable, setActiveTable] = React.useState(0)
  const [activeData, setActiveData] = React.useState({})
  const [showReject, setShowReject] = React.useState(false)

  return (
    <Context.Provider
      value={{
        activeTable,
        activeData,
        showReject,
        setActiveTable,
        setActiveData,
        setShowReject
      }}
    >
      {children}
    </Context.Provider>
  )
}

export default Provider
