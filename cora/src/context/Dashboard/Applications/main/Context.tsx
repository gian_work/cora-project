import React from "react"

export type CtxType = {
  showReject: boolean
  activeTable: number
  activeData: Record<string, any>
  setActiveTable: Function
  setActiveData: Function
  setShowReject: Function
}

export const Context = React.createContext<Partial<CtxType>>({})
