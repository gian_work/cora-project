import React, { useState, useContext } from "react"
import { toast } from "react-toastify"

/** Context */
import { Context as RootContext } from "context/Dashboard/Applications/main/Context"

/** Service */
import service from "services/Dashboard/Applications"

/** Utils */
import { toUnix, fromUnix } from "utils/date"
import { UploadImage } from "utils/uploadFile"
import { Context } from "./Context"

export interface AddApplicationsProviderProps {
  data?: Record<string, any>
  type?: number
  showMain?: Function
}

const Provider: React.FC<AddApplicationsProviderProps> = ({
  children,
  data,
  type,
  showMain
}) => {
  const isForApproving = type === 1
  const hasData = data !== undefined
  const rData = data?.request_data
  const rVehicle = rData?.vehicle
  const rMoving = rData?.move_in_out

  const { activeTable } = useContext(RootContext)
  const [forApproving] = useState(isForApproving)
  const [openDialog, setOpenDialog] = useState(false)
  const [photos, setPhotos] = useState<any>(rVehicle?.vehicle_photo?.photos || Array)
  const [files, setFiles] = useState<any>(data?.supporting_doc?.files || Array)
  const [attaching, setAttaching] = useState(false)
  const [fileAttaching, setFileAttaching] = useState(false)
  const [submittedData, setSubmittedData] = useState({})
  const [applicationDetails, setApplicationDetails] = useState({
    unitUID: data?.unit_uid || "",
    applicantName: data?.applicant_name || "",
    applicantUID: data?.applicant_account_uid || "",
    startDate: fromUnix(data?.start_date) || "",
    endDate: fromUnix(data?.end_date) || "",
    serialNo: data?.serial_no || "",
    remarks: data?.remarks || ""
  })
  const [residentInfo, setResidentInfo] = useState({
    name: data?.applicant_name || ""
  })

  const [detailsInfo, setDetailsInfo] = React.useState({})
  const [vehicleInfo, setVehicleInfo] = React.useState({
    carLabel: rVehicle?.car_label || "",
    vehicleType: rVehicle?.vehicle_model || "",
    vehicleNo: rVehicle?.vehicle_no || "",
    IUNo: rVehicle?.iu_no || "",
    model: rVehicle?.vehicle_model || "",
    photos: rVehicle?.photos || []
  })
  const [movingInfo, setMovingInfo] = React.useState({
    name: rMoving?.name || "",
    email: rMoving?.email || "",
    eta: rMoving?.eta_ms || "",
    mobile: rMoving?.mobile || "",
    purpose: rMoving?.purpose || "",
    remarks: rMoving?.remarks || "",
    numberOfPersons: rMoving?.no_of_persons || 0,
    multiplePersons: rMoving?.multiple_persons === true ? "2" : "1"
  })

  const ad = applicationDetails

  /** Notification */
  const notifyCreate = () =>
    toast("Successfully created the application.", {
      type: toast.TYPE.SUCCESS
    })

  const notifyUpdate = () =>
    toast("Successfully updated the application.", {
      type: toast.TYPE.SUCCESS
    })

  const notifyApprove = () =>
    toast("Successfully aprroved the application.", {
      type: toast.TYPE.SUCCESS
    })

  const applicationType = (category: number) => {
    switch (category) {
      case 0:
        return 2
      case 1:
        return 1
      default:
        return {}
    }
  }

  /** Methods */
  const vehiclePayload = {
    "vehicle": {
      "car_label": vehicleInfo?.carLabel,
      "iu_no": vehicleInfo?.IUNo,
      "owner_name": residentInfo?.name,
      "vehicle_model": vehicleInfo?.vehicleType,
      "vehicle_no": vehicleInfo?.vehicleNo,
      "vehicle_photo": {
        "photos": photos
      }
    }
  }

  const movingInOutPayload = {
    "move_in_out": {
      "unit_uid": ad?.unitUID,
      "contact_person": residentInfo?.name,
      "name": movingInfo?.name,
      "email": movingInfo?.email,
      "eta_ms": toUnix(movingInfo.eta),
      "mobile": movingInfo?.mobile,
      "purpose": movingInfo?.purpose,
      "remarks": movingInfo?.remarks,
      "no_of_persons":
        Number(movingInfo?.numberOfPersons) === 0
          ? 1
          : Number(movingInfo?.numberOfPersons),
      "multiple_persons": movingInfo?.multiplePersons !== "1"
    }
  }

  const requestPayload = (payloadType: number) => {
    switch (payloadType) {
      case 0:
        return movingInOutPayload
      case 1:
        return vehiclePayload
      default:
        return {}
    }
  }

  const payload = {
    "application_type": applicationType(activeTable || 0),
    "unit_uid": ad?.unitUID,
    "applicant_account_uid": ad?.applicantUID,
    "applicant_name": residentInfo?.name,
    "start_date": toUnix(ad?.startDate),
    "end_date": ad?.endDate !== "" ? toUnix(ad?.endDate) : 0,
    "remarks": ad?.remarks,
    "application_date": 1587375186000,
    "serial_no": ad?.serialNo,
    "supporting_doc": {
      "files": files
    },
    "request_data": requestPayload(activeTable || 0)
  }

  const handleCreateApplication = async () => {
    const endpoint = hasData
      ? service.updateApplication(payload, data?._uid)
      : service.createApplication(payload)

    const notify = () => (hasData ? notifyUpdate() : notifyCreate())

    try {
      const response = await endpoint
      setSubmittedData(response?.data?._data)
      notify()
      return response
    } catch (e) {
      return e
    }
  }

  const handleApproveApplication = async () => {
    const endpoint = service.updateApplication(payload, data?._uid)
    const approveEndpoint = service.approveApplication({ "_uid": data?._uid })

    try {
      const response = await endpoint
      const approved = await approveEndpoint
      setSubmittedData(response?.data?._data)
      notifyApprove()
      return approved
    } catch (e) {
      return e
    }
  }

  const attachingFile = (docType: number, value: boolean): void => {
    if (docType === 1) {
      setAttaching(value)
    } else {
      setFileAttaching(value)
    }
  }

  const setDocs = (docType: number, docs: any): void => {
    if (docType === 1) {
      setPhotos(docs)
    } else {
      setFiles(docs)
    }
  }

  function handleDocsUpload(event: any, docType: number) {
    const docs = docType === 1 ? photos : files
    const action = docType === 1 ? setPhotos : setFiles
    attachingFile(docType, true)
    UploadImage(event)
      .then((output: any) => {
        action([...docs, output])
        attachingFile(docType, false)
      })
      .catch(() => attachingFile(docType, false))
  }

  const removeDoc = (value: number, fileType: number) => {
    /** Type 1 Image; Type 2 Docs */
    const group = type === 1 ? photos : files
    const docsFiltered = group.filter((item: any, index: number) => {
      return index !== value
    })
    setDocs(fileType, docsFiltered)
  }

  return (
    <Context.Provider
      value={{
        forApproving,
        openDialog,
        applicationDetails,
        detailsInfo,
        attaching,
        photos,
        vehicleInfo,
        files,
        fileAttaching,
        movingInfo,
        residentInfo,
        submittedData,
        setOpenDialog,
        setApplicationDetails,
        setDetailsInfo,
        handleDocsUpload,
        removeDoc,
        setVehicleInfo,
        setMovingInfo,
        handleCreateApplication,
        handleApproveApplication,
        setResidentInfo,
        showMain
      }}
    >
      {children}
    </Context.Provider>
  )
}

export default Provider
