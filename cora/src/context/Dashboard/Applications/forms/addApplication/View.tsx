import React, { Suspense, useContext } from "react"
import Box from "@material-ui/core/Box"
import Card from "@material-ui/core/Card"
import { useMachine } from "@xstate/react"

/** Component */
import Loader from "components/Loader"
import Stepper from "components/Stepper"
import Dialog from "components/Dialog"
import Footer from "components/Forms/Footer/new"
/** Forms */
import PersonalDetails from "pages/Dashboard/Applications/components/AddApplication/PersonalDetails"
import ApplicationDetails from "pages/Dashboard/Applications/components/AddApplication/ApplicationDetails"
import SupportingDocuments from "pages/Dashboard/Applications/components/AddApplication/SupportingDocuments"
import Receipt from "pages/Dashboard/Applications/components/AddApplication/Receipt"
/** Context */
import { AddApplicationsMachine } from "machines/Dashboard/Applications/addApplication"
import { Context as RootContext } from "context/Dashboard/Applications//main/Context"
import { ApplicationCheck } from "config/Dashboard/Applications/validation"
import { CtxType } from "./Context"
import withContext from "./withContext"

/** Ref */
export const refSubmit = React.createRef<HTMLDivElement>()
export const refSubmitApplicationDetails = React.createRef<HTMLDivElement>()
export const refPrintBtn = React.createRef<HTMLButtonElement>()

const AddApplication: React.FC<CtxType> = ({
  openDialog,
  setOpenDialog,
  applicationDetails,
  vehicleInfo,
  movingInfo,
  handleCreateApplication,
  handleApproveApplication,
  forApproving,
  showMain
}) => {
  const { activeTable } = useContext(RootContext)
  const [current, send] = useMachine(AddApplicationsMachine)
  const xValue = current?.value
  const { unitUID, applicationUID, startDate, endDate } = applicationDetails

  /** Methods */
  const handleNext = () => {
    refSubmit.current?.click()
    /** Step 1 */
    if (xValue === "personalDetails" && unitUID !== "" && applicationUID !== "") {
      send("NEXT")
    } else {
      refSubmit.current?.click()
    }
    /** Step 2 */
    if (
      xValue === "applicationDetails" &&
      activeTable === 0 &&
      startDate !== "" &&
      endDate !== "" &&
      ApplicationCheck(movingInfo)
    ) {
      send("NEXT")
    } else {
      refSubmit.current?.click()
      refSubmitApplicationDetails.current?.click()
    }

    if (
      xValue === "applicationDetails" &&
      activeTable === 1 &&
      startDate !== "" &&
      endDate !== "" &&
      ApplicationCheck(vehicleInfo)
    ) {
      send("NEXT")
    } else {
      refSubmit.current?.click()
      refSubmitApplicationDetails.current?.click()
    }

    /** Step 3 */
    if (xValue === "supportingDocs") {
      if (forApproving) {
        handleApproveApplication().then(() => send("NEXT"))
      } else {
        handleCreateApplication().then(() => send("NEXT"))
      }
      // send("NEXT")
    } else {
      refSubmit.current?.click()
    }
  }

  /** Views */
  const ViewPersonalDetails = (): JSX.Element => {
    if (xValue !== "personalDetails") {
      return <div />
    }
    return <PersonalDetails />
  }

  const ViewDetails = (): JSX.Element => {
    if (xValue !== "applicationDetails") {
      return <div />
    }

    return <ApplicationDetails />
  }

  const ViewUploadDocs = (): JSX.Element => {
    if (xValue !== "supportingDocs") {
      return <div />
    }

    return <SupportingDocuments />
  }

  const ViewReceipt = (): JSX.Element => {
    if (xValue !== "receipt") {
      return <div />
    }

    return <Receipt />
  }

  const StepperView = ({ value }: any): JSX.Element => {
    const activeStep: Record<string, number> = {
      "personalDetails": 1,
      "applicationDetails": 2,
      "supportingDocs": 3,
      "receipt": 4
    }

    return (
      <Stepper
        items={[
          "personal details",
          "application details",
          "supporting docs",
          "receipt"
        ]}
        activeStep={activeStep[value]}
        justifyStart
      />
    )
  }

  return (
    <>
      <Card>
        <StepperView value={current?.value} />
        <Box>
          <Suspense fallback={<Loader forContent />}>
            {ViewPersonalDetails()}
            {ViewDetails()}
            {ViewUploadDocs()}
            {ViewReceipt()}
          </Suspense>
          <Box margin="0 0 30px">
            <Footer
              handleNext={() => handleNext()}
              handleBack={xValue === "personalDetails" ? null : () => send("BACK")}
              handleCancel={() => setOpenDialog && setOpenDialog(true)}
              label={xValue === "receipt" ? "Print" : "Next"}
              forAcknowledge={xValue === "receipt"}
              handlePrint={() => refPrintBtn.current?.click()}
              handleSendEmail={() => null}
              handleDone={() => setOpenDialog && setOpenDialog(true)}
            />
          </Box>
        </Box>
        <Dialog
          action={() => showMain()}
          isOpen={openDialog}
          setOpen={setOpenDialog}
          actionLabel="OK"
          title=""
          message="Are you sure you want to cancel?"
        />
      </Card>
    </>
  )
}

export default withContext(AddApplication)
