import React from "react"

export type CtxType = {
  residentInfo: Record<string, any>
  detailsInfo: any
  applicationDetails: any
  photos: any
  vehicleInfo: any
  files: any
  movingInfo: any
  submittedData: any
  forApproving: boolean
  openDialog: boolean
  attaching: boolean
  fileAttaching: boolean
  setApplicationDetails: Function
  setDetailsInfo: Function
  setOpenDialog: Function
  handleDocsUpload: Function
  removeDoc: Function
  setVehicleInfo: Function
  setMovingInfo: Function
  handleCreateApplication: Function
  setResidentInfo: Function
  handleApproveApplication: Function
  showMain: Function
}

export const Context = React.createContext<Partial<CtxType>>({})
