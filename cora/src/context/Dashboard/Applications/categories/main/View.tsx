import React from "react"
import Box from "@material-ui/core/Box"

import MovingInOut from "pages/Dashboard/Applications/categories/movingInOut"
import Vehicles from "pages/Dashboard/Applications/categories/vehicles"

/** Context */
import withContext from "context/Dashboard/Applications/main/withContext"
import { CtxType } from "context/Dashboard/Applications/main/Context"

const MainCategories: React.FC<CtxType> = ({ activeTable }) => {
  React.useEffect(() => {
    window.scrollTo(0, 0)
  }, [])

  const TableView = (activeTableParam: number) => {
    switch (activeTableParam) {
      case 0:
        return <MovingInOut />
      case 1:
        return <Vehicles />
      default:
        break
    }
    return activeTableParam
  }

  const MainView = () => {
    return <Box>{TableView(activeTable)}</Box>
  }

  return (
    <>
      <div>{MainView()}</div>
    </>
  )
}

export default withContext(MainCategories)
