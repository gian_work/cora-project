import React from "react"

/** Context */
import { Context } from "./Context"

/** Utils */

export interface ProviderProps {
  showMain: Function
  showAddApplication: Function
  showUpdateApplication: Function
  showApproveApplication: Function
}

const Provider: React.FC<ProviderProps> = ({
  children,
  showMain,
  showAddApplication,
  showUpdateApplication,
  showApproveApplication
}) => {
  /** States */
  const [activeFilter, setActiveFilter] = React.useState(0)

  /** Methods */
  const filterTable = (value: number) => {
    setActiveFilter(value)
  }

  const showTable = () => {
    showMain()
  }

  return (
    <Context.Provider
      value={{
        activeFilter,
        setActiveFilter,
        filterTable,
        showMain,
        showAddApplication,
        showUpdateApplication,
        showApproveApplication,
        showTable
      }}
    >
      {children}
    </Context.Provider>
  )
}

export default Provider
