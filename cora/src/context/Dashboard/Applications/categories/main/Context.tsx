import React from "react"

export type CtxType = {
  activeFilter: number
  setActiveFilter: Function
  filterTable: Function
  showMain: Function
  showAddApplication: Function
  showUpdateApplication: Function
  showApproveApplication: Function
  showTable: Function
}

export const Context = React.createContext<Partial<CtxType>>({})
