import React, { useState } from "react"
import axios from "axios"
import { toast } from "react-toastify"

/** Services */
import UnitManagementAPI from "services/Dashboard/UnitManagement"
import CommonAPI from "services/Dashboard/Common"

/** Context */
import { Context } from "./Context"

const CreateOwnerProvider: React.FC = (props: any) => {
  const [activeStep, setActiveStep] = useState(1)
  const [openDialog, setOpenDialog] = useState(false)
  const [attaching, setAttaching] = useState(false)
  const [photos, setPhotos] = useState(Array)
  const [userDetails, setuserDetails] = useState({
    firstName: "",
    lastName: "",
    phoneNo: "",
    mobileNo: "",
    unitUid: "",
    email: "",
    isResident: "true",
    registeredAddress: "",
    strataTitle: false,
    tenancyStart: "",
    tenancyEnd: "",
    roles: "",
    formType: "owner",
    blockUnit: ""
  })

  const { children, fetchData, setAddUnitActive } = props

  /** Notification */
  const notifyCreate = () =>
    toast("Successfully created an owner.", {
      type: toast.TYPE.SUCCESS
    })

  /** Methods */
  const handleRegisterUser = async () => {
    const ownerData = {
      "name": `${userDetails.firstName} ${userDetails.lastName}`,
      "phone_no": userDetails.phoneNo,
      "mobile_no": userDetails.mobileNo,
      "unit_uid": userDetails.blockUnit,
      "email": userDetails.email,
      "is_resident": userDetails.isResident === "true",
      "registered_address": userDetails.registeredAddress,
      "strata_title": userDetails.strataTitle,
      "tenancy_start": "",
      "tenancy_end": "",
      "roles": ["ce95b2b2-9ab1-4c0f-9e57-9464c154c806"]
    }

    const tenantData = {
      "name": `${userDetails.firstName} ${userDetails.lastName}`,
      "phone_no": userDetails.phoneNo,
      "mobile_no": userDetails.mobileNo,
      "unit_uid": userDetails.blockUnit,
      "email": userDetails.email,
      "registered_address": userDetails.registeredAddress,
      "tenancy_start": "2020-01-01",
      "tenancy_end": "2020-12-31",
      "roles": ["d28be918-2d7b-4438-8be2-6350eb815a22"]
    }

    const data = userDetails.formType === "owner" ? ownerData : tenantData
    try {
      const response =
        userDetails.formType === "owner"
          ? UnitManagementAPI.createOwner(data)
          : UnitManagementAPI.createTenant(data)
      await setAddUnitActive(false)
      fetchData()
      notifyCreate()
      return response
    } catch (e) {
      return e
    }

    // return userDetails.formType === "owner"
    //   ? UnitManagementAPI.createOwner(data)
    //       .then(() => notifyCreate())
    //       .catch((err: any) => err)
    //   : UnitManagementAPI.createTenant(data)
    //       .then(() => notifyCreate())
    //     .catch((err: any) => err)

    // setAddFeedbackActive(false)
  }

  const handleImageUpload = (event: any) => {
    const fileToUpload = event[0]
    const items: any = []
    setAttaching(true)
    CommonAPI.getPresigned()
      .then((res: any) => {
        const { key } = res.data._data
        const dataItem = {
          "key": key,
          "file_name": fileToUpload.name,
          "encoding": fileToUpload.type
        }
        axios
          .put(res.data._data.presigned_url, fileToUpload)
          .then(() => {
            items.push(dataItem)
            setPhotos([...photos, ...items])
            setAttaching(false)
          })
          .catch(() => setAttaching(false))
      })
      .catch(() => setAttaching(false))
  }

  const handleNext = () => {
    setActiveStep((prevActiveStep) => prevActiveStep + 1)
  }

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1)
  }

  const removePhoto = (value: number) => {
    const photoFiltered = photos.filter((item: any, index: number) => {
      return index !== value
    })
    setPhotos(photoFiltered)
  }

  const getSteps = () => {
    return ["select vms type", "visitor's information", "requester's information"]
  }

  return (
    <Context.Provider
      value={{
        handleRegisterUser,
        handleImageUpload,
        handleNext,
        handleBack,
        setActiveStep,
        setOpenDialog,
        setuserDetails,
        fetchData,
        setAddUnitActive,
        activeStep,
        userDetails,
        attaching,
        steps: getSteps,
        photos,
        setPhotos,
        removePhoto,
        openDialog
      }}
    >
      {children}
    </Context.Provider>
  )
}

export default CreateOwnerProvider
