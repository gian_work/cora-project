import React from "react"

export type CtxType = {
  handleImageUpload: Function
  handleNext: Function
  handleBack: Function
  setActiveStep: Function
  setOpenDialog: Function
  setuserDetails: Function
  handleRegisterUser: Function
  fetchData: Function
  setAddUnitActive: Function
  steps: Function
  setPhotos: Function
  removePhoto: Function
  activeStep: number
  openDialog: boolean
  attaching: boolean
  userDetails: Record<string, any>
  photos: any
}

export const Context = React.createContext<Partial<CtxType>>({})
