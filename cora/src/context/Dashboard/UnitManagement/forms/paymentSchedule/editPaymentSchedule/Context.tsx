import React from "react"

export type CtxType = {
  setActiveStep: Function
  setOpenDialog: Function
  handleCreatePS: Function
  setAddUnitActive: Function
  setPsActive: Function
  steps: Function
  createPs: Function
  setPsDetails: Function
  activeStep: number
  openDialog: boolean
  psDetails: Record<string, any>
  residentInfo: Record<string, any>
  data: Record<string, any>
  setResidentInfo: Function
}

export const Context = React.createContext<Partial<CtxType>>({})
