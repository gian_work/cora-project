import React, { useState } from "react"
import axios from "axios"
import { toast } from "react-toastify"
import Cookie from "js-cookie"

/** Services */
import UnitManagementAPI from "services/Dashboard/UnitManagement"
import CommonAPI from "services/Dashboard/Common"

/** Context */
import { Context as ContextMain } from "context/Dashboard/UnitManagement/categories/main/Context"
import { toUnix } from "utils/date"
import { Context } from "./Context"

/** Utils */

const CreateOwnerProvider: React.FC = (props: any) => {
  const { showMain } = React.useContext(ContextMain)

  const [openDialog, setOpenDialog] = useState(false)
  const [attaching, setAttaching] = useState(false)
  const [fileAttaching, setFileAttaching] = useState(false)
  const [photos, setPhotos] = useState<any>(Array)
  const [files, setFiles] = useState<any>(Array)
  const [userDetails, setuserDetails] = useState({
    firstName: "",
    lastName: "",
    phoneNo: "",
    mobileNo: "",
    unitUid: "",
    email: "",
    isResident: "true",
    registeredAddress: "",
    strataTitle: false,
    tenancyStart: "",
    tenancyEnd: "",
    roles: "",
    formType: "owner",
    blockUnit: ""
  })

  const { children } = props

  /** Notification */
  const notifyCreate = () =>
    toast("Successfully created an owner.", {
      type: toast.TYPE.SUCCESS
    })

  /** Methods */
  const handleRegisterUser = async () => {
    const ownerData = {
      "name": `${userDetails.firstName} ${userDetails.lastName}`,
      "phone_no": userDetails.phoneNo,
      "mobile_no": userDetails.mobileNo,
      "unit_uid": userDetails.blockUnit,
      "email": userDetails.email,
      "is_resident": userDetails.isResident === "true",
      "registered_address": userDetails.registeredAddress,
      "strata_title": userDetails.strataTitle,
      "tenancy_start": "",
      "tenancy_end": "",
      "profile_photo": {
        "key": photos[0]?.key || "",
        "file_name": photos[0]?.file_name || "",
        "encoding": photos[0]?.encoding || ""
      },
      "supporting_doc": {
        "files": files
      },
      "condo_uid": Cookie.get("condoUID")
    }

    const tenantData = {
      "name": `${userDetails.firstName} ${userDetails.lastName}`,
      "phone_no": userDetails.phoneNo,
      "mobile_no": userDetails.mobileNo,
      "unit_uid": userDetails.blockUnit,
      "email": userDetails.email,
      "registered_address": userDetails.registeredAddress,
      "tenancy_start": toUnix(userDetails?.tenancyStart),
      "tenancy_end": toUnix(userDetails?.tenancyEnd),
      "profile_photo": {
        "key": photos[0]?.key || "",
        "file_name": photos[0]?.file_name || "",
        "encoding": photos[0]?.encoding || ""
      },
      "supporting_doc": {
        "files": files
      },
      "condo_uid": Cookie.get("condoUID")
    }

    const data = userDetails.formType === "owner" ? ownerData : tenantData

    try {
      const response =
        userDetails.formType === "owner"
          ? UnitManagementAPI.createOwner(data)
          : UnitManagementAPI.createTenant(data)
      notifyCreate()
      showMain && showMain()
      return response
    } catch (e) {
      return e
    }
  }

  const handleImageUpload = (event: any) => {
    const fileToUpload = event[0]
    const items: any = []
    setAttaching(true)

    CommonAPI.getPresigned()
      .then((res: any) => {
        const { key } = res.data._data
        const dataItem = {
          "key": key,
          "file_name": fileToUpload.name,
          "encoding": fileToUpload.type
        }
        axios
          .put(res.data._data.presigned_url, fileToUpload)
          .then(() => {
            items.push(dataItem)
            setPhotos([...photos, ...items])
            setAttaching(false)
          })
          .catch(() => setAttaching(false))
      })
      .catch(() => setAttaching(false))
  }

  const handleFileUpload = (event: any) => {
    const fileToUpload = event[0]
    const items: any = []
    setFileAttaching(true)

    CommonAPI.getPresigned()
      .then((res: any) => {
        const { key } = res.data._data
        const dataItem = {
          "key": key,
          "file_name": fileToUpload.name,
          "encoding": fileToUpload.type
        }
        axios
          .put(res.data._data.presigned_url, fileToUpload)
          .then(() => {
            items.push(dataItem)
            setFiles([...files, ...items])
            setFileAttaching(false)
          })
          .catch(() => setFileAttaching(false))
      })
      .catch(() => setFileAttaching(false))
  }

  const removePhoto = (value: number) => {
    const photoFiltered = photos.filter((item: any, index: number) => {
      return index !== value
    })
    setPhotos(photoFiltered)
  }

  const removeFile = (value: number) => {
    const fileFiltered = files.filter((item: any, index: number) => {
      return index !== value
    })
    setFiles(fileFiltered)
  }

  return (
    <Context.Provider
      value={{
        handleRegisterUser,
        handleImageUpload,
        setPhotos,
        removePhoto,
        setOpenDialog,
        setuserDetails,
        setFiles,
        handleFileUpload,
        removeFile,
        userDetails,
        attaching,
        fileAttaching,
        photos,
        openDialog,
        files
      }}
    >
      {children}
    </Context.Provider>
  )
}

export default CreateOwnerProvider
