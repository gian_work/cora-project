import React, { lazy, Suspense } from "react"
import Box from "@material-ui/core/Box"
import Card from "@material-ui/core/Card"

/** Component */
import Loader from "components/Loader"
import Stepper from "components/Stepper"
import Dialog from "components/Dialog"

/** Context */
import { CtxType } from "./Context"
import withContext from "./withContext"

const GeneralInfo = lazy(() =>
  import("pages/Dashboard/UnitManagement/components/Common/UserForm/GeneralInfo")
)
const PersonalDetails = lazy(() =>
  import("pages/Dashboard/UnitManagement/components/Common/UserForm/PersonalDetails")
)
const UploadImage = lazy(() =>
  import("pages/Dashboard/UnitManagement/components/Common/UserForm/UploadImage")
)
const SupportingDocuments = lazy(() =>
  import(
    "pages/Dashboard/UnitManagement/components/Common/UserForm/SupportingDocuments"
  )
)

function getStepContent(step: any): JSX.Element {
  switch (step) {
    case 1:
      return <GeneralInfo />
    case 2:
      return <PersonalDetails />
    case 3:
      return <UploadImage />
    case 4:
      return <SupportingDocuments />
    default:
      return <div />
  }
}

const EditResidentView: React.FC<CtxType> = ({
  activeStep,
  setAddUnitActive,
  openDialog,
  setOpenDialog
}) => {
  // const [current] = useMachine(AddFeedbackMachine)
  return (
    <>
      <Card>
        <Stepper
          items={[
            "general info",
            "personal details",
            "upload image",
            "supporting documents"
          ]}
          activeStep={activeStep}
          justifyStart
        />
        <Box>
          <Suspense fallback={<Loader forContent />}>
            {getStepContent(activeStep)}
          </Suspense>
        </Box>
        <Dialog
          action={() => setAddUnitActive(false)}
          isOpen={openDialog}
          setOpen={setOpenDialog}
          actionLabel="OK"
          title=""
          message="Are you sure you want to cancel?"
        />
      </Card>
    </>
  )
}

export default withContext(EditResidentView)
