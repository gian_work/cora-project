import React, { useState } from "react"
import axios from "axios"
import { toast } from "react-toastify"
import Cookie from "js-cookie"

/** Services */
import UnitManagementAPI from "services/Dashboard/UnitManagement"
import CommonAPI from "services/Dashboard/Common"

/** Context */
import { Context as ContextMain } from "context/Dashboard/UnitManagement/categories/main/Context"
import { toUnix } from "utils/date"
import { Context } from "./Context"

/** Utils */

interface CreateUserProps {
  data?: Record<string, any>
}

const CreateUserProvider: React.FC<CreateUserProps> = ({ children, data }) => {
  const { showMain } = React.useContext(ContextMain)
  /** check if its from new request or owners/tenants */
  /** this will be used for the profile photos/attachments */
  const hasData = data !== undefined
  const fromData = data?.attachments !== undefined

  const [openDialog, setOpenDialog] = useState(false)
  const [attaching, setAttaching] = useState(false)
  const [fileAttaching, setFileAttaching] = useState(false)
  const [photos, setPhotos] = useState<any>(Array)
  const [files, setFiles] = useState<any>(data?.supporting_doc?.files || Array)
  const [userDetails, setuserDetails] = useState({
    uid: data?._uid,
    name: data?.name || "",
    firstName: "",
    lastName: "",
    phoneNo: data?.phone_no || "",
    mobileNo: data?.mobile_no || "",
    unitUid: data?.unit_uid || "",
    email: data?.email || "",
    isResident: data?.resident_user_type === 2,
    registeredAddress: data?.registered_address || "",
    strataTitle: data?.strata_title || false,
    tenancyStart: data?.tenancy_start === undefined ? "" : data?.tenancy_start,
    tenancyEnd: data?.tenancy_end === undefined ? "" : data?.tenancy_end,
    formType: data?.resident_user_type === 3 ? "tenant" : "owner",
    blockUnit: data?.unit_uid || data?.unit?._uid || ""
  })

  React.useEffect(() => {
    if (fromData) {
      setPhotos(data?.attachments)
    }
    if (data?.profile_photo || data?.profile_photo?.key !== "") {
      setPhotos([data?.profile_photo])
    }
    if (!hasData) {
      setPhotos([])
    }
    if (data?.attachments === undefined) {
      setPhotos([])
    }
  }, [])

  /** Notification */
  const notifyCreate = () =>
    toast(`Successfully created the owner.`, {
      type: toast.TYPE.SUCCESS
    })

  const notifyError = (errorMsg: string) =>
    toast(`${errorMsg}`, {
      type: toast.TYPE.ERROR
    })

  /** Methods */
  async function handleRegisterUser() {
    const ownerData = {
      "name": userDetails.name,
      "phone_no": userDetails.phoneNo,
      "mobile_no": userDetails.mobileNo,
      "unit_uid": userDetails.blockUnit,
      "email": userDetails.email,
      "is_resident": userDetails.isResident,
      "registered_address": userDetails.registeredAddress,
      "strata_title": userDetails.strataTitle,
      "profile_photo": {
        "key": photos[0]?.key || "",
        "file_name": photos[0]?.file_name || "",
        "encoding": photos[0]?.encoding || ""
      },
      "supporting_doc": {
        "files": files
      },
      "condo_uid": Cookie.get("condoUID"),
      "account_request_uid": data?._uid
    }
    const tenantData = {
      "name": userDetails.name,
      "phone_no": userDetails.phoneNo,
      "mobile_no": userDetails.mobileNo,
      "unit_uid": userDetails.blockUnit,
      "email": userDetails.email,
      "registered_address": userDetails.registeredAddress,
      "tenancy_start": toUnix(userDetails?.tenancyStart),
      "tenancy_end": toUnix(userDetails?.tenancyEnd),
      "profile_photo": {
        "key": photos[0]?.key || "",
        "file_name": photos[0]?.file_name || "",
        "encoding": photos[0]?.encoding || ""
      },
      "supporting_doc": {
        "files": files
      },
      "condo_uid": Cookie.get("condoUID"),
      "account_request_uid": data?._uid
    }
    const formData = userDetails.formType === "owner" ? ownerData : tenantData

    try {
      const response =
        userDetails.formType === "owner"
          ? await UnitManagementAPI.createOwner(formData)
          : await UnitManagementAPI.createTenant(formData)

      const statusCode = response?.data?._statusCode
      if (statusCode === -121) {
        notifyError(response?.data?._message)
        return
      }
      notifyCreate()
      showMain && showMain()
      // return response
    } catch (e) {
      notifyError("Error creating a user.")
    }
  }

  const handleImageUpload = (event: any) => {
    const fileToUpload = event[0]
    const items: any = []
    setAttaching(true)

    CommonAPI.getPresigned()
      .then((res: any) => {
        const { key } = res.data._data
        const dataItem = {
          "key": key,
          "file_name": fileToUpload.name,
          "encoding": fileToUpload.type
        }
        axios
          .put(res.data._data.presigned_url, fileToUpload)
          .then(() => {
            items.push(dataItem)
            setPhotos([...photos, ...items])
            setAttaching(false)
          })
          .catch(() => setAttaching(false))
      })
      .catch(() => setAttaching(false))
  }

  const handleFileUpload = (event: any) => {
    const fileToUpload = event[0]
    const items: any = []
    setFileAttaching(true)

    CommonAPI.getPresigned()
      .then((res: any) => {
        const { key } = res.data._data
        const dataItem = {
          "key": key,
          "file_name": fileToUpload.name,
          "encoding": fileToUpload.type
        }
        axios
          .put(res.data._data.presigned_url, fileToUpload)
          .then(() => {
            items.push(dataItem)
            setFiles([...files, ...items])
            setFileAttaching(false)
          })
          .catch(() => setFileAttaching(false))
      })
      .catch(() => setFileAttaching(false))
  }

  const removePhoto = (value: number) => {
    const photoFiltered = photos.filter((item: any, index: number) => {
      return index !== value
    })
    setPhotos(photoFiltered)
  }

  const removeFile = (value: number) => {
    const fileFiltered = files.filter((item: any, index: number) => {
      return index !== value
    })
    setFiles(fileFiltered)
  }

  return (
    <Context.Provider
      value={{
        handleRegisterUser,
        handleImageUpload,
        setPhotos,
        removePhoto,
        setOpenDialog,
        setuserDetails,
        setFiles,
        handleFileUpload,
        removeFile,
        userDetails,
        attaching,
        fileAttaching,
        photos,
        openDialog,
        files
      }}
    >
      {children}
    </Context.Provider>
  )
}

export default CreateUserProvider
