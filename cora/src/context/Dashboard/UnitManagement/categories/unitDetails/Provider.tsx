import React from "react"

/** Context */
import { Context } from "./Context"

const UnitDetailsProvider: React.FC = ({ children }) => {
  const [openDialog, setOpenDialog] = React.useState(false)
  const [activeData, setActiveData] = React.useState([])
  const [activeVehicle, setActiveVehicle] = React.useState([])

  return (
    <Context.Provider
      value={{
        setOpenDialog,
        setActiveData,
        setActiveVehicle,
        activeVehicle,
        activeData,
        openDialog
      }}
    >
      {children}
    </Context.Provider>
  )
}

export default UnitDetailsProvider
