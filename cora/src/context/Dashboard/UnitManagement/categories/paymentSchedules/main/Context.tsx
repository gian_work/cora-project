import React from "react"

export type CtxType = {
  activeFilter: number
  setActiveFilter: Function
  filterTable: Function
}

export const Context = React.createContext<Partial<CtxType>>({})
