import React from "react"

/** Context */
import { Context } from "./Context"

const ResidentsProvider: React.FC = ({ children }) => {
  const [openDialog, setOpenDialog] = React.useState(false)
  const [accountRequestData, setAccountRequestData] = React.useState([])
  const [activeData, setActiveData] = React.useState([])

  return (
    <Context.Provider
      value={{
        setOpenDialog,
        setAccountRequestData,
        openDialog,
        accountRequestData,
        setActiveData,
        activeData
      }}
    >
      {children}
    </Context.Provider>
  )
}

export default ResidentsProvider
