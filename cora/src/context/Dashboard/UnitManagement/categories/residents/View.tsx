import React from "react"
import Box from "@material-ui/core/Box"

/** components */
import TableHeader from "components/TableHeader"
import TabsFilter from "components/Table/components/TabsFilter/withRef"
import WithTable from "layout/WithTable"
import UnitDetails from "pages/Dashboard/UnitManagement/categories/unitDetails"
/** Table */
import NewResidents from "pages/Dashboard/UnitManagement/tables/residents/New"
import RegisteredResidents from "pages/Dashboard/UnitManagement/tables/residents/Registered"
import ArchivedResidents from "pages/Dashboard/UnitManagement/tables/residents/Archived"
import ChangeRequests from "pages/Dashboard/UnitManagement/tables/residents/ChangeRequests"
/** Form */
import CreateUserForm from "pages/Dashboard/UnitManagement/components/Common/UserForm"

/** Icons */
import { MenuIcons } from "pages/Dashboard/UnitManagement/config/Menu"

/** Context */
import { CtxType } from "context/Dashboard/UnitManagement/main/Context"
import withMainContext from "context/Dashboard/UnitManagement/main/withContext"
import { Context as ParentContext } from "context/Dashboard/UnitManagement/categories/main/Context"
import { filterResidents } from "config/Dashboard/UnitManagement"
import { Context as SelfContext } from "./Context"

/** Config */

const ResidentsView: React.FC<CtxType> = ({ activeTableFilter }) => {
  const { ownersTab, setOwnersTab } = activeTableFilter
  const { activeView, showCreateResident } = React.useContext(ParentContext)
  const { accountRequestData, activeData } = React.useContext(SelfContext)

  const TableView = (activeTableParam: number) => {
    switch (activeTableParam) {
      case 0:
        return <NewResidents />
      case 1:
        return <ChangeRequests />
      case 2:
        return <RegisteredResidents />
      case 3:
        return <ArchivedResidents />
      default:
        break
    }
    return activeTableParam
  }

  const ViewDetails = (): JSX.Element => {
    if (activeView !== "details") {
      return <div />
    }

    return <UnitDetails />
  }

  const ViewCreateResident = (): JSX.Element => {
    if (activeView !== "createResident") {
      return <div />
    }
    return <CreateUserForm />
  }

  const ViewCreateFromData = (): JSX.Element => {
    if (activeView !== "createFromData") {
      return <div />
    }
    return <CreateUserForm data={accountRequestData} />
  }

  const ViewEditResident = (): JSX.Element => {
    if (activeView !== "editResident") {
      return <div />
    }
    return <CreateUserForm data={activeData} />
  }

  const ViewEditOwner = (): JSX.Element => {
    if (activeView !== "editOwner") {
      return <div />
    }
    return <CreateUserForm data={activeData} />
  }

  const ViewTable = (): JSX.Element => {
    if (activeView !== "tableView") {
      return <div />
    }

    return (
      <WithTable>
        <TableHeader
          addActionLabel="Add New User"
          title="Residents"
          titleIcon={MenuIcons.residents}
          addAction={() => showCreateResident && showCreateResident()}
        />
        <Box paddingLeft="90px" borderBottom="1px solid #F2F2F2">
          <TabsFilter
            value={ownersTab}
            handleChange={setOwnersTab}
            options={filterResidents}
          />
        </Box>
        <Box>{TableView(ownersTab)}</Box>
      </WithTable>
    )
  }

  return (
    <>
      <WithTable>{ViewTable()}</WithTable>
      {ViewDetails()}
      {ViewCreateResident()}
      {ViewEditResident()}
      {ViewCreateFromData()}
      {ViewEditOwner()}
    </>
  )
}

export default withMainContext(ResidentsView)
