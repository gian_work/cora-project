import React, { useState } from "react"

/** Context */
import { Context } from "./Context"

const AccountRequestProvider: React.FC = ({ children }) => {
  const [openDialog, setOpenDialog] = useState(false)
  const [accountRequestData, setAccountRequestData] = useState({})
  const [showSideDetails, setShowSideDetails] = useState(false)

  const handleCreateAccount = (): JSX.Element => {
    return <div />
  }

  function showDetailsView(status: boolean, data: Record<string, any>) {
    setShowSideDetails(status)
    setAccountRequestData(data)
  }

  return (
    <Context.Provider
      value={{
        handleCreateAccount,
        setOpenDialog,
        setAccountRequestData,
        setShowSideDetails,
        showDetailsView,
        showSideDetails,
        openDialog,
        accountRequestData
      }}
    >
      {children}
    </Context.Provider>
  )
}

export default AccountRequestProvider
