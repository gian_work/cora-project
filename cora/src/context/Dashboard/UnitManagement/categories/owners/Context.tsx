import React from "react"

export type CtxType = {
  setOpenDialog: Function
  setAccountRequestData: Function
  setActiveData: Function
  openDialog: boolean
  activeData: Record<string, any>
  accountRequestData: Record<string, any>
}

export const Context = React.createContext<Partial<CtxType>>({})
