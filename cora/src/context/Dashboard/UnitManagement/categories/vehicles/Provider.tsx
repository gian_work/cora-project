import React, { useState } from "react"

/** Context */
import { Context } from "./Context"

const VehiclesProvider: React.FC = ({ children }) => {
  const [activeData, setActiveData] = useState({})

  /** Methods */

  return (
    <Context.Provider
      value={{
        activeData,
        setActiveData
      }}
    >
      {children}
    </Context.Provider>
  )
}

export default VehiclesProvider
