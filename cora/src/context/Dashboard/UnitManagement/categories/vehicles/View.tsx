import React, { useContext } from "react"
import Box from "@material-ui/core/Box"

/** components */
import TableHeader from "components/TableHeader"
import WithTable from "layout/WithTable"
import DetailsSummary from "pages/Dashboard/UnitManagement/components/Vehicles/Details/Summary"
import CreateVehicle from "pages/Dashboard/UnitManagement/components/Vehicles/CreateVehicle"
import Table from "pages/Dashboard/UnitManagement/tables/vehicles/main"

/** Icons */
import { MenuIcons } from "pages/Dashboard/UnitManagement/config/Menu"

/** Context */
import { Context as ParentContext } from "context/Dashboard/UnitManagement/categories/main/Context"
import { Context } from "./Context"

const VehiclesView: React.FC = () => {
  const { activeView, showCreateVehicle } = useContext(ParentContext)
  const { activeData } = useContext(Context)

  const ViewDetails = (): JSX.Element => {
    if (activeView?.details !== "unitDetailsMain") {
      return <div />
    }

    return <DetailsSummary />
  }

  const ViewCreateVehicle = (): JSX.Element => {
    if (activeView !== "createVehicle") {
      return <div />
    }
    return <CreateVehicle />
  }

  const ViewEditVehicle = (): JSX.Element => {
    if (activeView !== "editVehicle") {
      return <div />
    }
    return <CreateVehicle data={activeData} />
  }

  const ViewTable = (): JSX.Element => {
    if (activeView !== "tableView") {
      return <div />
    }
    return (
      <Box>
        <TableHeader
          addActionLabel="Add Vehicle"
          title="Vehicles"
          titleIcon={MenuIcons.vehicles}
          addAction={() => showCreateVehicle && showCreateVehicle()}
        />
        <Box>
          <Table />
        </Box>
      </Box>
    )
  }

  return (
    <>
      <WithTable>{ViewTable()}</WithTable>
      {ViewDetails()}
      {ViewCreateVehicle()}
      {ViewEditVehicle()}
    </>
  )
}

export default VehiclesView
