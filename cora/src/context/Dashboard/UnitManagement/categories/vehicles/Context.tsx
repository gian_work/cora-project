import React from "react"

export type CtxType = {
  activeData: any
  setActiveData: Function
}

export const Context = React.createContext<Partial<CtxType>>({})
