import React from "react"

export type CtxType = {
  showUpdateUser: Function
  showCreateFromData: Function
  showCreateVehicle: Function
  showEditVehicle: Function
  showVehicleDetails: Function
  showPSDetails: Function
  showDetails: Function
  showEditPS: Function
  showAddPS: Function
  setActiveFilter: Function
  filterTable: Function
  showMain: Function
  showCreateOwner: Function
  showEditOwner: Function
  showCreateResident: Function
  showEditResident: Function
  activeFilter: number
  activeTable: number
  activeView: string | any
  activeData: Record<string, any>
}

export const Context = React.createContext<Partial<CtxType>>({})
