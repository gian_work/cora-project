import React from "react"
import Box from "@material-ui/core/Box"
/** Components */
import AccountsRequest from "pages/Dashboard/UnitManagement/tables/accountsRequests"
import Owners from "pages/Dashboard/UnitManagement/tables/owners"
import Residents from "pages/Dashboard/UnitManagement/tables/residents"
import PaymentSchedules from "pages/Dashboard/UnitManagement/categories/paymentSchedules"
import Vehicles from "pages/Dashboard/UnitManagement/tables/vehicles"
/** Context */
import { CtxType } from "./Context"
import withContext from "./withContext"

const UMMainView: React.FC<CtxType> = ({ activeTable }) => {
  const TableView = (value: number) => {
    switch (value) {
      case 1:
        return <AccountsRequest />
      case 2:
        return <Owners />
      case 3:
        return <Residents />
      case 4:
        return <PaymentSchedules />
      case 5:
        return <Vehicles />
      default:
        break
    }
    return value
  }

  return <Box>{TableView(activeTable)}</Box>
}

export default withContext(UMMainView)
