import React from "react"

/** Context */
import { Context } from "./Context"

/** Interface */
export interface UMMainProviderProps {
  showUpdateUser: Function
  showCreateFromData: Function
  showCreateVehicle: Function
  showEditVehicle: Function
  showVehicleDetails: Function
  showCreateOwner: Function
  showEditOwner: Function
  showCreateResident: Function
  showEditResident: Function
  showAddPS: Function
  showEditPS: Function
  showPSDetails: Function
  showDetails: Function
  showMain: Function
  activeTable: number
  activeView: any
  activeData: Record<string, any>
}

const UMMainProvider: React.FC<UMMainProviderProps> = ({
  showUpdateUser,
  showCreateFromData,
  showCreateVehicle,
  showEditVehicle,
  showVehicleDetails,
  showCreateResident,
  showEditResident,
  showCreateOwner,
  showEditOwner,
  showAddPS,
  showEditPS,
  showPSDetails,
  showDetails,
  showMain,
  children,
  activeTable,
  activeView,
  activeData
}) => {
  /** State */
  const [activeFilter, setActiveFilter] = React.useState(0)

  /** Methods */
  const filterTable = (value: number) => {
    setActiveFilter(value)
  }

  return (
    <Context.Provider
      value={{
        filterTable,
        setActiveFilter,
        activeFilter,
        activeTable,
        showAddPS,
        showEditPS,
        showPSDetails,
        showDetails,
        showMain,
        showCreateOwner,
        showEditOwner,
        showCreateResident,
        showEditResident,
        activeView,
        activeData,
        showCreateVehicle,
        showEditVehicle,
        showVehicleDetails,
        showCreateFromData,
        showUpdateUser
      }}
    >
      {children}
    </Context.Provider>
  )
}

export default UMMainProvider
