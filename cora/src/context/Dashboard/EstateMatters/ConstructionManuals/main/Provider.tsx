import React, { useState, useEffect } from "react"

/** Context */
import service from "services/Dashboard/EstateMatters"
import { Context } from "./Context"

/** Service */

const Provider: React.FC = ({ children }) => {
  const [docsFilters, setDocsFilter] = useState([])
  const [activeDoc, setActiveDoc] = useState(null)
  const [activeTable, setActiveTable] = useState(0)
  const [accountRequestsTab, setAccountRequestsTab] = useState(0)
  const [filterTabs, setFilterTabs] = useState([])

  async function getFolders() {
    try {
      const response = await service.getEstateMattersFolder({
        "type": 2
      })
      return response?.data?._data
    } catch (e) {
      return e
    }
  }

  function selectTab(indexValue: number, item: any) {
    setActiveTable(indexValue)
    setActiveDoc(item)
  }

  useEffect(() => {
    getFolders().then((res) => {
      setFilterTabs(res)

      if (activeDoc === null) {
        selectTab(0, res[0])
      }
    })
  }, [])

  return (
    <Context.Provider
      value={{
        activeDoc,
        activeTable,
        accountRequestsTab,
        docsFilters,
        filterTabs,
        setDocsFilter,
        setAccountRequestsTab,
        setActiveTable,
        setActiveDoc,
        selectTab
      }}
    >
      {children}
    </Context.Provider>
  )
}

export default Provider
