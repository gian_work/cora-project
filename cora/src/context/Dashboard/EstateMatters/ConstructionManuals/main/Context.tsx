import React from "react"

export type CtxType = {
  activeTable: number
  accountRequestsTab: number
  docsFilters: any
  activeDoc: any
  filterTabs: any
  setActiveTable: Function
  setAccountRequestsTab: Function
  setDocsFilter: Function
  setActiveDoc: Function
  selectTab: Function
}

export const Context = React.createContext<Partial<CtxType>>({})
