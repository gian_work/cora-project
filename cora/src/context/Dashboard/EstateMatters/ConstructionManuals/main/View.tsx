import React from "react"
import Box from "@material-ui/core/Box"

/** Components */
import WithHeader from "layout/WithHeader"
import WithTable from "layout/WithTable"
import PageHeaderTitle from "components/PageHeaderTitle"
import Table from "pages/Dashboard/EstateMatters/ConstructionManuals/tables"
import Filters from "pages/Dashboard/EstateMatters/ConstructionManuals/Filters"
import TabsFilter from "components/Table/components/TabsFilter/withRef"
import TableHeader from "components/TableHeader"

import { filterDocs } from "config/Dashboard/EstateMatters"
import IconForm from "components/Icons/Documents/Forms"

import { CtxType } from "./Context"
import withContext from "./withContext"

const PublicDocuments: React.FC<CtxType> = ({
  accountRequestsTab,
  setAccountRequestsTab,
  activeDoc
}) => {
  React.useEffect(() => {
    window.scrollTo(0, 0)
  }, [])
  const MainView = () => {
    return (
      <WithHeader>
        <Box>
          <Box display="flex" justifyContent="space-between">
            <PageHeaderTitle
              title="Construction & Manuals"
              breadcrumbs="property management / Estate matters / construction & manuals"
            />
          </Box>

          <Box display="flex" marginBottom="15px">
            <Filters />
          </Box>

          <Box>
            <WithTable>
              <TableHeader
                addActionLabel="Add New"
                title={activeDoc?.title}
                titleIcon={IconForm}
                addAction={() => null}
              />
              <Box paddingLeft="90px" borderBottom="1px solid #F2F2F2">
                <TabsFilter
                  value={accountRequestsTab}
                  handleChange={setAccountRequestsTab}
                  options={filterDocs}
                />
              </Box>
              <Box>
                <Table />
              </Box>
            </WithTable>
          </Box>
        </Box>
      </WithHeader>
    )
  }

  return (
    <>
      <div>{MainView()}</div>
    </>
  )
}

export default withContext(PublicDocuments)
