import React from "react"

export type CtxType = {
  handleDocument: Function
  handleImageUpload: Function
  setOpenDialog: Function
  setDocDetails: Function
  setFiles: Function
  handleFileUpload: Function
  removeFile: Function
  setUploadError: Function
  sendBack: Function
  uploadError: boolean
  fileAttaching: boolean
  openDialog: boolean
  files: any
  docDetails: any
  filterTabs: any
}

export const Context = React.createContext<Partial<CtxType>>({})
