import React, { useState } from "react"
import { toast } from "react-toastify"
import Cookie from "js-cookie"

/** Services */
import service from "services/Dashboard/EstateMatters"

import { UploadImage } from "utils/uploadFile"

/** Context */
import { Context as ContextMain } from "context/Dashboard/EstateMatters/ManagementDocuments/main/Context"
import { toUnix } from "utils/date"
import { Context } from "./Context"

/** Utils */

export interface ProviderProps {
  data?: Record<string, any>
  sendBack: Function
}

const Provider: React.FC<ProviderProps> = ({ children, data, sendBack }) => {
  const { filterTabs } = React.useContext(ContextMain)
  const hasData = data !== undefined

  const [uploadError, setUploadError] = useState(false)
  const [openDialog, setOpenDialog] = useState(false)
  const [fileAttaching, setFileAttaching] = useState(false)
  const [files, setFiles] = useState<any>(
    hasData ? [data?.file] : data?.file || Array
  )
  const [docDetails, setDocDetails] = useState({
    condoUID: data?.condo_uid || "",
    title: data?.title || "",
    description: data?.description || "",
    startDate: data?.start_date || "",
    endDate: data?.end_date || "",
    file: data?.file || {},
    estateMatterFolder: data?.estate_matter_folder_uid || filterTabs[0]?._uid
  })

  /** Notification */
  const notifyCreate = () =>
    toast(`Successfully ${hasData ? "updated" : "created"} the document.`, {
      type: toast.TYPE.SUCCESS
    })

  /** Methods */
  const handleDocument = async () => {
    const dd = docDetails
    const payload = {
      "condo_uid": Cookie.get("condoUID"),
      "title": dd?.title,
      "description": dd?.description,
      "start_date": toUnix(dd?.startDate),
      "end_date": toUnix(dd?.endDate),
      "file": {
        ...files[0],
        "file_type": 2
      },
      "estate_matter_folder_uid": dd?.estateMatterFolder
    }

    const endpoint = hasData
      ? service.updateEstateMatter(payload, data?._uid)
      : service.createEstateMatter(payload)

    try {
      const response = await endpoint
      notifyCreate()
      return response?.data?._data
    } catch (e) {
      return e
    }
  }

  function handleFileUpload(event: any) {
    setUploadError(false)
    setFileAttaching(true)
    UploadImage(event, 2)
      .then((output: any) => {
        setFiles([...files, output])
        setFileAttaching(false)
      })
      .catch(() => setFileAttaching(false))
  }

  const removeFile = (value: number) => {
    const fileFiltered = files.filter((item: any, index: number) => {
      return index !== value
    })
    setFiles(fileFiltered)
  }

  return (
    <Context.Provider
      value={{
        handleDocument,
        setOpenDialog,
        setDocDetails,
        setFiles,
        handleFileUpload,
        removeFile,
        setUploadError,
        sendBack,
        uploadError,
        docDetails,
        fileAttaching,
        openDialog,
        files,
        filterTabs
      }}
    >
      {children}
    </Context.Provider>
  )
}

export default Provider
