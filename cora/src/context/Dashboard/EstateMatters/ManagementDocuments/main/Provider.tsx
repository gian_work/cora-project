import React, { useState, useEffect } from "react"
import { toast } from "react-toastify"

/** Context */
import service from "services/Dashboard/EstateMatters"
import { Context } from "./Context"

/** Service */

const Provider: React.FC = ({ children }) => {
  const [docsFilters, setDocsFilter] = useState([])
  const [activeDoc, setActiveDoc] = useState(null)
  const [activeTable, setActiveTable] = useState(0)
  const [accountRequestsTab, setAccountRequestsTab] = useState(0)
  const [filterTabs, setFilterTabs] = useState([])
  const [activeData, setActiveData] = useState([])
  const [showDocument, setShowDocument] = useState(false)

  /** Notification */
  const notifyDelete = () =>
    toast(`Successfully deleted the document.`, {
      type: toast.TYPE.SUCCESS
    })

  async function getFolders() {
    try {
      const response = await service.getEstateMattersFolder({
        "type": 3
      })
      return response?.data?._data
    } catch (e) {
      return e
    }
  }

  async function deleteDocument(uid: string) {
    try {
      const response = await service.deleteEstateMatter(uid)
      notifyDelete()
      return response?.data?._data
    } catch (e) {
      return e
    }
  }

  function selectTab(indexValue: number, item: any) {
    setActiveTable(indexValue)
    setActiveDoc(item)
  }

  useEffect(() => {
    getFolders().then((res) => {
      setFilterTabs(res)

      if (activeDoc === null) {
        selectTab(0, res[0])
      }
    })
  }, [])

  return (
    <Context.Provider
      value={{
        activeDoc,
        activeTable,
        accountRequestsTab,
        docsFilters,
        filterTabs,
        activeData,
        showDocument,
        setShowDocument,
        setActiveData,
        setDocsFilter,
        setAccountRequestsTab,
        setActiveTable,
        setActiveDoc,
        selectTab,
        deleteDocument
      }}
    >
      {children}
    </Context.Provider>
  )
}

export default Provider
