import React from "react"
import Box from "@material-ui/core/Box"
import Card from "@material-ui/core/Card"
import { useMachine } from "@xstate/react"

/** Component */
import Dialog from "components/Dialog"
import Footer from "components/Forms/Footer/new"
import DocumentUpdate from "pages/Dashboard/EstateMatters/PublicDocuments/components/DocumentFormUpdate/components/DocumentUpdate"

/** Machine */
import { CreatePublicDocumentsMachine } from "machines/Dashboard/EstateMatters/createPublicDocuments"

/** Context */
import withContext from "./withContext"
import { CtxType } from "./Context"

import styles from "./styles"

/** Refs */
export const refSubmit = React.createRef<HTMLDivElement>()

const CreateOwnerView: React.FC<CtxType> = ({
  openDialog,
  setOpenDialog,
  docDetails,
  handleDocument,
  sendBack,
  files
}) => {
  const { docTitle } = styles()
  const [current] = useMachine(CreatePublicDocumentsMachine)
  const xValue = current.value
  const dd = docDetails

  const handleNext = () => {
    if (
      xValue === "documentInfo" &&
      dd?.title !== "" &&
      dd?.remarks !== "" &&
      dd?.startDate !== "" &&
      dd?.endDate !== "" &&
      files[0] !== undefined
    ) {
      handleDocument().then(() => sendBack())
    } else {
      refSubmit.current?.click()
    }
  }

  const ViewDocumentUpdate = (): JSX.Element => {
    return <DocumentUpdate />
  }

  return (
    <>
      <Card>
        <Box>
          <Box borderBottom="1px solid #F2F2F2" className={docTitle}>
            {`S/No. ${dd?.refNo} ${dd?.title}`}
          </Box>
          <Box>{ViewDocumentUpdate()}</Box>
          <Box margin="0 0 30px 0">
            <Footer
              handleNext={() => handleNext()}
              handleBack={null}
              handleCancel={() => setOpenDialog && setOpenDialog(true)}
              label="Next"
            />
          </Box>
        </Box>
        <Dialog
          action={() => sendBack && sendBack()}
          isOpen={openDialog}
          setOpen={setOpenDialog}
          actionLabel="OK"
          title=""
          message="Are you sure you want to cancel?"
        />
      </Card>
    </>
  )
}

export default withContext(CreateOwnerView)
