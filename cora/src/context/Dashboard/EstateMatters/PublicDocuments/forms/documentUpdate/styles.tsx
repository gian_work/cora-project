import { makeStyles, Theme } from "@material-ui/core/styles"

const styles = makeStyles((theme: Theme) => ({
  docTitle: {
    color: theme.palette.secondary.dark,
    fontSize: "20px",
    fontWeight: 500,
    padding: "30px 60px 20px 60px"
  }
}))

export default styles
