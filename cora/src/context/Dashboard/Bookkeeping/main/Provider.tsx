import React, { useState } from "react"
import moment from "moment"

/** Context */
import dateHelper from "utils/date"
import { Context } from "./Context"

/** Utils */

const Provider: React.FC = ({ children }) => {
  /** States */
  const [startDateState, setstartDateState] = useState(moment().startOf("week"))
  const [endDateState, setendDateState] = useState(moment().endOf("week"))
  const [focusedInput, setfocusedInput] = useState("startDate")
  const [isopenModal, setisopenModal] = useState(false)
  // const [openReceipt, setOpenReceipt] = useState(false)
  const [activeTable, setActiveTable] = useState(0)

  // console.log(startDateState !== null && startDateState.startOf("week"))

  /** Methods */
  const onFocusChange = (value: any) => {
    setfocusedInput(value || "startDate")
  }

  const handleDateRange = ({ startDate, endDate }: any) => {
    setstartDateState(startDate)
    setendDateState(endDate)
  }

  const filterActiveTable = (value: number) => {
    setActiveTable(value)
  }

  function dateValue(start: any, end: any) {
    return {
      first: dateHelper.displayDate(start == null ? {} : start),
      last: dateHelper.displayDate(end == null ? {} : end)
    }
  }

  return (
    <Context.Provider
      value={{
        startDateState,
        endDateState,
        focusedInput,
        isopenModal,
        activeTable,
        filterActiveTable,
        setisopenModal,
        onFocusChange,
        handleDateRange,
        dateValue
      }}
    >
      {children}
    </Context.Provider>
  )
}

export default Provider
