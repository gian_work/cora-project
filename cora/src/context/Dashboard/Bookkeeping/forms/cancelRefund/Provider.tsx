import React, { useState } from "react"
import { toast } from "react-toastify"

/** Services */
import BookkeepingAPI from "services/Dashboard/Bookkeeping"

/** Context */
import { Context } from "./Context"

/** Interface */
export interface CancelRefundProviderProps {
  showPayment: boolean
  setShowPayment: Function
  data: Record<string, any>
}

const CancelRefundProvider: React.FC<CancelRefundProviderProps> = (props: any) => {
  /** Props */
  const { children, data, showPayment, setShowPayment } = props

  const [openDialog, setOpenDialog] = useState(false)
  const [paymentDetails, setPaymentDetails] = useState({
    receiptNo: data?.txn_id,
    unitUID: data?.unit.short_name,
    payeeAccountUID: data?.payee_account_uid,
    payeeName: data?.payee_name,
    description: data?.description,
    category: data?.category,
    paymentType: data?.payment_type,
    amt: data?.amt,
    tax: data?.tax,
    remarks: data?.admin_remarks,
    dueDate: data?.payment_due_date,
    addedBy: data?.added_by_account_uid,
    paymentMethod: "1",
    paymentDate: data?._created_at,
    paymentReference: "",
    ePaymentAgent: "",
    photos: data?.payment_photo.photos
  })

  /** Notification */
  const notifyCreate = () =>
    toast("Successfully cancelled the refund.", {
      type: toast.TYPE.SUCCESS
    })

  /** Methods */
  const handleCancelRefund = async () => {
    const payload = {
      "_uid": data?._uid,
      "cancelled_remarks": paymentDetails.remarks
    }

    try {
      const response = await BookkeepingAPI.cancelPayment(payload).then(() =>
        notifyCreate()
      )
      return response
    } catch (e) {
      return e
    }
  }

  return (
    <Context.Provider
      value={{
        handleCancelRefund,
        setOpenDialog,
        setPaymentDetails,
        openDialog,
        paymentDetails,
        showPayment,
        setShowPayment,
        data
      }}
    >
      {children}
    </Context.Provider>
  )
}

export default CancelRefundProvider
