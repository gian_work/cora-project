import React from "react"

export type CtxType = {
  paymentDetails: Record<string, any>
  openDialog: boolean
  setOpenDialog: Function
  setPaymentDetails: Function
  handleEditPayment: Function
  showPayment: boolean
  setShowPayment: Function
}

export const Context = React.createContext<Partial<CtxType>>({})
