import React, { Suspense } from "react"
import Box from "@material-ui/core/Box"
import Card from "@material-ui/core/Card"

/** Layout */
import WithHeader from "layout/WithHeader"

/** Component */
import Loader from "components/Loader"
import PageHeaderTitle from "components/PageHeaderTitle"
import Dialog from "components/Dialog"
import Form from "pages/Dashboard/Bookkeeping/components/AddPayment/Form"

/** Context */
import { CtxType } from "./Context"
import withContext from "./withContext"

const CreateOwnerView: React.FC<CtxType> = ({
  openDialog,
  setOpenDialog,
  setShowPayment
}) => {
  React.useEffect(() => {
    window.scrollTo(0, 0)
  }, [])
  return (
    <>
      <WithHeader>
        <PageHeaderTitle
          title="Create New Payment"
          breadcrumbs="property management / bookkeeping / new payment"
          backAction={() => setShowPayment(false)}
        />
        <Card>
          <Box>
            <Suspense fallback={<Loader forContent />}>
              <Form />
            </Suspense>
          </Box>
          <Dialog
            action={() => setShowPayment(false)}
            isOpen={openDialog}
            setOpen={setOpenDialog}
            actionLabel="OK"
            title=""
            message="Are you sure you want to cancel?"
          />
        </Card>
      </WithHeader>
    </>
  )
}

export default withContext(CreateOwnerView)
