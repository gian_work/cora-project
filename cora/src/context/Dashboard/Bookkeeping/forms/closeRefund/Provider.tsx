import React, { useState } from "react"
import { toast } from "react-toastify"

/** Services */
import BookkeepingAPI from "services/Dashboard/Bookkeeping"

/** Context */
import { Context } from "./Context"

/** Interface */
export interface RefundProviderProps {
  showPayment: boolean
  setShowPayment: Function
  data: Record<string, any>
}

const RefundProvider: React.FC<RefundProviderProps> = (props: any) => {
  /** Props */
  const { children, data, showPayment, setShowPayment } = props
  const [openDialog, setOpenDialog] = useState(false)
  const [paymentDetails, setPaymentDetails] = useState({
    unitUID: data?.unit_uid,
    payeeID: data?.payee_account_id,
    payeeUID: data?.payee_name,
    description: data?.description,
    remarks: data?.admin_remarks,
    paymentType: data?.payment_type,
    category: data?.category,
    amt: data?.amt,
    tax: data?.tax,
    refundTo: "1",
    closeRemarks: "",
    flags: data?.flags,
    unitName: data?.unit?.short_name,
    receiptNo: data?.txn_id,
    dueDate: data?.payment_due_date,
    addedBy: data?.added_by_account_uid,
    paymentMethod: "1",
    paymentDate: data?._created_at,
    paymentReference: "",
    ePaymentAgent: "",
    photos: data?.payment_photo.photos,
    paymentStatus: data?.payment_status
  })

  /** Notification */
  const notifyCreate = () =>
    toast("Successfully refunded the payment.", {
      type: toast.TYPE.SUCCESS
    })

  /** Methods */
  const handleRefund = async () => {
    const pd = paymentDetails
    const payload = {
      "_uid": data?._uid,
      "amt": pd?.amt,
      "tax": pd?.tax,
      "refund_policy": +pd?.refundTo,
      "closed_remarks": pd?.remarks
    }

    try {
      const response = await BookkeepingAPI.refundClose(payload).then(() =>
        notifyCreate()
      )
      return response
    } catch (e) {
      return e
    }
  }

  const handleRefundPayment = async () => {
    const pd = paymentDetails
    const payload = {
      "payment_uid": data?._uid,
      "amt": pd?.amt,
      "tax": pd?.tax,
      "refund_policy": +pd?.refundTo,
      "closed_remarks": pd?.remarks
    }

    try {
      const response = await BookkeepingAPI.refundPayment(payload).then(() =>
        notifyCreate()
      )
      return response
    } catch (e) {
      return e
    }
  }

  const handleRefundBalance = async () => {
    const pd = paymentDetails
    const payload = {
      "_uid": data?._uid,
      "closed_remarks": pd?.remarks
    }

    try {
      const response = await BookkeepingAPI.refundBalance(payload).then(() =>
        notifyCreate()
      )
      return response
    } catch (e) {
      return e
    }
  }

  return (
    <Context.Provider
      value={{
        handleRefund,
        handleRefundPayment,
        handleRefundBalance,
        setOpenDialog,
        setPaymentDetails,
        openDialog,
        paymentDetails,
        showPayment,
        setShowPayment,
        data
      }}
    >
      {children}
    </Context.Provider>
  )
}

export default RefundProvider
