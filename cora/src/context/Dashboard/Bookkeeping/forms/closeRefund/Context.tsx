import React from "react"

export type CtxType = {
  paymentDetails: Record<string, any>
  data: Record<string, any>
  openDialog: boolean
  showPayment: boolean
  setOpenDialog: Function
  setPaymentDetails: Function
  handleRefund: Function
  setShowPayment: Function
  handleRefundPayment: Function
  handleRefundBalance: Function
}

export const Context = React.createContext<Partial<CtxType>>({})
