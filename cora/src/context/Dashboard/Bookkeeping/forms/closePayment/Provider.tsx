import React, { useState } from "react"
import { toast } from "react-toastify"

/** Services */
import BookkeepingAPI from "services/Dashboard/Bookkeeping"

/** Context */
import { Context } from "./Context"

/** Interface */
export interface ClosePaymentProps {
  showPayment: boolean
  setShowPayment: Function
  data: Record<string, any>
}

const ClosePaymentProvider: React.FC<ClosePaymentProps> = (props: any) => {
  /** Props */
  const { children, data, showPayment, setShowPayment } = props
  const [openDialog, setOpenDialog] = useState(false)
  const [paymentDetails, setPaymentDetails] = useState({
    receiptNo: data?.txn_id,
    unitUID: data?.unit.short_name,
    payeeAccountUID: data?.payee_account_uid,
    payeeName: data?.payee_name,
    description: data?.description,
    category: data?.category,
    paymentType: data?.payment_type,
    amt: data?.amt,
    tax: data?.tax,
    remarks: data?.admin_remarks,
    dueDate: data?.payment_due_date,
    addedBy: data?.added_by_account_uid,
    paymentMethod: +data?.payment_method || "1",
    paymentDate: data?._created_at,
    paymentReference: "",
    ePaymentAgent: "",
    photos: data?.payment_photo.photos,
    paymentStatus: +data?.payment_status
  })

  /** Notification */
  const notifyCreate = () =>
    toast("Successfully closed the payment.", {
      type: toast.TYPE.SUCCESS
    })

  /** Methods */
  const handleClosePayment = async () => {
    const payload = {
      "_uid": data?._uid,
      "payment_method": +paymentDetails?.paymentMethod,
      "payment_reference": paymentDetails?.paymentReference,
      "e_payment_agent": paymentDetails?.ePaymentAgent,
      "closed_remarks": paymentDetails?.remarks
    }

    /** Reference
     * payment_reference": cheque no. or transfer id "e_payment_agent": bank name or transfer agent
     */

    try {
      const response = await BookkeepingAPI.closePayment(payload).then(() =>
        notifyCreate()
      )
      return response
    } catch (e) {
      return e
    }
  }

  return (
    <Context.Provider
      value={{
        handleClosePayment,
        openDialog,
        setOpenDialog,
        paymentDetails,
        setPaymentDetails,
        showPayment,
        setShowPayment,
        data
      }}
    >
      {children}
    </Context.Provider>
  )
}

export default ClosePaymentProvider
