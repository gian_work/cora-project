import React from "react"

export type CtxType = {
  paymentDetails: Record<string, any>
  data: Record<string, any>
  openDialog: boolean
  showPayment: boolean
  setOpenDialog: Function
  setPaymentDetails: Function
  handleClosePayment: Function
  setShowPayment: Function
}

export const Context = React.createContext<Partial<CtxType>>({})
