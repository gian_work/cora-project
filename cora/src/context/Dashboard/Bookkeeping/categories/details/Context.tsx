import React from "react"

export type CtxType = {
  showPayment: boolean
  setShowPayment: Function
  data: Record<string, any>
}

export const Context = React.createContext<Partial<CtxType>>({})
