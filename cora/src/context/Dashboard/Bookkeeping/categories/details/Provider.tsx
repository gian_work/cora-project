import React from "react"
// import {useSelector} from "react-redux"

/** Context */
import { Context } from "./Context"

/** Interface */
export interface DetailsProviderProps {
  showPayment: boolean
  setShowPayment: Function
  data: Record<string, any>
}

const DetailsProvider: React.FC<DetailsProviderProps> = ({
  children,
  showPayment,
  setShowPayment,
  data
}) => {
  return (
    <Context.Provider
      value={{
        showPayment,
        setShowPayment,
        data
      }}
    >
      {children}
    </Context.Provider>
  )
}

export default DetailsProvider
