import React from "react"
import Box from "@material-ui/core/Box"
import Card from "@material-ui/core/Card"
/** Layout */
import WithHeader from "layout/WithHeader"
/** Component */
import PageHeaderTitle from "components/PageHeaderTitle"
import Summary from "pages/Dashboard/Bookkeeping/components/DetailsPayment/Summary"
/** Context */
import { CtxType } from "./Context"
import withContext from "./withContext"

const DetailsView: React.FC<CtxType> = ({ setShowPayment }) => {
  return (
    <WithHeader>
      <PageHeaderTitle
        title="Payment Details"
        breadcrumbs="property management / bookkeeping / payment details"
        backAction={() => setShowPayment()}
      />
      <Card>
        <Box>
          <Summary />
        </Box>
      </Card>
    </WithHeader>
  )
}

export default withContext(DetailsView)
