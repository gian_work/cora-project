import React, { useState } from "react"

/** Context */
import { Context } from "./Context"

const Provider: React.FC = ({ children }) => {
  const [activeData, setActiveData] = useState()

  return (
    <Context.Provider
      value={{
        activeData,
        setActiveData
      }}
    >
      {children}
    </Context.Provider>
  )
}

export default Provider
