import React, { useState, useContext } from "react"
// import Cookie from "js-cookie"
import { toast } from "react-toastify"

/** Context */
import { Context as RootContext } from "context/Settings/PropertyManagement/Applications/main/Context"

/** Service */
import service from "services/Settings/PropertyManagement/Applications"

/** Utils */
import { formatTime } from "utils/date"
import { Context } from "./Context"

/** Interface */
export interface ProviderProps {
  data?: Record<string, any>
  sendBack: Function
}

const Provider: React.FC<ProviderProps> = ({ children, sendBack }) => {
  const { activeData } = useContext(RootContext)
  const hasData = activeData !== undefined

  const dateString = (timeString: string) => {
    return `Thu May 07 2020 ${timeString} GMT+0800 (Philippine Standard Time)`
  }

  const [applicationDetails, setApplicationDetails] = useState({
    name: activeData?.application_type_name || "",
    daysAdvanceBooking: activeData?.advance_booking_days || "",
    daysCancelBooking: activeData?.cancel_booking_days || "",
    allowOnline: activeData?.allow_online_application || false
  })
  const [weekdayTime, setWeekdayTime] = useState({
    weekdayStartTime:
      dateString(activeData?.weekday_time_slots[0]?.start_time) || "",
    weekdayEndTime:
      activeData?.weekday_time_slots[0]?.end_time !== null
        ? dateString(activeData?.weekday_time_slots[0]?.end_time)
        : ""
  })
  const [saturdayTime, setSaturdayTime] = useState({
    saturdayStartTime:
      dateString(activeData?.saturday_time_slots[0]?.start_time) || "",
    saturdayEndTime:
      activeData?.saturday_time_slots[0]?.end_time !== null
        ? dateString(activeData?.saturday_time_slots[0]?.end_time)
        : ""
  })
  const [holidayTime, setHolidayTime] = useState({
    holidayStartTime:
      dateString(activeData?.holiday_time_slots[0]?.start_time) || "",
    holidayEndTime:
      activeData?.holiday_time_slots[0]?.end_time !== null
        ? dateString(activeData?.holiday_time_slots[0]?.end_time)
        : ""
  })

  const payload = {
    "advance_booking_days": Number(applicationDetails?.daysAdvanceBooking),
    "cancel_booking_days": Number(applicationDetails?.daysCancelBooking),
    "allow_online_application": applicationDetails?.allowOnline,
    "weekday_time_slots": [
      {
        "start_time": `${formatTime(weekdayTime?.weekdayStartTime)}:00`,
        "end_time": `${
          weekdayTime?.weekdayEndTime !== null
            ? `${formatTime(weekdayTime?.weekdayEndTime)}:00`
            : ""
        }`
      }
    ],
    "saturday_time_slots": [
      {
        "start_time": `${formatTime(saturdayTime?.saturdayStartTime)}:00`,
        "end_time": `${
          weekdayTime?.weekdayEndTime !== null
            ? `${formatTime(saturdayTime?.saturdayEndTime)}:00`
            : ""
        }`
      }
    ],
    "holiday_time_slots": [
      {
        "start_time": `${formatTime(holidayTime?.holidayStartTime)}:00`,
        "end_time": `${
          weekdayTime?.weekdayEndTime !== null
            ? `${formatTime(holidayTime?.holidayEndTime)}:00`
            : ""
        }`
      }
    ]
  }

  /** Notification */
  const notifySuccess = (message: string) =>
    toast(`Successfully ${message} application category.`, {
      type: toast.TYPE.SUCCESS
    })

  const createCategory = async () => {
    /** 1: create; 2: update */
    const serviceCall = !hasData
      ? service.createApplication(payload)
      : service.updateApplication(payload, activeData?._uid)

    try {
      const response = await serviceCall
      notifySuccess(!hasData ? "created" : "updated")
      return response
    } catch (e) {
      return e
    }
  }

  return (
    <Context.Provider
      value={{
        applicationDetails,
        weekdayTime,
        saturdayTime,
        holidayTime,
        sendBack,
        setApplicationDetails,
        setWeekdayTime,
        setSaturdayTime,
        setHolidayTime,
        createCategory
      }}
    >
      {children}
    </Context.Provider>
  )
}

export default Provider
