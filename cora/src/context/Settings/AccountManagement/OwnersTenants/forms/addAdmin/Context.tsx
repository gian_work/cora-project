import React from "react"

export type CtxType = {}

export const Context = React.createContext<Partial<CtxType>>({})
