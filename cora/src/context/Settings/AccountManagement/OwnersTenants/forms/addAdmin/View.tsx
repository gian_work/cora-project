import React from "react"
import Box from "@material-ui/core/Box"

/** Context */
import withContext from "context/Settings/AccountManagement/Admin/forms/admin/withContext"
import { CtxType } from "context/Settings/AccountManagement/Admin/forms/admin/Context"

const AddAdminView: React.FC<CtxType> = () => {
  const MainView = (): JSX.Element => {
    return (
      <Box>
        <h1>Add Admin</h1>
      </Box>
    )
  }

  return <>{MainView()}</>
}

export default withContext(AddAdminView)
