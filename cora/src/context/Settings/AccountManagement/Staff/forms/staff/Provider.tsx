import React, { FunctionComponent, useState, useEffect, useContext } from "react"
import { toast } from "react-toastify"
import { mutate } from "swr"

/** Context */
import service from "services/Settings/AccountManagement/Staff"
import { Context as MainContext } from "context/Settings/AccountManagement/Staff/main/Context"
import { Context } from "./Context"

/** Service */

export interface Props {
  data?: any
}

const Provider: FunctionComponent<Props> = ({ data, children }) => {
  const hasData = Object.keys(data).length !== 0
  const { setShowForm } = useContext(MainContext)

  const [staffDetails, setStaffDetails] = useState({
    staffName: data?.account_name || "",
    staffEmail: data?.account_email || "",
    staffMobile: data?.account_mobile_no || "",
    methodType: "EMAIL_PASSWORD",
    staffPassword: "",
    role: data?.condo?.user_role?._uid || ""
  })

  useEffect(() => {
    if (hasData) {
      setStaffDetails({
        staffName: data?.account_name || "",
        staffEmail: data?.account_email || "",
        staffMobile: data?.account_mobile_no || "",
        methodType: "EMAIL_PASSWORD",
        staffPassword: "",
        role: data?.condo?.user_role?._uid || ""
      })
    }
  }, [data])

  /**
   * Notification
   */
  const notifyCreate = (adminName: string, action: number) =>
    toast(`Successfully ${action === 1 ? "created" : "updated"} ${adminName}.`, {
      type: toast.TYPE.SUCCESS
    })

  /** Method */
  async function handleFormSubmit() {
    const sd = staffDetails
    const payload = {
      "name": sd?.staffName,
      "email": sd?.staffEmail,
      "mobile_no": sd?.staffMobile,
      "method_type": "EMAIL_PASSWORD",
      "user_role_uid": sd?.role,
      "account_username": sd?.staffEmail,
      "account_pw": sd?.staffPassword
    }

    const updatePayload = {
      "name": sd?.staffName,
      "email": sd?.staffEmail,
      "mobile_no": sd?.staffMobile,
      "user_role_uid": sd?.role
    }

    const endpoint = hasData
      ? service.updateStaffUser(updatePayload, data?.account_uid)
      : service.createStaffUser(payload)

    try {
      const response = await endpoint
      notifyCreate(sd?.staffName, hasData ? 2 : 1)
      setShowForm && setShowForm(false)
      mutate("fetchSettingsStaff")
      return response
    } catch (e) {
      return e
    }
  }

  return (
    <Context.Provider
      value={{
        staffDetails,
        setStaffDetails,
        handleFormSubmit
      }}
    >
      {children}
    </Context.Provider>
  )
}

export default Provider
