import React from "react"

export type CtxType = {
  staffDetails: Record<string, any>
  setStaffDetails: Function
  handleFormSubmit: Function
}

export const Context = React.createContext<Partial<CtxType>>({})
