import React from "react"

export type CtxType = {
  formType: number
  activeRole: string
  activeRoleName: string
  setStaffPayload: Function
  setActiveRole: Function
  selectTab: Function
  setPayloads: Function
  setActiveRoleName: Function
  sendBack: Function
  setShowForm: Function
  showFormType: Function
  handleFormSubmit: Function
  setFormDetails: Function
  isFetchingRoles: boolean
  showForm: boolean
  submitting: boolean
  formDetails: any
  payloads: any
  tabValues: any
  staffPayload: any
}

export const Context = React.createContext<Partial<CtxType>>({})
