import React from "react"

export type CtxType = {
  activeData: Record<string, any>
  formType: number
  showForm: boolean
  setShowForm: Function
  setActiveData: Function
  showFormType: Function
}

export const Context = React.createContext<Partial<CtxType>>({})
