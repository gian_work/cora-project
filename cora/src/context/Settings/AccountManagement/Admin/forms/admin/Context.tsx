import React from "react"

export type CtxType = {
  adminDetails: Record<string, any>
  setAdminDetails: Function
  handleFormSubmit: Function
}

export const Context = React.createContext<Partial<CtxType>>({})
