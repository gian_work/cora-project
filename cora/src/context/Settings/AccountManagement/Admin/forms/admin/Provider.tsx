import React, { FunctionComponent, useState, useEffect, useContext } from "react"
import { toast } from "react-toastify"
import { mutate } from "swr"

/** Context */
import service from "services/Settings/AccountManagement/Admin"
import { Context as MainContext } from "context/Settings/AccountManagement/Admin/main/Context"
import { Context } from "./Context"

/** Service */

export interface Props {
  data?: any
}

const Provider: FunctionComponent<Props> = ({ data, children }) => {
  const hasData = Object.keys(data).length !== 0
  const { setShowForm } = useContext(MainContext)

  const [adminDetails, setAdminDetails] = useState({
    adminName: data?.account_name || "",
    adminEmail: data?.account_email || "",
    adminMobile: data?.account_mobile_no || "",
    methodType: "EMAIL_PASSWORD",
    adminPassword: "",
    role: data?.condo?.user_role?._uid || ""
  })

  useEffect(() => {
    if (hasData) {
      setAdminDetails({
        adminName: data?.account_name || "",
        adminEmail: data?.account_email || "",
        adminMobile: data?.account_mobile_no || "",
        methodType: "EMAIL_PASSWORD",
        adminPassword: "",
        role: data?.condo?.user_role?._uid || ""
      })
    }
  }, [data])

  /**
   * Notification
   */
  const notifyCreate = (adminName: string, action: number) =>
    toast(`Successfully ${action === 1 ? "created" : "updated"} ${adminName}.`, {
      type: toast.TYPE.SUCCESS
    })

  /** Method */
  async function handleFormSubmit() {
    const ad = adminDetails
    const payload = {
      "name": ad?.adminName,
      "email": ad?.adminEmail,
      "mobile_no": ad?.adminMobile,
      "method_type": "EMAIL_PASSWORD",
      "user_role_uid": ad?.role,
      "account_username": ad?.adminEmail,
      "account_pw": ad?.adminPassword
    }

    const updatePayload = {
      "name": ad?.adminName,
      "email": ad?.adminEmail,
      "mobile_no": ad?.adminMobile,
      "user_role_uid": ad?.role
    }

    const endpoint = hasData
      ? service.updateAdminUser(updatePayload, data?.account_uid)
      : service.createAdminUser(payload)

    try {
      const response = await endpoint
      notifyCreate(ad?.adminName, hasData ? 2 : 1)
      setShowForm && setShowForm(false)
      mutate("fetchSettingsAdmin")
      return response
    } catch (e) {
      return e
    }
  }

  return (
    <Context.Provider
      value={{
        adminDetails,
        setAdminDetails,
        handleFormSubmit
      }}
    >
      {children}
    </Context.Provider>
  )
}

export default Provider
