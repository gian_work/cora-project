import React, { useContext } from "react"
import Box from "@material-ui/core/Box"

import AdminForm from "pages/Settings/AccountManagement/Admin/components/Form/main"

/** Context */
import { Context as MainContext } from "context/Settings/AccountManagement/Admin/main/Context"
import withContext from "context/Settings/AccountManagement/Admin/forms/admin/withContext"
import { CtxType } from "context/Settings/AccountManagement/Admin/forms/admin/Context"

const AddAdminView: React.FC<CtxType> = () => {
  const { showForm, setShowForm, setActiveData } = useContext(MainContext)

  const MainView = (): JSX.Element => {
    return (
      <Box>
        <AdminForm
          openState={showForm || false}
          setopenState={() => {
            setShowForm && setShowForm(false)
            setActiveData && setActiveData({})
          }}
        />
      </Box>
    )
  }

  return <>{MainView()}</>
}

export default withContext(AddAdminView)
