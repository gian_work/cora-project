import React, { useState } from "react"

/** Context */
import { Context } from "./Context"

const Provider: React.FC = ({ children }) => {
  const [showForm, setShowForm] = useState(false)
  const [activeData, setActiveData] = useState({})
  const [formType, setFormType] = useState(1)

  function showFormType(state: boolean, type: number) {
    setShowForm(state)
    setFormType(type)
  }

  return (
    <Context.Provider
      value={{
        showForm,
        activeData,
        setShowForm,
        setActiveData,
        showFormType,
        formType
      }}
    >
      {children}
    </Context.Provider>
  )
}

export default Provider
