import React, { useState, createContext } from "react"
import Cookies from "js-cookie"
import { useDispatch } from "react-redux"

/* Redux */
import { setUserDetails } from "redux/user/actions"

/* Service */
import OnboardAPI from "services/Onboard"

/* Context */
const AuthContext = createContext({})

const AuthProvider = ({ children }: any) => {
  const dispatch = useDispatch()
  const [isAuth, setIsAuth] = useState(!!Cookies.get("bearer"))
  const [isLoading, setIsLoading] = useState(false)

  async function login(loginDetails: any) {
    setIsLoading(true)

    if (loginDetails.email === "" || loginDetails.pw === "") {
      setIsLoading(false)
      return null
    }

    try {
      const response = await OnboardAPI.loginUser(loginDetails)
      setIsAuth(true)
      setIsLoading(false)
      dispatch(setUserDetails(response.data._data.user))

      Cookies.set("bearer", response.data._data.access_token)
      Cookies.set("condoUID", "95e16b2c-f377-474d-9b9e-c24f3b62e336")
      window.location.href = "/#/property-management/dashboards"
      return response
    } catch (e) {
      setIsLoading(false)
      return e
    }
  }

  function logout() {
    Cookies.remove("bearer")
    Cookies.remove("condoUID")
    setIsAuth(false)
    window.location.href = "/"
  }

  return (
    <AuthContext.Provider
      value={{
        isAuth,
        login,
        logout,
        isLoading,
        bearer: Cookies.get("bearer")
      }}
    >
      {children}
    </AuthContext.Provider>
  )
}

const AuthConsumer = AuthContext.Consumer

export { AuthProvider, AuthConsumer }
