import Moment from "moment"

export function convertDate(value: Date): string {
  const formattedDate = `${value?.getMonth()} ${value?.getDate()} ${value?.getFullYear()}`
  return formattedDate
}

export function displayDate(value: Date, format?: string): string {
  if (format === undefined) {
    return Moment(value).format("MM/DD/YYYY")
  }

  return Moment(value).format(format)
}

export function formatDate(value: Date): string {
  return Moment(value).format("YYYY-MM-DD")
}

export function formatTime(value: string): string {
  return Moment(value).format("HH:mm")
}

export function formatDateTime(value: Date): string {
  return Moment(value).format("YYYY-MM-DD HH:MM:SS")
}

export function toUnix(value: string): number {
  return Moment(value).valueOf()
}

export function toUnixDateOnly(value: string): number | string {
  const main = Moment(value).format("MMM DD YYYY")

  return Moment(main).valueOf()
}

export function toUnixTomorrow(value: string, numDays: number): number | string {
  return Moment(toUnixDateOnly(value))
    .add(numDays, "days")
    .valueOf()
}

export function fromUnix(value: Date): string {
  return Moment(value).format("MMM DD YYYY")
}

export function fromUnixTime(value: Date): string {
  return Moment(value).format("h:mm A")
}

export function diffDays(value: Date): number {
  const before = Moment(value).format("YYYY-MM-DD")
  const numDays = Moment().diff(before, "days")
  return numDays
}

const dateHelper = {
  displayDate,
  formatDate,
  formatDateTime,
  formatTime,
  toUnix,
  toUnixTomorrow,
  fromUnix,
  fromUnixTime,
  diffDays,
  toUnixDateOnly,
  convertDate
}

export default dateHelper
