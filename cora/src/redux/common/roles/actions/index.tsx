import { SET_USER_ROLES } from "../constants"

export const setUserRoles = (roles: any) => ({
  type: SET_USER_ROLES,
  roles
})
