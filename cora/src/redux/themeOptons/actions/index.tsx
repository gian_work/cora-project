import {
  SET_ENABLE_BACKGROUND_IMAGE,
  SET_ENABLE_MOBILE_MENU,
  SET_ENABLE_MOBILE_MENU_SMALL,
  SET_ENABLE_FIXED_HEADER,
  SET_ENABLE_HEADER_SHADOW,
  SET_ENABLE_SIDEBAR_SHADOW,
  SET_ENABLE_FIXED_SIDEBAR,
  SET_ENABLE_CLOSED_SIDEBAR,
  SET_ENABLE_FIXED_FOOTER,
  SET_ENABLE_PAGETITLE_ICON,
  SET_ENABLE_PAGETITLE_SUBHEADING,
  SET_ENABLE_PAGE_TABS_ALT,
  SET_BACKGROUND_IMAGE,
  SET_BACKGROUND_COLOR,
  SET_COLOR_SCHEME,
  SET_BACKGROUND_IMAGE_OPACITY,
  SET_HEADER_BACKGROUND_COLOR
} from "../constants"

export const setEnableBackgroundImage = (enableBackgroundImage: any) => ({
  type: SET_ENABLE_BACKGROUND_IMAGE,
  enableBackgroundImage
})

export const setEnableFixedHeader = (enableFixedHeader: boolean) => ({
  type: SET_ENABLE_FIXED_HEADER,
  enableFixedHeader
})

export const setEnableHeaderShadow = (enableHeaderShadow: boolean) => ({
  type: SET_ENABLE_HEADER_SHADOW,
  enableHeaderShadow
})

export const setEnableSidebarShadow = (enableSidebarShadow: boolean) => ({
  type: SET_ENABLE_SIDEBAR_SHADOW,
  enableSidebarShadow
})

export const setEnablePageTitleIcon = (enablePageTitleIcon: boolean) => ({
  type: SET_ENABLE_PAGETITLE_ICON,
  enablePageTitleIcon
})

export const setEnablePageTitleSubheading = (
  enablePageTitleSubheading: boolean
) => ({
  type: SET_ENABLE_PAGETITLE_SUBHEADING,
  enablePageTitleSubheading
})

export const setEnablePageTabsAlt = (enablePageTabsAlt: boolean) => ({
  type: SET_ENABLE_PAGE_TABS_ALT,
  enablePageTabsAlt
})

export const setEnableFixedSidebar = (enableFixedSidebar: boolean) => ({
  type: SET_ENABLE_FIXED_SIDEBAR,
  enableFixedSidebar
})

export const setEnableClosedSidebar = (enableClosedSidebar: boolean) => ({
  type: SET_ENABLE_CLOSED_SIDEBAR,
  enableClosedSidebar
})

export const setEnableMobileMenu = (enableMobileMenu: boolean) => ({
  type: SET_ENABLE_MOBILE_MENU,
  enableMobileMenu
})

export const setEnableMobileMenuSmall = (enableMobileMenuSmall: string) => ({
  type: SET_ENABLE_MOBILE_MENU_SMALL,
  enableMobileMenuSmall
})

export const setEnableFixedFooter = (enableFixedFooter: boolean) => ({
  type: SET_ENABLE_FIXED_FOOTER,
  enableFixedFooter
})

export const setBackgroundColor = (backgroundColor: string) => ({
  type: SET_BACKGROUND_COLOR,
  backgroundColor
})

export const setHeaderBackgroundColor = (headerBackgroundColor: string) => ({
  type: SET_HEADER_BACKGROUND_COLOR,
  headerBackgroundColor
})

export const setColorScheme = (colorScheme: string) => ({
  type: SET_COLOR_SCHEME,
  colorScheme
})

export const setBackgroundImageOpacity = (backgroundImageOpacity: string) => ({
  type: SET_BACKGROUND_IMAGE_OPACITY,
  backgroundImageOpacity
})

export const setBackgroundImage = (backgroundImage: Record<string, any>) => ({
  type: SET_BACKGROUND_IMAGE,
  backgroundImage
})
