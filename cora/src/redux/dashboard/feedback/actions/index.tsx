import { SET_FEEDBACK_TABLE, SET_TAB_STATUS } from "../constants"

export const setFeedbackTable = (table: any) => ({
  type: SET_FEEDBACK_TABLE,
  table
})

export const setTabStatus = (status: number) => ({
  type: SET_TAB_STATUS,
  status
})
