import { SET_VMS_TABLE } from "../constants"

export const setVMSTable = (table: any) => ({
  type: SET_VMS_TABLE,
  table
})
