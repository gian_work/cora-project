import { SET_PAYMENTS_TABLE } from "../constants"

export const setPaymentsTable = (table: any) => ({
  type: SET_PAYMENTS_TABLE,
  table
})
