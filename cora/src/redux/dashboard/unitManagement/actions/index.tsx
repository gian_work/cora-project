import { SET_TABLE_SCHEDULED_PAYMENT } from "../constants"

export const setPS = (table: any) => ({
  type: SET_TABLE_SCHEDULED_PAYMENT,
  table
})
