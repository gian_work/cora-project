import * as React from "react"
import Card from "@material-ui/core/Card"
import Box from "@material-ui/core/Box"

// interface
interface WithTableProps {
  children: any
}

const WithTable: React.FC<WithTableProps> = ({ children }) => {
  return (
    <Card>
      <Box>{children}</Box>
    </Card>
  )
}

export default WithTable
