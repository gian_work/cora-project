import React from "react"
import { fade, makeStyles, Theme } from "@material-ui/core/styles"
import AppBar from "@material-ui/core/AppBar"
import Toolbar from "@material-ui/core/Toolbar"
import Grid from "@material-ui/core/Grid"
import Box from "@material-ui/core/Box"
import Divider from "@material-ui/core/Divider"

import Logo from "assets/images/Cora_logo.png"
import HeaderDots from "./Components/HeaderDots"
import UserBox from "./Components/UserBox"

// context

// logo

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    flexGrow: 1
  },
  header: {
    backgroundColor: theme.palette.common.white,
    borderBottom: "1px solid #D5D5D5",
    boxShadow: "none"
  },
  menuButton: {
    marginRight: theme.spacing(2)
  },
  title: {
    flexGrow: 1
  },
  logoContainer: {
    display: "flex",
    alignItems: "center",
    height: "100%",
    backgroundColor: "#017c8d",
    minWidth: "270px",
    borderTopRightRadius: "68px",
    paddingLeft: "25px"
  },
  search: {
    position: "relative",
    // borderRadius: theme.shape.borderRadius,
    background: "#F9FAFC",
    border: "1px solid #E3E3E3",
    borderRadius: "24px",
    height: "30px",
    overflow: "hidden",
    backgroundColor: fade(theme.palette.common.white, 0.15),
    "&:hover": {
      backgroundColor: fade(theme.palette.common.white, 0.25)
    },
    marginRight: theme.spacing(2),
    marginLeft: theme.spacing(4),
    width: "100%",
    [theme.breakpoints.up("sm")]: {
      width: "auto"
    }
  },
  searchIcon: {
    width: theme.spacing(7),
    height: "100%",
    position: "absolute",
    pointerEvents: "none",
    display: "flex",
    alignItems: "center",
    justifyContent: "center"
  },
  inputRoot: {
    color: "inherit",
    height: "100%"
  },
  inputInput: {
    backgroundColor: "#F9FAFC",
    color: "#131336",
    fontSize: "14px",
    height: "100%",
    padding: "0 10px 0 30px",
    transition: theme.transitions.create("width"),
    width: "100%",
    [theme.breakpoints.up("sm")]: {
      width: 438,
      "&:focus": {
        width: 538
      }
    }
  },
  subheader: {
    // backgroundColor: "#2760A9",
    width: "100%",
    height: "60px",
    marginTop: "64px",
    position: "fixed",
    zIndex: 9,
    display: "flex",
    alignItems: "center"
  },
  clientReservedBox: {
    backgroundColor: theme.palette.sidebar.main,
    width: "270px",
    height: "100%"
  },
  condoName: {
    color: theme.palette.body.main,
    fontSize: "20px",
    fontWeight: 600
  }
}))

const Header: React.FC = () => {
  const { condoName, header, logoContainer } = useStyles()
  // const userData = useContext(AppMainCtx)

  return (
    <AppBar position="fixed" className={header}>
      <Toolbar style={{ padding: 0 }}>
        <Grid
          justify="space-between"
          alignItems="center"
          container
          style={{ height: "81px", flexWrap: "nowrap" }}
        >
          <Box className={logoContainer}>
            <img src={Logo} alt="Cora" />
          </Box>

          <Box
            display="flex"
            flex="1"
            justifyContent="space-between"
            alignItems="center"
            padding="0 30px"
          >
            <Box className={condoName}>The Alps</Box>
            <Box display="flex" alignItems="center" style={{ height: "100%" }}>
              <HeaderDots />
              <Divider
                orientation="vertical"
                style={{ height: "30px", backgroundColor: "#454B57" }}
              />
              <UserBox />
            </Box>
          </Box>
        </Grid>
      </Toolbar>
    </AppBar>
  )
}

export default Header
