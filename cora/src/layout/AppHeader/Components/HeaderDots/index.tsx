import React from "react"
import { NavLink } from "react-router-dom"
import Box from "@material-ui/core/Box"
import Modal from "@material-ui/core/Modal"
import Button from "@material-ui/core/Button"
import styles from "./styles"

const IconSettings = (
  <svg
    width="20"
    height="20"
    viewBox="0 0 20 20"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M7.66602 0L7.17578 2.52344C6.35161 2.83425 5.59479 3.26993 4.93164 3.81445L2.50781 2.97852L0.171875 7.02148L2.11328 8.70898C2.03865 9.16721 2 9.59184 2 10C2 10.4088 2.03981 10.8326 2.11328 11.291V11.293L0.171875 12.9805L2.50781 17.0215L4.92969 16.1875C5.5929 16.7323 6.35143 17.1656 7.17578 17.4766L7.66602 20H12.334L12.8242 17.4766C13.6489 17.1655 14.4049 16.7306 15.0684 16.1855L17.4922 17.0215L19.8262 12.9805L17.8867 11.291C17.9614 10.8328 18 10.4082 18 10C18 9.59246 17.9611 9.16837 17.8867 8.71094V8.70898L19.8281 7.01953L17.4922 2.97852L15.0703 3.8125C14.4071 3.26768 13.6486 2.83443 12.8242 2.52344L12.334 0H7.66602ZM9.31445 2H10.6855L11.0742 4L12.1172 4.39453C12.7459 4.63147 13.3107 4.95675 13.8008 5.35938L14.6641 6.06641L16.5859 5.40625L17.2715 6.5918L15.7363 7.92773L15.9121 9.02734V9.0293C15.9733 9.40423 16 9.71877 16 10C16 10.2812 15.9733 10.5957 15.9121 10.9707L15.7344 12.0703L17.2695 13.4062L16.584 14.5938L14.6641 13.9316L13.7988 14.6406C13.3087 15.0432 12.7459 15.3685 12.1172 15.6055H12.1152L11.0723 16L10.6836 18H9.31445L8.92578 16L7.88281 15.6055C7.25415 15.3685 6.68933 15.0432 6.19922 14.6406L5.33594 13.9336L3.41406 14.5938L2.72852 13.4082L4.26562 12.0703L4.08789 10.9746V10.9727C4.02762 10.5961 4 10.2807 4 10C4 9.71877 4.02674 9.40428 4.08789 9.0293L4.26562 7.92969L2.72852 6.59375L3.41406 5.40625L5.33594 6.06836L6.19922 5.35938C6.68933 4.95675 7.25415 4.63147 7.88281 4.39453L8.92578 4L9.31445 2ZM10 6C7.80348 6 6 7.80348 6 10C6 12.1965 7.80348 14 10 14C12.1965 14 14 12.1965 14 10C14 7.80348 12.1965 6 10 6ZM10 8C11.1115 8 12 8.88852 12 10C12 11.1115 11.1115 12 10 12C8.88852 12 8 11.1115 8 10C8 8.88852 8.88852 8 10 8Z"
      fill="#454B57"
    />
  </svg>
)

const IconSettingsActive = (
  <svg
    width="20"
    height="20"
    viewBox="0 0 20 20"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M7.66602 0L7.17578 2.52344C6.35161 2.83425 5.59479 3.26993 4.93164 3.81445L2.50781 2.97852L0.171875 7.02148L2.11328 8.70898C2.03865 9.16721 2 9.59184 2 10C2 10.4088 2.03981 10.8326 2.11328 11.291V11.293L0.171875 12.9805L2.50781 17.0215L4.92969 16.1875C5.5929 16.7323 6.35143 17.1656 7.17578 17.4766L7.66602 20H12.334L12.8242 17.4766C13.6489 17.1655 14.4049 16.7306 15.0684 16.1855L17.4922 17.0215L19.8262 12.9805L17.8867 11.291C17.9614 10.8328 18 10.4082 18 10C18 9.59246 17.9611 9.16837 17.8867 8.71094V8.70898L19.8281 7.01953L17.4922 2.97852L15.0703 3.8125C14.4071 3.26768 13.6486 2.83443 12.8242 2.52344L12.334 0H7.66602ZM9.31445 2H10.6855L11.0742 4L12.1172 4.39453C12.7459 4.63147 13.3107 4.95675 13.8008 5.35938L14.6641 6.06641L16.5859 5.40625L17.2715 6.5918L15.7363 7.92773L15.9121 9.02734V9.0293C15.9733 9.40423 16 9.71877 16 10C16 10.2812 15.9733 10.5957 15.9121 10.9707L15.7344 12.0703L17.2695 13.4062L16.584 14.5938L14.6641 13.9316L13.7988 14.6406C13.3087 15.0432 12.7459 15.3685 12.1172 15.6055H12.1152L11.0723 16L10.6836 18H9.31445L8.92578 16L7.88281 15.6055C7.25415 15.3685 6.68933 15.0432 6.19922 14.6406L5.33594 13.9336L3.41406 14.5938L2.72852 13.4082L4.26562 12.0703L4.08789 10.9746V10.9727C4.02762 10.5961 4 10.2807 4 10C4 9.71877 4.02674 9.40428 4.08789 9.0293L4.26562 7.92969L2.72852 6.59375L3.41406 5.40625L5.33594 6.06836L6.19922 5.35938C6.68933 4.95675 7.25415 4.63147 7.88281 4.39453L8.92578 4L9.31445 2ZM10 6C7.80348 6 6 7.80348 6 10C6 12.1965 7.80348 14 10 14C12.1965 14 14 12.1965 14 10C14 7.80348 12.1965 6 10 6ZM10 8C11.1115 8 12 8.88852 12 10C12 11.1115 11.1115 12 10 12C8.88852 12 8 11.1115 8 10C8 8.88852 8.88852 8 10 8Z"
      fill="#E17E23"
    />
  </svg>
)

const IconPM = (
  <svg
    width="18"
    height="18"
    viewBox="0 0 18 18"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M0 0V1V18H8V0H0ZM10 0V1V18H18V0H10ZM2 2H6V16H2V2ZM12 2H16V16H12V2ZM3 4V10H5V4H3ZM13 4V10H15V4H13ZM3 12V14H5V12H3ZM13 12V14H15V12H13Z"
      fill="#454B57"
    />
  </svg>
)
const IconPMActive = (
  <svg
    width="18"
    height="18"
    viewBox="0 0 18 18"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M0 0V1V18H8V0H0ZM10 0V1V18H18V0H10ZM2 2H6V16H2V2ZM12 2H16V16H12V2ZM3 4V10H5V4H3ZM13 4V10H15V4H13ZM3 12V14H5V12H3ZM13 12V14H15V12H13Z"
      fill="#E17E23"
    />
  </svg>
)
const IconHelp = (
  <svg
    width="24"
    height="24"
    viewBox="0 0 24 24"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M12 2C6.489 2 2 6.489 2 12C2 17.511 6.489 22 12 22C17.511 22 22 17.511 22 12C22 6.489 17.511 2 12 2ZM12 4C16.4301 4 20 7.56988 20 12C20 16.4301 16.4301 20 12 20C7.56988 20 4 16.4301 4 12C4 7.56988 7.56988 4 12 4ZM12 6C9.79 6 8 7.79 8 10H10C10 8.9 10.9 8 12 8C13.1 8 14 8.9 14 10C14 12 11 12.367 11 15H13C13 13.349 16 12.5 16 10C16 7.79 14.21 6 12 6ZM11 16V18H13V16H11Z"
      fill="#454B57"
    />
  </svg>
)
const IconHelpActive = (
  <svg
    width="24"
    height="24"
    viewBox="0 0 24 24"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M12 2C6.489 2 2 6.489 2 12C2 17.511 6.489 22 12 22C17.511 22 22 17.511 22 12C22 6.489 17.511 2 12 2ZM12 4C16.4301 4 20 7.56988 20 12C20 16.4301 16.4301 20 12 20C7.56988 20 4 16.4301 4 12C4 7.56988 7.56988 4 12 4ZM12 6C9.79 6 8 7.79 8 10H10C10 8.9 10.9 8 12 8C13.1 8 14 8.9 14 10C14 12 11 12.367 11 15H13C13 13.349 16 12.5 16 10C16 7.79 14.21 6 12 6ZM11 16V18H13V16H11Z"
      fill="#E17E23"
    />
  </svg>
)

interface LocationProps {
  pathname: string
}

const HeaderDots: React.FC = () => {
  const {
    iconInactive,
    iconActive,
    container,
    name,
    menuHeader,
    modalWrapper,
    modalTrigger
  } = styles()
  const [open, setOpen] = React.useState(false)

  const handleOpen = () => {
    setOpen(true)
  }

  const handleClose = () => {
    setOpen(false)
  }

  const body = (
    <div className={modalWrapper}>
      <Button
        onClick={handleClose}
        color="primary"
        size="small"
        disableFocusRipple
        disableElevation
      >
        <svg
          width="24"
          height="24"
          viewBox="0 0 24 24"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            d="M12 2C6.489 2 2 6.489 2 12C2 17.511 6.489 22 12 22C17.511 22 22 17.511 22 12C22 6.489 17.511 2 12 2ZM12 4C16.4301 4 20 7.56988 20 12C20 16.4301 16.4301 20 12 20C7.56988 20 4 16.4301 4 12C4 7.56988 7.56988 4 12 4ZM8.70703 7.29297L7.29297 8.70703L10.5859 12L7.29297 15.293L8.70703 16.707L12 13.4141L15.293 16.707L16.707 15.293L13.4141 12L16.707 8.70703L15.293 7.29297L12 10.5859L8.70703 7.29297Z"
            fill="#777E86"
          />
        </svg>
      </Button>
      <h2 id="cora-modal_tile">Need additional help?</h2>
      <p id="cora-modal_description">
        <span>
          call us
          <a href="tel:00 3456 7890" target="_blank" rel="noopener noreferrer">
            00 3456 7890
          </a>
        </span>
        <span>
          <a
            href="mailto:cora.support@email.com"
            target="_blank"
            rel="noopener noreferrer"
          >
            cora.support@email.com
          </a>
        </span>
      </p>
    </div>
  )

  return (
    <>
      <Box display="flex" alignItems="center" className={container}>
        <NavLink
          to="/property-management"
          style={{
            display: "flex",
            alignItems: "center",
            textDecoration: "none",
            marginRight: "20px",
            height: "100%",
            color: "#454B57"
          }}
          className={menuHeader}
          activeClassName="activeMenu"
          activeStyle={{
            color: "#E17E23"
          }}
        >
          <div style={{ display: "flex" }}>
            <small className={iconInactive}>{IconPM}</small>
            <small className={iconActive}>{IconPMActive}</small>
          </div>
          <span className={name}>Property Management</span>
        </NavLink>
        <NavLink
          to="/settings"
          style={{
            display: "flex",
            alignItems: "center",
            textDecoration: "none",
            marginRight: "20px",
            height: "100%",
            color: "#454B57"
          }}
          className={menuHeader}
          activeClassName="activeMenu"
          activeStyle={{
            color: "#E17E23"
          }}
        >
          <div style={{ display: "flex" }}>
            <small className={iconInactive}>{IconSettings}</small>
            <small className={iconActive}>{IconSettingsActive}</small>
          </div>
          <span className={name}>Settings</span>
        </NavLink>
        {/* <NavLink
          to="/help"
          style={{
            display: "flex",
            alignItems: "center",
            textDecoration: "none",
            height: "100%",
            color: "#454B57",
          }}
          className={menuHeader}
          activeClassName="activeMenu"
          activeStyle={{
            color: "#E17E23",
          }}
        > */}
        <div style={{ display: "flex" }}>
          <button type="button" onClick={handleOpen} className={modalTrigger}>
            <div style={{ display: "flex" }}>
              <small className={iconInactive}>{IconHelp}</small>
              <small className={iconActive}>{IconHelpActive}</small>
            </div>
            <span className={name}>Help</span>
          </button>
        </div>

        {/* </NavLink> */}

        <Modal
          open={open}
          onClose={handleClose}
          aria-labelledby="cora-modal_tile"
          aria-describedby="cora-modal_description"
        >
          {body}
        </Modal>
      </Box>
    </>
  )
}

export default HeaderDots
