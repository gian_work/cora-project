import React, { useState } from "react"
import cx from "classnames"

const SearchBox: React.FC = () => {
  const [activeSearch] = useState(true)

  return (
    <>
      <div
        className={cx("search-wrapper", {
          active: activeSearch
        })}
      >
        <div className="input-holder">
          <input type="text" className="search-input" placeholder="Type to search" />
          <button className="search-icon" type="button">
            <span />
          </button>
        </div>
      </div>
    </>
  )
}

export default SearchBox
