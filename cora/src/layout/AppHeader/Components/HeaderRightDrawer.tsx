import React, { useState } from "react"
import IconButton from "@material-ui/core/IconButton"
import MenuIcon from "@material-ui/icons/Menu"

const HeaderRightDrawer: React.FC = () => {
  const [openLeft, setopenLeft] = useState(false)
  const [openRight, setopenRight] = useState(false)

  return (
    <>
      <div className={openLeft ? "active" : ""}>
        <IconButton
          edge="start"
          color="primary"
          aria-label="open drawer"
          onClick={() => [setopenRight(!openRight), setopenLeft(false)]}
        >
          <MenuIcon />
        </IconButton>
      </div>
    </>
  )
}

export default HeaderRightDrawer
