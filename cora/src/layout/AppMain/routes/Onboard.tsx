import OnboardLogin from "pages/Onboard/Login"
import OnboardSignup from "pages/Onboard/Signup"
import OnboardForgotPassword from "pages/Onboard/ForgotPassword"
import OnboardResetPassword from "pages/Onboard/ResetPassword"

const OnboardRoute = [
  {
    route: "/login",
    component: OnboardLogin
  },
  {
    route: "/signup",
    component: OnboardSignup
  },
  {
    route: "/forgot-password",
    component: OnboardForgotPassword
  },
  {
    route: "/reset-password",
    component: OnboardResetPassword
  }
]

export default OnboardRoute
