import React, { Suspense } from "react"
import { Route, Redirect, withRouter } from "react-router-dom"
import Loader from "react-loaders"
import { ToastContainer } from "react-toastify"

// context consumer
import { AuthConsumer } from "context/auth-context"

import EstateMatters from "pages/Dashboard/EstateMatters/index"
import { OnboardRoute, DashboardRoute, SettingsRoute } from "./routes"
// const Dashboards = lazy(() => import("../../pages/Dashboards/index"))
// pages for redirect

const AppMain = ({ location }: any) => {
  const ActiveRoutes: any =
    location.pathname.split("/")[1] === "property-management"
      ? DashboardRoute
      : SettingsRoute

  return (
    <>
      <AuthConsumer>
        {({ isAuth }: Record<any, boolean>) =>
          ActiveRoutes?.map((item: any) => (
            <Suspense
              fallback={
                <div className="loader-container">
                  <div className="loader-container-inner">
                    <div className="text-center">
                      <Loader active type="ball-pulse-rise" />
                    </div>
                    <h6 className="mt-5">
                      Please wait while we load all the Components examples
                    </h6>
                  </div>
                </div>
              }
            >
              {isAuth !== undefined && isAuth ? (
                <Route exact path={item.route} component={item.component} />
              ) : (
                <Redirect to="/login" />
              )}
            </Suspense>
          ))}
      </AuthConsumer>

      <Suspense
        fallback={
          <div className="loader-container">
            <div className="loader-container-inner">
              <div className="text-center">
                <Loader active type="ball-pulse-rise" />
              </div>
              <h6 className="mt-5">
                Please wait while we load all the Components examples
              </h6>
            </div>
          </div>
        }
      >
        <Route
          path="/property-management/unit-management/details"
          component={EstateMatters}
        />
      </Suspense>
      {/* Onboarding */}
      {OnboardRoute.map((onboard) => (
        <Suspense
          fallback={
            <div className="loader-container">
              <div className="loader-container-inner">
                <div className="text-center">
                  <Loader active type="ball-pulse-rise" />
                </div>
                <h6 className="mt-5">
                  Please wait while we load all the Components examples
                </h6>
              </div>
            </div>
          }
        >
          <Route exact path={onboard.route} component={onboard.component} />
        </Suspense>
      ))}
      {/* End of Onboarding */}
      <Route exact path="/" render={() => <Redirect to="/login" />} />
      <Route
        exact
        path="/property-management"
        render={() => <Redirect to="/property-management/dashboards" />}
      />
      <Route
        exact
        path="/settings"
        render={() => <Redirect to="/settings/account-management/admin" />}
      />
      <ToastContainer />
    </>
  )
}

export default withRouter(AppMain)
