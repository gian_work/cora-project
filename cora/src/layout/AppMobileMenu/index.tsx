import React from "react"
import { connect, useSelector, useDispatch } from "react-redux"
import Hamburger from "react-hamburgers"
// import { faEllipsisV } from "@fortawesome/free-solid-svg-icons"
// import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { Button } from "reactstrap"

const AppMobileMenu: React.FunctionComponent = () => {
  // redux
  const dispatch = useDispatch()
  const enableMobileMenu = useSelector(
    (state: any) => state.themeOptions.enableMobileMenu
  )
  const enableMobileMenuSmall = useSelector(
    (state: any) => state.themeOptions.enableMobileMenuSmall
  )

  const toggleMobileSidebar = () => {
    dispatch({
      type: "THEME_OPTIONS/SET_ENABLE_MOBILE_MENU",
      enableMobileMenu: !enableMobileMenu
    })
  }

  const toggleMobileSmall = () => {
    dispatch({
      type: "THEME_OPTIONS/SET_ENABLE_MOBILE_MENU_SMALL",
      enableMobileMenuSmall: !enableMobileMenuSmall
    })
  }

  return (
    <>
      <div className="app-header__mobile-menu">
        <div onClick={toggleMobileSidebar} role="button" tabIndex={0}>
          <Hamburger active={enableMobileMenu} type="elastic" />
        </div>
      </div>
      <div className="app-header__menu">
        <span onClick={toggleMobileSmall} role="button" tabIndex={0}>
          <Button size="sm" color="primary">
            <div className="btn-icon-wrapper">
              {/* <FontAwesomeIcon icon={faEllipsisV} /> */}
            </div>
          </Button>
        </span>
      </div>
    </>
  )
}

export default connect(null, null)(AppMobileMenu)
