import React from "react"
import DateFnsUtils from "@date-io/date-fns"
import { MuiPickersUtilsProvider, KeyboardDatePicker } from "@material-ui/pickers"

type DatepickerProps = {
  selectedDate: any
  handleDateChange: Function
}
const Datepicker: React.FC<DatepickerProps> = ({
  selectedDate,
  handleDateChange
}) => {
  return (
    <MuiPickersUtilsProvider utils={DateFnsUtils}>
      <KeyboardDatePicker
        inputVariant="outlined"
        format="MMMM dd, yyyy"
        margin="normal"
        value={selectedDate}
        onChange={() => handleDateChange()}
        KeyboardButtonProps={{
          "aria-label": "change date"
        }}
        style={{ backgroundColor: "#FFFFFF" }}
      />
    </MuiPickersUtilsProvider>
  )
}

export default Datepicker
