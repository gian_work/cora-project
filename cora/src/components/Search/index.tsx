import * as React from "react"
import InputBase from "@material-ui/core/InputBase"
import { fade, makeStyles, Theme } from "@material-ui/core/styles"
import SearchIcon from "@material-ui/icons/Search"

const useStyles = makeStyles((theme: Theme) => ({
  search: {
    position: "relative",
    // borderRadius: theme.shape.borderRadius,
    background: "#F9FAFC",
    border: "1px solid #E3E3E3",
    borderRadius: "24px",
    height: "30px",
    overflow: "hidden",
    backgroundColor: fade(theme.palette.common.white, 0.15),
    "&:hover": {
      backgroundColor: fade(theme.palette.common.white, 0.25)
    },
    width: "100%",
    [theme.breakpoints.up("sm")]: {
      width: "auto"
    }
  },
  searchIcon: {
    width: "10px",
    height: "100%",
    position: "absolute",
    pointerEvents: "none",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    left: "20px",
    zIndex: 1
  },
  inputRoot: {
    color: "inherit",
    height: "100%"
  },
  inputInput: {
    backgroundColor: "#F9FAFC",
    color: "#131336",
    fontSize: "14px",
    height: "100%",
    padding: "0 10px 0 50px",
    transition: theme.transitions.create("width"),
    width: "100%",
    [theme.breakpoints.up("sm")]: {
      width: 338
    }
  }
}))

const Search: React.FC = () => {
  const classes = useStyles()
  return (
    <div className={classes.search}>
      {/* <SearchIcon color="primary" /> */}
      <div className={classes.searchIcon}>
        <SearchIcon fontSize="small" style={{ color: "#C8C9D5" }} />
      </div>
      <InputBase
        placeholder="Search"
        classes={{
          root: classes.inputRoot,
          input: classes.inputInput
        }}
        inputProps={{ "aria-label": "search" }}
      />
    </div>
  )
}

export default Search
