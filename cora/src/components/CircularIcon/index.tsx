import * as React from "react"
import styled from "@emotion/styled"

// styled
const Container = styled.div`
  display: flex;
  align-items: center;
  padding: 22px 30px;
`
const Icon = styled.div`
  width: 44px;
  height: 44px;
  border-radius: 50%;
  background-color: #21b6bf;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-right: 25px;
`
const Title = styled.div`
  font-size: 16px;
  color: #131336;
`

interface CircularIconProps {
  bg: string
  icon: any
  title: string
}

const CircularIcon: React.FC<CircularIconProps> = ({ bg, icon, title = "" }) => {
  return (
    <Container>
      <Icon style={{ backgroundColor: bg }}>{icon()}</Icon>
      <Title>{title}</Title>
    </Container>
  )
}

export default CircularIcon
