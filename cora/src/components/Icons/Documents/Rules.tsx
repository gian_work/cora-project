import * as React from "react"

const Rules: React.FC = () => {
  return (
    <svg
      width="20"
      height="18"
      viewBox="0 0 20 18"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M3 0C1.35503 0 0 1.35503 0 3V6H3V15C3 16.645 4.35503 18 6 18H17C18.645 18 20 16.645 20 15V12H16V3C16 1.35503 14.645 0 13 0H3ZM3 2H10.416C10.279 2.34201 10 2.61165 10 3V4H2V3C2 2.43497 2.43497 2 3 2ZM6 8H13V10H6V8ZM6 12H13V14H6V12ZM16 14H18V15C18 15.565 17.565 16 17 16C16.435 16 16 15.565 16 15V14Z"
        fill="white"
      />
    </svg>
  )
}

export default Rules
