export { default as IconApplications } from "./Applications"
export { default as IconFacilities } from "./Facilities"
export { default as IconOthers } from "./Others"
export { default as IconScheduled } from "./Scheduled"
