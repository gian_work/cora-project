export { default as IconCash } from "./Cash"
export { default as IconCheque } from "./Cheque"
export { default as IconEPayment } from "./EPayment"
