import * as React from "react"

const DiningRoom: React.FC = () => {
  return (
    <svg
      width="16"
      height="20"
      viewBox="0 0 16 20"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M4 0C1.558 0 0 3.94 0 6C0 7.85844 1.27983 9.41009 3 9.85742V20H5V9.85742C6.72017 9.41009 8 7.85844 8 6C8 3.94 6.442 0 4 0ZM11 0V14V15V20H13V14.8438C14.7201 14.3546 16 12.6849 16 11C16 9 14 2 11 0Z"
        fill="white"
      />
    </svg>
  )
}

export default DiningRoom
