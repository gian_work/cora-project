import * as React from "react"

const OperationsManuals: React.FC = () => {
  return (
    <svg
      width="20"
      height="17"
      viewBox="0 0 20 17"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M0 0V13V14C0 15.105 0.895 16 2 16H8.58789C8.69117 16.2921 8.88237 16.5451 9.13522 16.7241C9.38807 16.9032 9.69017 16.9996 10 17C10.3098 16.9996 10.6119 16.9032 10.8648 16.7241C11.1176 16.5451 11.3088 16.2921 11.4121 16H18C19.105 16 20 15.105 20 14V13V0H14.002C12.3585 0 10.9114 0.803159 10 2.02734C9.0886 0.803159 7.64147 0 5.99805 0H0ZM14 3H16V5H14V3ZM14 7H16V12H14V7Z"
        fill="white"
      />
    </svg>
  )
}

export default OperationsManuals
