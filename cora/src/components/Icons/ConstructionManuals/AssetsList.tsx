import * as React from "react"

const AssetsList: React.FC = () => {
  return (
    <svg
      width="20"
      height="16"
      viewBox="0 0 20 16"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M3.58594 0L1.58594 2H0V4H2.41406L5 1.41406L3.58594 0ZM7 2V4H20V2H7ZM3.58594 6L1.58594 8H0V10H2.41406L5 7.41406L3.58594 6ZM7 8V10H20V8H7ZM3.58594 12L1.58594 14H0V16H2.41406L5 13.4141L3.58594 12ZM7 14V16H20V14H7Z"
        fill="white"
      />
    </svg>
  )
}

export default AssetsList
