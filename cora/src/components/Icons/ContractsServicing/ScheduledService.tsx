import * as React from "react"

const ScheduledService: React.FC = () => {
  return (
    <svg
      width="18"
      height="20"
      viewBox="0 0 18 20"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M8 0V2.05859C3.50759 2.55843 0 6.37565 0 11C0 15.963 4.038 20 9 20C13.962 20 18 15.963 18 11C18 6.37565 14.4924 2.55843 10 2.05859V0H8ZM8 5H10V10.5859L13.1484 13.7344L11.7344 15.1484L8 11.4141V5Z"
        fill="white"
      />
    </svg>
  )
}

export default ScheduledService
