import * as React from "react"

const LicensesPermits: React.FC = () => {
  return (
    <svg
      width="20"
      height="18"
      viewBox="0 0 20 18"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M4 0C1.80271 0 0 1.80271 0 4C0 5.8503 1.28421 7.40559 3 7.85547V18H8V16H5V14H7V12H5V7.85547C6.71579 7.40559 8 5.8503 8 4C8 1.80271 6.19729 0 4 0ZM4 2C5.11641 2 6 2.88359 6 4C6 5.11641 5.11641 6 4 6C2.88359 6 2 5.11641 2 4C2 2.88359 2.88359 2 4 2ZM10 3V8H20V3H10ZM10 10V15H20V10H10Z"
        fill="white"
      />
    </svg>
  )
}

export default LicensesPermits
