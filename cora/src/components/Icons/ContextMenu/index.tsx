export { default as IconEdit } from "./Edit"
export { default as IconView } from "./View"
export { default as IconDelete } from "./Delete"
