import React from "react"
import Dropzone from "react-dropzone"

import styles from "./styles"

const IconUpload = (
  <svg
    width="64"
    height="48"
    viewBox="0 0 64 48"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M32 0C25.7747 0 20.535 3.33199 17.1458 8.03646C7.69455 8.14371 0 15.8576 0 25.3333C0 34.8754 7.79129 42.6667 17.3333 42.6667H24V37.3333H17.3333C10.6727 37.3333 5.33333 31.994 5.33333 25.3333C5.33333 18.6727 10.6727 13.3333 17.3333 13.3333C17.4266 13.3333 17.6439 13.352 18 13.3698L19.6302 13.4531L20.4427 12.0365C22.747 8.03294 27.0344 5.33333 32 5.33333C38.6787 5.33333 44.1439 10.2013 45.151 16.5729L45.5573 19.1354L48.1302 18.8021C48.7258 18.7245 49.1064 18.6667 49.3333 18.6667C54.5185 18.6667 58.6667 22.8149 58.6667 28C58.6667 33.1851 54.5185 37.3333 49.3333 37.3333H40V42.6667H49.3333C57.4015 42.6667 64 36.0682 64 28C64 20.0257 57.5388 13.5365 49.599 13.3854C47.2798 5.71087 40.4056 0 32 0ZM32 16L21.3333 26.6667H29.3333V48H34.6667V26.6667H42.6667L32 16Z"
      fill="#777E86"
    />
  </svg>
)

interface UploadBoxProps {
  title?: string
  onDrop?: Function
  acceptedFile?: string
}

const defaultTitle = "Choose photo to upload"

const UploadBox: React.FC<UploadBoxProps> = ({
  title = defaultTitle,
  onDrop,
  acceptedFile = "image/*"
}) => {
  const { root, container, uploadBox, uploadContent, titleStyle } = styles()

  return (
    <div className={root}>
      {title && <div className={titleStyle}>{title}</div>}
      <div className={container}>
        <Dropzone accept={acceptedFile} onDrop={(files) => onDrop && onDrop(files)}>
          {({ getRootProps, getInputProps }) => (
            <div {...getRootProps()} className={uploadBox}>
              <input {...getInputProps()} />
              <div className={uploadContent}>
                <div>{IconUpload}</div>
                <div className="uploadButton">{title}</div>
                <div className="info">or drag & drop them here</div>
              </div>
            </div>
          )}
        </Dropzone>
      </div>
    </div>
  )
}

export default UploadBox
