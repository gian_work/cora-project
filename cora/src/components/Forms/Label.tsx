import React from "react"
import styles from "./styles"

interface FormLabelProps {
  label: string
  light?: boolean
}

const FormLabel: React.FC<FormLabelProps> = ({ label, light }) => {
  const { labelStyle } = styles()
  return <div className={`${labelStyle} ${light ? "light" : ""}`}>{label}</div>
}

export default FormLabel
