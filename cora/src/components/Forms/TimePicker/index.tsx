import React from "react"
import Box from "@material-ui/core/Box"
import DateFnsUtils from "@date-io/date-fns"
import { MuiPickersUtilsProvider, TimePicker } from "@material-ui/pickers"

import FormLabel from "components/Forms/Label"

interface FormInputProps {
  label: string
  value: any
  handleDateChange: Function
  onBlur?: Function
  error?: boolean | undefined
  helperText?: boolean | string | undefined
  name: string
  ampm?: boolean
  minutesStep?: number
}

const DateTimePicker: React.FC<FormInputProps> = ({
  label = "date",
  value,
  handleDateChange,
  onBlur,
  error,
  helperText,
  ampm = true,
  minutesStep = 1
}) => {
  return (
    <Box flex="1">
      <FormLabel label={label} />
      <MuiPickersUtilsProvider utils={DateFnsUtils}>
        <TimePicker
          inputVariant="outlined"
          value={value}
          onChange={(e) => handleDateChange(e)}
          onError={(e) => e}
          // format="HH:mm"
          onBlur={(e) => onBlur && onBlur(e)}
          error={error}
          helperText={helperText}
          minutesStep={minutesStep}
          ampm={ampm}
        />
      </MuiPickersUtilsProvider>
    </Box>
  )
}
export default DateTimePicker
