import React from "react"

// styles
import styles from "./styles"

interface FormWrapperProps {
  title?: string
  children: any
  width?: string
  hasBottomBorder?: boolean
}

const FormWrapper: React.FC<FormWrapperProps> = ({
  title,
  children,
  width,
  hasBottomBorder
}) => {
  const { container, titleStyle, hasBorder } = styles()
  const hasBorderClass = `${hasBottomBorder ? hasBorder : ""}`
  return (
    <div className={container}>
      {title && (
        <div className={titleStyle} style={{ width, margin: "auto" }}>
          {title}
        </div>
      )}
      <div className={hasBorderClass} style={{ margin: "auto" }}>
        {children}
      </div>
    </div>
  )
}
export default FormWrapper
