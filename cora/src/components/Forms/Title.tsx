import React from "react"

// styles
import styles from "./styles"

interface ValueProps {
  title: string
}

const Title: React.FC<ValueProps> = ({ title }) => {
  const { titleStyle } = styles()
  return <div className={titleStyle}>{title}</div>
}
export default Title
