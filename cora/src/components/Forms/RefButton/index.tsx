import React from "react"

interface RefButtonProps {
  action: Function
  refValue: any
}

const RefButton: React.FC<RefButtonProps> = ({ action, refValue }) => {
  return (
    <div
      onClick={() => action()}
      ref={refValue}
      role="button"
      tabIndex={-1}
      style={{ outline: "none", height: "1px" }}
    >
      &nbsp;
    </div>
  )
}
export default RefButton
