import * as React from "react"
import { Link } from "react-router-dom"
import { makeStyles, Theme } from "@material-ui/core/styles"
// import {CSVLink, CSVDownload} from "react-csv"

// components
import Box from "@material-ui/core/Box"
import Button from "@material-ui/core/Button"
import AddCircleOutlineIcon from "@material-ui/icons/AddCircleOutline"

const useStyles = makeStyles((theme: Theme) => ({
  container: {
    display: "flex",
    width: "100%",
    padding: "25px 20px",
    borderBottom: "1px solid #F2F2F2"
  },
  search: {
    position: "relative"
  },
  cta: {
    margin: "0 5px",
    textTransform: "capitalize"
  },
  titlePage: {
    fontSize: "20px",
    color: theme.palette.secondary.dark,
    display: "flex",
    alignItems: "flex-start",
    textTransform: "capitalize",
    "& > i": {
      padding: "3px 10px 0 0"
    },
    "& svg path": {
      fill: theme.palette.secondary.dark
    }
  }
}))

const IconDownload = () => (
  <svg
    width="20"
    height="20"
    viewBox="0 0 20 20"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M8.5 0C7.68294 0 7 0.682937 7 1.5V8H2.58594L10 15.4141L17.4141 8H13V1.5C13 0.682937 12.3171 0 11.5 0H8.5ZM9 2H11V10H12.5859L10 12.5859L7.41406 10H9V2ZM0 18V20H20V18H0Z"
      fill="#09707B"
    />
  </svg>
)

// interface
interface TableHeaderProps {
  downloadFile?: Function | undefined
  addAction?: Function
  hasSearch?: boolean
  hasDownloadCSV?: boolean
  isLink?: boolean
  addActionLabel?: string
  className?: string
  title?: string
  titleIcon?: any
  urlLink?: any
  dataDownload?: any
}

const TableHeader: React.FC<TableHeaderProps> = ({
  addAction,
  addActionLabel,
  title,
  titleIcon,
  isLink,
  urlLink
}) => {
  const { container, cta, titlePage } = useStyles()

  return (
    <Box className={container} justifyContent="space-between">
      <Box className={titlePage}>
        {titleIcon && <i>{titleIcon()}</i>}
        {title}
      </Box>
      <Box display="flex">
        <Box display="flex">
          <Button
            className={cta}
            size="medium"
            color="primary"
            startIcon={<IconDownload />}
            onClick={() => null}
          >
            Download CSV
          </Button>
          {/* {dataToDownload && (
            <CSVLink
              data={Object.values(dataToDownload)}
              headers={headers}
              onClick={(event: any) => {
                console.log(Object.values(dataToDownload))
              }}
            >
              Download me
            </CSVLink>
          )} */}
          {/* <CSVLink data={dataDownload}>Download me</CSVLink> */}
          {isLink && (
            <Button
              component={Link}
              to={urlLink}
              className={cta}
              size="medium"
              color="primary"
              startIcon={<AddCircleOutlineIcon />}
            >
              {addActionLabel}
            </Button>
          )}
          {addAction && (
            <Button
              className={`${cta} cta-add-btn`}
              size="medium"
              color="primary"
              startIcon={<AddCircleOutlineIcon />}
              onClick={(): void => addAction && addAction(true)}
              tabIndex={0}
            >
              {addActionLabel}
            </Button>
          )}
        </Box>
      </Box>
    </Box>
  )
}

export default TableHeader
