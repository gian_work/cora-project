import React from "react"

interface SectionProps {
  children: any
  noBorder?: boolean
}

const Section: React.FC<SectionProps> = ({ children, noBorder }) => {
  return (
    <div
      style={{
        margin: "30px 0 0 0",
        padding: "0 30px 10px",
        borderBottom: noBorder ? "1px solid transparent" : "1px solid #F2F2F2"
      }}
    >
      {children}
    </div>
  )
}

export default Section
