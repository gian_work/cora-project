import * as React from "react"
import styled from "@emotion/styled"

// styled
const Container = styled.div`
  display: flex;
  align-items: center;
  cursor: pointer;
`
const LabelDefault = styled.div`
  font-size: 12px;
  line-height: 15px;
  color: #8998ac;
  text-transform: capitalize;
  padding-left: 5px;
  cursor: pointer;
`
const LabelPrimary = styled.div`
  font-size: 12px;
  line-height: 15px;
  color: #006eff;
  text-transform: capitalize;
  padding-left: 5px;
  cursor: pointer;
`

const IconClear = () => (
  <svg
    width="14"
    height="14"
    viewBox="0 0 14 14"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M7.00065 0.333374C3.31398 0.333374 0.333984 3.31337 0.333984 7.00004C0.333984 10.6867 3.31398 13.6667 7.00065 13.6667C10.6873 13.6667 13.6673 10.6867 13.6673 7.00004C13.6673 3.31337 10.6873 0.333374 7.00065 0.333374ZM10.334 9.39337L9.39398 10.3334L7.00065 7.94004L4.60732 10.3334L3.66732 9.39337L6.06065 7.00004L3.66732 4.60671L4.60732 3.66671L7.00065 6.06004L9.39398 3.66671L10.334 4.60671L7.94065 7.00004L10.334 9.39337Z"
      fill="#C8C9D5"
    />
  </svg>
)

const IconAdd = () => (
  <svg
    width="20"
    height="20"
    viewBox="0 0 20 20"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M10 0C4.477 0 0 4.477 0 10C0 15.523 4.477 20 10 20C15.523 20 20 15.523 20 10C20 4.477 15.523 0 10 0ZM15 11H11V15H9V11H5V9H9V5H11V9H15V11Z"
      fill="#006EFF"
    />
  </svg>
)

const IconDelete = () => (
  <svg
    width="20"
    height="20"
    viewBox="0 0 20 20"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M10 0C4.47 0 0 4.47 0 10C0 15.53 4.47 20 10 20C15.53 20 20 15.53 20 10C20 4.47 15.53 0 10 0ZM15 13.59L13.59 15L10 11.41L6.41 15L5 13.59L8.59 10L5 6.41L6.41 5L10 8.59L13.59 5L15 6.41L11.41 10L15 13.59Z"
      fill="#C8C9D5"
    />
  </svg>
)

// interface
interface ClearActionProps {
  label?: string
  onClick: Function
  type: string
}

const ClearAction: React.FC<ClearActionProps> = ({ label, onClick, type }) => {
  return (
    <Container onClick={() => onClick()}>
      {type === "clear" && (
        <>
          <IconClear />
          <LabelDefault>{label}</LabelDefault>
        </>
      )}
      {type === "add" && (
        <>
          <IconAdd />
          <LabelPrimary>{label}</LabelPrimary>
        </>
      )}
      {type === "delete" && (
        <>
          <IconDelete />
        </>
      )}
    </Container>
  )
}

export default ClearAction
