import React from "react"
import Box from "@material-ui/core/Box"

// components
import FilterButtons from "../FilterButtons"

// interface
interface TableFilterMenuProps {
  menu: Record<string, any>
  filterText: string
  filterData?: Function
  filterAll?: Function
  activeTable?: string
}

const TableFilterMenu: React.FC<TableFilterMenuProps> = ({
  menu,
  filterText,
  filterData
}) => (
  <Box display="flex" overflow="auto">
    {/* <FilterButtons
      label="All"
      onClick={() => filterAll && filterAll()}
      isActive={activeTable === "all"}
    /> */}
    {menu.map((item: Record<string, any>) => {
      const { label, value } = item
      return (
        <FilterButtons
          key={value}
          label={label}
          onClick={() => filterData && filterData(value, label)}
          isActive={filterText === value}
        />
      )
    })}
  </Box>
)

export default TableFilterMenu
