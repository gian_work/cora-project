import React from "react"
import Box from "@material-ui/core/Box"
import Button from "@material-ui/core/Button"
import HighlightOffIcon from "@material-ui/icons/HighlightOff"

// interface
interface TableClearFilterProps {
  onClick: Function
}

const TableClearFilter: React.FC<TableClearFilterProps> = ({ onClick }) => {
  return (
    <Box display="flex" justifyContent="flex-end">
      <Button size="small" onClick={() => onClick()}>
        <HighlightOffIcon style={{ marginRight: "8px" }} />
        Clear Filters
      </Button>
    </Box>
  )
}

export default TableClearFilter
