import { useEffect } from "react"

const UseInitialfocus = (ref: any) => {
  useEffect(() => {
    ref.current.focus()
  }, [ref])
}

export default UseInitialfocus
