import React from "react"
import Box from "@material-ui/core/Box"

// styles
import styles from "./styles"

interface TabsButtonProps {
  options: Record<any, any>
  action?: Function
  activeTab: number
}

const TabsButton: React.FC<TabsButtonProps> = ({ options, activeTab }) => {
  const { btn } = styles()
  return (
    <>
      {Object.keys(options).map((option) => {
        return (
          <Box
            className={`${btn} ${activeTab === parseInt(option, 0) ? "active" : ""}`}
          >
            {options[parseInt(option, 0)]}
          </Box>
        )
      })}
    </>
  )
}
export default TabsButton
