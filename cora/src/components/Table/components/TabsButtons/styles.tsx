import { makeStyles, Theme } from "@material-ui/core/styles"

const styles = makeStyles((theme: Theme) => ({
  btn: {
    background: "#F2F2F2",
    color: theme.palette.body.gray,
    fontSize: "16px",
    padding: "0 60px",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    height: "52px",
    cursor: "pointer",
    "&.active": {
      background: theme.palette.secondary.main,
      color: theme.palette.common.white
    },
    "&:first-child": {
      borderRadius: "10px 0px 0px 0px"
    },
    "&:last-child": {
      borderRadius: "0px 10px 0px 0px"
    }
  }
}))

export default styles
