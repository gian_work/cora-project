import React from "react"
import Box from "@material-ui/core/Box"

// styles
import styles from "./styles"

interface TabsButtonProps {
  options: Record<any, any>
  action: Function
  activeTab: number
}

const TabsButton: React.FC<TabsButtonProps> = ({ options, activeTab, action }) => {
  const { btn } = styles()
  return (
    <>
      <Box display="flex">
        {Object.keys(options).map((option) => {
          return (
            <Box
              className={`${btn} ${
                activeTab === parseInt(option, 0) ? "active" : ""
              }`}
              onClick={() => action(parseInt(option, 0))}
            >
              {options[parseInt(option, 0)]}
            </Box>
          )
        })}
      </Box>
    </>
  )
}
export default TabsButton
