import React from "react"
import { makeStyles } from "@material-ui/core/styles"
import Tabs from "@material-ui/core/Tabs"
import Tab from "@material-ui/core/Tab"

const useStyles = makeStyles(() => ({
  tabsStyle: {},
  tabStyle: {
    padding: "20px 12px"
  }
}))

export type TabsFilterProps = {
  options: Array<string>
  refs?: any
  onFilter: Function | undefined
  children?: any
  tabsFilter?: number
}
export const TabsFilter: React.FC<TabsFilterProps> = ({
  children,
  options,
  onFilter,
  tabsFilter
}) => {
  const { tabsStyle, tabStyle } = useStyles()

  return (
    <Tabs value={tabsFilter} className={tabsStyle}>
      {options.map((item, index) => {
        return (
          <Tab
            key={item}
            label={item}
            className={`${tabStyle} tab-${item
              .toLocaleLowerCase()
              .split(" ")
              .join("")}`}
            onClick={() => onFilter && [onFilter(index)]}
          >
            {children}
          </Tab>
        )
      })}
    </Tabs>
  )
}

export default TabsFilter
