import React, { useState } from "react"
import { makeStyles, Theme } from "@material-ui/core/styles"
import Tabs from "@material-ui/core/Tabs"
import Tab from "@material-ui/core/Tab"

const useStyles = makeStyles((theme: Theme) => ({
  tabsStyle: {},
  tabStyle: {
    padding: "20px 12px",
    color: theme.palette.primary.main
  }
}))

export type TabsFilterProps = {
  options: Array<string>
  onFilter: Function | undefined
  setTabsFilter?: Function
  children?: any
  testClick?: Function
}
export const TabsFilter: React.FC<TabsFilterProps> = ({
  children,
  options,
  onFilter,
  setTabsFilter,
  testClick
}) => {
  const { tabsStyle, tabStyle } = useStyles()
  const [value, setValue] = useState(0)

  const handleChange = (event: any, newValue: number) => {
    setValue(newValue)
  }
  return (
    <Tabs value={value} onChange={handleChange} className={tabsStyle}>
      {options.map((item) => {
        return (
          <Tab
            ref={() => testClick && testClick()}
            key={item}
            label={item}
            className={tabStyle}
            onClick={() =>
              onFilter && [
                onFilter(parseInt(item, 0)),
                setTabsFilter && setTabsFilter(item)
              ]}
          >
            {children}
          </Tab>
        )
      })}
    </Tabs>
  )
}

export default TabsFilter
