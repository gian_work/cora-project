import React from "react"
import styled from "@emotion/styled"

const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  width: 100%;
  position: relative;
  z-index: 2;
  margin: 0 0 -20px 0;
  cursor: pointer;
`
const Container = styled.div`
  text-align: center;
  width: 40px;
  height: 40px;
  border-radius: 50%;
  background: #ffffff;
  box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.1);
  display: flex;
  align-items: center;
  justify-content: center;

  &.iscollapse {
    transform: rotate(180deg);
  }
`
// asset
const CollapseIcon = () => (
  <svg
    width="24"
    height="25"
    viewBox="0 0 24 25"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M6.83867 16.1089C6.44909 16.4984 5.86471 16.4984 5.47513 16.1089C5.28034 15.9141 5.18294 15.7193 5.18294 15.4271C5.18294 15.1349 5.28034 14.9401 5.47513 14.7453L11.3189 8.90156C11.7085 8.51198 12.2928 8.51198 12.6824 8.90156L18.5262 14.7453C18.9158 15.1349 18.9158 15.7193 18.5262 16.1089C18.1366 16.4984 17.5522 16.4984 17.1626 16.1089L12.0007 10.9469L6.83867 16.1089Z"
      fill="#006EFF"
    />
    <mask
      id="mask0"
      mask-type="alpha"
      maskUnits="userSpaceOnUse"
      x="5"
      y="8"
      width="14"
      height="9"
    >
      <path
        d="M6.83867 16.1089C6.44909 16.4984 5.86471 16.4984 5.47513 16.1089C5.28034 15.9141 5.18294 15.7193 5.18294 15.4271C5.18294 15.1349 5.28034 14.9401 5.47513 14.7453L11.3189 8.90156C11.7085 8.51198 12.2928 8.51198 12.6824 8.90156L18.5262 14.7453C18.9158 15.1349 18.9158 15.7193 18.5262 16.1089C18.1366 16.4984 17.5522 16.4984 17.1626 16.1089L12.0007 10.9469L6.83867 16.1089Z"
        fill="white"
      />
    </mask>
    <g mask="url(#mask0)" />
  </svg>
)

// interface
interface ToggleTABLEProps {
  action: Function
  isCollapsed?: boolean
}

const ToggleTABLE: React.FC<ToggleTABLEProps> = ({ action, isCollapsed }) => {
  const stateClass = isCollapsed ? "iscollapse" : ""
  return (
    <Wrapper>
      <Container className={stateClass} onClick={() => action()}>
        <CollapseIcon />
      </Container>
    </Wrapper>
  )
}

export default ToggleTABLE
