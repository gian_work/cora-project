import React from "react"
import Button from "@material-ui/core/Button"

interface ActionButtonProps {
  label: string
  action: Function
}

const ActionButton: React.FC<ActionButtonProps> = ({ label, action }) => {
  return (
    <Button size="large" color="primary" onClick={() => action()}>
      {label}
    </Button>
  )
}
export default ActionButton
