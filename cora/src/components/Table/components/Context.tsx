import React, { useRef } from "react"
import styled from "@emotion/styled"
import useInitialFocus from "../util/Focus"

const IconApprove = (
  <svg
    width="21"
    height="20"
    viewBox="0 0 21 20"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M10 0C4.486 0 0 4.486 0 10C0 15.514 4.486 20 10 20C15.514 20 20 15.514 20 10C20 8.874 19.804 7.7942 19.459 6.7832L17.8398 8.40234C17.9448 8.91834 18 9.453 18 10C18 14.411 14.411 18 10 18C5.589 18 2 14.411 2 10C2 5.589 5.589 2 10 2C11.633 2 13.1519 2.49389 14.4199 3.33789L15.8516 1.90625C14.2036 0.71225 12.185 0 10 0ZM19.293 1.29297L9 11.5859L5.70703 8.29297L4.29297 9.70703L9 14.4141L20.707 2.70703L19.293 1.29297Z"
      fill="black"
    />
  </svg>
)

const IconEdit = (
  <svg
    width="20"
    height="22"
    viewBox="0 0 20 22"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M2 0C0.9 0 0 0.9 0 2V18C0 19.1 0.9 20 2 20H13.1719L11.1719 18H2V2H9V7H14V11.1719L16 13.1719V6L10 0H2ZM11 13V15L16.1465 20.1465L18.1465 18.1465L13 13H11ZM18.8535 18.8535L16.8535 20.8535L17.8535 21.8535C18.0485 22.0485 18.3655 22.0485 18.5605 21.8535L19.8535 20.5605C20.0485 20.3645 20.0485 20.0485 19.8535 19.8535L18.8535 18.8535Z"
      fill="black"
    />
  </svg>
)

const IconDelete = (
  <svg
    width="20"
    height="22"
    viewBox="0 0 20 22"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M6 0L5 1H0V3H1V18C1 19.0931 1.90694 20 3 20H11V18H3V3H13V13H15V3H16V1H15H11L10 0H6ZM5 5V16H7V5H5ZM9 5V16H11V5H9ZM13.8789 14.9297L12.4648 16.3438L14.5859 18.4648L12.4648 20.5859L13.8789 22L16 19.8789L18.1211 22L19.5352 20.5859L17.4141 18.4648L19.5352 16.3438L18.1211 14.9297L16 17.0508L13.8789 14.9297Z"
      fill="black"
    />
  </svg>
)

const Container = styled.div`
  width: 238px;
  height: 135px;
  background-color: #ffffff;
  display: flex;
  flex-direction: column;
  border-radius: 10px;
  box-shadow: 0px 2px 4px #eff2f7;
  position: absolute;
  z-index: 1;
`
const Item = styled.div`
  display: flex;
  flex: 1;
  align-items: center;
  border-bottom: 1px solid #f0f3f7;
  padding: 0 20px;

  &:last-child {
    border-bottom: 1px solid transparent;
  }

  & > i {
    margin-right: 18px;
  }
`

// interface
interface ContextProps {
  position?: any
  setshowContext: Function
}

const Context: React.FC<ContextProps> = ({ position, setshowContext }) => {
  const mainRef = useRef(null)
  useInitialFocus(mainRef)

  return (
    <Container
      className="test"
      style={{
        top: position.y,
        left: position.x
      }}
      onBlur={() => setshowContext(false)}
      ref={mainRef}
      tabIndex={-1}
    >
      <Item>
        <i>{IconApprove}</i>
        <span>Approve</span>
      </Item>
      <Item>
        <i>{IconEdit}</i>
        <span>Edit</span>
      </Item>
      <Item>
        <i>{IconDelete}</i>
        <span>Delete</span>
      </Item>
    </Container>
  )
}

export default Context
