import React from "react"
import ReactTable from "react-table"
import withFixedColumns from "react-table-hoc-fixed-columns"
import Box from "@material-ui/core/Box"

import "./styles/index.scss"
import "react-table-hoc-fixed-columns/lib/styles.css"

const ReactTableFixedColumns = withFixedColumns(ReactTable)

// interface
interface TableDataProps {
  data: Record<string, any>
  columns: Record<string, any>
  children?: Record<string, any>
  size?: number
  minRows?: number
  action?: Function
  rowClick?: Function
  isCollapsed?: boolean
  hideToggle?: boolean
  filtered?: Record<string, any>
  onFilteredChange?: Function
  position?: any
  showPagination?: boolean
  getTrProps?: any
  isFilterable?: boolean
  tableClass?: string
  setRef?: Function
  hasHeaderGroup?: boolean
}

const TableData: React.FC<TableDataProps> = ({
  data,
  columns,
  children,
  isFilterable,
  showPagination = true,
  minRows,
  hasHeaderGroup,
  tableClass = `-striped -highlight ${hasHeaderGroup ? "hasHeaderGroup" : ""}`
}) => {
  const filterCaseInsensitive = ({ id, value }: any, row: any) =>
    row[id] ? row[id].toLowerCase().includes(value.toLowerCase()) : true

  return (
    <>
      <Box>
        {children && (
          <Box
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "space-between"
            }}
          >
            {children}
          </Box>
        )}
        <ReactTableFixedColumns
          className={tableClass}
          data={data}
          columns={columns}
          minRows={minRows}
          defaultPageSize={10}
          pageSizeOptions={[5, 10]}
          showPagination={showPagination}
          getHeaderProps={() => ({ style: { padding: 0 } })}
          defaultFilterMethod={filterCaseInsensitive}
          filterable={isFilterable}
        />
      </Box>
    </>
  )
}

export default TableData
