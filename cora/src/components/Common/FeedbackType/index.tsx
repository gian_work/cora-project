import React, { useState } from "react"
import FormControl from "@material-ui/core/FormControl"
import Box from "@material-ui/core/Box"
import Select from "@material-ui/core/Select"
import FormHelperText from "@material-ui/core/FormHelperText"
import MenuItem from "@material-ui/core/MenuItem"
// import {useCookies} from "react-cookie"

import Label from "components/Forms/Label"

// service
// import vmsAPI from "services/Dashboard/VMS"

interface NamesProps {
  value: string
  onChange: Function
  onBlur?: Function
  error?: boolean
  helperText?: string
}

const Names: React.FC<NamesProps> = ({
  value,
  onChange,
  onBlur,
  // condoId,
  helperText,
  error = false
}) => {
  // const [units, setUnits] = useState([])
  const [isLoading] = useState(false)
  // const [cookies] = useCookies(["bearer"])

  // const fetchUnits = async () => {
  //   setIsLoading(true)
  //   try {
  //     const { data } = await vmsAPI.getUnits(cookies.bearer, {
  //       "condo_uid": condoId,
  //     })
  //     setUnits(data._data)
  //     setIsLoading(false)
  //     return
  //   } catch (e) {
  //     setIsLoading(false)
  //   }
  // }

  // React.useEffect(() => {
  //   fetchUnits()
  // }, [])

  return (
    <Box>
      <Label label="Feedback Type" />
      <FormControl variant="outlined" error={error}>
        {isLoading ? (
          <div
            style={{
              backgroundColor: "red",
              width: "100%",
              height: "100%",
              position: "absolute",
              top: 0,
              bottom: 0,
              left: 0,
              right: 0,
              display: "flex",
              alignItems: "center",
              justifyContent: "center"
            }}
          >
            Loading
          </div>
        ) : (
          <>
            <Select
              displayEmpty
              value={value}
              onChange={(e) => onChange(e)}
              onBlur={(e) => onBlur && onBlur(e)}
              error={error}
              name="feedbackType"
            >
              <MenuItem value="" disabled>
                Select Feedback Type
              </MenuItem>
              <MenuItem className="feedbacktype-cleanliness" value={1}>
                Cleanliness
              </MenuItem>
              <MenuItem className="feedbacktype-security" value={2}>
                Security
              </MenuItem>
              <MenuItem className="feedbacktype-defects" value={3}>
                Defects
              </MenuItem>
              <MenuItem className="feedbacktype-landscape" value={4}>
                Landscape
              </MenuItem>
              <MenuItem className="feedbacktype-others" value={5}>
                Others
              </MenuItem>
            </Select>
            <FormHelperText>{helperText}</FormHelperText>
          </>
        )}
      </FormControl>
    </Box>
  )
}
export default Names

// const (
//   FeedbackTypeCleanliness int = 1
//   FeedbackTypeSecurity = 2
//   FeedbackTypeDefects = 3
//   FeedbackTypeLandscape = 4
//   FeedbackTypeOthers = 5
// )
