import { makeStyles, Theme, createStyles } from "@material-ui/core/styles"

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: 221,
      fontSize: 13
    },
    inputBase: {
      width: "100%",
      maxHeight: "42px",
      "& > div": {
        padding: "0 !important"
      },
      "& input": {
        height: "42px !important",
        padding: "0 16px 0 16px !important",
        backgroundColor: "rgba(237, 240, 241, 0.2) !important",
        fontSize: 14,
        "&:focus": {
          borderColor: theme.palette.primary.main
        }
      }
    },
    paper: {
      // backgroundColor: "rgba(237, 240, 241, 0.2) !important",
    },
    option: {
      backgroundColor: theme.palette.common.white,
      minHeight: "auto",
      alignItems: "flex-start",
      padding: 8
    }
  })
)

export default useStyles
