import React, { useState } from "react"
import FormControl from "@material-ui/core/FormControl"
import Box from "@material-ui/core/Box"
import Select from "@material-ui/core/Select"
import FormHelperText from "@material-ui/core/FormHelperText"
import MenuItem from "@material-ui/core/MenuItem"
import Cookies from "js-cookie"

import Label from "components/Forms/Label"

/** Service */
import vmsAPI from "services/Dashboard/VMS"

interface BlockUnitProps {
  value: string
  onChange: Function
  onBlur?: Function
  error?: boolean
  helperText?: string | undefined | boolean
}

const BlockUnit: React.FC<BlockUnitProps> = ({
  value,
  onChange,
  onBlur,
  helperText,
  error = false
}) => {
  const [units, setUnits] = useState([])
  const [isLoading, setIsLoading] = useState(false)

  const fetchUnits = async () => {
    setIsLoading(true)
    try {
      const { data } = await vmsAPI.getUnits({
        "condo_uid": Cookies.get("condoUID")
      })
      setUnits(data._data)
      setIsLoading(false)
      return
    } catch (e) {
      setIsLoading(false)
    }
  }

  React.useEffect(() => {
    fetchUnits()
  }, [])

  return (
    <Box>
      <Label label="Block/Unit No." />
      <FormControl variant="outlined" error={error}>
        {isLoading ? (
          <Select
            displayEmpty
            value={value}
            onChange={(e) => onChange(e)}
            onBlur={(e) => onBlur && onBlur(e)}
            error={error}
            name="blockUnit"
          >
            <MenuItem value="" disabled>
              Loading...
            </MenuItem>
          </Select>
        ) : (
          <>
            <Select
              displayEmpty
              value={value}
              onChange={(e) => onChange(e)}
              onBlur={(e) => onBlur && onBlur(e)}
              error={error}
              name="blockUnit"
            >
              <MenuItem value="" disabled>
                Select Block/Unit No.
              </MenuItem>
              {units.map((unit: any) => {
                return (
                  <MenuItem className={unit._uid} value={unit._uid}>
                    {unit.short_name}
                  </MenuItem>
                )
              })}
            </Select>
            <FormHelperText>{helperText}</FormHelperText>
          </>
        )}
      </FormControl>
    </Box>
  )
}
export default BlockUnit
