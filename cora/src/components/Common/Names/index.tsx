import React, { useState } from "react"
import FormControl from "@material-ui/core/FormControl"
import Box from "@material-ui/core/Box"
import Select from "@material-ui/core/Select"
import FormHelperText from "@material-ui/core/FormHelperText"
import MenuItem from "@material-ui/core/MenuItem"
import Cookies from "js-cookie"

import Label from "components/Forms/Label"

/** Service */
import UnitManagementAPI from "services/Dashboard/UnitManagement"

interface NamesProps {
  value: string
  onChange: Function
  onBlur?: Function
  unitUID: string
  error?: boolean
  helperText?: string | boolean | undefined
  forID?: boolean
  disabled?: boolean
}

const Names: React.FC<NamesProps> = ({
  value,
  onChange,
  onBlur,
  unitUID,
  helperText,
  forID = false,
  error = false,
  disabled = false
}) => {
  const [names, setNames] = useState([])
  const [isLoading, setIsLoading] = useState(false)
  const CondoUID = Cookies.get("condoUID")

  const fetchResidents = async () => {
    setIsLoading(true)
    try {
      /** Params: condoUId / unitUID */
      const response = await UnitManagementAPI.getResidentsByUnit({
        "condo_uid": CondoUID,
        "unit_uid": unitUID
      })
      setNames(response.data._data)
      setIsLoading(false)
      return
    } catch (e) {
      setIsLoading(false)
    }
  }

  React.useEffect(() => {
    fetchResidents()
  }, [unitUID])

  return (
    <Box>
      <Label label="Name" />
      <FormControl variant="outlined" error={error} disabled={disabled}>
        {isLoading ? (
          <Select
            displayEmpty
            value={value}
            onChange={(e) => onChange(e)}
            onBlur={(e) => onBlur && onBlur(e)}
            error={error}
            name="blockUnit"
          >
            <MenuItem value="" disabled>
              Loading...
            </MenuItem>
          </Select>
        ) : (
          <>
            <Select
              displayEmpty
              value={value}
              onChange={(e) => onChange(e)}
              onBlur={(e) => onBlur && onBlur(e)}
              error={error}
              name="residentName"
            >
              <MenuItem value="" disabled>
                Select Resident Name
              </MenuItem>
              {names?.map(
                (name: any) =>
                  name.account_info_uid !== "" && (
                    <MenuItem
                      className={name._uid}
                      value={forID ? name.account_info_uid : name.name}
                      data-userInfo={JSON.stringify(name)}
                    >
                      {name.name}
                    </MenuItem>
                  )
              )}
            </Select>
            <FormHelperText>{helperText}</FormHelperText>
          </>
        )}
      </FormControl>
    </Box>
  )
}
export default Names
