import React from "react"
import Box from "@material-ui/core/Box"

/** Icons */
const IconImage = (
  <svg
    width="28"
    height="22"
    viewBox="0 0 28 22"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M3.33329 0.333344C1.87588 0.333344 0.666626 1.54259 0.666626 3.00001V19C0.666626 20.4574 1.87588 21.6667 3.33329 21.6667H24.6666C26.124 21.6667 27.3333 20.4574 27.3333 19V3.00001C27.3333 1.54259 26.124 0.333344 24.6666 0.333344H3.33329ZM3.33329 3.00001H24.6666V19H3.33329V3.00001ZM11.3333 5.66668C10.9797 5.66668 10.6405 5.80715 10.3905 6.0572C10.1404 6.30725 9.99996 6.64639 9.99996 7.00001C9.99996 7.35363 10.1404 7.69277 10.3905 7.94282C10.6405 8.19287 10.9797 8.33334 11.3333 8.33334C11.6869 8.33334 12.0261 8.19287 12.2761 7.94282C12.5262 7.69277 12.6666 7.35363 12.6666 7.00001C12.6666 6.64639 12.5262 6.30725 12.2761 6.0572C12.0261 5.80715 11.6869 5.66668 11.3333 5.66668ZM17.3333 9.66668L12.6666 15L9.33329 11.6667L5.70308 16.3333H22.3333L17.3333 9.66668Z"
      fill="#454B57"
    />
  </svg>
)

const IconDownload = (
  <svg
    width="20"
    height="20"
    viewBox="0 0 20 20"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M8.5 0C7.68294 0 7 0.682937 7 1.5V8H2.58594L10 15.4141L17.4141 8H13V1.5C13 0.682937 12.3171 0 11.5 0H8.5ZM9 2H11V10H12.5859L10 12.5859L7.41406 10H9V2ZM0 18V20H20V18H0Z"
      fill="#454B57"
    />
  </svg>
)

// const IconDelete = (
//   <svg
//     width="18"
//     height="20"
//     viewBox="0 0 18 20"
//     fill="none"
//     xmlns="http://www.w3.org/2000/svg"
//   >
//     <path
//       d="M7 0L6 1H0V3H1.10938L2.90234 18.2344C3.01959 19.234 3.882 20 4.88867 20H13.1113C14.118 20 14.9804 19.234 15.0977 18.2344L16.8906 3H18V1H16H12L11 0H7ZM3.12305 3H4H14.877L13.1113 18H4.88867L3.12305 3ZM6 5V15C6 15.552 6.448 16 7 16H8V5H6ZM10 5V16H11C11.552 16 12 15.552 12 15V5H10Z"
//       fill="#777E86"
//     />
//   </svg>
// )

interface AttachmentProps {
  fileName: string
  url: string
  encoding?: string
}

const Attachment: React.FC<AttachmentProps> = ({ fileName, url }) => {
  return (
    <Box
      border="1px solid #F2F2F2"
      borderRadius="10px"
      width="100%"
      height="80px"
      display="flex"
      justifyContent="space-between"
      alignItems="center"
      padding="0 40px"
      marginBottom="10px"
      key={fileName}
    >
      <Box display="flex" alignItems="center" maxWidth="80%">
        <Box marginRight="20px">{IconImage}</Box>
        <Box
          // maxWidth="80%"
          whiteSpace="nowrap"
          overflow="hidden"
          textOverflow="ellipsis"
        >
          {fileName}
        </Box>
      </Box>

      <Box>
        <a href={url} target="_blank" rel="noopener noreferrer">
          {IconDownload}
        </a>
      </Box>
    </Box>
  )
}
export default Attachment
