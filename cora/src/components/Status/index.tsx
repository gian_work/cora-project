import * as React from "react"
import Box from "@material-ui/core/Box"

const Approved = (
  <>
    <svg
      width="21"
      height="20"
      viewBox="0 0 21 20"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M10 0C4.486 0 0 4.486 0 10C0 15.514 4.486 20 10 20C15.514 20 20 15.514 20 10C20 8.874 19.804 7.7942 19.459 6.7832L17.8398 8.40234C17.9448 8.91834 18 9.453 18 10C18 14.411 14.411 18 10 18C5.589 18 2 14.411 2 10C2 5.589 5.589 2 10 2C11.633 2 13.1519 2.49389 14.4199 3.33789L15.8516 1.90625C14.2036 0.71225 12.185 0 10 0ZM19.293 1.29297L9 11.5859L5.70703 8.29297L4.29297 9.70703L9 14.4141L20.707 2.70703L19.293 1.29297Z"
        fill="#18A20C"
      />
    </svg>
    <span style={{ paddingLeft: "5px", fontWeight: 700 }}>APPROVED</span>
  </>
)

const Editing = (
  <>
    <svg
      width="20"
      height="22"
      viewBox="0 0 20 22"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M2 0C0.9 0 0 0.9 0 2V18C0 19.1 0.9 20 2 20H13.1719L11.1719 18H2V2H9V7H14V11.1719L16 13.1719V6L10 0H2ZM11 13V15L16.1465 20.1465L18.1465 18.1465L13 13H11ZM18.8535 18.8535L16.8535 20.8535L17.8535 21.8535C18.0485 22.0485 18.3655 22.0485 18.5605 21.8535L19.8535 20.5605C20.0485 20.3645 20.0485 20.0485 19.8535 19.8535L18.8535 18.8535Z"
        fill="#006AFF"
      />
    </svg>
    <span style={{ paddingLeft: "5px", fontWeight: 700 }}>EDITING</span>
  </>
)

const Pending = (
  <>
    <svg
      width="23"
      height="22"
      viewBox="0 0 23 22"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M11.0006 0.015625C10.6075 0.015625 10.2144 0.158812 9.92838 0.445312L0.445953 9.92773C-0.146047 10.5197 -0.146047 11.4813 0.445953 12.0723L9.92838 21.5566C10.2144 21.8426 10.5956 22 11.0006 22C11.4056 22 11.7859 21.8426 12.0729 21.5566L21.5573 12.0723C22.1493 11.4803 22.1493 10.5187 21.5573 9.92773L12.0729 0.445312C11.7864 0.158812 11.3938 0.015625 11.0006 0.015625ZM11.0006 2.19922L19.8014 11L11.0006 19.8008L2.19986 11L11.0006 2.19922ZM10.0006 6V12H12.0006V6H10.0006ZM10.0006 14V16H12.0006V14H10.0006Z"
        fill="#FF0000"
      />
    </svg>
    <span style={{ paddingLeft: "5px", fontWeight: 700 }}>PENDING</span>
  </>
)

// interface
interface StatusProps {
  type: number
}

const Status: React.FC<StatusProps> = ({ type }) => {
  const Icon = () => {
    let value
    switch (type) {
      case 1:
        value = Approved
        break
      case 2:
        value = Editing
        break
      case 3:
        value = Pending
        break
      default:
        break
    }
    return value
  }

  return (
    <>
      <Box display="flex" alignItems="center">
        {Icon()}
      </Box>
    </>
  )
}

export default Status
