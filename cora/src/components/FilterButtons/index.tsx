import React from "react"
import Box from "@material-ui/core/Box"
import { makeStyles, Theme } from "@material-ui/core/styles"

const useStyles = makeStyles((theme: Theme) => ({
  container: {
    position: "relative",
    background: "transparent",
    borderRadius: "28px",
    height: "48px",
    border: "none",
    display: "flex",
    alignItems: "center",
    cursor: "pointer",
    width: "auto",
    marginBottom: "10px",
    "&.active": {
      background: theme.palette.body.orange,
      color: theme.palette.common.white
    }
  },
  name: {
    fontWeight: 500,
    fontSize: "15px",
    lineHeight: "20px",
    color: theme.palette.primary.main,
    flex: 1,
    padding: "0 16px",
    textAlign: "center",
    "&.active": {
      color: theme.palette.common.white
    }
  },
  mediumWidth: {
    minWidth: "140px"
  },
  smallWidth: {
    minWidth: "95px"
  },
  largeWidth: {
    minWidth: "180px"
  }
}))

// interface
interface FilterButtonsProps {
  label: string
  onClick: Function
  isActive?: boolean
  style?: any
}

const FilterButtons: React.FC<FilterButtonsProps> = ({
  label,
  onClick,
  isActive,
  style
}) => {
  const { container, name, mediumWidth, largeWidth, smallWidth } = useStyles()

  const activeClass = isActive ? "active" : "container"
  const lengthClass = (labelLength: number) => {
    if (labelLength > 10 && labelLength < 16) {
      return mediumWidth
    }
    if (labelLength > 16) {
      return largeWidth
    }
    return smallWidth
  }
  const classes = `${container} ${activeClass} ${lengthClass(label.length)}`
  return (
    <Box className={classes} onClick={() => onClick()} style={style}>
      <Box className={`${name} ${activeClass}`}>{label}</Box>
    </Box>
  )
}

export default FilterButtons
