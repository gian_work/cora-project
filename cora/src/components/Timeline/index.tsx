import * as React from "react"
import styled from "@emotion/styled"

const Container = styled.div`
  width: 364px;
  height: 395px;
  background: #ffffff;
  border: 1px solid #f5f8fc;
  box-shadow: 0px 2px 4px #eff2f7;
  border-radius: 10px;
  padding: 25px 15px;
  display: flex;
  flex-direction: column;
  position: relative;
  z-index: 1;
  overflow: hidden;
  transition: all 130ms ease-in;

  &.collapsed {
    height: 80px;
    transition: all 130ms ease-out;
  }
`
const Head = styled.div`
  display: flex;
  justify-content: space-between;
  position: relative;
  z-index: 1;
`
const Title = styled.div`
  font-weight: bold;
  font-size: 16px;
  line-height: 24px;
  text-transform: uppercase;
  color: #3b4859;
`
const Count = styled.div`
  border-radius: 50%;
  background-color: #ff0000;
  color: #ffffff;
  min-width: 30px;
  min-height: 30px;
  display: flex;
  justify-content: center;
  align-items: center;
  font-weight: bold;
  font-size: 16px;
  line-height: 24px;
`
const Body = styled.div`
  flex: 1;
  position: relative;
  z-index: 1;
  margin-top: 20px;
`
const Item = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: 20px;
`
const Complaint = styled.div`
  font-weight: bold;
  font-size: 16px;
  line-height: 24px;
`
const Description = styled.div`
  font-weight: normal;
  font-size: 12px;
  line-height: 16px;
`
const Date = styled.div`
  font-weight: 300;
  font-size: 10px;
  line-height: 24px;
`
const Image = styled.div`
  min-width: 55px;
  min-height: 55px;
  border-radius: 50%;
  background-color: #e1e1e1;
  margin: 0 20px 0 15px;
`
const Button = styled.button`
  background: #006eff;
  border-radius: 5px;
  color: #ffffff;
  height: 38px;
  display: flex;
  align-items: center;
  justify-content: center;
  font-weight: 500;
  font-size: 14px;
  line-height: 17px;
  width: 100%;
`
const Footer = styled.div`
  height: 40px;
  position: relative;
  z-index: 1;
`
const Line = styled.div`
  position: absolute;
  height: 250px;
  width: 5px;
  z-index: 0;
  top: 60px;
  left: 110px;
  background: #c4c4c4;
  opacity: 0.3;
  border-radius: 10px;
`

// interface
interface TimelineProps {
  title: string
  isCollapsed?: boolean
}

const Timeline: React.FC<TimelineProps> = ({ title, isCollapsed }) => {
  const url = "http://lorempixel.com/400/200/"
  const stateClass = isCollapsed ? "collapsed" : ""
  return (
    <Container className={stateClass}>
      <Head>
        <Title>{title}</Title>
        <Count>12</Count>
      </Head>
      {!isCollapsed ? (
        <>
          <Body>
            <Item>
              <Date>12/04/2019</Date>
              <Image
                style={{
                  backgroundImage: `url(${url})`,
                  backgroundSize: "cover"
                }}
              />
              <div>
                <Complaint>Pool Complaints</Complaint>
                <Description>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit. ...
                </Description>
              </div>
            </Item>
            <Item>
              <Date>12/04/2019</Date>
              <Image
                style={{
                  backgroundImage: `url(${url})`,
                  backgroundSize: "cover"
                }}
              />
              <div>
                <Complaint>Pool Complaints</Complaint>
                <Description>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit. ...
                </Description>
              </div>
            </Item>
            <Item>
              <Date>12/04/2019</Date>
              <Image
                style={{
                  backgroundImage: `url(${url})`,
                  backgroundSize: "cover"
                }}
              />
              <div>
                <Complaint>Pool Complaints</Complaint>
                <Description>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit. ...
                </Description>
              </div>
            </Item>
          </Body>
          <Footer>
            <Button>View All</Button>
          </Footer>
          <Line />
        </>
      ) : null}
    </Container>
  )
}

export default Timeline
