import * as React from "react"

const DownloadCSV: React.FC = () => {
  return (
    <svg
      width="24"
      height="25"
      viewBox="0 0 24 25"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path d="M11.7214 17.2295L4.47914 10.0085H8.79092V3.67773C8.79092 2.88195 9.45803 2.2168 10.2562 2.2168H13.1866C13.9848 2.2168 14.6519 2.88195 14.6519 3.67773V10.0085H18.9637L11.7214 17.2295ZM11.7214 14.475L14.2474 11.9564H12.6982V4.16471H10.7446V11.9564H9.19538L11.7214 14.475ZM1.95312 20.1513V22.0992H21.4897V20.1513H1.95312Z" />
    </svg>
  )
}

export default DownloadCSV
