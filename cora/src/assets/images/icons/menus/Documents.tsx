import * as React from "react"

const Documents: React.FC = () => {
  return (
    <svg
      width="19"
      height="22"
      viewBox="0 0 19 22"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M2.24888 0C1.18817 0 0.320312 0.9 0.320312 2V16H2.24888V2H13.8203V0H2.24888ZM6.10603 4C5.04531 4 4.17746 4.9 4.17746 6V20C4.17746 21.1 5.04531 22 6.10603 22H16.7132C17.7739 22 18.6417 21.1 18.6417 20V9L13.8203 4H6.10603ZM6.10603 6H12.856V10H16.7132V20H6.10603V6ZM8.0346 12V14H14.7846V12H8.0346ZM8.0346 16V18H14.7846V16H8.0346Z"
        fill="white"
      />
    </svg>
  )
}

export default Documents
