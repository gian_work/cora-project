import * as React from "react"

const ChequePayment: React.FC = () => {
  return (
    <svg
      width="30"
      height="18"
      viewBox="0 0 30 18"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <rect
        x="1.75"
        y="1"
        width="27"
        height="15.3929"
        rx="2"
        stroke="white"
        strokeWidth="2"
      />
      <ellipse cx="5.49443" cy="4.80609" rx="1.85185" ry="1.91327" fill="white" />
      <line
        x1="7.38086"
        y1="8.65308"
        x2="24.0475"
        y2="8.65308"
        stroke="white"
        strokeWidth="2"
      />
      <line
        x1="7.38086"
        y1="12.5229"
        x2="24.0475"
        y2="12.5229"
        stroke="white"
        strokeWidth="2"
      />
    </svg>
  )
}

export default ChequePayment
