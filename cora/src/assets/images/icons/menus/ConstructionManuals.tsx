import * as React from "react"

const ConstructionManuals: React.FC = () => {
  return (
    <svg
      width="18"
      height="18"
      viewBox="0 0 18 18"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M2.24888 0C1.19486 0 0.320312 0.906937 0.320312 2V16C0.320312 17.0931 1.19486 18 2.24888 18H15.7489C16.8029 18 17.6775 17.0931 17.6775 16V2C17.6775 0.906937 16.8029 0 15.7489 0H11.8917V2H15.7489V8H13.8203V10H15.7489V16H8.99889V10H10.9275V8H5.14174V10H7.07031V16H2.24888V2H6.67104L9.55448 4.99023L10.918 3.57617L7.46959 0H2.24888Z"
        fill="white"
      />
    </svg>
  )
}

export default ConstructionManuals
