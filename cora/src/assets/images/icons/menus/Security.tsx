import * as React from "react"

const Security: React.FC = () => {
  return (
    <svg
      width="18"
      height="22"
      viewBox="0 0 18 22"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M8.86218 0L0.714844 4C0.714844 4 0.714844 8 0.714844 10C0.714844 17.83 6.54381 21.486 8.86218 22C11.1805 21.486 17.0095 17.83 17.0095 10C17.0095 8 17.0095 4 17.0095 4L8.86218 0ZM15.199 10C15.199 16.134 10.8673 19.254 8.86218 19.937C6.85703 19.254 2.52536 16.134 2.52536 10V5.3L8.86218 2.189L15.199 5.3V10Z"
        fill="white"
      />
    </svg>
  )
}

export default Security
