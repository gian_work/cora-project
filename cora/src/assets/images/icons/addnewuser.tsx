import * as React from "react"

const AddNewUser: React.FC = () => {
  return (
    <svg
      width="21"
      height="21"
      viewBox="0 0 21 21"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path d="M10.4271 0.947998C5.04791 0.947998 0.6875 5.30841 0.6875 10.6876C0.6875 16.0668 5.04791 20.4272 10.4271 20.4272C15.8063 20.4272 20.1667 16.0668 20.1667 10.6876C20.1667 5.30841 15.8063 0.947998 10.4271 0.947998ZM15.2969 11.6615H11.401V15.5574H9.45312V11.6615H5.55729V9.71362H9.45312V5.81779H11.401V9.71362H15.2969V11.6615Z" />
    </svg>
  )
}

export default AddNewUser
