import React from "react"
import Box from "@material-ui/core/Box"

import { withRouter, RouteComponentProps } from "react-router-dom"

// components
import LoginCard from "./LoginCard"
import Brand from "./Brand"

const useStyles = {
  container: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    width: "100%",
    height: "100vh"
  },
  section: {
    display: "flex",
    height: "100%",
    flex: "1"
  }
}

interface LoginProps extends RouteComponentProps {
  userData: Record<string, string | number>
  setUserData: Function
}

// const Login: React.FC<LoginProps> = props => {
//   const {container, section} = useStyles()
//   console.log(props)
//   return (
//     <Box className={container}>
//       <Box className={section}>
//         <Brand />
//       </Box>
//       <Box className={section} justifyContent="center" alignItems="center">
//         <LoginCard {...props} />
//       </Box>
//     </Box>
//   )
// }

const Login: React.FC<LoginProps> = () => {
  const { container, section } = useStyles
  return (
    <Box style={container}>
      <Box style={section}>
        <Brand />
      </Box>
      <Box style={section} justifyContent="center" alignItems="center">
        <LoginCard />
      </Box>
    </Box>
  )
}

export default withRouter(Login)
