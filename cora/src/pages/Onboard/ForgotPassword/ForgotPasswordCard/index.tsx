import React, { useState } from "react"
import Box from "@material-ui/core/Box"
import Button from "@material-ui/core/Button"
import { makeStyles, Theme } from "@material-ui/core/styles"

// service
import OnboardAPI from "services/Onboard"

// component
import Header from "./Header"
import Form from "./Form"

const useStyles = makeStyles((theme: Theme) => ({
  container: {
    maxWidth: "412px",
    marginBottom: "40px"
  },
  forgot: {
    color: theme.palette.primary.main,
    fontSize: "16px",
    display: "flex",
    flexDirection: "column",
    padding: "0 28px"
  },
  copyright: {
    color: theme.palette.body.dark,
    fontSize: "12px"
  },
  ctaButton: {
    width: "100%"
  }
}))

const LoginCard: React.FC = () => {
  const { container, copyright, ctaButton } = useStyles()
  const [condo, setcondo] = useState("")
  const [keepLoggedIn, setkeepLoggedIn] = useState(false)
  const [details, setDetails] = useState({
    email: "",
    pw: "",
    "device_id": "web",
    "method_type": "EMAIL_PASSWORD"
  })

  const handlePasswordReset = (): void => {
    OnboardAPI.forgotPassword(details.email)
      .then(() => [
        setDetails({
          ...details,
          email: ""
        })
      ])
      .catch((err: Error) => err)
  }

  const handleChange = (key: string, value: string): void => {
    setDetails({
      ...details,
      [key]: value
    })
  }

  return (
    <Box display="flex" flexDirection="column" alignItems="center">
      <Box className={container}>
        <Box padding="28px 28px 0">
          <Header />
          <Form
            setcondo={setcondo}
            condo={condo}
            loginDetails={details}
            setloginDetails={setDetails}
            handleChange={handleChange}
            keepLoggedIn={keepLoggedIn}
            setkeepLoggedIn={setkeepLoggedIn}
          />
        </Box>
        {/* CTA  */}
        <Box display="flex" flexDirection="column" padding="28px">
          <Box marginBottom="30px" flex="1">
            <Button
              variant="contained"
              color="primary"
              size="large"
              onClick={(): void => handlePasswordReset()}
              className={ctaButton}
            >
              Submit
            </Button>
          </Box>
        </Box>
      </Box>
      <Box className={copyright}>Copyright © CORA 2020</Box>
    </Box>
  )
}

export default LoginCard
