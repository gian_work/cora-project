import React from "react"
import Box from "@material-ui/core/Box"

import { makeStyles, Theme } from "@material-ui/core/styles"

const styles = makeStyles((theme: Theme) => ({
  title: {
    fontSize: "48px",
    color: theme.palette.body.dark,
    lineHeight: "56px",
    paddingBottom: "15px"
  },
  subtitle: {
    fontSize: "16px",
    color: "#204B82",
    "& span": {
      color: theme.palette.body.dark
    },
    "& a": {
      color: theme.palette.primary.main,
      textDecoration: "none"
    }
  }
}))

const Header: React.FC = () => {
  const { title, subtitle } = styles()
  return (
    <Box marginBottom="30px">
      <Box className={title}>Forgotten your password?</Box>
      <Box className={subtitle}>
        <span>
          {`CORA Support Team will send an email to you. Please check
          SPAM folder. If you do not receive email from CORA Support Team,
          please contact CORA Support Team 123-456-7890`}
        </span>
      </Box>
    </Box>
  )
}
export default Header
