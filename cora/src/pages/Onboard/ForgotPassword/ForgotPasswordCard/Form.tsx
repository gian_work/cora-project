import React from "react"
import Box from "@material-ui/core/Box"
import TextField from "@material-ui/core/TextField"

// components
import FormLabel from "components/Forms/Label"

interface HeaderProps {
  condo: string
  loginDetails: Record<string, string | number>
  keepLoggedIn: boolean
  setloginDetails: Function
  setcondo: Function
  handleChange: Function
  setkeepLoggedIn: Function
}

const Form: React.FC<HeaderProps> = ({ loginDetails, handleChange }) => {
  return (
    <Box>
      <FormLabel label="email" />
      <TextField
        placeholder="email@email.com"
        variant="outlined"
        value={loginDetails.email}
        onChange={(e: React.ChangeEvent<HTMLInputElement>): void =>
          handleChange("email", e.target.value)}
      />
    </Box>
  )
}
export default Form
