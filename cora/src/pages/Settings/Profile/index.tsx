import React from "react"

import SettingsApplicationsProvider from "context/Settings/Profile/main/Provider"
import SettingsApplicationsView from "context/Settings/Profile/main/View"

const SettingsApplications: React.FC = () => {
  return (
    <SettingsApplicationsProvider>
      <SettingsApplicationsView />
    </SettingsApplicationsProvider>
  )
}

export default SettingsApplications
