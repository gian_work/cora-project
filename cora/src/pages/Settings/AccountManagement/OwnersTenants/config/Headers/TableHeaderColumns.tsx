import React from "react"
import Box from "@material-ui/core/Box"
import Checkbox from "components/Forms/Checkbox"

interface RowOriginalProps {
  original: {
    title: string
  }
}

const TableHeaderColumns = (): Array<{}> => [
  {
    fixed: "left",
    disableFilters: true,
    columns: [
      {
        Header: "",
        id: "title",
        accessor: "title",
        Cell: ({ original }: RowOriginalProps): JSX.Element => {
          return <Box padding="0 30px">{original.title}</Box>
        }
      }
    ]
  },
  {
    columns: [
      {
        Header: "Create",
        Cell: (): JSX.Element => {
          return <Checkbox label={null} value onChange={(): null => null} />
        }
      },
      {
        Header: "Edit",
        Cell: (): JSX.Element => {
          return <Checkbox label={null} value={false} onChange={(): null => null} />
        }
      },
      {
        Header: "Remove",
        Cell: (): JSX.Element => {
          return <Checkbox label={null} value={false} onChange={(): null => null} />
        }
      }
    ]
  }
]

export default TableHeaderColumns
