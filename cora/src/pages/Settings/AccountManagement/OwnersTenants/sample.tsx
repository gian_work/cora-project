const SampleData = [
  {
    serialNumber: "1",
    name: "Donovan Mitchell",
    email: "terry@email.com",
    phoneNo: "123-456-7890",
    userRoles: "Condo Executive",
    loginCode: "123456"
  }
]

export default SampleData
