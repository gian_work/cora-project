import React, { useState } from "react"
import Box from "@material-ui/core/Box"
import Checkbox from "components/Forms/Checkbox"

/** Styles */
import styles from "./styles"

interface Props {
  handleUpdate: Function
  data: any
  activeRoleName: string
}

const Announcement: React.FC<Props> = ({ handleUpdate, data, activeRoleName }) => {
  const { category, section } = styles()
  const [title] = useState("payment")

  return (
    <Box display="flex" alignItems="center" className={section}>
      <Box flex="3" className={category}>
        {title}
      </Box>
      <Box flex="1">
        {/* Create */}
        <Checkbox
          label={null}
          value={Boolean(data?.payment_edit)}
          onChange={() =>
            handleUpdate(
              "payment_edit",
              Boolean(!data?.payment_edit),
              activeRoleName
            )}
        />
      </Box>
      <Box flex="1">
        {/* Update */}
        <Checkbox
          label={null}
          value={Boolean(data?.payment_view)}
          onChange={() =>
            handleUpdate(
              "payment_view",
              Boolean(!data?.payment_view),
              activeRoleName
            )}
        />
      </Box>
    </Box>
  )
}
export default Announcement
