import React, { useState } from "react"
import Box from "@material-ui/core/Box"
import Checkbox from "components/Forms/Checkbox"

/** Styles */
import styles from "./styles"

interface Props {
  handleUpdate: Function
  data: any
  activeRoleName: string
}

const Announcement: React.FC<Props> = ({ handleUpdate, data, activeRoleName }) => {
  const { category, section } = styles()
  const [title] = useState("facilities")

  return (
    <Box display="flex" alignItems="center" className={section}>
      <Box flex="3" className={category}>
        {title}
      </Box>
      <Box flex="1">
        {/* Create */}
        <Checkbox
          label={null}
          value={Boolean(data?.facility_edit)}
          onChange={() =>
            handleUpdate(
              "facility_edit",
              Boolean(!data?.facility_edit),
              activeRoleName
            )}
        />
      </Box>
      <Box flex="1">
        {/* Update */}
        <Checkbox
          label={null}
          value={Boolean(data?.facility_view)}
          onChange={() =>
            handleUpdate(
              "facility_view",
              Boolean(!data?.facility_view),
              activeRoleName
            )}
        />
      </Box>
    </Box>
  )
}
export default Announcement
