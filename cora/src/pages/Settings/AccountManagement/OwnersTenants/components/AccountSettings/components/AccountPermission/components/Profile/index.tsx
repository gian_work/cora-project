import React, { useState } from "react"
import Box from "@material-ui/core/Box"
import Checkbox from "components/Forms/Checkbox"

/** Styles */
import styles from "./styles"

interface Props {
  handleUpdate: Function
  data: any
  activeRoleName: string
}

const Announcement: React.FC<Props> = ({ handleUpdate, data, activeRoleName }) => {
  const { category, section } = styles()
  const [title] = useState("profile")

  return (
    <Box display="flex" alignItems="center" className={section}>
      <Box flex="3" className={category}>
        {title}
      </Box>
      <Box flex="1">
        {/* Create */}
        <Checkbox
          label={null}
          value={Boolean(data?.profile_edit)}
          onChange={() =>
            handleUpdate(
              "profile_edit",
              Boolean(!data?.profile_edit),
              activeRoleName
            )}
        />
      </Box>
      <Box flex="1">
        {/* Update */}
        <Checkbox
          label={null}
          value={Boolean(data?.profile_view)}
          onChange={() =>
            handleUpdate(
              "profile_view",
              Boolean(!data?.profile_view),
              activeRoleName
            )}
        />
      </Box>
    </Box>
  )
}
export default Announcement
