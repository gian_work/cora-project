import React from "react"
import Box from "@material-ui/core/Box"

import FormControlLabel from "@material-ui/core/FormControlLabel"
import FormControl from "@material-ui/core/FormControl"
import Radio from "@material-ui/core/Radio"
import RadioGroup from "@material-ui/core/RadioGroup"
import Button from "@material-ui/core/Button"
import { makeStyles } from "@material-ui/core/styles"
import TextField from "@material-ui/core/TextField"

// component
import Drawer from "components/Drawer"
import Header from "components/Header"
import Spacer from "components/Spacer"
import FormLabel from "components/Forms/Label"

const useStyles = makeStyles(() => ({
  root: {
    display: "flex",
    "& .MuiTextField-root": {
      flex: 1
    }
  },
  formControl: {
    width: "100%"
  },
  formControlButtons: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    width: "100%",
    padding: "0 20px"
  },
  formControlButton: {
    width: "49%"
  },
  radioLabel: {
    paddingBottom: "8px"
  }
}))

interface AddUserRoleProps {
  openUserForm: boolean
  setopenUserForm: Function
}

const AddUserRole: React.FC<AddUserRoleProps> = ({
  openUserForm,
  setopenUserForm
}) => {
  const { root, formControlButtons, formControlButton } = useStyles()

  return (
    <Drawer openState={openUserForm} setopenState={setopenUserForm}>
      <Box>
        <Header title="Add User Role" subtitle="user role" subtitleAbove />
        <Box
          padding="30px"
          display="flex"
          flexDirection="column"
          flex="1"
          height="calc(100% - 103px)"
          maxWidth="350px"
        >
          <Box margin="0 0 39px">
            <div style={{ margin: "0 0 8px" }}>
              <FormLabel label="user role name" />
            </div>
            <TextField label="User Role" fullWidth variant="outlined" />
          </Box>
          <Box>
            <div style={{ margin: "0 0 11px" }}>
              <FormLabel label="user role name" />
            </div>
            <RadioGroup
              aria-label="credit type"
              name="creditType"
              value=""
              onChange={() => null}
            >
              <FormControlLabel
                value="1"
                control={<Radio color="primary" />}
                label="Executive"
                labelPlacement="end"
              />
              <FormControlLabel
                value="2"
                control={<Radio color="primary" />}
                label="Officer"
                labelPlacement="end"
              />
              <FormControlLabel
                value="3"
                control={<Radio color="primary" />}
                label="Supervisor"
                labelPlacement="end"
              />
            </RadioGroup>
          </Box>
        </Box>
        <Box
          className={root}
          margin="20px 0"
          borderTop="1px solid #F2F2F2"
          padding="30px 0 0"
        >
          <FormControl className={formControlButtons}>
            <Button
              size="large"
              color="primary"
              className={formControlButton}
              onClick={() => null}
            >
              CANCEL
            </Button>
            <Spacer isDefault />
            <Button
              size="large"
              variant="contained"
              color="primary"
              className={formControlButton}
            >
              SAVE
            </Button>
          </FormControl>
        </Box>
      </Box>
    </Drawer>
  )
}

export default AddUserRole
