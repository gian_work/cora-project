import React, { useState } from "react"
import Box from "@material-ui/core/Box"
import Checkbox from "components/Forms/Checkbox"

/** Styles */
import styles from "./styles"

interface Props {
  handleUpdate: Function
  data: any
  activeRoleName: string
}

const Announcement: React.FC<Props> = ({ handleUpdate, data, activeRoleName }) => {
  const { category, section } = styles()
  const [title] = useState("applications")

  return (
    <Box display="flex" alignItems="center" className={section}>
      <Box flex="3" className={category}>
        {title}
      </Box>
      <Box flex="1">
        {/* Create */}
        <Checkbox
          label={null}
          value={Boolean(data?.application_edit)}
          onChange={() =>
            handleUpdate(
              "application_edit",
              Boolean(!data?.application_edit),
              activeRoleName
            )}
        />
      </Box>
      <Box flex="1">
        {/* Update */}
        <Checkbox
          label={null}
          value={Boolean(data?.application_view)}
          onChange={() =>
            handleUpdate(
              "application_view",
              Boolean(!data?.application_view),
              activeRoleName
            )}
        />
      </Box>
    </Box>
  )
}
export default Announcement
