import React from "react"

import AddAdminProvider from "context/Settings/AccountManagement/Admin/forms/admin/Provider"
import AddAdminView from "context/Settings/AccountManagement/Admin/forms/admin/View"

const AddAdmin: React.FC = () => (
  <AddAdminProvider>
    <AddAdminView />
  </AddAdminProvider>
)

export default AddAdmin
