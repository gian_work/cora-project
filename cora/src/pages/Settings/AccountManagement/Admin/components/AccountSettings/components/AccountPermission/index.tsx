import React from "react"
import Box from "@material-ui/core/Box"
import Button from "@material-ui/core/Button"
import { toast } from "react-toastify"
import Fade from "@material-ui/core/Fade"

/** Service */
import service from "services/Settings/AccountManagement/Admin"
/** Categories */
import { CtxType } from "context/Settings/AccountManagement/Admin/forms/accountSettings/Context"
import withContext from "context/Settings/AccountManagement/Admin/forms/accountSettings/withContext"
import Loader from "components/Loader"
import Announcement from "./components/Announcement"
import Applications from "./components/Applications"
import Attendance from "./components/Attendance"
import Bookkeeping from "./components/Bookkeeping"
import EstateMatters from "./components/EstateMatters"
import Feedback from "./components/Feedback"
import PaymentSchedule from "./components/PaymentSchedule"
import UnitManagement from "./components/UnitManagement"
import VisitorManagement from "./components/VisitorManagement"

/** Styles */
import styles from "./styles"

const AccountPermission: React.FC<CtxType> = ({
  payloads,
  setPayloads,
  activeRole,
  isFetchingRoles
}) => {
  const { sectionHead, sectionItems } = styles()

  /** Notification */
  const notifyUpdate = () =>
    toast("Successfully updated the permission.", {
      type: toast.TYPE.SUCCESS
    })

  function handleUpdate(key: string, value: boolean) {
    setPayloads &&
      setPayloads({
        ...payloads,
        [key]: value
      })
  }

  async function updatePermissions() {
    try {
      const response = await service.updatePermission(payloads, activeRole)
      notifyUpdate()
      return response
    } catch (e) {
      return e
    }
  }

  if (isFetchingRoles) {
    return <Loader dark />
  }

  return (
    <Box>
      <Box display="flex" className={sectionHead}>
        <Box flex="3" />
        <Box flex="1">Create</Box>
        <Box flex="1">Edit</Box>
        <Box flex="1">Remove</Box>
      </Box>

      <Fade in={!isFetchingRoles} timeout={1000}>
        <Box className={sectionItems}>
          <Box>
            <Attendance data={payloads} handleUpdate={handleUpdate} />
            <Announcement data={payloads} handleUpdate={handleUpdate} />
            <Applications data={payloads} handleUpdate={handleUpdate} />
            <Bookkeeping data={payloads} handleUpdate={handleUpdate} />
            <Feedback data={payloads} handleUpdate={handleUpdate} />
            <UnitManagement data={payloads} handleUpdate={handleUpdate} />
            <VisitorManagement data={payloads} handleUpdate={handleUpdate} />
            <EstateMatters data={payloads} handleUpdate={handleUpdate} />
            <PaymentSchedule data={payloads} handleUpdate={handleUpdate} />
          </Box>
        </Box>
      </Fade>
      <Box
        padding="30px 40px"
        display="flex"
        justifyContent="space-between"
        alignItems="center"
      >
        <Box>
          <div />
        </Box>
        <Box width="150px">
          <Button
            fullWidth
            variant="contained"
            color="primary"
            size="large"
            onClick={() => updatePermissions()}
          >
            SAVE
          </Button>
        </Box>
      </Box>
    </Box>
  )
}
export default withContext(AccountPermission)
