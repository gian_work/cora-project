import React, { useState } from "react"
import Box from "@material-ui/core/Box"
import Checkbox from "components/Forms/Checkbox"

/** Styles */
import styles from "./styles"

interface Props {
  handleUpdate: Function
  data: any
}

const PaymentSchedule: React.FC<Props> = ({ handleUpdate, data }) => {
  const { category, section } = styles()
  const [title] = useState("payment schedule")

  return (
    <Box display="flex" alignItems="center" className={section}>
      <Box flex="3" className={category}>
        {title}
      </Box>
      <Box flex="1">
        {/* Create */}
        <Checkbox
          label={null}
          value={Boolean(data?.payment_schedule_create)}
          onChange={() =>
            handleUpdate(
              "payment_schedule_create",
              Boolean(!data?.payment_schedule_create)
            )}
        />
      </Box>
      <Box flex="1">
        {/* Update */}
        <Checkbox
          label={null}
          value={Boolean(data?.payment_schedule_edit)}
          onChange={() =>
            handleUpdate(
              "payment_schedule_edit",
              Boolean(!data?.payment_schedule_edit)
            )}
        />
      </Box>
      <Box flex="1">
        {/* Delete */}
        <Checkbox
          label={null}
          value={Boolean(data?.payment_schedule_remove)}
          onChange={() =>
            handleUpdate(
              "payment_schedule_remove",
              Boolean(!data?.payment_schedule_remove)
            )}
        />
      </Box>
    </Box>
  )
}
export default PaymentSchedule
