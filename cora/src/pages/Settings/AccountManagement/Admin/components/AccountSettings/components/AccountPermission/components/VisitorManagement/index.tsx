import React, { useState } from "react"
import Box from "@material-ui/core/Box"
import Checkbox from "components/Forms/Checkbox"

/** Styles */
import styles from "./styles"

interface Props {
  handleUpdate: Function
  data: any
}

const VisitorManagement: React.FC<Props> = ({ handleUpdate, data }) => {
  const { category, section } = styles()
  const [title] = useState("visitor management")

  return (
    <Box display="flex" alignItems="center" className={section}>
      <Box flex="3" className={category}>
        {title}
      </Box>
      <Box flex="1">
        {/* Create */}
        <Checkbox
          label={null}
          value={Boolean(data?.vms_create)}
          onChange={() => handleUpdate("vms_create", Boolean(!data?.vms_create))}
        />
      </Box>
      <Box flex="1">
        {/* Update */}
        <Checkbox
          label={null}
          value={Boolean(data?.vms_edit)}
          onChange={() => handleUpdate("vms_edit", Boolean(!data?.vms_edit))}
        />
      </Box>
      <Box flex="1">
        {/* Delete */}
        <Checkbox
          label={null}
          value={Boolean(data?.vms_remove)}
          onChange={() => handleUpdate("vms_remove", Boolean(!data?.vms_remove))}
        />
      </Box>
    </Box>
  )
}
export default VisitorManagement
