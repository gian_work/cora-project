import React, { useState } from "react"
import Box from "@material-ui/core/Box"
import Checkbox from "components/Forms/Checkbox"

/** Styles */
import styles from "./styles"

interface Props {
  handleUpdate: Function
  data: any
}

const Announcement: React.FC<Props> = ({ handleUpdate, data }) => {
  const { category, section } = styles()
  const [title] = useState("announcement")

  return (
    <Box display="flex" alignItems="center" className={section}>
      <Box flex="3" className={category}>
        {title}
      </Box>
      <Box flex="1">
        {/* Create */}
        <Checkbox
          label={null}
          value={Boolean(data?.announcement_create)}
          onChange={() =>
            handleUpdate("announcement_create", Boolean(!data?.announcement_create))}
        />
      </Box>
      <Box flex="1">
        {/* Update */}
        <Checkbox
          label={null}
          value={Boolean(data?.announcement_edit)}
          onChange={() =>
            handleUpdate("announcement_edit", Boolean(!data?.announcement_edit))}
        />
      </Box>
      <Box flex="1">
        {/* Delete */}
        <Checkbox
          label={null}
          value={Boolean(data?.announcement_remove)}
          onChange={() =>
            handleUpdate("announcement_remove", Boolean(!data?.announcement_remove))}
        />
      </Box>
    </Box>
  )
}
export default Announcement
