import React, { useState } from "react"
import Box from "@material-ui/core/Box"
import Checkbox from "components/Forms/Checkbox"

/** Styles */
import styles from "./styles"

interface Props {
  handleUpdate: Function
  data: any
}

const Feedback: React.FC<Props> = ({ handleUpdate, data }) => {
  const { category, section } = styles()
  const [title] = useState("feedback")

  return (
    <Box display="flex" alignItems="center" className={section}>
      <Box flex="3" className={category}>
        {title}
      </Box>
      <Box flex="1">
        {/* Create */}
        <Checkbox
          label={null}
          value={Boolean(data?.feedback_create)}
          onChange={() =>
            handleUpdate("feedback_create", Boolean(!data?.feedback_create))}
        />
      </Box>
      <Box flex="1">
        {/* Update */}
        <Checkbox
          label={null}
          value={Boolean(data?.feedback_edit)}
          onChange={() =>
            handleUpdate("feedback_edit", Boolean(!data?.feedback_edit))}
        />
      </Box>
      <Box flex="1">
        {/* Delete */}
        <Checkbox
          label={null}
          value={Boolean(data?.feedback_delete)}
          onChange={() =>
            handleUpdate("feedback_delete", Boolean(!data?.feedback_delete))}
        />
      </Box>
    </Box>
  )
}
export default Feedback
