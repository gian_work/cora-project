import React, { FunctionComponent } from "react"
import Box from "@material-ui/core/Box"

/** Components */
import Drawer from "components/Drawer"
import Header from "components/Header"
import Form from "./form"

interface Props {
  openState: boolean
  setopenState: Function
}

const AdminAccountSettingsFormDrawer: FunctionComponent<Props> = ({
  openState,
  setopenState
}) => {
  return (
    <Drawer openState={openState} setopenState={setopenState}>
      <Header title="Add User Role" subtitle="user role" subtitleAbove />
      <Box height="100%">
        <Form />
      </Box>
    </Drawer>
  )
}

export default AdminAccountSettingsFormDrawer
