import React from "react"
import Box from "@material-ui/core/Box"
import Switch from "@material-ui/core/Switch"

/** Styles */
import styles from "./styles"

const Others: React.FC = () => {
  const { sectionHead, section, otherTitle } = styles()
  return (
    <Box>
      <Box display="flex" className={sectionHead}>
        <Box flex="5" className={otherTitle}>
          Other Settings
        </Box>
        <Box flex="1" />
      </Box>

      <Box className={section}>
        <Box flex="5">Manage Condo Settings</Box>
        <Box flex="1">
          <Switch onChange={(): null => null} value color="primary" />
        </Box>
      </Box>
      <Box className={section}>
        <Box flex="5">Mobile Accounts Settings</Box>
        <Box flex="1">
          <Switch onChange={(): null => null} value color="primary" />
        </Box>
      </Box>
      <Box className={section}>
        <Box flex="5">Mobile Accounts Management</Box>
        <Box flex="1">
          <Switch onChange={(): null => null} value color="primary" />
        </Box>
      </Box>
    </Box>
  )
}
export default Others
