import React, { useState } from "react"
import Box from "@material-ui/core/Box"
import Checkbox from "components/Forms/Checkbox"

/** Styles */
import styles from "./styles"

interface Props {
  handleUpdate: Function
  data: any
}

const Bookkeeping: React.FC<Props> = ({ handleUpdate, data }) => {
  const { category, section } = styles()
  const [title] = useState("bookkeeping")

  return (
    <Box display="flex" alignItems="center" className={section}>
      <Box flex="3" className={category}>
        {title}
      </Box>
      <Box flex="1">
        {/* Create */}
        <Checkbox
          label={null}
          value={Boolean(data?.bookkeeping_create)}
          onChange={() =>
            handleUpdate("bookkeeping_create", Boolean(!data?.bookkeeping_create))}
        />
      </Box>
      <Box flex="1">
        {/* Update */}
        <Checkbox
          label={null}
          value={Boolean(data?.bookkeeping_edit)}
          onChange={() =>
            handleUpdate("bookkeeping_edit", Boolean(!data?.bookkeeping_edit))}
        />
      </Box>
      <Box flex="1">
        {/* Delete */}
        <Checkbox
          label={null}
          value={Boolean(data?.bookkeeping_remove)}
          onChange={() =>
            handleUpdate("bookkeeping_remove", Boolean(!data?.bookkeeping_remove))}
        />
      </Box>
    </Box>
  )
}
export default Bookkeeping
