import React, { useState } from "react"
import Box from "@material-ui/core/Box"
import Checkbox from "components/Forms/Checkbox"

/** Styles */
import styles from "./styles"

interface Props {
  handleUpdate: Function
  data: any
}

const ConstructionManuals: React.FC<Props> = ({ handleUpdate, data }) => {
  const { category, section } = styles()
  const [title] = useState("construction / manuals")

  return (
    <Box display="flex" alignItems="center" className={section}>
      <Box flex="3" className={category}>
        {title}
      </Box>
      <Box flex="1">
        {/* Create */}
        <Checkbox
          label={null}
          value={Boolean(data?.estate_matter_construction_manuals_create)}
          onChange={() =>
            handleUpdate(
              "estate_matter_construction_manuals_create",
              Boolean(!data?.estate_matter_construction_manuals_create)
            )}
        />
      </Box>
      <Box flex="1">
        {/* Update */}
        <Checkbox
          label={null}
          value={Boolean(data?.estate_matter_construction_manuals_edit)}
          onChange={() =>
            handleUpdate(
              "estate_matter_construction_manuals_edit",
              Boolean(!data?.estate_matter_construction_manuals_edit)
            )}
        />
      </Box>
      <Box flex="1">
        {/* Delete */}
        <Checkbox
          label={null}
          value={Boolean(data?.estate_matter_construction_manuals_remove)}
          onChange={() =>
            handleUpdate(
              "estate_matter_construction_manuals_remove",
              Boolean(!data?.estate_matter_construction_manuals_remove)
            )}
        />
      </Box>
    </Box>
  )
}
export default ConstructionManuals
