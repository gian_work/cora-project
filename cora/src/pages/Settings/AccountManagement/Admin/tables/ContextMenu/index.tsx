import React, { useContext, FC } from "react"
import List from "@material-ui/core/List"
import ListItem from "@material-ui/core/ListItem"

import { Context } from "context/Settings/AccountManagement/Admin/main/Context"

/** Styles */
import { IconEdit } from "components/Icons/ContextMenu"
import styles from "./styles"

/** Icon */

interface Props {
  data: any
}

const ContextAdmin: FC<Props> = ({ data }) => {
  const { listItem } = styles()
  const { setActiveData, showFormType } = useContext(Context)

  function showEditView(items: Record<string, any>) {
    setActiveData && setActiveData(items)
    showFormType && showFormType(true, 2)
  }

  return (
    <List component="nav">
      <ListItem className={listItem} button onClick={() => showEditView(data)}>
        <i>{IconEdit}</i>
        Edit
      </ListItem>
    </List>
  )
}

export default ContextAdmin
