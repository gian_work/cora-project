import React from "react"
import Fade from "@material-ui/core/Fade"

/** Components */
import Table from "components/Table"
import tableHeaders from "./table/tableHeaders"

import AdminSettingsTableHook from "./hooks"

const TableSettingAdmin: React.FC = () => {
  /** Methods */
  const { isValidating, tableData } = AdminSettingsTableHook()

  return (
    <Fade in={!isValidating} timeout={1000}>
      <div>
        <Table
          data={tableData || []}
          columns={tableHeaders()}
          minRows={2}
          size={10}
        />
      </div>
    </Fade>
  )
}

export default TableSettingAdmin
