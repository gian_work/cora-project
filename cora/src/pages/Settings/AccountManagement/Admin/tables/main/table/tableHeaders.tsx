import React from "react"
import Button from "@material-ui/core/Button"

/** Components */
import ContextMenu from "components/ContextMenu"

/** Config */
import ContextSettingsFeedback from "../../ContextMenu"

const TableHeaderColumns = () => [
  {
    fixed: "left",
    disableFilters: true,
    columns: [
      {
        width: 70,
        Cell: (row: any) => {
          return (
            <ContextMenu>
              <ContextSettingsFeedback data={row.original} />
            </ContextMenu>
          )
        },
        filterable: false
      }
    ]
  },
  {
    columns: [
      {
        Header: "Username",
        id: "username",
        accessor: "account_username"
      },
      {
        Header: "Name",
        id: "name",
        // accessor: "account_name"
        accessor: (original: Record<string, any>) => {
          return original?.account_name
        },
        Cell: ({ original }: Record<string, any>) => {
          return original?.account_name === "" || null
            ? "N/A"
            : original?.account_name
        }
      },
      {
        Header: "Email",
        id: "email",
        // accessor: "account_email"
        accessor: (original: Record<string, any>) => {
          return original?.account_email === "" || original?.account_email === null
            ? "N/A"
            : original?.account_email
        },
        Cell: ({ original }: Record<string, any>) => {
          return original?.account_email === "" || original?.account_email === null
            ? "N/A"
            : original?.account_email
        }
      },
      {
        Header: "Phone Number",
        id: "phone_no",
        accessor: (original: Record<string, any>) => {
          return original?.account_mobile_no === "" ||
            original?.account_mobile_no === null
            ? "N/A"
            : original?.account_mobile_no
        },
        Cell: ({ original }: Record<string, any>) => {
          return original?.account_mobile_no === "" ||
            original?.account_mobile_no === null
            ? "N/A"
            : original?.account_mobile_no
        }
      },
      {
        Header: "User Roles",
        id: "user_role",
        // accessor: "condo.user_role.role_name"
        accessor: (original: Record<string, any>) => {
          return original?.condo.user_role.role_name
        },
        Cell: ({ original }: Record<string, any>) => {
          return original?.condo.user_role.role_name === "" || null
            ? "N/A"
            : original?.condo.user_role.role_name
        }
      },
      {
        id: "reset password",
        accessor: (original: Record<string, any>) => {
          return original?.condo.admin_contact_no
        },
        Cell: () => {
          return (
            <Button color="primary" variant="outlined" onClick={(): null => null}>
              RESET PASSWORD
            </Button>
          )
        }
      }
    ]
  }
]

export default TableHeaderColumns
