const Options = [
  {
    title: "Attendance"
  },
  {
    title: "Announcement"
  },
  {
    title: "Facilities"
  },
  {
    title: "Applications"
  },
  {
    title: "Bookkeeping"
  },
  {
    title: "Feedback"
  },
  {
    title: "Unit Management"
  },
  {
    title: "Visitor Management"
  },
  {
    title: "Estate Matters"
  },
  {
    title: "Payment Schedule"
  }
]

export default Options
