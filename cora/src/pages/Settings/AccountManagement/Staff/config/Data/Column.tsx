export const columnConfig = {
  header: [
    {
      headerTitle: ""
    },
    {
      headerTitle: "Create"
    },
    {
      headerTitle: "Edit"
    },
    {
      headerTitle: "Delete"
    }
  ],
  column: [
    {
      title: "Attendance",
      checkbox1: true,
      checkbox2: true,
      checkbox3: true
    },
    {
      title: "Announcement",
      checkbox1: true,
      checkbox2: true,
      checkbox3: true
    },
    {
      title: "Facilities",
      checkbox1: true,
      checkbox2: true,
      checkbox3: true
    },
    {
      title: "Applications",
      checkbox1: true,
      checkbox2: true,
      checkbox3: true
    },
    {
      title: "Bookkeeping",
      checkbox1: true,
      checkbox2: true,
      checkbox3: true
    },
    {
      title: "Feedback",
      checkbox1: true,
      checkbox2: true,
      checkbox3: true
    },
    {
      title: "Unit Management",
      checkbox1: true,
      checkbox2: true,
      checkbox3: true
    },
    {
      title: "Visitor Management",
      checkbox1: true,
      checkbox2: true,
      checkbox3: true
    },
    {
      title: "Estate Matters",
      checkbox1: true,
      checkbox2: true,
      checkbox3: true
    },
    {
      title: "Payment Schedule",
      checkbox1: true,
      checkbox2: true,
      checkbox3: true
    }
  ]
}

export const othersConfig = {
  others: [
    {
      title: "Manage Condo Settings"
    },
    {
      title: "Mobile Accounts Settings"
    },
    {
      title: "Mobile Accounts Management"
    }
  ]
}
