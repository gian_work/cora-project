import React from "react"
import Box from "@material-ui/core/Box"
import Switch from "components/Forms/Switch"

interface RowOriginalProps {
  original: {
    title: string
  }
}

const TableHeaderOtherSettings = (): Array<{}> => [
  {
    columns: [
      {
        Header: "Other Settings",
        id: "title",
        accessor: "title",
        width: 300,
        Cell: ({ original }: RowOriginalProps): JSX.Element => {
          return <Box padding="0 30px">{original.title}</Box>
        }
      },
      {
        Header: "",
        width: 100,
        Cell: (): JSX.Element => {
          return <Switch label={null} action={(): null => null} checked />
        }
      }
    ]
  }
]

export default TableHeaderOtherSettings
