import React from "react"
import ContextMenu from "components/ContextMenu"

// component
import ActionButton from "components/Table/components/ActionButton"

// config
import ContextAdmin from "../../components/ContextMenu"

const TableHeaderColumns: Array<{}> = [
  {
    fixed: "left",
    disableFilters: true,
    columns: [
      {
        width: 70,
        Cell: (): JSX.Element => {
          return (
            <ContextMenu>
              <ContextAdmin />
            </ContextMenu>
          )
        },
        filterable: false
      }
    ]
  },
  {
    columns: [
      {
        Header: "S/N",
        id: "serialNumber",
        accessor: "serialNumber"
      },
      {
        Header: "Name",
        id: "name",
        accessor: "name"
      },
      {
        Header: "Email",
        id: "email",
        accessor: "email"
      },
      {
        Header: "Phone Number",
        id: "phoneNo",
        accessor: "phoneNo"
      },
      {
        Header: "User Roles",
        id: "userRoles",
        accessor: "userRoles"
      },
      {
        Header: "Login Code",
        id: "loginCode",
        accessor: "loginCode"
      }
    ]
  },
  {
    fixed: "right",
    disableFilters: true,
    columns: [
      {
        Header: "",
        Cell: (): JSX.Element => {
          return <ActionButton label="RESET CODE" action={(): null => null} />
        },
        filterable: false
      }
    ]
  }
]

export default TableHeaderColumns
