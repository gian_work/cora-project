import React from "react"
import useSWR from "swr"
import Fade from "@material-ui/core/Fade"
/** Service */
import service from "services/Settings/AccountManagement/Staff"
/** Components */
import Table from "components/Table"
import tableHeaders from "./table/tableHeaders"

const TableSettingAdmin: React.FC = () => {
  /** Methods */
  const fetchSettingsStaff = async () => service.getStaff()

  const { data, isValidating } = useSWR("fetchSettingsStaff", fetchSettingsStaff, {
    revalidateOnFocus: true
  })
  const tableData = data?.data?._data
  return (
    <Fade in={!isValidating} timeout={1000}>
      <div>
        <Table
          data={tableData || []}
          columns={tableHeaders()}
          minRows={2}
          size={10}
        />
      </div>
    </Fade>
  )
}

export default TableSettingAdmin
