import React from "react"

/** Components */
import ItemRow from "../components/ItemRow"

interface VisitorManagementProps {
  title?: string
}

const VisitorManagement: React.FC<VisitorManagementProps> = () => {
  return <ItemRow title="Visitor Management" />
}
export default VisitorManagement
