import React from "react"

/** Components */
import ItemRow from "../components/ItemRow"

interface VmsProps {
  title?: string
}

const Vms: React.FC<VmsProps> = () => {
  return <ItemRow title="VMS" />
}
export default Vms
