import React from "react"

/** Components */
import ItemRow from "../components/ItemRow"

interface ApplicationsProps {
  title?: string
}

const Applications: React.FC<ApplicationsProps> = () => {
  return <ItemRow title="Applications" />
}
export default Applications
