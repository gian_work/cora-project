import React from "react"

/** Components */
import ItemRow from "../components/ItemRow"

interface ProfileProps {
  title?: string
}

const Profile: React.FC<ProfileProps> = () => {
  return <ItemRow title="Profile" />
}
export default Profile
