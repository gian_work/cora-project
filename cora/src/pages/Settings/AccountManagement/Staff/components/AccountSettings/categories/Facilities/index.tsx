import React from "react"

/** Components */
import ItemRow from "../components/ItemRow"

interface FacilitiesProps {
  title?: string
}

const Facilities: React.FC<FacilitiesProps> = () => {
  return <ItemRow title="Facilities" />
}
export default Facilities
