import React from "react"

/** Components */
import ItemRow from "../components/ItemRow"

interface TakeAttendanceProps {
  title?: string
}

const TakeAttendance: React.FC<TakeAttendanceProps> = () => {
  return <ItemRow title="Take Attendance" />
}
export default TakeAttendance
