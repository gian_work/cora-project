import React from "react"
import FormControl from "@material-ui/core/FormControl"
import Box from "@material-ui/core/Box"
import Select from "@material-ui/core/Select"
import MenuItem from "@material-ui/core/MenuItem"
import { useSelector } from "react-redux"

// components
import Label from "components/Forms/Label"

// service

interface RolesProps {
  value: string
  onChange: Function
  onBlur?: Function
  error?: boolean
  label?: string
  setActiveRole: Function
}

const Roles: React.FC<RolesProps> = ({ value, onChange, onBlur, label }) => {
  const roles = useSelector((state: any) => state.roles)
  return (
    <Box width="300px">
      {label && <Label label={label} />}
      <FormControl variant="outlined">
        <Select
          displayEmpty
          value={value}
          onChange={(e) => onChange(e?.target?.value)}
          onBlur={(e) => onBlur && onBlur(e)}
          name="role"
        >
          <MenuItem value="" disabled>
            Select Role
          </MenuItem>
          {roles &&
            Object.keys(roles).map((role: any) => {
              return (
                <MenuItem value={roles[role].role_name} data-id={roles[role]._uid}>
                  {roles[role].role_name}
                </MenuItem>
              )
            })}
        </Select>
      </FormControl>
    </Box>
  )
}
export default Roles
