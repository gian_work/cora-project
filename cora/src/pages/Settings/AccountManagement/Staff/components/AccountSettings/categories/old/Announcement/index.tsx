import React from "react"

/** Components */
import ItemRow from "../components/ItemRow"

interface AnnoucementProps {
  title?: string
}

const Annoucement: React.FC<AnnoucementProps> = () => {
  return <ItemRow title="Annoucement" />
}
export default Annoucement
