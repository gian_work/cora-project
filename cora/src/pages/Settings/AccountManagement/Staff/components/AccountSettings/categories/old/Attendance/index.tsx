import React from "react"

/** Components */
import ItemRow from "../components/ItemRow"

interface AttendanceProps {
  title?: string
}

const Attendance: React.FC<AttendanceProps> = () => {
  return <ItemRow title="Attendance" />
}
export default Attendance
