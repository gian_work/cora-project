import React from "react"

/** Components */
import ItemRow from "../components/ItemRow"

interface UnitManagementProps {
  title?: string
}

const UnitManagement: React.FC<UnitManagementProps> = () => {
  return <ItemRow title="Unit Management" />
}
export default UnitManagement
