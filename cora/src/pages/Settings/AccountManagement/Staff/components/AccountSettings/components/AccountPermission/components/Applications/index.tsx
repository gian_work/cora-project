import React, { useState } from "react"
import Box from "@material-ui/core/Box"
import Checkbox from "components/Forms/Checkbox"

/** Styles */
import styles from "./styles"

interface Props {
  handleUpdate: Function
  data: any
}

const Applications: React.FC<Props> = ({ handleUpdate, data }) => {
  const { category, section } = styles()
  const [title] = useState("Applications")

  return (
    <Box display="flex" alignItems="center" className={section}>
      <Box flex="3" className={category}>
        {title}
      </Box>
      <Box flex="1">
        {/* Create */}
        <Checkbox
          label={null}
          value={Boolean(data?.application_edit)}
          onChange={() =>
            handleUpdate("application_edit", Boolean(!data?.application_edit))}
        />
      </Box>
      <Box flex="1">
        {/* Update */}
        <Checkbox
          label={null}
          value={Boolean(data?.application_view_self)}
          onChange={() =>
            handleUpdate(
              "application_view_self",
              Boolean(!data?.application_view_self)
            )}
        />
      </Box>
      <Box flex="1">
        {/* Delete */}
        <Checkbox
          label={null}
          value={Boolean(data?.application_view_all)}
          onChange={() =>
            handleUpdate(
              "application_view_all",
              Boolean(!data?.application_view_all)
            )}
        />
      </Box>
    </Box>
  )
}
export default Applications
