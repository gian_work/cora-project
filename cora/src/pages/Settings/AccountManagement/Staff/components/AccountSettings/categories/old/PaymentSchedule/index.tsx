import React from "react"

/** Components */
import ItemRow from "../components/ItemRow"

interface PaymentScheduleProps {
  title?: string
}

const PaymentSchedule: React.FC<PaymentScheduleProps> = () => {
  return <ItemRow title="Payment Schedule" />
}
export default PaymentSchedule
