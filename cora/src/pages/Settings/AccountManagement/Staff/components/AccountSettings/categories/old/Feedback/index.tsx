import React from "react"

/** Components */
import ItemRow from "../components/ItemRow"

interface FeedbackProps {
  title?: string
}

const Feedback: React.FC<FeedbackProps> = () => {
  return <ItemRow title="Feedback" />
}
export default Feedback
