import React from "react"

/** Components */
import ItemRow from "../components/ItemRow"

interface BookkeepingProps {
  title?: string
}

const Bookkeeping: React.FC<BookkeepingProps> = () => {
  return <ItemRow title="Bookkeeping" />
}
export default Bookkeeping
