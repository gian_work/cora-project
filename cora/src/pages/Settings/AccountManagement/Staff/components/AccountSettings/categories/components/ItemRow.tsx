import React from "react"
import Box from "@material-ui/core/Box"

/** Components */
import Checkbox from "components/Forms/Checkbox"

/** Styles */
import styles from "./styles"

interface ItemRowProps {
  title: string
}

const ItemRow: React.FC<ItemRowProps> = ({ title }) => {
  const { category, section } = styles()
  return (
    <Box display="flex" alignItems="center" className={section}>
      <Box flex="3" className={category}>
        {title}
      </Box>
      <Box flex="1">
        <Checkbox label={null} value onChange={() => null} />
      </Box>
      <Box flex="1">
        <Checkbox label={null} value={false} onChange={(): null => null} />
      </Box>
      <Box flex="1">
        <Checkbox label={null} value onChange={(): null => null} />
      </Box>
    </Box>
  )
}
export default ItemRow
