import React from "react"

/** Components */
import ItemRow from "../components/ItemRow"

interface MyAttendanceProps {
  title?: string
}

const MyAttendance: React.FC<MyAttendanceProps> = () => {
  return <ItemRow title="My Attendance" />
}
export default MyAttendance
