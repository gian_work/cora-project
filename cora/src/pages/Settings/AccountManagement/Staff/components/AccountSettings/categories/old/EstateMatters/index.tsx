import React from "react"

/** Components */
import ItemRow from "../components/ItemRow"

interface EstateMattersProps {
  title?: string
}

const EstateMatters: React.FC<EstateMattersProps> = () => {
  return <ItemRow title="Estate Matters" />
}
export default EstateMatters
