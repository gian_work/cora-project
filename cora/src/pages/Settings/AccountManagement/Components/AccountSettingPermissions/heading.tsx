import * as React from "react"

import styles from "./styles"

export interface ColumnHeaderProps {
  headerTitle?: string
}

// const ColumnStatic = ({ title }: ColumnProps) => (
const ColumnHeaderStatic = ({ headerTitle }: ColumnHeaderProps) => {
  const { titleStyle, container, row, column } = styles()
  return (
    <div className={container}>
      <div className={row}>
        <div className={column}>
          <div className={titleStyle} style={{ textAlign: "center" }}>
            {headerTitle}
          </div>
        </div>
      </div>
    </div>
  )
}

export default ColumnHeaderStatic
