import * as React from "react"
import Box from "@material-ui/core/Box"
import ColumnStatic, { ColumnProps } from "./index"
import ColumnHeaderStatic, { ColumnHeaderProps } from "./heading"
import styles from "./styles"

interface ColumnGroupProps {
  column: ColumnProps[]
  header: ColumnHeaderProps[]
}

const ColumnGroup = ({ column, header }: ColumnGroupProps) => {
  const { root, headerWrapper } = styles()
  return (
    <div className={root}>
      <Box className={headerWrapper} display="flex">
        {header.map((item) => (
          <ColumnHeaderStatic {...column} headerTitle={item.headerTitle} />
        ))}
      </Box>
      {column.map((item) => (
        <ColumnStatic
          {...column}
          title={item.title}
          checkbox1={item.checkbox1}
          checkbox2={item.checkbox2}
          checkbox3={item.checkbox3}
        />
      ))}
    </div>
  )
}

export default ColumnGroup
