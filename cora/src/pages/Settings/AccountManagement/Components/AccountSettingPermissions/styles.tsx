import { makeStyles } from "@material-ui/core/styles"

const styles = makeStyles(() => ({
  root: {
    width: "100%",
    height: "100%",
    backgroundColor: "#FFF",
    overflow: "hidden",
    boxShadow: "none",
    borderRadius: "10px",
    paddingTop: "15px",
    "& .wrapper:last-child": {
      borderBottom: "0px solid #e9ecef"
    }
  },
  headerWrapper: {},
  wrapper: {
    borderBottom: "1px solid #e9ecef",
    "&:last-child": {
      borderBottom: "0px solid #e9ecef"
    }
  },
  titleStyle: {
    color: "#646464",
    fontSize: "14px",
    lineHeight: "22px",
    padding: "0px 30px"
  },
  container: {
    flex: "1 0 auto",
    display: "flex",
    flexDirection: "column",
    alignItems: "stretch"
  },
  row: {
    flex: "1 0 auto",
    display: "inline-flex",
    alignItems: "center"
  },
  column: {
    flex: "100 0 auto",
    width: "100px",
    "& label": {
      margin: "0",
      padding: "0"
    }
  },
  uploadBox: {
    width: "100%",
    height: "100%",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    outline: "none",
    cursor: "pointer"
  },
  uploadContent: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center"
  }
}))

export default styles
