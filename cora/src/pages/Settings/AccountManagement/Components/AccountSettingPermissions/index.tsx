import * as React from "react"
import Box from "@material-ui/core/Box"
import Checkbox from "components/Forms/Checkbox"

import styles from "./styles"

export interface ColumnProps {
  title?: string
  checkbox1?: true | false
  checkbox2?: true | false
  checkbox3?: true | false
}

// const ColumnStatic = ({ title }: ColumnProps) => (
const ColumnStatic = ({ title, checkbox1, checkbox2, checkbox3 }: ColumnProps) => {
  const { wrapper, container, row, column, titleStyle } = styles()
  return (
    <Box display="flex" className={wrapper}>
      <div className={container}>
        <div className={row}>
          <div className={column}>
            <div className={titleStyle}>{title}</div>
          </div>
          <div
            className={column}
            style={{
              padding: "12px 20px",
              display: "flex",
              justifyContent: "center"
            }}
          >
            {checkbox1 === true && checkbox1 && (
              <Checkbox label={null} value onChange={(): null => null} />
            )}
          </div>
          <div
            className={column}
            style={{
              padding: "12px 20px",
              display: "flex",
              justifyContent: "center"
            }}
          >
            {checkbox2 === true && checkbox1 && (
              <Checkbox label={null} value onChange={(): null => null} />
            )}
          </div>
          <div
            className={column}
            style={{
              padding: "12px 20px",
              display: "flex",
              justifyContent: "center"
            }}
          >
            {checkbox3 === true && checkbox1 && (
              <Checkbox label={null} value onChange={(): null => null} />
            )}
          </div>
        </div>
      </div>
    </Box>
  )
}

export default ColumnStatic
