import * as React from "react"
import OthersSettings, { OthersSettingsProps } from "./index"
import styles from "./styles"

interface OthersGroupProps {
  others: OthersSettingsProps[]
}

const OthersGroup = ({ others }: OthersGroupProps) => {
  const { root, settingsTitle } = styles()
  return (
    <div className={root}>
      <div className={settingsTitle}>Other Settings</div>
      {others.map((item) => (
        <OthersSettings {...others} title={item.title} />
      ))}
    </div>
  )
}

export default OthersGroup
