import { makeStyles } from "@material-ui/core/styles"

const styles = makeStyles(() => ({
  root: {
    width: "100%",
    height: "100%",
    backgroundColor: "#FFF",
    overflow: "hidden",
    boxShadow: "none",
    borderRadius: "10px",
    paddingTop: "15px",
    "& .wrapper:last-child": {
      borderBottom: "0px solid #e9ecef"
    }
  },
  wrapper: {
    borderBottom: "1px solid #e9ecef",
    "&:last-child": {
      borderBottom: "0px solid #e9ecef"
    }
  },
  settingsTitle: {
    fontSize: "20px",
    lineHeight: "18px",
    padding: "12px 20px",
    color: "#39475b",
    fontWeight: 500,
    borderBottom: "3px solid #F2F2F2"
  },
  titleStyle: {
    fontSize: "14px",
    lineHeight: "22px",
    padding: "0px 30px"
  },
  container: {
    flex: "1 0 auto",
    display: "flex",
    flexDirection: "column",
    alignItems: "stretch"
  },
  row: {
    flex: "1 0 auto",
    display: "inline-flex",
    alignItems: "center",
    justifyContent: "space-between"
  },
  column: {
    flex: "100 0 auto",
    maxWidth: "290px"
  },
  uploadBox: {
    width: "100%",
    height: "100%",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    outline: "none",
    cursor: "pointer"
  },
  uploadContent: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center"
  }
}))

export default styles
