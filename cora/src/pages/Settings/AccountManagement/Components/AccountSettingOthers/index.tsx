import * as React from "react"
import Box from "@material-ui/core/Box"
import Switch from "@material-ui/core/Switch"

import styles from "./styles" // dba eto muna ttwagain ko yung style sa same folder ng index ko

export interface OthersSettingsProps {
  title?: string
}

// const ColumnStatic = ({ title }: ColumnProps) => (
const OthersSettings = ({ title }: OthersSettingsProps) => {
  const { wrapper, container, row, column, titleStyle } = styles() // then idedeclare ko yung mga values dito
  return (
    <Box display="flex" className={wrapper}>
      <div className={container}>
        <div className={row}>
          <div className={column}>
            <div className={titleStyle}>{title}</div>
          </div>
          <div className={column} style={{ padding: "12px 20px" }}>
            <Switch onChange={(): null => null} value color="primary" />
          </div>
        </div>
      </div>
    </Box>
  )
}

export default OthersSettings
