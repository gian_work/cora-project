import React from "react"
import Box from "@material-ui/core/Box"

// Layout
import WithHeader from "../../../../layout/WithHeader"

// components
import CateogoryBox from "./components/CategoryBox"
import FacilityCard from "./components/FacilityCard"

interface FacilitiesProps {
  title: string
}

const Facilities: React.FC<FacilitiesProps> = () => {
  return (
    <>
      <WithHeader>
        <Box>
          <Box margin="0 0 28px 0">
            <CateogoryBox />
          </Box>
          <Box display="flex" flexWrap="wrap" justifyContent="space-between">
            <Box flex="1" maxWidth="49.5%" minWidth="49.5%" marginBottom="23px">
              <FacilityCard type={1} />
            </Box>
            <Box flex="1" maxWidth="49.5%" minWidth="49.5%" marginBottom="23px">
              <FacilityCard type={2} />
            </Box>
            <Box flex="1" maxWidth="49.5%" minWidth="49.5%" marginBottom="23px">
              <FacilityCard type={3} />
            </Box>
          </Box>
        </Box>
      </WithHeader>
    </>
  )
}

export default Facilities
