import React from "react"
import Box from "@material-ui/core/Box"
import Card from "@material-ui/core/Card"
import Image from "material-ui-image"
import { makeStyles, Theme } from "@material-ui/core/styles"

// components
// import Spacer from "../../../../../components/Spacer"

// assets
import bbq1 from "../../assets/bbq1.png"
// import bbq2 from "../../assets/bbq2.png"
// import bbq3 from "../../assets/bbq3.png"

// const sampleConfig = {
//   1: [
//     {
//       name: "BBQ Pit 1",
//       image: bbq1
//     },
//     {
//       name: "BBQ Pit 2",
//       image: bbq2
//     },
//     {
//       name: "BBQ Pit 3",
//       image: bbq3
//     }
//   ]
// }

const useStyles = makeStyles((theme: Theme) => ({
  titleStyle: {
    color: theme.palette.primary.main,
    fontSize: "16px",
    fontWeight: 500,
    borderBottom: "1px solid #F2F2F2",
    padding: "18px"
  },
  actionTitle: {
    color: theme.palette.primary.main,
    fontSize: "14px",
    fontWeight: 500,
    display: "flex",
    cursor: "pointer",
    marginRight: "15px",
    "& svg": {
      marginRight: "10px"
    }
  },
  actions: {
    display: "flex",
    alignItems: "center",
    padding: "30px 22px 35px"
  },
  addButton: {
    textTransform: "capitalize",
    height: "40px",
    "& svg": {
      marginRight: "10px"
    }
  },
  importButton: {
    color: theme.palette.primary.main,
    fontSize: "14px",
    fontWeight: 500,
    display: "flex",
    "& svg": {
      marginRight: "10px"
    }
  }
}))

const IconEdit = (
  <svg
    width="23"
    height="21"
    viewBox="0 0 23 21"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M2.00627 0.00927734C0.90282 0.00927734 0 0.917506 0 2.02756V14.1373C0 15.2473 0.90282 16.1556 2.00627 16.1556H12.21L14.2163 14.1373H2.00627V4.04585H18.0564V10.2742L20.0627 8.25587V4.04585C20.0627 2.93579 19.1598 2.02756 18.0564 2.02756H10.0313L8.02506 0.00927734H2.00627ZM20.2703 11.1099C20.1421 11.1099 20.0135 11.1593 19.9157 11.2577L18.9126 12.2668L20.9188 14.2851L21.922 13.276C22.1176 13.0792 22.1176 12.7593 21.922 12.5625L20.625 11.2577C20.5267 11.1593 20.3986 11.1099 20.2703 11.1099ZM18.2033 12.9803L13.0407 18.1739V20.1921H15.047L20.2096 14.9986L18.2033 12.9803Z"
      fill="#006CC1"
    />
  </svg>
)

const IconAdd = (
  <svg
    width="20"
    height="21"
    viewBox="0 0 20 21"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M10.4554 0.703613C5.18354 0.703613 0.910156 5.03745 0.910156 10.3838C0.910156 15.7302 5.18354 20.0641 10.4554 20.0641C15.7272 20.0641 20.0006 15.7302 20.0006 10.3838C20.0006 5.03745 15.7272 0.703613 10.4554 0.703613ZM15.228 11.3519H11.4099V15.224H9.50084V11.3519H5.68276V9.41582H9.50084V5.54373H11.4099V9.41582H15.228V11.3519Z"
      fill="#006CC1"
    />
  </svg>
)

// interface
interface CategoryBoxProps {
  type: number
}

const CategoryBox: React.FC<CategoryBoxProps> = () => {
  const { titleStyle, actionTitle } = useStyles()
  return (
    <Card>
      <Box display="flex" justifyContent="space-between" alignItems="center">
        <Box className={titleStyle}>Barbecue Pit</Box>
        <Box display="flex" padding="18px">
          <Box className={actionTitle}>
            {IconEdit}
            Edit Global Settings
          </Box>
          <Box className={actionTitle}>
            {IconAdd}
            Add Facility
          </Box>
        </Box>
      </Box>
      <Box>
        <Box display="flex" padding="20px">
          <Box width="106px" height="106px" marginRight="20px">
            <Image src={bbq1} />
          </Box>
          <Box>
            <Box>BBQ Pit 1</Box>
            <Box>Location : Block 500</Box>
          </Box>
        </Box>
      </Box>
    </Card>
  )
}

export default CategoryBox
