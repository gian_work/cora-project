import React from "react"
import Box from "@material-ui/core/Box"
import Card from "@material-ui/core/Card"
import TextField from "@material-ui/core/TextField"
import Button from "@material-ui/core/Button"
import AddCircleIcon from "@material-ui/icons/AddCircle"
import { makeStyles, Theme } from "@material-ui/core/styles"

// components
import Spacer from "../../../../../../components/Spacer"

const useStyles = makeStyles((theme: Theme) => ({
  titleStyle: {
    color: theme.palette.primary.main,
    fontSize: "24px",
    fontWeight: 500,
    borderBottom: "1px solid #F2F2F2",
    padding: "30px 22px 24px"
  },
  actions: {
    display: "flex",
    alignItems: "center",
    padding: "30px 22px 35px"
  },
  addButton: {
    textTransform: "capitalize",
    height: "40px",
    "& svg": {
      marginRight: "10px"
    }
  },
  importButton: {
    color: theme.palette.primary.main,
    fontSize: "14px",
    fontWeight: 500,
    display: "flex",
    "& svg": {
      marginRight: "10px"
    }
  }
}))

const IconImport = (
  <svg
    width="21"
    height="21"
    viewBox="0 0 21 21"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M10.6868 0.856934L3.46586 8.07792H7.76497V14.4087C7.76497 15.2044 8.43013 15.8696 9.22591 15.8696H12.1478C12.9436 15.8696 13.6087 15.2044 13.6087 14.4087V8.07792H17.9078L10.6868 0.856934ZM10.6868 3.61141L13.2054 6.13H11.6608V13.9217H9.71289V6.13H8.16825L10.6868 3.61141ZM0.947266 18.7915V20.7394H20.4264V18.7915H0.947266Z"
      fill="#006CC1"
    />
  </svg>
)

const CategoryBox: React.FC = () => {
  const { actions, addButton, importButton, titleStyle } = useStyles()
  return (
    <Card>
      <Box>
        <Box className={titleStyle}>Add a category</Box>
        <Box className={actions}>
          <Box width="314px">
            <TextField label="Category name" variant="outlined" value="" />
          </Box>
          <Spacer isDefault />
          <Box>
            <Button variant="contained" color="primary" className={addButton}>
              <AddCircleIcon />
              {' '}
              Add Category
            </Button>
          </Box>
          <Spacer isDefault />
          <Box className={importButton}>
            {IconImport}
            {' '}
            Import CSV
          </Box>
        </Box>
      </Box>
    </Card>
  )
}

export default CategoryBox
