import React, { useContext } from "react"
import Box from "@material-ui/core/Box"
import { Formik } from "formik"

/** Component */
import FormWrapper from "components/Forms/FormWrapper"
import RefButton from "components/Forms/RefButton"
import TimePicker from "components/Forms/TimePicker"

/** Config */
import { addTimeSlots } from "config/Settings/PropertyManagement/VisitorManagement/validation"

/** Context */
import { Context } from "context/Settings/PropertyManagement/VisitorManagement/forms/category/Context"

/** Styles */
import { refSubmit } from "context/Settings/PropertyManagement/VisitorManagement/forms/category/View"
import styles from "./styles"

/** Ref */

const CategoryDetails: React.FC = () => {
  const { section, title } = styles()
  const {
    setWeekdayTime,
    weekdayTime,
    saturdayTime,
    setSaturdayTime,
    holidayTime,
    setHolidayTime
  } = useContext(Context)

  const { weekdayStartTime, weekdayEndTime } = weekdayTime
  const { saturdayStartTime, saturdayEndTime } = saturdayTime
  const { holidayStartTime, holidayEndTime } = holidayTime

  /** Methods */
  const handleHolidays = (
    setFieldValue: Function,
    key: string,
    value: string | number | boolean
  ): void => {
    return setFieldValue(key, value).then(
      () =>
        setHolidayTime &&
        setHolidayTime({
          ...holidayTime,
          [key]: value
        })
    )
  }

  const handleWeekdays = (
    setFieldValue: Function,
    key: string,
    value: string | number | boolean
  ): void => {
    return setFieldValue(key, value).then(
      () =>
        setWeekdayTime &&
        setWeekdayTime({
          ...weekdayTime,
          [key]: value
        })
    )
  }

  const handleSaturdays = (
    setFieldValue: Function,
    key: string,
    value: string | number | boolean
  ): void => {
    return setFieldValue(key, value).then(
      () =>
        setSaturdayTime &&
        setSaturdayTime({
          ...saturdayTime,
          [key]: value
        })
    )
  }

  return (
    <div>
      <Box margin="auto" padding="0 0 40px 0">
        <FormWrapper title="Time Slot" width="50%">
          <Formik
            initialValues={{
              weekdayStartTime,
              weekdayEndTime,
              saturdayStartTime,
              saturdayEndTime,
              holidayStartTime,
              holidayEndTime
            }}
            onSubmit={(values, actions): void => {
              JSON.stringify(values, null, 2)
              actions.setSubmitting(false)
            }}
            validationSchema={addTimeSlots}
          >
            {({
              touched,
              errors,
              handleBlur,
              handleSubmit,
              setFieldValue
            }): JSX.Element => {
              return (
                <form onSubmit={handleSubmit}>
                  <Box className={section}>
                    <Box className={title}>Monday - Friday</Box>
                    <Box display="flex" alignItems="flex-start">
                      <Box flex="1">
                        <TimePicker
                          ampm={false}
                          label="Start Time"
                          name="weekdayStartTime"
                          value={weekdayStartTime}
                          handleDateChange={(value: string): void =>
                            handleWeekdays(setFieldValue, "weekdayStartTime", value)}
                          onBlur={(e: Event): void => handleBlur(e)}
                          error={
                            touched.weekdayStartTime &&
                            Boolean(errors.weekdayStartTime)
                          }
                          helperText={
                            touched.weekdayStartTime &&
                            errors.weekdayStartTime?.toString()
                          }
                        />
                      </Box>
                      <Box padding="30px 20px 0">to</Box>
                      <Box flex="1">
                        <TimePicker
                          ampm={false}
                          label="End Time"
                          name="weekdayEndTime"
                          value={weekdayEndTime}
                          handleDateChange={(value: string): void =>
                            handleWeekdays(setFieldValue, "weekdayEndTime", value)}
                          onBlur={(e: Event): void => handleBlur(e)}
                          error={
                            touched.weekdayEndTime && Boolean(errors.weekdayEndTime)
                          }
                          helperText={
                            touched.weekdayEndTime &&
                            errors.weekdayEndTime?.toString()
                          }
                        />
                      </Box>
                    </Box>
                  </Box>

                  {/* Saturdays */}
                  <Box className={section}>
                    <Box className={title}>Saturday</Box>
                    <Box display="flex" alignItems="flex-start">
                      <Box flex="1">
                        <TimePicker
                          ampm={false}
                          label="Start Time"
                          name="saturdayStartTime"
                          value={saturdayStartTime}
                          handleDateChange={(value: string): void =>
                            handleSaturdays(
                              setFieldValue,
                              "saturdayStartTime",
                              value
                            )}
                          onBlur={(e: Event): void => handleBlur(e)}
                          error={
                            touched.saturdayStartTime &&
                            Boolean(errors.saturdayStartTime)
                          }
                          helperText={
                            touched.saturdayStartTime &&
                            errors.saturdayStartTime?.toString()
                          }
                        />
                      </Box>
                      <Box padding="30px 20px 0">to</Box>
                      <Box flex="1">
                        <TimePicker
                          ampm={false}
                          label="End Time"
                          name="saturdayEndTime"
                          value={saturdayEndTime}
                          handleDateChange={(value: string): void =>
                            handleSaturdays(setFieldValue, "saturdayEndTime", value)}
                          onBlur={(e: Event): void => handleBlur(e)}
                          error={
                            touched.saturdayEndTime &&
                            Boolean(errors.saturdayEndTime)
                          }
                          helperText={
                            touched.saturdayEndTime &&
                            errors.saturdayEndTime?.toString()
                          }
                        />
                      </Box>
                    </Box>
                  </Box>

                  {/* Sunday - Holidays */}
                  <Box className={section}>
                    <Box className={title}>Sunday - Holidays</Box>
                    <Box display="flex" alignItems="flex-start">
                      <Box flex="1">
                        <TimePicker
                          ampm={false}
                          label="Start Time"
                          name="holidayStartTime"
                          value={holidayStartTime}
                          handleDateChange={(value: string): void =>
                            handleHolidays(setFieldValue, "holidayStartTime", value)}
                          onBlur={(e: Event): void => handleBlur(e)}
                          error={
                            touched.holidayStartTime &&
                            Boolean(errors.holidayStartTime)
                          }
                          helperText={
                            touched.holidayStartTime &&
                            errors.holidayStartTime?.toString()
                          }
                        />
                      </Box>
                      <Box padding="30px 20px 0">to</Box>
                      <Box flex="1">
                        <TimePicker
                          ampm={false}
                          label="End Time"
                          name="holidayEndTime"
                          value={holidayEndTime}
                          handleDateChange={(value: string): void =>
                            handleHolidays(setFieldValue, "holidayEndTime", value)}
                          onBlur={(e: Event): void => handleBlur(e)}
                          error={
                            touched.holidayEndTime && Boolean(errors.holidayEndTime)
                          }
                          helperText={
                            touched.holidayEndTime &&
                            errors.holidayEndTime?.toString()
                          }
                        />
                      </Box>
                    </Box>
                  </Box>

                  <RefButton refValue={refSubmit} action={handleSubmit} />
                </form>
              )
            }}
          </Formik>
        </FormWrapper>
      </Box>
    </div>
  )
}

export default CategoryDetails
