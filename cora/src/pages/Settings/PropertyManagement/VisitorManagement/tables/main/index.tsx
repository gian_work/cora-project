import React from "react"
import useSWR from "swr"
import Fade from "@material-ui/core/Fade"
import Cookie from "js-cookie"
/** Service */
import service from "services/Settings/PropertyManagement/VisitorManagement"
/** Components */
import Table from "components/Table"
import tableHeaders from "./table/tableHeaders"

interface Props {
  showEdit: Function
}

const TableSettingsVMS: React.FC<Props> = ({ showEdit }) => {
  const condoUID = Cookie.get("condoUID")

  /** Methods */
  const fetchSettingsVMS = async () =>
    service.getVMS({
      "condo_uid": condoUID
    })

  const { data, isValidating } = useSWR("fetchSettingsVMS", fetchSettingsVMS, {
    revalidateOnFocus: true
  })
  const tableData = data?.data?._data

  return (
    <Fade in={!isValidating} timeout={1000}>
      <div>
        <Table
          data={tableData || []}
          columns={tableHeaders(showEdit)}
          minRows={2}
          size={10}
          hasHeaderGroup
        />
      </div>
    </Fade>
  )
}

export default TableSettingsVMS
