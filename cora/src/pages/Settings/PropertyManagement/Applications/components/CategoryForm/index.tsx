import React from "react"

import Provider, {
  ProviderProps
} from "context/Settings/PropertyManagement/Applications/forms/category/Provider"
import View from "context/Settings/PropertyManagement/Applications/forms/category/View"

const CategoryForm: React.FC<ProviderProps> = ({ data, sendBack }) => {
  return (
    <Provider data={data} sendBack={sendBack}>
      <View />
    </Provider>
  )
}

export default CategoryForm
