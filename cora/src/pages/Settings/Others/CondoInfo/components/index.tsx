import React, { useContext } from "react"
import Box from "@material-ui/core/Box"
import FormWrapper from "components/Forms/FormWrapper"
import { Formik } from "formik"

/** Component */
import FormInput from "components/Forms/FormInput"
import Footer from "components/Forms/Footer/new"
import Editor from "components/Editor"
import Label from "components/Forms/Label"
import Loader from "components/Loader"

/** Contxt */
import { Context } from "context/Settings/Others/CondoInfo/main/Context"

/** Validation */
import { updateCondoInfo as updateCondoInfoValidation } from "config/Settings/Others/CondoInfo/validation"

const Form = () => {
  const {
    condoInfo,
    setCondoInfo,
    updateCondoInfo,
    submitting,
    isLoading
  } = useContext(Context)

  /** Methods */
  const handleFormChange = (
    name: string,
    e: React.ChangeEvent<HTMLInputElement>,
    handleChange: Function,
    setFieldTouched: Function
  ) => {
    handleChange(e)
    setFieldTouched(name, true, false)

    if (e?.target === undefined) {
      setCondoInfo &&
        setCondoInfo((data: any) => ({
          ...data,
          [name]: e
        }))
    } else {
      setCondoInfo &&
        setCondoInfo((data: any) => ({
          ...data,
          [name]: e?.target?.value
        }))
    }
  }

  return (
    <Box>
      <Box paddingBottom="100px">
        <Formik
          initialValues={{
            adminEmail: condoInfo?.email,
            adminContact: condoInfo?.adminContact,
            sosNumber: condoInfo?.sosNumber,
            terms: condoInfo?.terms
          }}
          onSubmit={(values, actions): void => {
            JSON.stringify(values, null, 2)
            actions.setSubmitting(false)
          }}
          validationSchema={updateCondoInfoValidation}
        >
          {({
            touched,
            errors,
            handleBlur,
            handleChange,
            setFieldTouched
          }): JSX.Element => {
            return (
              <form>
                <Box width="60%" margin="auto">
                  <FormWrapper width="60%">
                    <Box
                      borderBottom="1px solid #F2F2F2"
                      marginBottom="30px"
                      paddingBottom="30px"
                    >
                      <Box marginBottom="20px">
                        <FormInput
                          name="adminEmail"
                          value={condoInfo?.email}
                          idValue="adminEmail"
                          label="Admin Email"
                          placeholder="admin@email.com"
                          handleOnChange={(
                            e: React.ChangeEvent<HTMLInputElement>
                          ): void =>
                            handleFormChange(
                              "email",
                              e,
                              handleChange,
                              setFieldTouched
                            )}
                          onBlur={handleBlur}
                          error={touched.adminEmail && Boolean(errors.adminEmail)}
                          helperText={
                            errors.adminEmail &&
                            touched.adminEmail &&
                            errors.adminEmail
                          }
                        />
                      </Box>

                      <Box display="flex" justifyContent="space-between">
                        <Box flex="1" maxWidth="49%">
                          <FormInput
                            name="adminContact"
                            value={condoInfo?.adminContact}
                            idValue="adminContact"
                            label="Admin Contact Number"
                            placeholder="123-456-7890"
                            handleOnChange={(
                              e: React.ChangeEvent<HTMLInputElement>
                            ): void =>
                              handleFormChange(
                                "adminContact",
                                e,
                                handleChange,
                                setFieldTouched
                              )}
                            onBlur={handleBlur}
                            error={
                              touched.adminContact && Boolean(errors.adminContact)
                            }
                            helperText={
                              errors.adminContact &&
                              touched.adminContact &&
                              errors.adminContact
                            }
                          />
                        </Box>

                        <Box flex="1" maxWidth="49%">
                          <FormInput
                            name="sosPhoneNumber"
                            value={condoInfo?.sosNumber}
                            idValue="sosPhoneNumber"
                            label="SOS PHONE Number"
                            placeholder="123-456-7890"
                            handleOnChange={(
                              e: React.ChangeEvent<HTMLInputElement>
                            ): void =>
                              handleFormChange(
                                "sosNumber",
                                e,
                                handleChange,
                                setFieldTouched
                              )}
                            onBlur={handleBlur}
                            error={touched.sosNumber && Boolean(errors.sosNumber)}
                            helperText={
                              errors.sosNumber &&
                              touched.sosNumber &&
                              errors.sosNumber
                            }
                          />
                        </Box>
                      </Box>
                    </Box>

                    <Box>
                      <Label label="Terms & Condition" />
                      {!isLoading ? (
                        <Editor
                          error={touched.terms && condoInfo?.terms === "<p></p>"}
                          onBlur={handleBlur}
                          content={condoInfo?.terms}
                          setContent={(content: any) =>
                            handleFormChange(
                              "terms",
                              content,
                              handleChange,
                              setFieldTouched
                            )}
                        />
                      ) : (
                        <Loader dark />
                      )}
                    </Box>
                  </FormWrapper>
                </Box>
              </form>
            )
          }}
        </Formik>
      </Box>
      <Box margin="0 0 30px">
        <Footer
          handleNext={() => updateCondoInfo && updateCondoInfo()}
          handleBack={null}
          handleCancel={null}
          label={submitting ? "Submitting..." : "Submit"}
        />
      </Box>
    </Box>
  )
}

export default Form
