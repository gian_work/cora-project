import React from "react"

import SettingsOthersProvider from "context/Settings/Others/UploadCSV/main/Provider"
import SettingsOthersView from "context/Settings/Others/UploadCSV/main/View"

const SettingsOthers: React.FC = () => (
  <SettingsOthersProvider>
    <SettingsOthersView />
  </SettingsOthersProvider>
)

export default SettingsOthers
