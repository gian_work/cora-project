export const uploadsConfig = {
  uploads: [
    {
      title: "Block & Unit Numbers & Share Value",
      dataUpload: "Sample_Document_Format.CSV"
    },
    {
      title: "Deposit List",
      dataUpload: "Sample_Document_Format.CSV"
    },
    {
      title: "Bookkeeping Settings",
      dataUpload: "Sample_Document_Format.CSV"
    },
    {
      title: "Strata Roll",
      dataUpload: "Sample_Document_Format.CSV"
    },
    {
      title: "Payment Schedule",
      dataUpload: "Sample_Document_Format.CSV"
    },
    {
      title: "Admin User Roles",
      dataUpload: "Sample_Document_Format.CSV"
    },
    {
      title: "Residents List",
      dataUpload: "Sample_Document_Format.CSV"
    },
    {
      title: "Application Settings",
      dataUpload: "Sample_Document_Format.CSV"
    },
    {
      title: "Admin User Names",
      dataUpload: "Sample_Document_Format.CSV"
    },
    {
      title: "Access Cards List",
      dataUpload: "Sample_Document_Format.CSV"
    },
    {
      title: "Facilities Settings",
      dataUpload: "Sample_Document_Format.CSV"
    },
    {
      title: "Staff User Roles",
      dataUpload: "Sample_Document_Format.CSV"
    },
    {
      title: "Vehicles List",
      dataUpload: "Sample_Document_Format.CSV"
    },
    {
      title: "Feedback Settings",
      dataUpload: "Sample_Document_Format.CSV"
    },
    {
      title: "Staff User Names",
      dataUpload: "Sample_Document_Format.CSV"
    },
    {
      title: "Booked/Pending Facilities",
      dataUpload: "Sample_Document_Format.CSV"
    },
    {
      title: "VMS Settings",
      dataUpload: "Sample_Document_Format.CSV"
    }
  ]
}
