import { makeStyles, Theme } from "@material-ui/core/styles"

const styles = makeStyles((theme: Theme) => ({
  container: {
    display: "flex",
    height: "72px",
    alignItems: "center",
    justifyContent: "space-around",
    borderBottom: "1px solid #F2F2F2"
  },
  stepper: {
    fontSize: "14px",
    textTransform: "uppercase",
    letterSpacing: "0.0125em",
    color: theme.palette.body.dark,
    display: "flex",
    alignItems: "center",
    height: "100%",
    borderBottom: "2px solid transparent",
    padding: "0 20px",
    "& > span": {
      fontSize: "20px",
      fontWeight: 600,
      marginRight: "8px"
    },
    "&.active": {
      color: theme.palette.body.green
    }
  }
}))

export default styles
