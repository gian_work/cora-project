import React from "react"
import Box from "@material-ui/core/Box"

/** Components */
import FormWrapper from "components/Forms/FormWrapper"
import UploadBox from "components/UploadBox/new"

/** Context */
import { CtxType } from "context/Dashboard/UnitManagement/forms/users/Context"
import withContext from "context/Dashboard/UnitManagement/forms/users/withContext"

/** Styles */
import styles from "./styles"

const SupportingDocument: React.FC<CtxType> = ({
  handleFileUpload,
  files,
  removeFile,
  fileAttaching
}) => {
  const { root } = styles()

  return (
    <div className={root}>
      <FormWrapper title="Supporting Documents" width="50%">
        <Box width="50%" margin="auto" padding="0 0 40px 0">
          <UploadBox
            hasTitle={false}
            onDrop={handleFileUpload}
            files={files}
            removePhoto={removeFile}
            attaching={fileAttaching}
            title="Choose file to upload"
            acceptedFile="application/pdf"
          />
        </Box>
      </FormWrapper>
    </div>
  )
}

export default withContext(SupportingDocument)
