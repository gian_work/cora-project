import React from "react"

// styles
import styles from "./styles"

const IconPDF = (
  <svg
    width="24"
    height="30"
    viewBox="0 0 24 30"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M3 0C1.35862 0 0 1.35862 0 3V27C0 28.6414 1.35862 30 3 30H21C22.6414 30 24 28.6414 24 27V9L15 0H3ZM3 3H13.5V10.5H21V27H3V3ZM11.748 10.5C10.923 10.5 10.3051 11.0602 10.1836 12.0352C10.0636 13.0087 10.5462 14.711 11.3877 16.292C11.0262 17.387 10.6651 18.3595 10.1836 19.333C8.61909 19.819 7.17572 20.5478 6.57422 21.2783C5.61272 22.3718 6.09398 23.1023 6.33398 23.4668C6.57548 23.8313 6.93647 24 7.41797 24C7.65947 24 7.9001 23.9516 8.1416 23.8301C9.1046 23.4656 10.0643 22.0052 11.0273 20.3027C11.8688 20.0597 12.7112 19.8178 13.5527 19.6963C14.3942 20.6683 15.2394 21.2785 15.9609 21.5215C16.8024 21.7645 17.5242 21.3979 17.8857 20.5459C18.1257 19.8154 18.0038 19.2098 17.4023 18.8438C16.6793 18.3577 15.4762 18.356 14.0332 18.4775C13.5517 17.7485 13.0734 17.0196 12.7119 16.2891C13.3134 14.4651 13.5525 12.8856 13.3125 11.9121C13.071 11.0601 12.573 10.5 11.748 10.5Z"
      fill="#646464"
    />
  </svg>
)

const iconTrash = (
  <svg
    width="18"
    height="20"
    viewBox="0 0 18 20"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M7 0L6 1H0V3H1.10938L2.90234 18.2344C3.01959 19.234 3.882 20 4.88867 20H13.1113C14.118 20 14.9804 19.234 15.0977 18.2344L16.8906 3H18V1H16H12L11 0H7ZM3.12305 3H4H14.877L13.1113 18H4.88867L3.12305 3ZM6 5V15C6 15.552 6.448 16 7 16H8V5H6ZM10 5V16H11C11.552 16 12 15.552 12 15V5H10Z"
      fill="#777E86"
    />
  </svg>
)

const Document: React.FC = () => {
  const { documentContainer, documentDetails } = styles()
  return (
    <div className={documentContainer}>
      <div className={documentDetails}>
        <div className="filetype">{IconPDF}</div>
        <div>
          <div className="filename">Contract_2020.pdf</div>
          <div className="filesize">128 kb</div>
        </div>
      </div>
      <div role="button" tabIndex={0}>
        {iconTrash}
      </div>
    </div>
  )
}
export default Document
