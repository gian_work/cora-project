import React from "react"
import Box from "@material-ui/core/Box"
import { Formik } from "formik"

/** Component */
import FormWrapper from "components/Forms/FormWrapper"
import BlockUnit from "components/Common/BlockUnit"
import FormInput from "components/Forms/FormInput"
import Names from "components/Common/Names"
import RefButton from "components/Forms/RefButton"

/** Config */
import validation from "config/Dashboard/Bookkeeping/validation"

/** Context */
import { CtxType } from "context/Dashboard/UnitManagement/forms/paymentSchedule/createPaymentSchedule/Context"
import withContext from "context/Dashboard/UnitManagement/forms/paymentSchedule/createPaymentSchedule/withContext"

/** Ref */
import { refSubmit } from "context/Dashboard/UnitManagement/forms/paymentSchedule/createPaymentSchedule/View"

import styles from "./styles"

const PersonalDetails: React.FC<CtxType> = ({
  psDetails,
  setPsDetails,
  setResidentInfo
}) => {
  const { personalDetails, spaceBottom } = styles()

  /** Methods */
  const handleBlockUnit = (
    setFieldValue: Function,
    value: string | boolean
  ): void => {
    setFieldValue("unitUID", value)
    setPsDetails &&
      setPsDetails({
        ...psDetails,
        unitUID: value,
        payeeAccountUID: ""
      })
  }

  /* eslint-disable @typescript-eslint/no-explicit-any */
  const handleResidentName = (value: Record<string, any>): void => {
    const residentInfo = JSON.parse(value.currentTarget?.dataset?.userinfo)

    setResidentInfo({
      name: residentInfo?.name
    })

    return (
      setPsDetails &&
      setPsDetails({
        ...psDetails,
        payeeAccountUID: residentInfo?.account_info_uid
      })
    )
  }

  const handleFormChange = (
    setFieldValue: Function,
    key: string,
    e: React.ChangeEvent<HTMLInputElement>
  ): void => {
    setFieldValue(key, e?.target?.value)
    setPsDetails &&
      setPsDetails({
        ...psDetails,
        [key]: e?.target?.value
      })
  }

  return (
    <div className={personalDetails}>
      <Box margin="auto" padding="0 0 40px 0">
        <FormWrapper title="Personal Details" width="60%">
          <Formik
            initialValues={{
              unitUID: psDetails?.unitUID,
              payeeAccountUID: psDetails?.payeeAccountUID,
              payeeAddress: psDetails?.payeeAddress,
              payeePhone: psDetails?.payeePhone,
              payeeEmail: psDetails?.payeeEmail
            }}
            onSubmit={(values, actions): void => {
              JSON.stringify(values, null, 2)
              actions.setSubmitting(false)
            }}
            validationSchema={validation.personalDetails}
          >
            {({
              touched,
              errors,
              handleBlur,
              handleSubmit,
              setFieldValue
            }): JSX.Element => {
              return (
                <form onSubmit={handleSubmit}>
                  <Box width="60%" margin="auto" className={spaceBottom}>
                    <Box className={spaceBottom}>
                      <BlockUnit
                        value={psDetails.unitUID}
                        onChange={(e: React.ChangeEvent<HTMLInputElement>): void =>
                          handleBlockUnit(setFieldValue, e.target.value)}
                        onBlur={(e: Event): void => handleBlur(e)}
                        error={touched.unitUID && Boolean(errors.unitUID)}
                        helperText={
                          touched.unitUID &&
                          errors.unitUID &&
                          errors.unitUID.toString()
                        }
                      />
                    </Box>

                    {psDetails.unitUID !== "" && (
                      <Box className={spaceBottom}>
                        <Names
                          forID
                          value={psDetails.payeeAccountUID}
                          onChange={(
                            e: React.ChangeEvent<HTMLInputElement>
                          ): void => {
                            handleFormChange(setFieldValue, "payeeAccountUID", e)
                            handleResidentName(e)
                          }}
                          unitUID={psDetails.unitUID}
                          onBlur={(e: Event): void => handleBlur(e)}
                          error={
                            touched.payeeAccountUID &&
                            Boolean(errors.payeeAccountUID)
                          }
                          helperText={
                            touched.payeeAccountUID &&
                            errors.payeeAccountUID &&
                            errors.payeeAccountUID.toString()
                          }
                        />
                      </Box>
                    )}

                    <Box className={spaceBottom}>
                      <FormInput
                        name="payeeEmail"
                        label="Email Address"
                        placeholder="Email Address"
                        value={psDetails.payeeEmail}
                        handleOnChange={(
                          e: React.ChangeEvent<HTMLInputElement>
                        ): void => handleFormChange(setFieldValue, "payeeEmail", e)}
                        error={touched.payeeEmail && Boolean(errors.payeeEmail)}
                        helperText={
                          errors.payeeEmail &&
                          touched.payeeEmail &&
                          errors.payeeEmail
                        }
                      />
                    </Box>
                    <Box className={spaceBottom}>
                      <Box flex="1">
                        <FormInput
                          name="payeePhone"
                          label="Phone Number"
                          placeholder="Phone Number"
                          value={psDetails.payeePhone}
                          handleOnChange={(
                            e: React.ChangeEvent<HTMLInputElement>
                          ): void =>
                            handleFormChange(setFieldValue, "payeePhone", e)}
                          onBlur={handleBlur}
                          error={touched.payeePhone && Boolean(errors.payeePhone)}
                          helperText={
                            errors.payeePhone &&
                            touched.payeePhone &&
                            errors.payeePhone
                          }
                        />
                      </Box>
                    </Box>
                    <Box className={spaceBottom}>
                      <FormInput
                        name="payeeAddress"
                        label="Registered Address"
                        placeholder="Registered Address"
                        value={psDetails.payeeAddress}
                        handleOnChange={(
                          e: React.ChangeEvent<HTMLInputElement>
                        ): void =>
                          handleFormChange(setFieldValue, "payeeAddress", e)}
                        onBlur={handleBlur}
                        error={touched.payeeAddress && Boolean(errors.payeeAddress)}
                        helperText={
                          errors.payeeAddress &&
                          touched.payeeAddress &&
                          errors.payeeAddress
                        }
                      />
                    </Box>
                  </Box>
                  <RefButton refValue={refSubmit} action={handleSubmit} />
                </form>
              )
            }}
          </Formik>
        </FormWrapper>
      </Box>
    </div>
  )
}

export default withContext(PersonalDetails)
