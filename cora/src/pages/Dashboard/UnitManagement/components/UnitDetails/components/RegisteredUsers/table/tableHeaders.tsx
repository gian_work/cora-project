import React from "react"
import Button from "@material-ui/core/Button"

import ContextMenu from "components/ContextMenu"

import ContextItems from "./ContextMenu"

const tableHeaders = (createAccount?: Function) => {
  return [
    {
      fixed: "left",
      disableFilters: true,
      columns: [
        {
          width: 70,
          Cell: (row: any): JSX.Element => {
            return (
              <ContextMenu>
                <ContextItems data={row?.original} />
              </ContextMenu>
            )
          },
          filterable: false
        }
      ]
    },
    {
      Header: "",
      columns: [
        {
          Header: "Block/Unit No",
          id: "unit_no",
          accessor: "unit[0].short_name",
          width: 150
        },
        {
          Header: "Name",
          id: "name",
          accessor: "name"
        },
        {
          Header: "Email",
          id: "email",
          accessor: "email",
          Cell: ({ original }: any) => {
            return original?.email === undefined || original?.email === "" ? (
              "N/A"
            ) : (
              <div>{original?.email}</div>
            )
          }
        },
        {
          Header: "Mobile No.",
          id: "mobile_no",
          accessor: "mobile_no",
          Cell: ({ original }: any) => {
            return original?.mobile_no === undefined ||
              original?.mobile_no === "" ? (
              "N/A"
            ) : (
              <div>{original?.mobile_no}</div>
            )
          }
        }
      ]
    },
    {
      fixed: "right",
      disableFilters: true,
      columns: [
        {
          Header: "Action",
          Cell: (row: any): JSX.Element => {
            return (
              <Button
                color="primary"
                onClick={() => createAccount && createAccount(row?.original)}
              >
                RESET CODE
              </Button>
            )
          },
          filterable: false
        }
      ]
    }
  ]
}

export default tableHeaders
