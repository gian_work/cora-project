import * as React from "react"
import { makeStyles, Theme } from "@material-ui/core/styles"
import Card from "@material-ui/core/Card"
import Box from "@material-ui/core/Box"

const Icon = (
  <svg
    width="18"
    height="20"
    viewBox="0 0 18 20"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M2.88829 0V2H1.93461C0.882702 2 0.0272522 2.897 0.0272522 4V18C0.0272522 19.103 0.882702 20 1.93461 20H15.2861C16.338 20 17.1935 19.103 17.1935 18V4C17.1935 2.897 16.338 2 15.2861 2H14.3324V0H12.4251V2H4.79564V0H2.88829ZM1.93461 4H15.2861V6H1.93461V4ZM1.93461 8H15.2861L15.288 18H1.93461V8ZM11.2814 9.30078L8.13352 12.5996L6.41615 10.8008L5.08249 12.1992L8.13352 15.4004L12.6151 10.6992L11.2814 9.30078Z"
      fill="#464646"
    />
  </svg>
)

const useStyles = makeStyles((theme: Theme) => ({
  container: {
    minHeight: "335px",
    height: "100%"
  },
  title: {
    color: theme.palette.body.secondary,
    fontSize: "16px",
    fontWeight: 600
  },
  timelineTitle: {
    color: theme.palette.body.main,
    fontSize: "14px"
  },
  timelineSubtitle: {
    color: theme.palette.body.secondary,
    fontSize: "12px"
  },
  dot: {
    width: "8px",
    height: "8px",
    backgroundColor: "#D14339",
    position: "absolute",
    top: "5px",
    left: "-25px",
    borderRadius: "50%"
  }
}))

const Reminders: React.FC = () => {
  const { container, dot, title, timelineTitle, timelineSubtitle } = useStyles()

  return (
    <Card className={container}>
      <Box
        display="flex"
        alignItems="center"
        padding="16px 23px"
        borderBottom="1px solid #F2F2F2"
      >
        <Box padding="5px 15px 0 0">{Icon}</Box>
        <Box className={title}>ActiveBooking</Box>
      </Box>

      <Box
        borderLeft="3px solid #EDF0F1"
        padding="10px 20px 0 20px"
        margin="20px 30px 30px"
      >
        <Box
          display="flex"
          flexDirection="column"
          paddingLeft="15px"
          marginBottom="15px"
          position="relative"
        >
          <Box className={timelineTitle}>KTV Room 1</Box>
          <Box className={timelineSubtitle}>01/27/2020 | 17:00 - 22:00</Box>
          <Box className={dot} />
        </Box>

        <Box
          display="flex"
          flexDirection="column"
          paddingLeft="15px"
          marginBottom="15px"
          position="relative"
        >
          <Box className={timelineTitle}>Service Provider</Box>
          <Box className={timelineSubtitle}>01/27/2020 | 17:00 - 22:00 </Box>
          <Box className={dot} />
        </Box>

        <Box
          display="flex"
          flexDirection="column"
          paddingLeft="15px"
          marginBottom="15px"
          position="relative"
        >
          <Box className={timelineTitle}>Delivery / Pickup</Box>
          <Box className={timelineSubtitle}>01/27/2020 | 17:00 - 22:00 </Box>
          <Box className={dot} />
        </Box>

        <Box
          display="flex"
          flexDirection="column"
          paddingLeft="15px"
          marginBottom="15px"
          position="relative"
        >
          <Box className={timelineTitle}>Council Meeting</Box>
          <Box className={timelineSubtitle}>7:00 pm - 9:00 pm</Box>
          <Box className={dot} />
        </Box>
      </Box>
    </Card>
  )
}

export default Reminders
