import React, { useContext } from "react"
import List from "@material-ui/core/List"
import ListItem from "@material-ui/core/ListItem"

/** Context */
import { WrapperContext } from "pages/Dashboard/UnitManagement/components/UnitDetails"
/** Styles */
import { IconEdit } from "components/Icons/ContextMenu"
import styles from "./styles"

interface ContextOwnerProps {
  data: Record<string, any>
}

const ContextBookKeeping: React.FC<ContextOwnerProps> = ({ data }) => {
  const { listItem } = styles()
  const { showEdit } = useContext(WrapperContext)

  /** Methods */
  const handleEdit = (dataItems: Record<string, any>): void => {
    showEdit && showEdit(dataItems)
  }

  return (
    <>
      <List component="nav">
        <ListItem className={listItem} button onClick={(): void => handleEdit(data)}>
          <i>{IconEdit}</i>
          Edit
        </ListItem>
        {/* <ListItem className={listItem} button>
          <i>{IconDelete}</i>
          Delete
        </ListItem> */}
      </List>
      {/* <Dialog
        action={() => handleUpdateStatus()}
        isOpen={openDialog}
        setOpen={setOpenDialog}
        actionLabel="Confirm"
        title=""
        message="Are you sure you want to update the status?"
      /> */}
    </>
  )
}

export default ContextBookKeeping
