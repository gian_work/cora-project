import { makeStyles } from "@material-ui/core/styles"

const styles = makeStyles(() => ({
  container: {},
  header: {
    color: "#3B4859",
    fontSize: "16px",
    fontWeight: 700,
    borderBottom: "1px solid #F2F2F2",
    padding: "20px"
  },
  space: {
    paddingBottom: "20px"
  },
  twocol: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    "& > div:first-child": {
      flex: 2
    },
    "& > div:last-child": {
      flex: 1
    }
  }
}))

export default styles
