import { makeStyles, Theme } from "@material-ui/core/styles"

export const styles = makeStyles((theme: Theme) => ({
  title: {
    color: theme.palette.body.secondary,
    fontSize: "16px",
    fontWeight: 600
  }
}))
