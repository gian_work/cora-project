import React from "react"
import Box from "@material-ui/core/Box"
import Card from "@material-ui/core/Card"

/** Components */
import Label from "components/Forms/Label"
import Value from "components/Forms/Value"

/** Context */
import { Context as MainContext } from "context/Dashboard/UnitManagement/categories/main/Context"

/** Styles */
import styles from "./styles"

const colors = ["red", "green", "blue", "orange", "yellow"]

const Details: React.FC = () => {
  const { space, twocol } = styles()
  const { activeData } = React.useContext(MainContext)
  const ad = activeData

  return (
    <Card>
      <Box display="flex" padding="25px 36px">
        <Box width="60%">
          <Box className={space}>
            <Label label="name" />
            <Value val={ad?.name} />
          </Box>
          <Box className={space}>
            <Label label="block / unit no." />
            <Value val={ad?.unit[0].short_name} />
          </Box>
          <Box>
            <Box className={twocol}>
              <Box className={space}>
                <Label label="email" />
                <Value val={ad?.email} />
              </Box>
              <Box className={space}>
                <Label label="mobile number" />
                <Value val={ad?.mobile_no} />
              </Box>
            </Box>
          </Box>
          <Box>
            <Box className={twocol}>
              <Box className={space}>
                <Label label="registered address" />
                <Value val={ad?.registered_address} />
              </Box>
              <Box className={space}>
                <Label label="login code" />
                <Value val={ad?.access_code} />
              </Box>
            </Box>
          </Box>
        </Box>
        <Box display="flex" justifyContent="flex-end" width="40%">
          <Box
            width="150px"
            height="150px"
            borderRadius="50%"
            bgcolor={colors[Math.floor(Math.random() * colors.length)]}
            overflow="hidden"
            display="flex"
            alignItems="center"
            justifyContent="center"
          >
            <Box fontSize="100px" fontWeight="700" color="#FFFFFF">
              {ad?.name[0]}
            </Box>
          </Box>
        </Box>
      </Box>
    </Card>
  )
}

export default Details
