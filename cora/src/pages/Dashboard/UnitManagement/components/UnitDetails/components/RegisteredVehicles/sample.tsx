const data = [
  {
    "name": "Range Rover",
    "registeredDate": "01/21/2018",
    "serialNo": "123456",
    "vehicleNo": "SBY1111A",
    "IUNo": "123456789"
  },
  {
    "name": "BMW",
    "registeredDate": "01/21/2018",
    "serialNo": "123456",
    "vehicleNo": "SDJ2222P",
    "IUNo": "123456790"
  },
  {
    "name": "Bentley Phantom",
    "registeredDate": "01/21/2018",
    "serialNo": "123456",
    "vehicleNo": "QPR998W",
    "IUNo": "123456791"
  }
]

export default data
