import React from "react"
import Box from "@material-ui/core/Box"
import { Formik } from "formik"

/** Component */
import FormWrapper from "components/Forms/FormWrapper"
import FormInput from "components/Forms/FormInput"
import RefButton from "components/Forms/RefButton"
import BlockUnit from "components/Common/BlockUnit"
import Names from "components/Common/Names"
import DatePicker from "components/Forms/DatePicker"
import UploadBox from "components/UploadBox/new"

/** Context */
import { CtxType } from "context/Dashboard/UnitManagement/forms/vehicles/createVehicle/Context"
import withContext from "context/Dashboard/UnitManagement/forms/vehicles/createVehicle/withContext"

/** Config */
import { addVehicleDetails } from "config/Dashboard/UnitManagement/validation"

/** Refs */
import { refSubmit } from "context/Dashboard/UnitManagement/forms/vehicles/createVehicle/View"

const PersonalDetails: React.FC<CtxType> = ({
  vehicleDetails,
  setVehicleDetails,
  handleDocsUpload,
  removeDoc,
  photos,
  attaching
}) => {
  const {
    carLabel,
    vehicleNumber,
    IUnumber,
    vehicleModel,
    blockUnit,
    ownerAccountUID,
    activationDate,
    expiryDate
  } = vehicleDetails

  /** Methods */
  const handleFormChange = (setFieldValue: Function, key: string, e: any): void => {
    if (e.target === undefined) {
      setVehicleDetails &&
        setVehicleDetails({
          ...vehicleDetails,
          [key]: e
        })
      setFieldValue(key, e)
    } else {
      setVehicleDetails &&
        setVehicleDetails({
          ...vehicleDetails,
          [key]: e?.target?.value
        })
      setFieldValue(key, e?.target?.value)
    }
  }

  const handleOwnerName = (value: Record<string, any>): void => {
    const ownerInfo = JSON.parse(value.currentTarget?.dataset?.userinfo)
    setTimeout(() => {
      setVehicleDetails &&
        setVehicleDetails({
          ...vehicleDetails,
          ownerName: ownerInfo?.name && ownerInfo?.name,
          ownerAccountUID: ownerInfo?.account_info_uid
        })
    }, 500)
  }

  const handleBlockUnit = (setFieldValue: any, value: string | boolean) => {
    return [
      setFieldValue("blockUnit", value),
      setVehicleDetails &&
        setVehicleDetails({
          ...vehicleDetails,
          blockUnit: value,
          ownerAccountUID: ""
        })
    ]
  }

  return (
    <div>
      <Box margin="auto" padding="0 0 40px 0">
        <Formik
          initialValues={{
            blockUnit,
            ownerAccountUID,
            carLabel,
            vehicleNumber,
            IUnumber,
            vehicleModel,
            activationDate,
            expiryDate
          }}
          onSubmit={(values: any) => {
            return values
          }}
          validationSchema={addVehicleDetails}
        >
          {({
            touched,
            errors,
            handleBlur,
            handleSubmit,
            setFieldValue
          }): JSX.Element => {
            return (
              <form onSubmit={handleSubmit}>
                <Box margin="auto" width="60%" padding="0 0 30px 0">
                  <Box borderBottom="1px solid #F2F2F2">
                    <FormWrapper title="Personal Details">
                      <Box display="flex" marginBottom="30px">
                        <Box flex="1" marginRight="15px">
                          <BlockUnit
                            value={blockUnit}
                            onChange={(e: any) =>
                              handleBlockUnit(setFieldValue, e.target.value)}
                            onBlur={(e: Event): void => handleBlur(e)}
                            error={touched.blockUnit && Boolean(errors.blockUnit)}
                            helperText={
                              touched.blockUnit && errors.blockUnit?.toString()
                            }
                          />
                        </Box>

                        <Box flex="1">
                          <Names
                            forID
                            value={ownerAccountUID}
                            onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                              handleFormChange(setFieldValue, "ownerAccountUID", e)
                              handleOwnerName(e)
                            }}
                            unitUID={blockUnit}
                            onBlur={(e: Event) => handleBlur(e)}
                            error={
                              touched.ownerAccountUID &&
                              Boolean(errors.ownerAccountUID)
                            }
                            helperText={
                              errors.ownerAccountUID &&
                              touched.ownerAccountUID &&
                              errors.ownerAccountUID.toString()
                            }
                          />
                        </Box>
                      </Box>
                    </FormWrapper>
                  </Box>

                  <Box borderBottom="1px solid #F2F2F2">
                    <FormWrapper title="Vehicle Details">
                      <Box display="flex" marginBottom="30px">
                        <Box flex="1" marginRight="15px">
                          <FormInput
                            name="carLabel"
                            value={carLabel}
                            idValue="carLabel"
                            label="Car Label"
                            placeholder="Car Label"
                            handleOnChange={(
                              e: React.ChangeEvent<HTMLInputElement>
                            ): void =>
                              handleFormChange(setFieldValue, "carLabel", e)}
                            onBlur={handleBlur}
                            error={touched.carLabel && Boolean(errors.carLabel)}
                            helperText={
                              errors.carLabel && touched.carLabel && errors.carLabel
                            }
                          />
                        </Box>
                        <Box flex="1">
                          <FormInput
                            name="IUnumber"
                            value={IUnumber}
                            idValue="IUnumber"
                            label="IU Number"
                            placeholder="IU Number"
                            handleOnChange={(
                              e: React.ChangeEvent<HTMLInputElement>
                            ): void =>
                              handleFormChange(setFieldValue, "IUnumber", e)}
                            onBlur={handleBlur}
                            error={touched.IUnumber && Boolean(errors.IUnumber)}
                            helperText={
                              errors.IUnumber && touched.IUnumber && errors.IUnumber
                            }
                          />
                        </Box>
                      </Box>

                      <Box display="flex" marginBottom="30px">
                        <Box flex="1" marginRight="15px">
                          <FormInput
                            name="vehicleNumber"
                            value={vehicleNumber}
                            idValue="vehicleNumber"
                            label="Vehicle Number"
                            placeholder="Vehicle Number"
                            handleOnChange={(
                              e: React.ChangeEvent<HTMLInputElement>
                            ): void =>
                              handleFormChange(setFieldValue, "vehicleNumber", e)}
                            onBlur={handleBlur}
                            error={
                              touched.vehicleNumber && Boolean(errors.vehicleNumber)
                            }
                            helperText={
                              errors.vehicleNumber &&
                              touched.vehicleNumber &&
                              errors.vehicleNumber
                            }
                          />
                        </Box>
                        <Box flex="1">
                          <FormInput
                            name="vehicleModel"
                            value={vehicleModel}
                            idValue="vehicleModel"
                            label="Vehicle Model"
                            placeholder="Vehicle Model"
                            handleOnChange={(
                              e: React.ChangeEvent<HTMLInputElement>
                            ): void =>
                              handleFormChange(setFieldValue, "vehicleModel", e)}
                            onBlur={handleBlur}
                            error={
                              touched.vehicleModel && Boolean(errors.vehicleModel)
                            }
                            helperText={
                              errors.vehicleModel &&
                              touched.vehicleModel &&
                              errors.vehicleModel
                            }
                          />
                        </Box>
                      </Box>

                      <Box display="flex" marginBottom="30px">
                        <Box flex="1" marginRight="15px" maxWidth="49%">
                          <DatePicker
                            placeholder="mm/dd/yyyy"
                            format="MM/dd/yyyy"
                            minDate={activationDate}
                            label="Activation Date"
                            name="activationDate"
                            value={activationDate}
                            handleDateChange={(value: string): void =>
                              handleFormChange(
                                setFieldValue,
                                "activationDate",
                                value
                              )}
                            onBlur={(e: Event): void => handleBlur(e)}
                            error={
                              touched.activationDate &&
                              Boolean(errors.activationDate)
                            }
                            helperText={
                              touched.activationDate &&
                              errors.activationDate?.toString()
                            }
                          />
                        </Box>
                        <Box flex="1" maxWidth="49%">
                          <DatePicker
                            placeholder="mm/dd/yyyy"
                            format="MM/dd/yyyy"
                            minDate={expiryDate}
                            label="Expiry Date"
                            name="expiryDate"
                            value={expiryDate}
                            handleDateChange={(value: string): void =>
                              handleFormChange(setFieldValue, "expiryDate", value)}
                            onBlur={(e: Event): void => handleBlur(e)}
                            error={touched.expiryDate && Boolean(errors.expiryDate)}
                            helperText={
                              touched.expiryDate && errors.expiryDate?.toString()
                            }
                          />
                        </Box>
                      </Box>
                    </FormWrapper>
                  </Box>
                  <Box>
                    <FormWrapper title="Photo">
                      <UploadBox
                        hasTitle={false}
                        onDrop={(event: any) => handleDocsUpload(event)}
                        files={photos}
                        removePhoto={(event: any) => removeDoc(event)}
                        attaching={attaching}
                        title="Choose file to upload"
                      />
                    </FormWrapper>
                  </Box>
                </Box>
                <RefButton action={handleSubmit} refValue={refSubmit} />
              </form>
            )
          }}
        </Formik>
      </Box>
    </div>
  )
}

export default withContext(PersonalDetails)
