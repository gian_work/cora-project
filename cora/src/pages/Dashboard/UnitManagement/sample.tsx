const ApplicationsData = [
  {
    blockUnitNo: "21/#00-00",
    name: "soh sai bor (su caibo)",
    status: "live",
    ownersTenants: "owner",
    residency: "N",
    tenancyStart: "01/20/2018",
    tenancyEnd: "01/20/2019",
    tenancyDoc: "N/A",
    email: "email@email.com",
    phone: "1234567",
    remarks: "Landlord, no cards, no cars"
  },
  {
    blockUnitNo: "21/#00-00",
    name: "mrs soh",
    status: "live",
    ownersTenants: "owner",
    residency: "N",
    tenancyStart: "01/20/2018",
    tenancyEnd: "01/20/2019",
    tenancyDoc: "N/A",
    email: "mrssoh@email.com",
    phone: "1234567",
    remarks: "Landlord, no cards, no cars"
  },
  {
    blockUnitNo: "21/#00-00",
    name: "soh son",
    status: "live",
    ownersTenants: "owner",
    residency: "N",
    tenancyStart: "01/20/2018",
    tenancyEnd: "01/20/2019",
    tenancyDoc: "N/A",
    email: "sohson@email.com",
    phone: "1234567",
    remarks: "Landlord, no cards, no cars"
  }
]

export default ApplicationsData
