import React from "react"
import useSWR from "swr"
import Fade from "@material-ui/core/Fade"
/** Service */
import BookkeepingAPI from "services/Dashboard/Bookkeeping"
/** Components */
import Table from "components/Table"
/** Context */
import withMainContext from "context/Dashboard/UnitManagement/categories/main/withContext"
import tableHeaders from "./table/tableHeaders"

interface TableAllProps {
  activeFilter: number
}

const TableAll: React.FC<TableAllProps> = ({ activeFilter }) => {
  /** Methods */
  const fetchScheduledPayment = async () => BookkeepingAPI.getScheduledPayments()

  const { data, isValidating } = useSWR(
    "fetchScheduledPayment",
    fetchScheduledPayment
  )

  const tableData = data?.data?._data
  const filteredData = tableData?.filter((i: any) => i.frequency === activeFilter)
  const activeData = activeFilter !== 0 ? filteredData : tableData

  return (
    <Fade in={!isValidating} timeout={1000}>
      <div>
        <Table
          data={activeData || []}
          columns={tableHeaders}
          size={10}
          isFilterable
        />
      </div>
    </Fade>
  )
}

export default withMainContext(TableAll)
