import React from "react"
/** Component */
import ContextMenu from "components/ContextMenu"
/** Config */
import { frequency } from "config/Dashboard/Bookkeeping"
/** Utils */
import dateHelper from "utils/date"
import ContextItems from "../ContextMenu"
/** Interface */
interface RowOriginalProps {
  original: {
    frequency: string
    unit: { short_name: string }
    amt: number
    start_date: Date
    notify_on_days_before: number
    flags: number
    notify_on_days_after_due: number
  }
  amt: number
  frequency: string
  notify_on_days_before: number
  flags: number
  notify_on_days_after_due: number
}

const tableHeaders = [
  {
    fixed: "left",
    disableFilters: true,
    columns: [
      {
        width: 70,
        Cell: ({ row }: any): JSX.Element => {
          return (
            <ContextMenu>
              <ContextItems data={row._original} />
            </ContextMenu>
          )
        },
        filterable: false
      }
    ]
  },
  {
    Header: "",
    columns: [
      {
        Header: "Block/Unit No",
        id: "blockUnitNo",
        accessor: "unit.short_name",
        width: 150,
        Cell: ({ original }: RowOriginalProps): JSX.Element => {
          return <div>{original.unit.short_name}</div>
        }
      },
      {
        Header: "Payment Name",
        id: "paymentName",
        accessor: "payment_name",
        width: 250
      },
      {
        Header: "Frequency",
        id: "frequency",
        accessor: (c: RowOriginalProps): string => {
          return frequency[c.frequency]
        },
        Cell: ({ original }: RowOriginalProps): JSX.Element => {
          return <div>{`${frequency[original.frequency]}`}</div>
        },
        width: 150
      },
      {
        Header: "Amount",
        id: "amount",
        accessor: (c: RowOriginalProps): string => {
          return c.amt.toFixed(2)
        },
        Cell: ({ original }: RowOriginalProps): string | JSX.Element => {
          return original.amt === undefined ? (
            "N/A"
          ) : (
            <div>{original.amt.toFixed(2)}</div>
          )
        },
        width: 150
      },
      {
        Header: "Start Date",
        id: "startDate",
        Cell: ({ original }: RowOriginalProps): string => {
          return original.start_date === undefined
            ? "N/A"
            : dateHelper.fromUnix(original.start_date)
        },
        width: 150
      },
      {
        Header: "Email Reminder",
        id: "emailReminder",
        accessor: (c: RowOriginalProps): string => {
          return `${c.notify_on_days_before} days`
        },
        Cell: ({ original }: RowOriginalProps): JSX.Element => {
          return <div>{`${original.notify_on_days_before} days`}</div>
        },
        width: 200
      },
      {
        Header: "Send Email",
        id: "sendEmail",
        accessor: (c: RowOriginalProps): string => {
          return c.flags === 1 ? "Yes" : "No"
        },
        Cell: ({ original }: RowOriginalProps): JSX.Element => {
          return <div>{`${original.flags === 1 ? "Yes" : "No"}`}</div>
        },
        width: 150
      },
      {
        Header: "Re-send Email",
        id: "resendEmail",
        accessor: (c: RowOriginalProps): string => {
          return `${c.notify_on_days_after_due} days`
        },
        Cell: ({ original }: RowOriginalProps): JSX.Element => {
          return <div>{`${original.notify_on_days_after_due} days`}</div>
        },
        width: 150
      },
      {
        Header: "Name",
        id: "payeeName",
        accessor: "payee_name",
        width: 250
      },
      {
        Header: "Email",
        id: "payeeEmail",
        accessor: "payee_email",
        width: 250
      },
      {
        Header: "Phone",
        id: "payeePhone",
        accessor: "payee_phone",
        width: 250
      },
      {
        Header: "Address",
        id: "payeeAddress",
        accessor: "payee_registered_address",
        width: 350
      }
    ]
  }
]

export default tableHeaders
