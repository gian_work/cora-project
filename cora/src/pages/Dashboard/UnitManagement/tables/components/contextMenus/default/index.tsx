import React from "react"
import List from "@material-ui/core/List"
import ListItem from "@material-ui/core/ListItem"

/** Styles */
// import {Context} from "context/Dashboard/UnitManagement/categories/accountsRequests/Context"

/** Context */
import { Context as CategoriesMainCtx } from "context/Dashboard/UnitManagement/categories/main/Context"
import { Context as OwnersCtx } from "context/Dashboard/UnitManagement/categories/owners/Context"
import { Context as ResidentsCtx } from "context/Dashboard/UnitManagement/categories/residents/Context"
import { IconEdit, IconView } from "components/Icons/ContextMenu"
import styles from "./styles"

interface ContextOwnerProps {
  data: Record<string, any>
}

const ContextBookKeeping: React.FC<ContextOwnerProps> = ({ data }) => {
  const { listItem } = styles()

  const { showDetails, showEditOwner, showEditResident } = React.useContext(
    CategoriesMainCtx
  )

  const Owners = React.useContext(OwnersCtx)
  const Residents = React.useContext(ResidentsCtx)

  /** Methods */
  const handleEdit = (dataItems: Record<string, any>): void => {
    const type = dataItems?.owner_or_tenant
    Owners.setActiveData && Owners.setActiveData(dataItems)
    Residents.setActiveData && Residents.setActiveData(dataItems)

    if (type === 1) {
      showEditOwner && showEditOwner()
    }

    if (type === 2) {
      showEditResident && showEditResident()
    }
  }

  return (
    <>
      <List component="nav">
        <ListItem
          className={listItem}
          button
          onClick={(): void => showDetails && showDetails(data)}
        >
          <i>{IconView}</i>
          View Details
        </ListItem>
        <ListItem className={listItem} button onClick={(): void => handleEdit(data)}>
          <i>{IconEdit}</i>
          Edit
        </ListItem>
        {/* <ListItem className={listItem} button>
          <i>{IconDelete}</i>
          Delete
        </ListItem> */}
      </List>
      {/* <Dialog
        action={() => handleUpdateStatus()}
        isOpen={openDialog}
        setOpen={setOpenDialog}
        actionLabel="Confirm"
        title=""
        message="Are you sure you want to update the status?"
      /> */}
    </>
  )
}

export default ContextBookKeeping
