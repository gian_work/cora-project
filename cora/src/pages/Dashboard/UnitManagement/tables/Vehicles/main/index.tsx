import React from "react"
import useSWR from "swr"
import Fade from "@material-ui/core/Fade"
/** Service */
import service from "services/Dashboard/Applications"
/** Components */
import Table from "components/Table"
/** Context */
import withMainContext from "context/Dashboard/UnitManagement/categories/main/withContext"
import tableHeaders from "./table/tableHeaders"

interface TableAllProps {
  activeFilter: number
}

const TableVehiclesList: React.FC<TableAllProps> = () => {
  /** Methods */
  const fetchUnitManagementVehicles = async () => service.getVehicles()

  const { data, isValidating } = useSWR(
    "fetchUnitManagementVehicles",
    fetchUnitManagementVehicles,
    {
      revalidateOnFocus: true
    }
  )

  const tableData = data?.data?._data

  return (
    <Fade in={!isValidating} timeout={1000}>
      <div>
        <Table
          data={tableData || []}
          columns={tableHeaders}
          size={10}
          isFilterable
        />
      </div>
    </Fade>
  )
}

export default withMainContext(TableVehiclesList)
