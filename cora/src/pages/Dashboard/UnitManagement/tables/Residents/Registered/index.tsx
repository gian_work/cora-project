import React from "react"
import useSWR from "swr"
import Fade from "@material-ui/core/Fade"
/** Service */
import UnitManagementAPI from "services/Dashboard/UnitManagement"
/** Components */
import Table from "components/Table"
/** Context */
import withMainContext from "context/Dashboard/UnitManagement/categories/main/withContext"
import tableHeaders from "../../components/tableHeaders/default"

interface TableAllProps {
  activeFilter: number
}

const TableAll: React.FC<TableAllProps> = () => {
  /** Methods */
  const fetchResidentsRegistered = async () =>
    UnitManagementAPI.getResidents({
      "is_registered": true,
      "owner_or_tenant": "2",
      "is_resident": true,
      "is_deactivated": false
    })

  const { data, isValidating } = useSWR(
    "fetchResidentsRegistered",
    fetchResidentsRegistered,
    {
      revalidateOnFocus: true
    }
  )

  const tableData = data?.data?._data

  return (
    <Fade in={!isValidating} timeout={1000}>
      <div>
        <Table
          data={tableData || []}
          columns={tableHeaders()}
          size={10}
          isFilterable
        />
      </div>
    </Fade>
  )
}

export default withMainContext(TableAll)
