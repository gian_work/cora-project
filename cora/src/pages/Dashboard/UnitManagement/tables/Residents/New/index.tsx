import React from "react"
import useSWR from "swr"
import Fade from "@material-ui/core/Fade"
/** Service */
import UnitManagementAPI from "services/Dashboard/UnitManagement"
/** Components */
import Table from "components/Table"
/** Context */
import { Context as CtxAccountRequest } from "context/Dashboard/UnitManagement/categories/residents/Context"
import { Context as ParentContext } from "context/Dashboard/UnitManagement/categories/main/Context"
import tableHeaders from "../../components/tableHeaders/default"

const TableNew: React.FC = () => {
  const { setAccountRequestData } = React.useContext(CtxAccountRequest)
  const { showCreateFromData } = React.useContext(ParentContext)

  /** Methods */
  const fetchResidentsNew = async () =>
    UnitManagementAPI.getResidents({
      "is_registered": false,
      "owner_or_tenant": "1,2",
      "is_resident": true,
      "is_deactivated": false
    })

  const { data, isValidating } = useSWR("fetchResidentsNew", fetchResidentsNew, {
    revalidateOnFocus: true
  })

  const tableData = data?.data?._data

  const createAccount = (activeData: Record<string, any>) => {
    setAccountRequestData && setAccountRequestData(activeData)
    showCreateFromData && showCreateFromData()
  }

  return (
    <Fade in={!isValidating} timeout={1000}>
      <div>
        <Table
          data={tableData || []}
          columns={tableHeaders(createAccount)}
          size={10}
          isFilterable
        />
      </div>
    </Fade>
  )
}

export default TableNew
