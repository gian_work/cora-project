import React from "react"
import useSWR from "swr"
import Fade from "@material-ui/core/Fade"
import Cookie from "js-cookie"
/** Service */
import UnitManagementAPI from "services/Dashboard/UnitManagement"
/** Components */
import Table from "components/Table"
/** Context */
import { Context as CtxAccountRequest } from "context/Dashboard/UnitManagement/categories/accountsRequests/Context"
import { Context as ParentContext } from "context/Dashboard/UnitManagement/categories/main/Context"

import tableHeaders from "./table/tableHeaders"

const TableNew: React.FC = () => {
  const condoUID = Cookie.get("condoUID")
  const { setAccountRequestData } = React.useContext(CtxAccountRequest)
  const { showCreateFromData } = React.useContext(ParentContext)

  /** Methods */
  const fetchAccountRequests = async () =>
    UnitManagementAPI.getAccountRequests({
      "condo_uid": condoUID
    })

  const { data, isValidating } = useSWR(
    "fetchAccountRequests",
    fetchAccountRequests,
    {
      revalidateOnFocus: true
    }
  )

  const tableData = data?.data?._data

  const createAccount = (activeData: Record<string, any>) => {
    setAccountRequestData && setAccountRequestData(activeData)
    showCreateFromData && showCreateFromData()
  }

  return (
    <Fade in={!isValidating} timeout={1000}>
      <div>
        <Table
          data={tableData || []}
          columns={tableHeaders(createAccount)}
          size={10}
          minRows={1}
          isFilterable
        />
      </div>
    </Fade>
  )
}

export default TableNew
