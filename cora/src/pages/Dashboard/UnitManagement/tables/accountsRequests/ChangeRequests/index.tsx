import React from "react"
import useSWR from "swr"
import Fade from "@material-ui/core/Fade"
import Cookie from "js-cookie"
/** Service */
import UnitManagementAPI from "services/Dashboard/UnitManagement"
/** Components */
import Table from "components/Table"
/** Context */
import withMainContext from "context/Dashboard/UnitManagement/categories/main/withContext"
import tableHeaders from "./table/tableHeaders"

interface TableAllProps {
  activeFilter: number
}

const TableAll: React.FC<TableAllProps> = () => {
  const condoUID = Cookie.get("condoUID")

  /** Methods */
  const fetchChangeAddressRequests = async () =>
    UnitManagementAPI.getChangeAddressRequests({
      "condo_uid": condoUID,
      "is_approved": false
    })

  const { data, isValidating } = useSWR(
    "fetchChangeAddressRequests",
    fetchChangeAddressRequests,
    {
      revalidateOnFocus: true
    }
  )

  const tableData = data?.data?._data
  const filteredData = tableData?.filter((i: any) => i.is_approved === false)

  return (
    <Fade in={!isValidating} timeout={1000}>
      <div>
        <Table
          data={filteredData || []}
          columns={tableHeaders}
          size={10}
          minRows={1}
          isFilterable
        />
      </div>
    </Fade>
  )
}

export default withMainContext(TableAll)
