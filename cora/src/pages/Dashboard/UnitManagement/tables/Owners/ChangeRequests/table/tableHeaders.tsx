import React from "react"

import ContextMenu from "components/ContextMenu"

import ContextItems from "../../../components/contextMenus/changeAddress"

const tableHeaders = [
  {
    fixed: "left",
    disableFilters: true,
    columns: [
      {
        width: 70,
        Cell: (row: any): JSX.Element => {
          return (
            <ContextMenu>
              <ContextItems data={row?.original} />
            </ContextMenu>
          )
        },
        filterable: false
      }
    ]
  },
  {
    Header: "",
    columns: [
      {
        Header: "Current Block/Unit No",
        id: "current_unit.short_name",
        accessor: "current_unit.short_name",
        width: 200
      },
      {
        Header: "New Block/Unit No",
        id: "new_unit.short_name",
        accessor: "new_unit.short_name",
        width: 200
      },
      {
        Header: "Name",
        id: "name",
        accessor: "user_detail.name"
      },
      {
        Header: "Email",
        id: "email",
        accessor: "user_detail.email"
      },
      {
        Header: "Mobile No.",
        id: "mobile_no",
        accessor: "user_detail.mobile_no"
      }
    ]
  }
]

export default tableHeaders
