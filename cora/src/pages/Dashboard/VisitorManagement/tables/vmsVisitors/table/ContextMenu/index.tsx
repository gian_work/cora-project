import React, { useState } from "react"
import List from "@material-ui/core/List"
import ListItem from "@material-ui/core/ListItem"
import DialogMessage from "components/Dialog"
import { mutate } from "swr"
import { toast } from "react-toastify"

/** Context */
import { CtxType } from "context/Dashboard/VMS/main/Context"
import withContext from "context/Dashboard/VMS/main/withContext"
import { CtxMenuContextConsumer, AddBookingCtxType } from "components/ContextMenu"
/** Service */
import vmsAPI from "services/Dashboard/VMS"
/** styles */
import styles from "./styles"

interface ContextVMSProps extends CtxType {
  status?: number | string
  data: any
}

const ContextVMS: React.FC<ContextVMSProps> = ({ data }) => {
  const { listItem } = styles()
  const [open, setOpen] = useState(false)
  const [option, setOption] = useState(0)

  /** Notification */
  const notifySuccess = (message: string) =>
    toast(message, {
      type: toast.TYPE.SUCCESS
    })

  const updateStatus = async (statusType: number): Promise<Function> => {
    const params = {
      "vms_uid": data.vms._uid,
      "status": statusType
    }

    try {
      await vmsAPI
        .updateStatusVMS(data.vms_type, params)
        .then(() => notifySuccess("Successfully updated the status."))
      return mutate("fetchAllVisitors")
    } catch (e) {
      return e
    }
  }

  const handleOptions = (optionType: number): void => {
    setOpen(true)
    setOption(optionType)
  }

  return (
    <CtxMenuContextConsumer>
      {({ openContext }: Partial<AddBookingCtxType>): JSX.Element => (
        <>
          {data?.vms_type !== 2 ? (
            <List component="nav">
              <ListItem
                className={listItem}
                button
                onClick={(): Array<Function> => [
                  openContext && openContext(null),
                  handleOptions(1)
                ]}
              >
                {/* <i>{IconPending}</i> */}
                Pending
              </ListItem>
              <ListItem
                className={listItem}
                button
                onClick={(): Array<Function> => [
                  openContext && openContext(null),
                  handleOptions(2)
                ]}
              >
                {/* <i>{IconCancel}</i> */}
                arrived
              </ListItem>
              <ListItem
                className={listItem}
                button
                onClick={(): Array<Function> => [
                  openContext && openContext(null),
                  handleOptions(3)
                ]}
              >
                {/* <i>{IconClose}</i> */}
                closed
              </ListItem>
              <ListItem
                className={listItem}
                button
                onClick={(): Array<Function> => [
                  openContext && openContext(null),
                  handleOptions(4)
                ]}
              >
                {/* <i>{IconClose}</i> */}
                Cancelled
              </ListItem>
            </List>
          ) : (
            <List component="nav">
              {/* 
                 "All 0",
                  "Pending 1",
                  "Arrived 2",
                  "Closed 3",
                  "Cancelled 4",
                  "Received 5",
                  "Collected 6",
              */}
              <ListItem
                className={listItem}
                button
                onClick={(): Array<Function> => [
                  openContext && openContext(null),
                  handleOptions(1)
                ]}
              >
                {/* <i>{IconPending}</i> */}
                Pending
              </ListItem>
              <ListItem
                className={listItem}
                button
                onClick={(): Array<Function> => [
                  openContext && openContext(null),
                  handleOptions(2)
                ]}
              >
                {/* <i>{IconCancel}</i> */}
                arrived
              </ListItem>
              <ListItem
                className={listItem}
                button
                onClick={(): Array<Function> => [
                  openContext && openContext(null),
                  handleOptions(5)
                ]}
              >
                {/* <i>{IconApprove}</i> */}
                Received
              </ListItem>
              <ListItem
                className={listItem}
                button
                onClick={(): Array<Function> => [
                  openContext && openContext(null),
                  handleOptions(6)
                ]}
              >
                {/* <i>{IconClose}</i> */}
                Collected
              </ListItem>
              <ListItem
                className={listItem}
                button
                onClick={(): Array<Function> => [
                  openContext && openContext(null),
                  handleOptions(3)
                ]}
              >
                {/* <i>{IconClose}</i> */}
                Closed
              </ListItem>
              <ListItem
                className={listItem}
                button
                onClick={(): Array<Function> => [
                  openContext && openContext(null),
                  handleOptions(4)
                ]}
              >
                {/* <i>{IconClose}</i> */}
                Cancelled
              </ListItem>
            </List>
          )}
          <DialogMessage
            action={(): Promise<Function> => updateStatus(option)}
            actionLabel="OK"
            isOpen={open}
            setOpen={setOpen}
            title=""
            message="Are you sure you want to update the status?"
          />
        </>
      )}
    </CtxMenuContextConsumer>
  )
}

export default withContext(ContextVMS)
