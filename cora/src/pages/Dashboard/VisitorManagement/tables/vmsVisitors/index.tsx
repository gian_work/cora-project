import React from "react"
import useSWR from "swr"
import Fade from "@material-ui/core/Fade"
import Cookie from "js-cookie"
/** Service */
import vmsAPI from "services/Dashboard/VMS"
/** Components */
import Table from "components/Table"
/** Context */
import tableHeaders from "./table/tableHeaders"
/** Interface */
interface TableVisitorsProps {
  activeFilter: number
}
const TableVisitors: React.FC<TableVisitorsProps> = ({ activeFilter }) => {
  const fetchAllVisitors = async () =>
    vmsAPI.getVMS({
      "vms_type": 1,
      "condo_uid": Cookie.get("condoUID")
    })

  const { data, isValidating } = useSWR("fetchAllVisitors", fetchAllVisitors, {
    revalidateOnFocus: true
  })
  const tableData = data?.data?._data
  const filteredData = tableData?.filter((i: any) => i.vms.status === activeFilter)
  const activeData = activeFilter !== 0 ? filteredData : tableData

  return (
    <Fade in={!isValidating} timeout={1000}>
      <div>
        <Table
          data={activeData || []}
          columns={tableHeaders}
          size={10}
          isFilterable
        />
      </div>
    </Fade>
  )
}

export default TableVisitors
