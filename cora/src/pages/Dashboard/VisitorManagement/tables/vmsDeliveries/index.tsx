import React from "react"
import useSWR from "swr"
import Fade from "@material-ui/core/Fade"
import Cookie from "js-cookie"
/** Service */
import vmsAPI from "services/Dashboard/VMS"
/** Components */
import Table from "components/Table"
/** Config */
import tableHeaders from "./table/tableHeaders"

interface TableDeliveriesProps {
  activeFilter: number
}
const TableDeliveries: React.FC<TableDeliveriesProps> = ({ activeFilter }) => {
  const fetchAllDeliveries = async () =>
    vmsAPI.getVMS({
      "vms_type": 2,
      "condo_uid": Cookie.get("condoUID")
    })

  const { data, isValidating } = useSWR("fetchAllDeliveries", fetchAllDeliveries)
  const tableData = data?.data?._data
  const filteredData = tableData?.filter((i: any) => i.vms.status === activeFilter)
  const activeData = activeFilter !== 0 ? filteredData : tableData

  return (
    <Fade in={!isValidating} timeout={1000}>
      <div>
        <Table
          data={activeData || []}
          columns={tableHeaders}
          size={10}
          isFilterable
        />
      </div>
    </Fade>
  )
}

export default TableDeliveries
