import React from "react"
import useSWR from "swr"
import Fade from "@material-ui/core/Fade"
import Cookie from "js-cookie"
/** Service */
import vmsAPI from "services/Dashboard/VMS"
/** Components */
import Table from "components/Table"
/** Config */
import tableHeaders from "./table/tableHeaders"

interface TableServiceProvidersProps {
  activeFilter: number
}

const TableServiceProviders: React.FC<TableServiceProvidersProps> = ({
  activeFilter
}) => {
  const fetchAllServiceProviders = async () =>
    vmsAPI.getVMS({
      "vms_type": 3,
      "condo_uid": Cookie.get("condoUID")
    })

  const { data, isValidating } = useSWR(
    "fetchAllServiceProviders",
    fetchAllServiceProviders
  )
  const tableData = data?.data?._data
  const filteredData = tableData?.filter((i: any) => i.vms.status === activeFilter)
  const activeData = activeFilter !== 0 ? filteredData : tableData

  return (
    <Fade in={!isValidating} timeout={1000}>
      <div>
        <Table
          data={activeData || []}
          columns={tableHeaders}
          size={10}
          isFilterable
        />
      </div>
    </Fade>
  )
}

export default TableServiceProviders
