import React from "react"
import useSWR from "swr"
import Fade from "@material-ui/core/Fade"
import Cookie from "js-cookie"

/** Service */
import vmsAPI from "services/Dashboard/VMS"
/** Components */
import Table from "components/Table"
/** Context */
import tableHeaders from "./table/tableHeaders"

interface TableServiceProvidersProps {
  activeFilter: number
}

const TableAll: React.FC<TableServiceProvidersProps> = () => {
  const fetchAllVMS = async () =>
    vmsAPI.getVMS({
      "condo_uid": Cookie.get("condoUID")
    })

  const { data, isValidating } = useSWR("fetchAllVMS", fetchAllVMS)
  const tableData = data?.data?._data

  return (
    <Fade in={!isValidating}>
      <div>
        <Table
          data={tableData || []}
          columns={tableHeaders}
          size={10}
          isFilterable
        />
      </div>
    </Fade>
  )
}

export default TableAll
