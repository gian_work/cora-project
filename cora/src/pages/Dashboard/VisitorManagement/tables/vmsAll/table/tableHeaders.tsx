import React from "react"
import dateHelper from "utils/date"
/** Components */
import ContextMenu from "components/ContextMenu"
import StatusBlock from "components/Table/components/StatusBlock"
/** Config */
import vmsConfig from "config/Dashboard/VMS"
import ColorStatus from "utils/status"
import ContextVMS from "./ContextMenu"
/** Interface */
interface RowOriginalProps {
  original: {
    status: string
    vms_type: number
    vms: {
      status: string
      mobile: string
      email: string
      purpose: string
      no_of_persons: string
      contact_person: string
      otp: string
      vms_type: number
      multiple_persons: boolean
      start_date_ms: Date
      end_date_ms: Date
      eta_ms: Date
    }
  }
}

const TableHeaderColumns: Array<{}> = [
  {
    fixed: "left",
    disableFilters: true,
    columns: [
      {
        width: 70,
        Cell: (row: RowOriginalProps): JSX.Element => {
          return (
            <ContextMenu>
              <ContextVMS data={row.original} />
            </ContextMenu>
          )
        },
        filterable: false
      }
    ]
  },
  {
    Header: "",
    columns: [
      {
        Header: "Status",
        id: "status",
        width: 150,
        accessor: (c: any) => {
          const type =
            c.vms_type === 1 || c.vms_type === 3
              ? vmsConfig.status.visitor
              : vmsConfig.status.delivery

          return type[c.vms.status]
        },
        Cell: ({ original }: RowOriginalProps): JSX.Element => {
          const type =
            original.vms_type === 1 || original.vms_type === 3
              ? vmsConfig.status.visitor
              : vmsConfig.status.delivery
          const { status } = original.vms
          return (
            <StatusBlock
              status={type[status]}
              color={ColorStatus.code(type[status])}
            />
          )
        }
      },
      {
        Header: "Block/Unit No.",
        id: "blockUnitNo",
        accessor: "unit.short_name",
        width: 150
      },
      {
        Header: "Name",
        id: "name",
        accessor: "vms.name",
        width: 200
      },
      {
        Header: "Mobile",
        id: "mobile",
        accessor: "vms.mobile",
        width: 200,
        Cell: ({ original }: RowOriginalProps): string => {
          return original.vms.mobile === "null" || original.vms.mobile === ""
            ? "N/A"
            : original.vms.mobile
        }
      },
      {
        Header: "Email",
        id: "email",
        accessor: "vms.email",
        width: 200,
        Cell: ({ original }: RowOriginalProps): string => {
          return original.vms.email === "null" || original.vms.email === ""
            ? "N/A"
            : original.vms.email
        }
      },
      {
        Header: "Category",
        id: "category",
        accessor: "vms_type",
        width: 150,
        Cell: ({ original }: RowOriginalProps): string => {
          if (original.vms_type === 1) {
            return "Visitors"
          }
          if (original.vms_type === 2) {
            return "Deliveries/Pickup"
          }
          return "Service Providers"
        }
      },
      {
        Header: "Purpose",
        id: "purpose",
        accessor: "vms.purpose",
        width: 150,
        Cell: ({ original }: RowOriginalProps): string => {
          return original.vms.purpose === undefined ||
            original.vms.purpose === "null"
            ? "N/A"
            : original.vms.purpose
        }
      },
      {
        Header: "Start Date",
        id: "startDate",
        accessor: "vms.start_date_ms",
        width: 150,
        Cell: ({ original }: RowOriginalProps): string => {
          return original.vms.start_date_ms === undefined
            ? "N/A"
            : dateHelper.fromUnix(original.vms.start_date_ms)
        }
      },
      {
        Header: "End Date",
        id: "endDate",
        accessor: "vms.end_date",
        width: 150,
        Cell: ({ original }: RowOriginalProps): string => {
          return original.vms.end_date_ms === undefined
            ? "N/A"
            : dateHelper.fromUnix(original.vms.end_date_ms)
        }
      },
      {
        Header: "Multiple Visitors",
        id: "visitorsCount",
        accessor: "visitorsCount",
        width: 150,
        Cell: ({ original }: RowOriginalProps): string => {
          return original.vms.multiple_persons === true ? "Yes" : "No"
        }
      },
      {
        Header: "Visitors Count",
        id: "visitorsCount",
        accessor: "vms.no_of_persons",
        width: 150,
        Cell: ({ original }: RowOriginalProps): string => {
          return original.vms.no_of_persons === undefined
            ? "N/A"
            : original.vms.no_of_persons
        }
      },
      {
        Header: "Visitation Date",
        id: "vmDate",
        accessor: "vmDate",
        width: 150,
        Cell: ({ original }: RowOriginalProps): string => {
          return original.vms.eta_ms !== undefined
            ? dateHelper.fromUnix(original.vms.eta_ms)
            : "N/A"
        }
      },
      {
        Header: "Visitation Time",
        id: "vmTime",
        accessor: "vmTime",
        width: 150,
        Cell: ({ original }: RowOriginalProps): string => {
          return dateHelper.fromUnixTime(original.vms.eta_ms)
        }
      },
      {
        Header: "OTP",
        id: "otp",
        accessor: "vms.otp",
        width: 150,
        Cell: ({ original }: RowOriginalProps): string => {
          return original.vms.otp === undefined ? "N/A" : original.vms.otp
        }
      },
      {
        Header: "Resident Name",
        id: "vmNames",
        accessor: "vms.contact_person",
        width: 150,
        Cell: ({ original }: RowOriginalProps): string => {
          return original.vms.contact_person === ""
            ? "n/a"
            : original.vms.contact_person
        }
      },
      {
        Header: "Remarks",
        id: "remarks",
        accessor: "vms.remarks",
        width: 150
      }
    ]
  }
]

export default TableHeaderColumns
