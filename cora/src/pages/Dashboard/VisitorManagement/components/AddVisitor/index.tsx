import React from "react"

import AddUpdateProvider, {
  AddVisitorProps
} from "context/Dashboard/VMS/add-update/AddUpdateProvider"
import AddUpdateView from "context/Dashboard/VMS/add-update/AddUpdateView"

const AddUpdateMain: React.FC<AddVisitorProps> = ({ showform }) => (
  <AddUpdateProvider showform={showform}>
    <AddUpdateView />
  </AddUpdateProvider>
)

export default AddUpdateMain
