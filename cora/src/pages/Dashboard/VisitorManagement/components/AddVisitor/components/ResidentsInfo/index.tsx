import React from "react"
import Box from "@material-ui/core/Box"
import FormControlLabel from "@material-ui/core/FormControlLabel"
import Checkbox from "@material-ui/core/Checkbox"
import FormGroup from "@material-ui/core/FormGroup"
import { Formik } from "formik"
/** Components */
import BlockUnit from "components/Common/BlockUnit"
import Names from "components/Common/Names"
import FormWrapper from "components/Forms/FormWrapper"
import RefButton from "components/Forms/RefButton"
/** Context */
import withContext from "context/Dashboard/VMS/add-update/withContext"
import { AddUpdateCtxType } from "context/Dashboard/VMS/add-update/AddUpdateContext"
/** Validation */
import { vmsResidentInfo } from "config/Dashboard/VMS/validation"
/** Styles */
import { refSubmit } from "context/Dashboard/VMS/add-update/AddUpdateView"
import styles from "./styles"
/** Ref */

const ResidentInfo: React.FC<AddUpdateCtxType> = ({
  setVisitorDetails,
  visitorDetails
}) => {
  const { generalInfo } = styles()
  const validationSchema = visitorDetails?.byAdmin ? null : vmsResidentInfo.default

  const handleFormChange = (
    setFieldValue: Function,
    key: string,
    value: string | boolean
  ): void =>
    setFieldValue(key, value).then(
      () =>
        setVisitorDetails &&
        setVisitorDetails({
          ...visitorDetails,
          [key]: value
        })
    )

  const handleBlockUnit = (
    setFieldValue: Function,
    value: string | boolean
  ): void => {
    setFieldValue("unitUid", value).then(
      () =>
        setVisitorDetails &&
        setVisitorDetails({
          ...visitorDetails,
          unitUid: value,
          contactPerson: ""
        })
    )
  }

  return (
    <div className={generalInfo}>
      <Formik
        initialValues={{
          unitUid: "",
          contactPerson: ""
        }}
        onSubmit={(values, actions): void => {
          JSON.stringify(values, null, 2)
          actions.setSubmitting(false)
        }}
        validationSchema={validationSchema}
      >
        {({
          touched,
          errors,
          handleBlur,
          handleSubmit,
          setFieldValue
        }): JSX.Element => {
          return (
            <form>
              <FormWrapper title="Requester's Information" width="40%">
                <div className="section" style={{ width: "40%", margin: "auto" }}>
                  <Box marginBottom="25px">
                    <FormGroup>
                      <FormControlLabel
                        control={
                          <Checkbox
                            checked={visitorDetails.byAdmin}
                            onChange={(): void =>
                              handleFormChange(
                                setFieldValue,
                                "byAdmin",
                                !visitorDetails.byAdmin
                              )}
                            value={visitorDetails.byAdmin}
                            color="primary"
                          />
                        }
                        label="Admin Visitor"
                      />
                    </FormGroup>
                  </Box>
                  {!visitorDetails.byAdmin && (
                    <Box>
                      <Box marginBottom="25px">
                        <BlockUnit
                          value={visitorDetails?.unitUid}
                          onChange={(e: React.ChangeEvent<HTMLInputElement>): void =>
                            handleBlockUnit(setFieldValue, e.target.value)}
                          onBlur={(e: Event): void => handleBlur(e)}
                          error={touched.unitUid && Boolean(errors.unitUid)}
                          helperText={
                            errors.unitUid && touched.unitUid && errors.unitUid
                          }
                        />
                      </Box>
                      {visitorDetails?.unitUid !== "" && (
                        <Box marginBottom="25px">
                          <Names
                            value={visitorDetails.contactPerson}
                            onChange={(
                              e: React.ChangeEvent<HTMLInputElement>
                            ): void =>
                              handleFormChange(
                                setFieldValue,
                                "contactPerson",
                                e.target.value
                              )}
                            unitUID={visitorDetails.unitUid}
                            onBlur={(e: Event): void => handleBlur(e)}
                            error={
                              touched.contactPerson && Boolean(errors.contactPerson)
                            }
                            helperText={
                              errors.contactPerson &&
                              touched.contactPerson &&
                              errors.contactPerson
                            }
                          />
                        </Box>
                      )}
                    </Box>
                  )}
                </div>
                <RefButton refValue={refSubmit} action={handleSubmit} />
              </FormWrapper>
            </form>
          )
        }}
      </Formik>
    </div>
  )
}

export default withContext(ResidentInfo)
