import { makeStyles } from "@material-ui/core/styles"

const styles = makeStyles(() => ({
  selectType: {
    margin: "30px auto",
    "& .section": {
      marginBottom: "30px",
      borderBottom: "1px solid #F2F2F2",
      margin: "0 auto",
      paddingBottom: "30px"
    }
  },
  userOptions: {
    display: "flex",
    flexDirection: "column"
  },
  spaceBottom: {
    paddingBottom: "30px"
  }
}))

export default styles
