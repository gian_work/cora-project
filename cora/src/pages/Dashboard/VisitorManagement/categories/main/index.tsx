import React from "react"

import VMSMainProvider, {
  VMSMainProviderProps
} from "context/Dashboard/VMS/categories/main/Provider"
import VMSMainView from "context/Dashboard/VMS/categories/main/View"

const VMSMain: React.FC<VMSMainProviderProps> = ({
  showAddForm,
  activeTable
  // contextActions,
}) => (
  <VMSMainProvider showAddForm={showAddForm} activeTable={activeTable}>
    <VMSMainView />
  </VMSMainProvider>
)

export default VMSMain
