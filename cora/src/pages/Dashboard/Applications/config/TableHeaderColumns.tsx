import React from "react"

import StatusBlock from "components/Table/components/StatusBlock"
import ContextMenu from "components/ContextMenu"
import ColorStatus from "utils/status"
import ContextApplications from "../components/ContextMenu"

const TableHeaderColumns = (setActiveRow: Function) => [
  {
    fixed: "left",
    disableFilters: true,
    columns: [
      {
        width: 70,
        Cell: ({ row }: any) => {
          return (
            <ContextMenu setopenDetails={() => null} openDetails>
              <ContextApplications setDetailsActive={setActiveRow} activeRow={row} />
            </ContextMenu>
          )
        },
        filterable: false
      }
    ]
  },
  {
    columns: [
      {
        Header: "Status",
        id: "status",
        accessor: "status",
        sortable: false,
        filterable: false,
        width: 150,
        Cell: ({ original }: any) => (
          <StatusBlock
            status={original.status}
            color={ColorStatus.code(original.status)}
          />
        )
      },
      {
        Header: "Date Received",
        id: "dateReceived",
        accessor: "dateReceived",
        width: 150
      },
      {
        Header: "Block/Unit No.",
        id: "blockUnitNo",
        accessor: "blockUnitNo",
        width: 150
      },
      {
        Header: "Name",
        id: "name",
        accessor: "name",
        width: 200
      },
      {
        Header: "Category",
        id: "category",
        accessor: "category",
        width: 150
      },
      {
        Header: "Description",
        id: "applicationDescription",
        accessor: "applicationDescription",
        width: 150
      },
      {
        Header: "Start Date",
        id: "startDate",
        accessor: "startDate",
        width: 150
      },
      {
        Header: "End Date",
        id: "endDate",
        accessor: "endDate",
        width: 150
      },
      {
        Header: "Vehicle #",
        id: "vehicleNumber",
        accessor: "vehicleNumber",
        width: 150
      },
      {
        Header: "Vehicle IU#",
        id: "vehicleIU",
        accessor: "vehicleIU",
        width: 150
      },
      {
        Header: "Picture",
        id: "picture",
        accessor: "picture",
        width: 150
      },
      {
        Header: "Deposit Due",
        id: "depositDue",
        accessor: "depositDue",
        width: 150
      },
      {
        Header: "Fee Due",
        id: "feeDue",
        accessor: "feeDue",
        width: 150
      },
      {
        Header: "Status Date",
        id: "statusDate",
        accessor: "statusDate",
        width: 150
      },
      {
        Header: "Status Remarks",
        id: "statusRemarks",
        accessor: "statusRemarks",
        width: 150
      },
      {
        Header: "Serial No",
        id: "serialNumber",
        accessor: "serialNumber",
        width: 150
      },
      {
        Header: "Remarks",
        id: "remarks",
        accessor: "remarks",
        width: 150
      },
      {
        Header: "Managed by",
        id: "managedBy",
        accessor: "managedBy",
        width: 150
      }
    ]
  }
]

export default TableHeaderColumns
