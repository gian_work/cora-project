import React from "react"
import useSWR from "swr"
import Fade from "@material-ui/core/Fade"
import Cookie from "js-cookie"
/** Service */
import ApplicationsAPI from "services/Dashboard/Applications"
/** Components */
import Table from "components/Table"
import { Context } from "context/Dashboard/Applications/categories/main/Context"
import tableHeaders from "./table/tableHeaders"
/** Context */

const TableMovingInOut: React.FC = () => {
  const condoUID = Cookie.get("condoUID")
  const { activeFilter } = React.useContext(Context)

  /** Methods */
  const fetchApplicationsMovingInOut = async () =>
    ApplicationsAPI.getApplications({
      "condo_uid": condoUID,
      "application_type": 2
    })

  const { data, isValidating } = useSWR(
    "fetchApplicationsMovingInOut",
    fetchApplicationsMovingInOut,
    {
      revalidateOnFocus: true
    }
  )

  const filterItem = activeFilter !== undefined && activeFilter + 1

  const tableData = data?.data?._data
  const filteredData = tableData?.filter((i: any) => i.status === filterItem)

  return (
    <Fade in={!isValidating} timeout={1000}>
      <div>
        <Table
          data={filteredData || []}
          columns={tableHeaders}
          minRows={2}
          size={10}
          isFilterable
        />
      </div>
    </Fade>
  )
}

export default TableMovingInOut
