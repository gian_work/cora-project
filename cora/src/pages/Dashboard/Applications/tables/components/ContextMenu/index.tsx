import React, { useContext } from "react"
import List from "@material-ui/core/List"
import ListItem from "@material-ui/core/ListItem"

/** Component */
import IconDetails from "components/Icons/ContextMenu/View"

/** Context */
import { CtxMenuContext } from "components/ContextMenu"
import { Context as MainContext } from "context/Dashboard/Applications/categories/main/Context"
import { Context as RootContext } from "context/Dashboard/Applications/main/Context"

/** Context */
import styles from "./styles"

interface ContextProps {
  data: Record<string, any>
}

const ContextNewAccountRequests: React.FC<ContextProps> = ({ data }) => {
  const { listItem } = styles()
  const status = data?.status

  const { openContext } = React.useContext(CtxMenuContext)
  const { showUpdateApplication, showApproveApplication } = useContext(MainContext)
  const { setShowReject, setActiveData } = useContext(RootContext)

  /** Methods */

  function handleReject() {
    setShowReject && setShowReject(true)
    setActiveData && setActiveData(data)
    openContext && openContext(false)
  }

  return (
    <>
      <List component="nav">
        <ListItem className={listItem} button onClick={(): null => null}>
          <i>
            <IconDetails />
          </i>
          View Details
        </ListItem>
        {status === 1 && (
          <ListItem
            className={listItem}
            button
            onClick={() => showUpdateApplication && showUpdateApplication(data)}
          >
            Edit
          </ListItem>
        )}
        {status !== 2 && (
          <ListItem
            className={listItem}
            button
            onClick={() => showApproveApplication && showApproveApplication(data)}
          >
            Approve
          </ListItem>
        )}
        {status !== 3 && (
          <ListItem className={listItem} button onClick={() => handleReject()}>
            Reject
          </ListItem>
        )}
      </List>
    </>
  )
}

export default ContextNewAccountRequests
