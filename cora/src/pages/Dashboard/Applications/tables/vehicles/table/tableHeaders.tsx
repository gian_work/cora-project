import React from "react"

/** Components */
import StatusBlock from "components/Table/components/StatusBlock"
import ContextMenu from "components/ContextMenu"

/** Utils */
import dateHelper from "utils/date"

/** Config */
import { status, statusColor } from "config/Dashboard/Applications"
import ContextApplications from "../../components/ContextMenu"

const TableHeaderColumns = [
  {
    fixed: "left",
    disableFilters: true,
    columns: [
      {
        width: 70,
        Cell: ({ row }: any) => {
          return (
            <ContextMenu>
              <ContextApplications data={row?._original} />
            </ContextMenu>
          )
        },
        filterable: false
      }
    ]
  },
  {
    columns: [
      {
        Header: "Status",
        id: "status",

        sortable: false,
        filterable: false,
        width: 150,
        accessor: (c: any) => {
          return status[c.status]
        },
        Cell: ({ original }: any) => {
          return (
            <StatusBlock
              status={status[original.status]}
              color={statusColor[original.status]}
            />
          )
        }
      },
      {
        Header: "Date Received",
        id: "dateReceived",
        width: 150,
        accessor: (c: any) => {
          return dateHelper.fromUnix(c.application_date)
        },
        Cell: ({ original }: any) => {
          return original.application_date === undefined
            ? "N/A"
            : dateHelper.fromUnix(original.application_date)
        }
      },
      {
        Header: "Block/Unit No.",
        id: "blockUnitNo",
        accessor: "blockUnitNo",
        width: 150,
        Cell: ({ original }: any) => {
          return <div>{original.unit.short_name}</div>
        }
      },
      {
        Header: "Name",
        id: "applicant_name",
        accessor: "applicant_name",
        width: 200
      },
      // {
      //   Header: "Category",
      //   id: "application_type",
      //   accessor: "application_type",
      //   width: 150,
      // },
      {
        Header: "Start Date",
        id: "startDate",
        width: 150,
        accessor: (c: any) => {
          return dateHelper.fromUnix(c.start_date)
        },
        Cell: ({ original }: any) => {
          return original.start_date === undefined
            ? "N/A"
            : dateHelper.fromUnix(original.start_date)
        }
      },
      {
        Header: "End Date",
        id: "endDate",
        width: 150,
        accessor: (c: any) => {
          return dateHelper.fromUnix(c.end_date)
        },
        Cell: ({ original }: any) => {
          return original.end_date === undefined
            ? "N/A"
            : dateHelper.fromUnix(original.end_date)
        }
      },
      {
        Header: "Vehicle #",
        id: "vehicleNumber",
        width: 150,
        accessor: (c: any) => {
          return c?.request_data?.vehicle?.vehicle_no
        },
        Cell: ({ original }: any) => {
          return original?.request_data?.vehicle?.vehicle_no === ""
            ? "N/A"
            : original?.request_data?.vehicle?.vehicle_no
        }
      },
      {
        Header: "Vehicle IU#",
        id: "vehicleIU",
        width: 150,
        accessor: (c: any) => {
          return c?.request_data?.vehicle?.iu_no
        },
        Cell: ({ original }: any) => {
          return original?.request_data?.vehicle?.iu_no === ""
            ? "N/A"
            : original?.request_data?.vehicle?.iu_no
        }
      },
      // {
      //   Header: "Picture",
      //   id: "picture",
      //   accessor: "picture",
      //   width: 150,
      // },
      // {
      //   Header: "Deposit Due",
      //   id: "depositDue",
      //   accessor: "depositDue",
      //   width: 150,
      // },
      // {
      //   Header: "Fee Due",
      //   id: "feeDue",
      //   accessor: "feeDue",
      //   width: 150,
      // },
      // {
      //   Header: "Status Date",
      //   id: "statusDate",
      //   accessor: "statusDate",
      //   width: 150,
      // },
      // {
      //   Header: "Status Remarks",
      //   id: "statusRemarks",
      //   accessor: "statusRemarks",
      //   width: 150,
      // },
      // {
      //   Header: "Serial No",
      //   id: "serialNumber",
      //   accessor: "serialNumber",
      //   width: 150,
      // },
      // {
      //   Header: "Managed by",
      //   id: "managedBy",
      //   accessor: "managedBy",
      //   width: 150,
      // },
      {
        Header: "Remarks",
        id: "remarks",
        accessor: (c: any) => {
          return c?.remarks
        },
        Cell: ({ original }: any) => {
          return original?.remarks === "" ? "N/A" : <div>{original?.remarks}</div>
        }
      }
    ]
  }
]

export default TableHeaderColumns
