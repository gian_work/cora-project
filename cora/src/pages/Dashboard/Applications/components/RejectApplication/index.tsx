import React, { useState } from "react"
import Box from "@material-ui/core/Box"
import Button from "@material-ui/core/Button"
import { Formik } from "formik"
import { toast } from "react-toastify"
import { mutate } from "swr"

/** Components */
import Drawer from "components/Drawer/new"
import Header from "components/Header"
import FormInput from "components/Forms/FormInput"
import FormWrapper from "components/Forms/FormWrapper"
import Label from "components/Forms/Label"

/** Service */
import Service from "services/Dashboard/Applications"

/** Config */
import { tableName } from "config/Dashboard/Applications"

/** Styles */

/** Utils */
import { fromUnix } from "utils/date"

/** Validation */
import { rejectApplication } from "config/Dashboard/Applications/validation"
import styles from "./styles"

interface RejectApplicationProps {
  openState: boolean
  setOpenState: Function
  data?: Record<string, any>
}

const RejectApplication: React.FC<RejectApplicationProps> = ({
  openState,
  setOpenState,
  data
}) => {
  const { value } = styles()

  const [remarks, setRemarks] = useState("")
  const [isSubmitting, setSubmitting] = useState(false)

  /** Notification */
  const notifyRejected = () =>
    toast("Successfully rejected the application", {
      type: toast.TYPE.SUCCESS
    })

  /** Methods */

  const RejectItem = async () => {
    const itemPayload = {
      "_uid": data?._uid,
      "remarks": remarks
    }
    setSubmitting(true)
    try {
      const response = await Service.rejectApplication(itemPayload).then(() => {
        setSubmitting(false)
        setOpenState(false)
        setRemarks("")
        notifyRejected()
        mutate(tableName[0])
      })
      return response
    } catch (e) {
      setSubmitting(false)
      return e
    }
  }

  function handleReject(handleSubmit: Function): void {
    handleSubmit()

    if (remarks === "") {
      return
    }

    RejectItem()
  }

  const handleFormChange = (
    name: string,
    e: React.ChangeEvent<HTMLInputElement>,
    handleChange: Function,
    setFieldTouched: Function,
    setFieldValue: Function
  ) => {
    handleChange(e)
    setFieldTouched(name, true, false)
    setFieldValue(name, e.target.value)
    setRemarks(e.target.value)
  }

  return (
    <>
      <Drawer openState={openState} setOpenState={setOpenState}>
        <Header title="Reject Application" />

        <Box padding="40px 30px">
          <Box borderBottom="1px solid #F2F2F2">
            <Box marginBottom="15px">
              <Label label="Applicant Name" />
              <Box className={value}>{data?.applicant_name}</Box>
            </Box>

            <Box marginBottom="15px">
              <Label label="Application Date" />
              <Box className={value}>{fromUnix(data?.application_date)}</Box>
            </Box>

            <Box marginBottom="15px">
              <Label label="Serial No." />
              <Box className={value}>
                {data?.serial_no === "" ? "N/A" : data?.serial_no}
              </Box>
            </Box>
          </Box>

          <Formik
            initialValues={{
              remarks
            }}
            onSubmit={(values, actions): void => {
              JSON.stringify(values, null, 2)
              actions.setSubmitting(false)
            }}
            validationSchema={rejectApplication}
          >
            {({
              touched,
              errors,
              handleBlur,
              handleSubmit,
              setFieldValue,
              handleChange,
              setFieldTouched
            }): JSX.Element => {
              return (
                <form>
                  <FormWrapper>
                    <Box marginBottom="25px">
                      <FormInput
                        name="remarks"
                        value={remarks}
                        idValue="remarks"
                        label="Remarks"
                        placeholder="Remarks"
                        multiline
                        rows={3}
                        handleOnChange={(
                          e: React.ChangeEvent<HTMLInputElement>
                        ): void =>
                          handleFormChange(
                            "remarks",
                            e,
                            handleChange,
                            setFieldTouched,
                            setFieldValue
                          )}
                        onBlur={handleBlur}
                        error={touched.remarks && Boolean(errors.remarks)}
                        helperText={
                          errors.remarks && touched.remarks && errors.remarks
                        }
                      />
                    </Box>
                    <Box display="flex" justifyContent="space-between">
                      <Box flex="1" maxWidth="49%">
                        <Button
                          variant="outlined"
                          color="primary"
                          size="large"
                          onClick={() => setOpenState(false)}
                          className="btn-cancel"
                          fullWidth
                          disabled={isSubmitting}
                        >
                          Cancel
                        </Button>
                      </Box>
                      <Box flex="1" maxWidth="49%">
                        <Button
                          variant="contained"
                          color="primary"
                          size="large"
                          onClick={() => handleReject(handleSubmit)}
                          className="btn-cancel"
                          fullWidth
                          disabled={isSubmitting}
                        >
                          {isSubmitting ? "Submitting..." : "Reject"}
                        </Button>
                      </Box>
                    </Box>
                  </FormWrapper>
                </form>
              )
            }}
          </Formik>
        </Box>
      </Drawer>
    </>
  )
}

export default RejectApplication
