import React from "react"
import Box from "@material-ui/core/Box"

/** Components */
import FormWrapper from "components/Forms/FormWrapper"
import UploadBox from "components/UploadBox/new"

/** Context */
import { CtxType } from "context/Dashboard/Applications/forms/addApplication/Context"
import withContext from "context/Dashboard/Applications/forms/addApplication/withContext"

/** Styles */
import styles from "./styles"

const SupportingDocument: React.FC<CtxType> = ({
  handleDocsUpload,
  photos,
  removeDoc,
  attaching
}) => {
  const { root } = styles()

  return (
    <div className={root}>
      <FormWrapper title="Photos" width="60%">
        <Box width="60%" margin="auto" padding="0 0 40px 0">
          <UploadBox
            hasTitle={false}
            onDrop={(event: any) => handleDocsUpload(event, 1)}
            files={photos}
            removePhoto={(event: any) => removeDoc(event, 1)}
            attaching={attaching}
            title="Choose file to upload"
          />
        </Box>
      </FormWrapper>
    </div>
  )
}

export default withContext(SupportingDocument)
