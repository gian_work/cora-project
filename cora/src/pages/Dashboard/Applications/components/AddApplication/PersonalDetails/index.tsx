import React from "react"
import Box from "@material-ui/core/Box"
import { Formik } from "formik"
/** Components */
import BlockUnit from "components/Common/BlockUnit"
import Names from "components/Common/Names"
import FormWrapper from "components/Forms/FormWrapper"
import RefButton from "components/Forms/RefButton"

/** Context */
import { Context as MainContext } from "context/Dashboard/Applications/forms/addApplication/Context"

/** Validation */
import { addApplicationsResidentInfo } from "config/Dashboard/Applications/validation"

/** Styles */
import { refSubmit } from "context/Dashboard/Applications/forms/addApplication/View"
import styles from "./styles"

const ResidentInfo: React.FC = () => {
  const { generalInfo } = styles()
  const {
    setApplicationDetails,
    applicationDetails,
    setResidentInfo
  } = React.useContext(MainContext)

  const { unitUID, applicantUID } = applicationDetails

  const handleFormChange = (
    setFieldValue: Function,
    key: string,
    value: string | boolean
  ): void => {
    return setFieldValue(key, value).then(
      () =>
        setApplicationDetails &&
        setApplicationDetails({
          ...applicationDetails,
          [key]: value
        })
    )
  }

  /* eslint-disable @typescript-eslint/no-explicit-any */
  const handleResidentName = (value: Record<string, any>): void => {
    const residentInfo = JSON.parse(value.currentTarget?.dataset?.userinfo)
    setResidentInfo &&
      setResidentInfo({
        name: residentInfo?.name
      })

    return (
      setApplicationDetails &&
      setApplicationDetails({
        ...applicationDetails,
        applicationName: residentInfo?.name,
        applicantUID: residentInfo?.account_info_uid
      })
    )
  }

  const handleBlockUnit = (
    setFieldValue: Function,
    value: string | boolean
  ): void => {
    setFieldValue("unitUID", value).then(
      () =>
        setApplicationDetails &&
        setApplicationDetails({
          ...applicationDetails,
          unitUID: value,
          applicantUID: ""
        })
    )
  }

  return (
    <div className={generalInfo}>
      <Formik
        initialValues={{
          unitUID,
          applicantUID
        }}
        onSubmit={(values, actions): void => {
          JSON.stringify(values, null, 2)
          actions.setSubmitting(false)
        }}
        validationSchema={addApplicationsResidentInfo}
      >
        {({
          touched,
          errors,
          handleBlur,
          handleSubmit,
          setFieldValue
        }): JSX.Element => {
          return (
            <form>
              <FormWrapper title="Personal Details" width="40%">
                <div
                  className="section"
                  style={{
                    width: "40%",
                    margin: "auto"
                  }}
                >
                  <Box>
                    <Box marginBottom="25px">
                      <BlockUnit
                        value={unitUID}
                        onChange={(e: React.ChangeEvent<HTMLInputElement>): void =>
                          handleBlockUnit(setFieldValue, e.target.value)}
                        onBlur={(e: Event): void => handleBlur(e)}
                        error={touched.unitUID && Boolean(errors.unitUID)}
                        helperText={
                          errors.unitUID &&
                          touched.unitUID &&
                          errors.unitUID.toString()
                        }
                      />
                    </Box>

                    <Box marginBottom="25px">
                      <Names
                        value={applicantUID}
                        onChange={(e: any): void => {
                          handleFormChange(
                            setFieldValue,
                            "applicantUID",
                            e.target.value
                          )
                          handleResidentName(e)
                        }}
                        forID
                        unitUID={unitUID}
                        onBlur={(e: Event): void => handleBlur(e)}
                        error={touched.applicantUID && Boolean(errors.applicantUID)}
                        helperText={
                          errors.applicantUID &&
                          touched.applicantUID &&
                          errors.applicantUID.toString()
                        }
                      />
                    </Box>
                  </Box>
                </div>
                <RefButton refValue={refSubmit} action={handleSubmit} />
              </FormWrapper>
            </form>
          )
        }}
      </Formik>
    </div>
  )
}

export default ResidentInfo
