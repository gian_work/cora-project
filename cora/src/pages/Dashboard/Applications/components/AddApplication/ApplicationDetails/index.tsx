import React from "react"
import Box from "@material-ui/core/Box"
import { Formik } from "formik"
/** Components */
import FormInput from "components/Forms/FormInput"
import FormWrapper from "components/Forms/FormWrapper"
import RefButton from "components/Forms/RefButton"
import DatePicker from "components/Forms/DatePicker"
import Spacer from "components/Spacer"

/** Context */
import { Context as MainContext } from "context/Dashboard/Applications/forms/addApplication/Context"
import { Context as RootContext } from "context/Dashboard/Applications/main/Context"

/** Validation */
import { addApplicationDetails } from "config/Dashboard/Applications/validation"

/** Styles */
import { refSubmitApplicationDetails } from "context/Dashboard/Applications/forms/addApplication/View"
import styles from "./styles"

const VehicleRegistration = React.lazy(() =>
  import(
    "pages/Dashboard/Applications/components/AddApplication/Types/VehicleRegistration"
  )
)
const MovingInOut = React.lazy(() =>
  import("pages/Dashboard/Applications/components/AddApplication/Types/MovingInOut")
)

const ResidentInfo: React.FC = () => {
  const { generalInfo } = styles()
  const { applicationDetails, setApplicationDetails } = React.useContext(MainContext)
  const { activeTable } = React.useContext(RootContext)
  const { startDate, endDate, serialNo, remarks } = applicationDetails

  const FormView = (type: number): JSX.Element => {
    switch (type) {
      case 0:
        return <MovingInOut />
      case 1:
        return <VehicleRegistration />
      default:
        return <div />
    }
  }

  /** Methods */
  const handleFormChange = (
    name: string,
    e: React.ChangeEvent<HTMLInputElement>,
    handleChange: Function,
    setFieldTouched: Function
  ) => {
    handleChange(e)
    setFieldTouched(name, true, false)
    setApplicationDetails &&
      setApplicationDetails({
        ...applicationDetails,
        [name]: e.target.value
      })
  }

  const handleDateChange = (name: string, e: string, setFieldValue: Function) => {
    setFieldValue(name, e)
    setApplicationDetails &&
      setApplicationDetails({
        ...applicationDetails,
        [name]: e
      })
  }

  return (
    <div className={generalInfo}>
      <Box borderBottom="1px solid #F2F2F2">{FormView(activeTable || 0)}</Box>

      <Box>
        <Formik
          initialValues={{
            serialNo,
            remarks,
            startDate,
            endDate
          }}
          onSubmit={(values, actions): void => {
            JSON.stringify(values, null, 2)
            actions.setSubmitting(false)
          }}
          validationSchema={addApplicationDetails}
        >
          {({
            touched,
            errors,
            handleBlur,
            handleSubmit,
            setFieldValue,
            handleChange,
            setFieldTouched
          }): JSX.Element => {
            return (
              <form>
                <FormWrapper title="Application Details" width="60%">
                  <div
                    className="section"
                    style={{
                      width: "60%",
                      margin: "auto"
                    }}
                  >
                    <Box marginBottom="25px">
                      <FormInput
                        name="serialNo"
                        value={serialNo}
                        idValue="serialNo"
                        label="Serial No."
                        placeholder="Serial No."
                        handleOnChange={(
                          e: React.ChangeEvent<HTMLInputElement>
                        ): void =>
                          handleFormChange(
                            "serialNo",
                            e,
                            handleChange,
                            setFieldTouched
                          )}
                        onBlur={handleBlur}
                        error={touched.serialNo && Boolean(errors.serialNo)}
                        helperText={
                          errors.serialNo && touched.serialNo && errors.serialNo
                        }
                      />
                    </Box>

                    <Box marginBottom="25px">
                      <FormInput
                        name="remarks"
                        value={remarks}
                        idValue="remarks"
                        label="Remarks"
                        placeholder="Remarks"
                        multiline
                        rows={3}
                        handleOnChange={(
                          e: React.ChangeEvent<HTMLInputElement>
                        ): void =>
                          handleFormChange(
                            "remarks",
                            e,
                            handleChange,
                            setFieldTouched
                          )}
                        onBlur={handleBlur}
                        error={touched.remarks && Boolean(errors.remarks)}
                        helperText={
                          errors.remarks && touched.remarks && errors.remarks
                        }
                      />
                    </Box>

                    <Box display="flex" marginBottom="25px">
                      <Box flex="1">
                        <DatePicker
                          label="Start Date"
                          name="startDate"
                          format="MM/dd/yyyy"
                          value={startDate}
                          placeholder="mm/dd/yyyy"
                          handleDateChange={(value: string): void =>
                            handleDateChange("startDate", value, setFieldValue)}
                          onBlur={(e: Event): void => handleBlur(e)}
                          error={touched.startDate && Boolean(errors.startDate)}
                          helperText={
                            errors.startDate && touched.startDate && errors.startDate
                          }
                        />
                      </Box>
                      <Spacer isDefault />
                      <Box flex="1">
                        <DatePicker
                          label="End Date"
                          name="endDate"
                          format="MM/dd/yyyy"
                          value={endDate}
                          placeholder="mm/dd/yyyy"
                          minDate={startDate}
                          handleDateChange={(value: string): void =>
                            handleDateChange("endDate", value, setFieldValue)}
                          onBlur={(e: Event): void => handleBlur(e)}
                          error={touched.endDate && Boolean(errors.endDate)}
                          helperText={
                            errors.endDate && touched.endDate && errors.endDate
                          }
                        />
                      </Box>
                    </Box>
                  </div>
                  <RefButton
                    refValue={refSubmitApplicationDetails}
                    action={handleSubmit}
                  />
                </FormWrapper>
              </form>
            )
          }}
        </Formik>
      </Box>
    </div>
  )
}

export default ResidentInfo
