import React from "react"
import Box from "@material-ui/core/Box"
import TextField from "@material-ui/core/TextField"
import Select from "@material-ui/core/Select"
import MenuItem from "@material-ui/core/MenuItem"
import FormControl from "@material-ui/core/FormControl"
import Button from "@material-ui/core/Button"
import { makeStyles, Theme } from "@material-ui/core/styles"

// components
import Header from "../../../../../components/Header"

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    display: "flex",
    padding: "0 0 8px",
    "& .MuiTextField-root": {
      flex: 1
    }
  },
  formControl: {
    width: "100%"
  },
  formControlButtons: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    width: "100%",
    padding: "0 30px"
  },
  formControlButton: {
    width: "49%"
  },
  labelStyle: {
    fontSize: "10px",
    marginBottom: "8px",
    color: theme.palette.body.secondary,
    textTransform: "uppercase"
  },
  section: {
    margin: "30px 0 0 0",
    padding: "0 30px 10px",
    borderBottom: "1px solid #F2F2F2"
  }
}))

// interface
interface ApplicationFormProps {
  title: string
  subtitle: string
  hasData?: boolean
  setType?: Function
}

const ApplicationForm: React.FC<ApplicationFormProps> = ({
  title,
  subtitle,
  hasData,
  setType
}) => {
  const {
    labelStyle,
    root,
    formControl,
    formControlButtons,
    formControlButton,
    section
  } = useStyles()

  const [name, setName] = React.useState(hasData ? "Damian Lillard" : "")
  const [remarks, setremarks] = React.useState(
    hasData ? "This is a sample Remarks" : ""
  )

  const handleName = (event: React.ChangeEvent<{ value: unknown }>) => {
    setName(event.target.value as string)
  }

  const handleRemarks = (event: React.ChangeEvent<{ value: unknown }>) => {
    setremarks(event.target.value as string)
  }

  const handleCancel = () => {
    if (setType) {
      setType("")
    }
  }

  const Spacer = () => <div style={{ width: "20px", height: "20px" }} />

  return (
    <Box display="flex" flexDirection="column" height="100%" width="100%">
      <Header title={title} subtitle={subtitle} />

      <Box display="flex" flexDirection="column" flex="1">
        <Box className={section}>
          <Box>
            <Box className={labelStyle}>Block/Unit No.</Box>
            <TextField placeholder="00#00-00" fullWidth variant="outlined" />
          </Box>
          <Spacer />
          <Box>
            <Box className={labelStyle}>Personal Details</Box>
            <FormControl variant="outlined" className={formControl}>
              <Select displayEmpty value={name} onChange={handleName}>
                <MenuItem value="" disabled>
                  Select Name
                </MenuItem>
                <MenuItem value="Damian Lillard">Damian Lillard</MenuItem>
                <MenuItem value="Paul George">Paul George</MenuItem>
                <MenuItem value="Bradley Beal">Bradley Beal</MenuItem>
              </Select>
            </FormControl>
          </Box>
          <Spacer />
        </Box>

        <Box margin="30px 0 0 0" padding="0 30px" className={section}>
          <Box>
            <Box className={labelStyle}>Payment Description</Box>
            <FormControl variant="outlined" className={formControl}>
              <Select displayEmpty value={name} onChange={handleName}>
                <MenuItem value="" disabled>
                  Payment Type
                </MenuItem>
                <MenuItem value="Payment type 1">Payment type 1</MenuItem>
                <MenuItem value="Payment type 2">Payment type 2</MenuItem>
                <MenuItem value="Payment type 3">Payment type 3</MenuItem>
              </Select>
            </FormControl>
          </Box>
          <Spacer />
          <Box>
            <Box className={labelStyle}>Amount</Box>
            <TextField placeholder="$ 00.00" fullWidth variant="outlined" />
          </Box>
          <Spacer />
          <Box>
            <Box className={labelStyle}>Tax</Box>
            <TextField placeholder="$ 00.00" fullWidth variant="outlined" />
          </Box>
          <Spacer />
          <Box className={root}>
            <TextField
              multiline
              rows={5}
              name="Remarks"
              label="Remarks"
              autoComplete="off"
              variant="outlined"
              value={remarks}
              onChange={handleRemarks}
            />
          </Box>
          <Spacer />
        </Box>

        <Box className={root} margin="30px 0">
          <FormControl className={formControlButtons}>
            <Button
              size="large"
              color="primary"
              className={formControlButton}
              onClick={handleCancel}
            >
              Cancel
            </Button>
            <Spacer />
            <Button
              size="large"
              variant="contained"
              color="primary"
              className={formControlButton}
            >
              Save
            </Button>
          </FormControl>
        </Box>
      </Box>
    </Box>
  )
}

export default ApplicationForm
