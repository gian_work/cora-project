import React from "react"
import FormControlLabel from "@material-ui/core/FormControlLabel"
import Box from "@material-ui/core/Box"
import FormControl from "@material-ui/core/FormControl"
import Radio from "@material-ui/core/Radio"
import RadioGroup from "@material-ui/core/RadioGroup"
import { Formik } from "formik"

/** Components */
import FormWrapper from "components/Forms/FormWrapper"
import FormLabel from "components/Forms/Label"
import Spacer from "components/Spacer"
import FormInput from "components/Forms/FormInput"
import DateTimePicker from "components/Forms/DateTimePicker"
import RefButton from "components/Forms/RefButton"

/** Config */
import { addApplicationsMovingInOut } from "config/Dashboard/Applications/validation"

/** Context */
import withContext from "context/Dashboard/Applications/forms/addApplication/withContext"
import { CtxType } from "context/Dashboard/Applications/forms/addApplication/Context"

/** Styles */
import { refSubmit } from "context/Dashboard/Applications/forms/addApplication/View"
import styles from "./styles"

const MovingInOut = ({ movingInfo, setMovingInfo }: CtxType): JSX.Element => {
  const { generalInfo } = styles()
  const {
    name,
    email,
    eta,
    mobile,
    purpose,
    remarks,
    numberOfPersons,
    contactPerson,
    multiplePersons
  } = movingInfo

  const handleFormChange = (
    inputName: string,
    e: any,
    setFieldValue: Function,
    setFieldTouched: Function
  ) => {
    setFieldValue(inputName, e.target.value)
    setFieldTouched(inputName, true, false)
    setMovingInfo &&
      setMovingInfo({
        ...movingInfo,
        [inputName]: e.target.value
      })
  }

  const handleDateChange = (
    inputName: string,
    e: string,
    setFieldValue: Function
  ) => {
    setFieldValue(name, e)
    setMovingInfo &&
      setMovingInfo({
        ...movingInfo,
        [inputName]: e
      })
  }

  return (
    <div className={generalInfo} style={{ position: "relative" }}>
      <Formik
        initialValues={{
          name,
          email,
          eta,
          mobile,
          purpose,
          remarks,
          numberOfPersons,
          contactPerson,
          multiplePersons
        }}
        onSubmit={(values, actions): void => {
          JSON.stringify(values, null, 2)
          actions.setSubmitting(false)
        }}
        validationSchema={addApplicationsMovingInOut}
      >
        {({
          touched,
          errors,
          handleBlur,
          handleSubmit,
          setFieldValue,
          setFieldTouched
        }): JSX.Element => {
          return (
            <form onSubmit={handleSubmit}>
              <FormWrapper title="Moving In/Out Details" width="60%">
                <div className="section" style={{ width: "60%", margin: "auto" }}>
                  <Box>
                    <Box marginBottom="25px">
                      <FormInput
                        name="name"
                        idValue="name"
                        label="Name"
                        placeholder="Name"
                        value={name}
                        handleOnChange={(e: any): void =>
                          handleFormChange("name", e, setFieldValue, setFieldTouched)}
                        onBlur={handleBlur}
                        error={touched.name && Boolean(errors.name)}
                        helperText={errors.name && touched.name && errors.name}
                      />
                    </Box>
                    {/* ETA */}
                    <Box display="flex" marginBottom="25px">
                      <DateTimePicker
                        label="ETA"
                        name="eta"
                        value={eta}
                        handleDateChange={(value: string): void =>
                          handleDateChange("eta", value, setFieldValue)}
                        onBlur={(e: Event): void => handleBlur(e)}
                        error={touched.eta && Boolean(errors.eta)}
                        helperText={touched.eta && errors.eta?.toString()}
                      />
                    </Box>
                    {/* ETA */}

                    {/* Purpose */}
                    <Box marginBottom="25px">
                      <FormInput
                        name="purpose"
                        idValue="purpose"
                        label="Purpose"
                        value={purpose}
                        placeholder="Purpose"
                        multiline
                        rows={2}
                        onBlur={(e: Event): void => handleBlur(e)}
                        error={touched.purpose && Boolean(errors.purpose)}
                        helperText={
                          errors.purpose && touched.purpose && errors.purpose
                        }
                        handleOnChange={(e: any): void =>
                          handleFormChange(
                            "purpose",
                            e,
                            setFieldValue,
                            setFieldTouched
                          )}
                      />
                    </Box>

                    <Box display="flex" marginBottom="25px">
                      <Box flex="1">
                        <FormInput
                          name="mobile"
                          idValue="mobile"
                          label="mobile number"
                          value={mobile}
                          placeholder="123-456-7890"
                          onBlur={handleBlur}
                          error={touched.mobile && Boolean(errors.mobile)}
                          helperText={
                            errors.mobile && touched.mobile && errors.mobile
                          }
                          handleOnChange={(e: any): void =>
                            handleFormChange(
                              "mobile",
                              e,
                              setFieldValue,
                              setFieldTouched
                            )}
                        />
                      </Box>
                      <Spacer isDefault />
                      <Box flex="1">
                        <FormInput
                          name="email"
                          label="email"
                          value={email}
                          placeholder="email@email.com"
                          onBlur={handleBlur}
                          error={touched.email && Boolean(errors.email)}
                          helperText={errors.email && touched.email && errors.email}
                          handleOnChange={(e: any): void =>
                            handleFormChange(
                              "email",
                              e,
                              setFieldValue,
                              setFieldTouched
                            )}
                        />
                      </Box>
                    </Box>
                    <Box marginBottom="25px">
                      <FormInput
                        name="remarks"
                        label="Remarks"
                        value={remarks}
                        placeholder="Remarks"
                        multiline
                        rows={2}
                        onBlur={handleBlur}
                        error={touched.remarks && Boolean(errors.remarks)}
                        helperText={
                          errors.remarks && touched.remarks && errors.remarks
                        }
                        handleOnChange={(e: any): void =>
                          handleFormChange(
                            "remarks",
                            e,
                            setFieldValue,
                            setFieldTouched
                          )}
                      />
                    </Box>
                    {/* Entry */}
                    <Box>
                      <FormControl>
                        <FormLabel label="entry" />
                        <RadioGroup
                          aria-label="entry"
                          name="entry"
                          value={multiplePersons}
                          onChange={(e: React.ChangeEvent<HTMLInputElement>): void =>
                            handleFormChange(
                              "multiplePersons",
                              e,
                              setFieldValue,
                              setFieldTouched
                            )}
                        >
                          <FormControlLabel
                            value="1"
                            control={<Radio color="primary" />}
                            label="Single"
                            labelPlacement="end"
                          />
                          <FormControlLabel
                            value="2"
                            control={<Radio color="primary" />}
                            label="Multiple"
                            labelPlacement="end"
                          />
                        </RadioGroup>
                      </FormControl>
                    </Box>
                    {/* Entry */}
                    {multiplePersons === "2" && (
                      <Box marginTop="25px">
                        <FormInput
                          type="number"
                          name="noOfPersons"
                          idValue="noOfPersons"
                          label="Number of Persons"
                          placeholder="Number of Persons"
                          value={numberOfPersons}
                          handleOnChange={(e: any): void =>
                            handleFormChange(
                              "numberOfPersons",
                              e,
                              setFieldValue,
                              setFieldTouched
                            )}
                        />
                      </Box>
                    )}
                  </Box>
                </div>
                <RefButton refValue={refSubmit} action={handleSubmit} />
              </FormWrapper>
            </form>
          )
        }}
      </Formik>
    </div>
  )
}

export default withContext(MovingInOut)
