import { makeStyles } from "@material-ui/core/styles"

const styles = makeStyles(() => ({
  generalInfo: {
    margin: "0 auto",
    "& .section": {
      width: "40%"
      // marginBottom: "40px",
    }
  },
  twoCol: {
    display: "flex",
    justifyContent: "space-between",
    "& > div": {
      width: "48%"
    }
  },
  threeCol: {
    display: "flex",
    justifyContent: "space-between",
    "& > div": {
      width: "32%"
    }
  },
  spaceBottom: {
    paddingBottom: "30px"
  }
}))

export default styles
