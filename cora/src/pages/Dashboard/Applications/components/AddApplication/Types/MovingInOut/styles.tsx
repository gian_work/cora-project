import { makeStyles } from "@material-ui/core/styles"

const styles = makeStyles(() => ({
  generalInfo: {
    margin: "0 auto",
    padding: "0 0 20px 0",
    "& .section": {
      margin: "0 auto"
    }
  },
  userOptions: {
    display: "flex",
    flexDirection: "row"
  },
  spaceBottom: {
    paddingBottom: "30px"
  }
}))

export default styles
