import React from "react"
import Box from "@material-ui/core/Box"
import { Formik } from "formik"
/** Components */
import FormInput from "components/Forms/FormInput"
import FormWrapper from "components/Forms/FormWrapper"
import RefButton from "components/Forms/RefButton"
import UploadImage from "pages/Dashboard/Applications/components/AddApplication/Common/UploadImage"

/** Context */
import { Context as MainContext } from "context/Dashboard/Applications/forms/addApplication/Context"

/** Validation */
import { addApplicationsVehicle } from "config/Dashboard/Applications/validation"

/** Styles */
import { refSubmit } from "context/Dashboard/Applications/forms/addApplication/View"
import styles from "./styles"

const ResidentInfo: React.FC = () => {
  const { generalInfo } = styles()
  const { setVehicleInfo, vehicleInfo } = React.useContext(MainContext)

  const { vehicleNo, carLabel, IUNo, vehicleType, model } = vehicleInfo

  /** Methods */

  const handleFormChange = (
    name: string,
    e: any,
    handleChange: Function,
    setFieldTouched: Function
  ) => {
    handleChange(e)
    setFieldTouched(name, true, false)
    setVehicleInfo &&
      setVehicleInfo({
        ...vehicleInfo,
        [name]: e.target.value
      })
  }

  return (
    <div className={generalInfo}>
      <Formik
        initialValues={{
          carLabel,
          vehicleType,
          vehicleNo,
          IUNo,
          model
        }}
        onSubmit={(values, actions): void => {
          JSON.stringify(values, null, 2)
          actions.setSubmitting(false)
        }}
        validationSchema={addApplicationsVehicle}
      >
        {({
          touched,
          errors,
          handleBlur,
          handleSubmit,
          // setFieldValue,
          handleChange,
          setFieldTouched
        }): JSX.Element => {
          return (
            <form>
              <FormWrapper title="Vehicle Registration" width="60%">
                <div
                  className="section"
                  style={{
                    width: "60%",
                    margin: "auto"
                  }}
                >
                  <Box display="flex" flexWrap="wrap" justifyContent="space-between">
                    <Box minWidth="48%" marginBottom="15px">
                      <FormInput
                        name="carLabel"
                        value={carLabel}
                        idValue="carLabel"
                        label="Car Label"
                        placeholder="Vehicle Type"
                        handleOnChange={(e: any): void =>
                          handleFormChange(
                            "carLabel",
                            e,
                            handleChange,
                            setFieldTouched
                          )}
                        onBlur={handleBlur}
                        error={touched.carLabel && Boolean(errors.carLabel)}
                        helperText={
                          errors.carLabel && touched.carLabel && errors.carLabel
                        }
                      />
                    </Box>

                    <Box minWidth="48%" marginBottom="15px">
                      <FormInput
                        name="model"
                        value={model}
                        idValue="model"
                        label="Vehicle Model"
                        placeholder="Vehicle Model"
                        handleOnChange={(e: any): void =>
                          handleFormChange("model", e, handleChange, setFieldTouched)}
                        onBlur={handleBlur}
                        error={touched.model && Boolean(errors.model)}
                        helperText={errors.model && touched.model && errors.model}
                      />
                    </Box>

                    <Box minWidth="48%" marginBottom="15px">
                      <FormInput
                        name="vehicleType"
                        value={vehicleType}
                        idValue="vehicleType"
                        label="Vehicle Type"
                        placeholder="Vehicle Type"
                        handleOnChange={(e: any): void =>
                          handleFormChange(
                            "vehicleType",
                            e,
                            handleChange,
                            setFieldTouched
                          )}
                        onBlur={handleBlur}
                        error={touched.vehicleType && Boolean(errors.vehicleType)}
                        helperText={
                          errors.vehicleType &&
                          touched.vehicleType &&
                          errors.vehicleType
                        }
                      />
                    </Box>

                    <Box minWidth="48%" marginBottom="15px">
                      <FormInput
                        name="vehicleNo"
                        value={vehicleNo}
                        idValue="vehicleNo"
                        label="Vehicle Number"
                        placeholder="Vehicle Number"
                        handleOnChange={(e: any): void =>
                          handleFormChange(
                            "vehicleNo",
                            e,
                            handleChange,
                            setFieldTouched
                          )}
                        onBlur={handleBlur}
                        error={touched.vehicleNo && Boolean(errors.vehicleNo)}
                        helperText={
                          errors.vehicleNo && touched.vehicleNo && errors.vehicleNo
                        }
                      />
                    </Box>

                    <Box minWidth="48%" marginBottom="15px">
                      <FormInput
                        name="IUNo"
                        value={IUNo}
                        idValue="IUNo"
                        label="Vehicle IU / Electronic Number"
                        placeholder="Vehicle IU / Electronic Number"
                        handleOnChange={(e: any): void =>
                          handleFormChange("IUNo", e, handleChange, setFieldTouched)}
                        onBlur={handleBlur}
                        error={touched.IUNo && Boolean(errors.IUNo)}
                        helperText={errors.IUNo && touched.IUNo && errors.IUNo}
                      />
                    </Box>
                  </Box>
                </div>
                <RefButton refValue={refSubmit} action={handleSubmit} />
              </FormWrapper>

              <Box>
                <UploadImage />
              </Box>
            </form>
          )
        }}
      </Formik>
    </div>
  )
}

export default ResidentInfo
