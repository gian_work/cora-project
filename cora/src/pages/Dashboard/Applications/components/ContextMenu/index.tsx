import React from "react"
import List from "@material-ui/core/List"
import ListItem from "@material-ui/core/ListItem"
import { makeStyles, Theme } from "@material-ui/core/styles"

const useStyles = makeStyles((theme: Theme) => ({
  listItem: {
    display: "flex",
    alignItems: "center",
    color: theme.palette.primary.main,
    fontSize: "14px",
    fontWeight: 500,
    textTransform: "uppercase",
    borderBottom: "1px solid #F0F3F7",
    padding: "10px 10px 10px 20px",
    width: "100%",
    "& i": {
      marginRight: "15px",
      paddingTop: "3px",
      width: "25px",
      height: "25px",
      display: "flex",
      alignItems: "center",
      justifyContent: "center"
    }
  },
  innerList: {
    padding: "10px",
    "& > div": {
      paddingLeft: "55px"
    }
  },
  collapseControl: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    width: "100%",
    "& i": {
      paddingTop: "3px"
    },
    "& > div": {
      display: "flex",
      alignItems: "center"
    }
  }
}))

const IconPending = (
  <svg
    width="14"
    height="18"
    viewBox="0 0 14 18"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M0 0V2H2V5.41406L5.58594 9L2 12.5859V16H0V18H2H4H10H12H14V16H12V12.5859L8.41406 9L12 5.41406V2H14V0H0ZM4 2H10V4.58594L7 7.58594L4 4.58594V2ZM7 10.4141L10 13.4141V16H4V13.4141L7 10.4141Z"
      fill="#09707B"
    />
  </svg>
)

const IconCancel = (
  <svg
    width="20"
    height="20"
    viewBox="0 0 20 20"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M10 0C4.489 0 0 4.489 0 10C0 15.511 4.489 20 10 20C15.511 20 20 15.511 20 10C20 4.489 15.511 0 10 0ZM10 2C14.4301 2 18 5.56988 18 10C18 14.4301 14.4301 18 10 18C5.56988 18 2 14.4301 2 10C2 5.56988 5.56988 2 10 2ZM6.70703 5.29297L5.29297 6.70703L8.58594 10L5.29297 13.293L6.70703 14.707L10 11.4141L13.293 14.707L14.707 13.293L11.4141 10L14.707 6.70703L13.293 5.29297L10 8.58594L6.70703 5.29297Z"
      fill="#09707B"
    />
  </svg>
)

const IconApprove = (
  <svg
    width="20"
    height="20"
    viewBox="0 0 20 20"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M10 0C4.489 0 0 4.489 0 10C0 15.511 4.489 20 10 20C15.511 20 20 15.511 20 10C20 4.489 15.511 0 10 0ZM10 2C14.4301 2 18 5.56988 18 10C18 14.4301 14.4301 18 10 18C5.56988 18 2 14.4301 2 10C2 5.56988 5.56988 2 10 2ZM14.293 6.29297L8 12.5859L5.70703 10.293L4.29297 11.707L8 15.4141L15.707 7.70703L14.293 6.29297Z"
      fill="#09707B"
    />
  </svg>
)

const IconDetails = (
  <svg
    width="18"
    height="18"
    viewBox="0 0 18 18"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M2 0C0.906937 0 0 0.906937 0 2V16C0 17.0931 0.906937 18 2 18H16C17.0931 18 18 17.0931 18 16V2C18 0.906937 17.0931 0 16 0H2ZM2 2H16V16H2V2ZM9 5C5 5 3 9 3 9C3 9 5 13 9 13C13 13 15 9 15 9C15 9 13 5 9 5ZM9 7C10.104 7 11 7.896 11 9C11 10.104 10.104 11 9 11C7.896 11 7 10.104 7 9C7 7.896 7.896 7 9 7Z"
      fill="#09707B "
    />
  </svg>
)

interface ContextApplicationsProps {
  setDetailsActive: Function
  activeRow: Record<string, any>
}

const ContextApplications: React.FC<ContextApplicationsProps> = ({
  setDetailsActive,
  activeRow
}) => {
  const { listItem } = useStyles()

  return (
    <List component="nav">
      <ListItem
        className={listItem}
        button
        onClick={() => setDetailsActive(true, activeRow)}
      >
        <i>{IconDetails}</i>
        View
      </ListItem>
      <ListItem className={listItem} button>
        <i>{IconPending}</i>
        Pending
      </ListItem>
      <ListItem className={listItem} button>
        <i>{IconApprove}</i>
        approved
      </ListItem>
      <ListItem className={listItem} button>
        <i>{IconCancel}</i>
        cancel
      </ListItem>
    </List>
  )
}

export default ContextApplications
