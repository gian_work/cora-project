import React from "react"

// component
import Drawer from "../../../../../components/Drawer"
import ApplicationForm from "../AddApplication/ApplicationForm"

interface ApplicationDetailsProps {
  openDetails: boolean
  setopenDetails: Function
}

const ApplicationDetails: React.FC<ApplicationDetailsProps> = ({
  openDetails,
  setopenDetails
}) => {
  return (
    <>
      <Drawer openState={openDetails} setopenState={setopenDetails}>
        <ApplicationForm
          title="Access Card"
          subtitle="Application ID # 009009842"
          hasData
        />
      </Drawer>
    </>
  )
}

export default ApplicationDetails
