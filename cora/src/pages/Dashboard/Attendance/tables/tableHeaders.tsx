import React from "react"
import Button from "@material-ui/core/Button"
import ContextMenu from "components/ContextMenu"
import FileLink from "components/Table/components/FileLink"

/** Utils */
import { fromUnix, fromUnixTime } from "utils/date"
import { RolesCategory } from "config/Common/Roles"
import ContextItems from "./ContextMenu"

const TableHeaderColumns = (handleTimeout: Function) => [
  {
    fixed: "left",
    disableFilters: true,
    columns: [
      {
        width: 70,
        Cell: (row: any): JSX.Element => {
          return (
            <ContextMenu>
              <ContextItems data={row?.original} />
            </ContextMenu>
          )
        },
        filterable: false
      }
    ]
  },
  {
    Header: "",
    columns: [
      {
        Header: "Date",
        id: "date",
        accessor: (original: Record<string, any>) => {
          return fromUnix(original?.attendance_date_time)
        },
        Cell: ({ original }: Record<string, any>) => {
          return fromUnix(original?.attendance_date_time)
        },
        width: 200
      },
      {
        Header: "Name",
        id: "name",
        accessor: "name",
        width: 250
      },
      {
        Header: "Category",
        id: "category",
        accessor: (original: Record<string, any>) => {
          return RolesCategory[original?.user_role_category]
        },
        Cell: ({ original }: Record<string, any>) => {
          return RolesCategory[original?.user_role_category]
        },
        width: 250
      },
      {
        Header: "Position",
        id: "position",
        accessor: (original: Record<string, any>) => {
          return original?.user_role_name
        },
        Cell: ({ original }: Record<string, any>) => {
          return original?.user_role_name
        },
        width: 200
      },
      {
        Header: "Time-in",
        id: "timein",
        accessor: (original: Record<string, any>) => {
          return fromUnixTime(original?.attendance_date_time)
        },
        Cell: ({ original }: Record<string, any>) => {
          return fromUnixTime(original?.attendance_date_time)
        },
        width: 150
      },
      {
        Header: "Photo",
        id: "position",
        sortable: false,
        filterable: false,
        accessor: (original: Record<string, any>) => {
          return original?.photo?.key === "" ? "N/A" : original?.photo?.file_name
        },
        Cell: ({ original }: Record<string, any>) => {
          return original?.photo?.key === "" ? (
            "N/A"
          ) : (
            <FileLink
              url={original?.photo?.url}
              name={original?.photo?.file_name}
              fileType={2}
            />
          )
        }
      },
      {
        Header: "Remarks",
        id: "remarks",
        accessor: (original: Record<string, any>) => {
          return original?.remarks === "" ? "N/A" : original?.remarks
        },
        width: 250
      }
    ]
  },
  {
    fixed: "right",
    disableFilters: true,
    columns: [
      {
        width: 130,
        Cell: ({ original }: Record<string, any>): JSX.Element => {
          if (original?.attendance_type === 2) {
            return <div />
          }
          return (
            <Button color="primary" onClick={(): void => handleTimeout(original)}>
              TIME OUT
            </Button>
          )
        },
        filterable: false
      }
    ]
  }
]

export default TableHeaderColumns
