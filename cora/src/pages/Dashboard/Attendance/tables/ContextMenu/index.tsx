import React, { useState } from "react"
import List from "@material-ui/core/List"
import ListItem from "@material-ui/core/ListItem"
/** Components */
import Dialog from "components/Dialog"
/** Context */
import { CtxMenuContext } from "components/ContextMenu"
import withContext from "context/Dashboard/Attendance/main/withContext"
import { CtxType } from "context/Dashboard/Attendance/main/Context"
/** Styles */
import { IconView, IconEdit, IconDelete } from "components/Icons/ContextMenu"
import styles from "./styles"
/** Config */

interface ContextAttendance extends CtxType {
  data: Record<string, any>
  showDrawer: Function
  handleDeleteLog: Function
  setCurrentAction: Function
}

const ContextAttendance: React.FC<ContextAttendance> = ({
  data,
  showDrawer,
  setCurrentAction,
  handleDeleteLog
}) => {
  const { listItem } = styles()
  const [openDialog, setOpenDialog] = useState(false)
  const { openContext } = React.useContext(CtxMenuContext)

  function handleShowDetails(items: Record<string, any>) {
    showDrawer(2, true, items)
    setCurrentAction(4)
  }

  function handleShowUpdate(items: Record<string, any>) {
    showDrawer(1, true, items)
    setCurrentAction(3)
  }

  function handleDelete(logUID: string) {
    handleDeleteLog(logUID)
    openContext && openContext(false)
  }

  return (
    <>
      <List component="nav">
        <ListItem
          className={listItem}
          button
          onClick={() => handleShowDetails(data)}
        >
          <i>{IconView}</i>
          View Details
        </ListItem>
        <ListItem className={listItem} button onClick={() => handleShowUpdate(data)}>
          <i>{IconEdit}</i>
          Edit
        </ListItem>
        <ListItem className={listItem} button onClick={() => setOpenDialog(true)}>
          <i>{IconDelete}</i>
          Delete
        </ListItem>
      </List>
      <Dialog
        action={() => handleDelete(data?._uid)}
        isOpen={openDialog}
        setOpen={setOpenDialog}
        actionLabel="Confirm"
        title=""
        message="Are you sure you want to delete this time log?"
      />
    </>
  )
}
export default withContext(ContextAttendance)
