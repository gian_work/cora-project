import React from "react"
import useSWR from "swr"
import Fade from "@material-ui/core/Fade"
import Cookie from "js-cookie"
/** Service */
import service from "services/Dashboard/Attendances"
/** Components */
import Table from "components/Table"
import Loader from "components/Loader"
/** Utils */
import { toUnix, toUnixTomorrow, toUnixDateOnly } from "utils/date"
import tableHeaders from "./tableHeaders"
import tableHeadersNoAction from "./tableHeadersNoAction"

interface Props {
  activeTable?: number
  activeStatus?: number
  date: any
  showTimeout: Function
}

const TableAttendances: React.FC<Props> = ({
  date,
  activeTable,
  activeStatus,
  showTimeout
}) => {
  const condoUID = Cookie.get("condoUID")
  const unixDate = toUnix(date)
  const status = activeStatus !== undefined && activeStatus + 1

  const fetchAttendances = async () => {
    const payload = {
      "condo_uid": condoUID,
      "start_date": toUnixDateOnly(date),
      "end_date": toUnixTomorrow(date, 1),
      "user_role_category": activeTable
    }

    const payloadAll = {
      "condo_uid": condoUID,
      "start_date": toUnixDateOnly(date),
      "end_date": toUnixTomorrow(date, 1)
    }

    const payloadOption = activeTable === 0 ? payloadAll : payload

    return service.getAttendances(payloadOption)
  }

  const { data, isValidating } = useSWR(
    `fetchAttendances-${unixDate}-${activeTable}`,
    fetchAttendances
  )

  const tableData = data?.data?._data
  const filteredData = tableData?.filter((i: Record<string, any>) => {
    if (status === 1) {
      return (
        i?.attendance_type === 1 &&
        i?.time_in_reference_uid === "" &&
        i?.time_out?._uid === ""
      )
    }
    return (
      i?.attendance_type === 1 &&
      i?.time_in_reference_uid === "" &&
      i?.time_out?._uid !== ""
    )
  })

  /**
   * Methods
   */
  function handleTimeout(dataItems: Record<string, any>) {
    showTimeout(true, dataItems)
  }

  /**
   * View
   */
  if (isValidating) {
    return <Loader dark />
  }

  return (
    <Fade in={!isValidating} timeout={1500}>
      <div>
        <Table
          data={filteredData || []}
          columns={
            status === 2 ? tableHeadersNoAction() : tableHeaders(handleTimeout)
          }
          minRows={2}
          isFilterable
          size={10}
        />
      </div>
    </Fade>
  )
}

export default TableAttendances
