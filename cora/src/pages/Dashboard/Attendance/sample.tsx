const ApplicationsData = [
  {
    date: "18/10/2019",
    name: "Donovan Mitchell",
    category: "office",
    position: "condo manager",
    timein: "08:30",
    timeout: "17:30",
    remarks: "Present"
  },
  {
    date: "18/10/2019",
    name: "Jordan Clarkson",
    category: "office",
    position: "asst condo manager",
    timein: "08:30",
    timeout: "17:30",
    remarks: "Present"
  },
  {
    date: "18/10/2019",
    name: "Damian Lillard",
    category: "office",
    position: "property executive",
    timein: "08:30",
    timeout: "17:30",
    remarks: "Present"
  },
  {
    date: "18/10/2019",
    name: "Klay Thompson",
    category: "office",
    position: "property officer",
    timein: "08:30",
    timeout: "17:30",
    remarks: "Present"
  },
  {
    date: "18/10/2019",
    name: "Paul George",
    category: "office",
    position: "relations officer",
    timein: "08:30",
    timeout: "17:30",
    remarks: "Absent"
  },
  {
    date: "18/10/2019",
    name: "Kawhi Leonard",
    category: "office",
    position: "admin assistant",
    timein: "08:30",
    timeout: "17:30",
    remarks: "Present"
  },
  {
    date: "18/10/2019",
    name: "Ja Morant",
    category: "office",
    position: "accounts assistant",
    timein: "08:30",
    timeout: "17:30",
    remarks: "Present"
  },
  {
    date: "18/10/2019",
    name: "Luka Donćic",
    category: "technician",
    position: "technician 1",
    timein: "08:30",
    timeout: "17:30",
    remarks: "Present"
  },
  {
    date: "18/10/2019",
    name: "Kyle Kuzma",
    category: "technician",
    position: "technician 2",
    timein: "08:30",
    timeout: "17:30",
    remarks: "Present"
  },
  {
    date: "18/10/2019",
    name: "Zion Williamson",
    category: "technician",
    position: "technician 3",
    timein: "08:30",
    timeout: "17:30",
    remarks: "Present"
  },
  {
    date: "18/10/2019",
    name: "Bradley Beal",
    category: "security",
    position: "sso",
    timein: "08:30",
    timeout: "17:30",
    remarks: "Absent"
  },
  {
    date: "18/10/2019",
    name: "Devin Booker",
    category: "security",
    position: "sso1",
    timein: "08:30",
    timeout: "17:30",
    remarks: "Present"
  },
  {
    date: "18/10/2019",
    name: "Victor Oladipo",
    category: "security",
    position: "sso2",
    timein: "08:30",
    timeout: "17:30",
    remarks: "Half Day"
  },
  {
    date: "18/10/2019",
    name: "Derrick Rose",
    category: "security",
    position: "sso3",
    timein: "08:30",
    timeout: "17:30",
    remarks: "Present"
  },
  {
    date: "18/10/2019",
    name: "Jimmy Buckets",
    category: "security",
    position: "sso4",
    timein: "08:30",
    timeout: "17:30",
    remarks: "Half Day"
  },
  {
    date: "18/10/2019",
    name: "DeAaron Fox",
    category: "security",
    position: "sso5",
    timein: "08:30",
    timeout: "17:30",
    remarks: "Present"
  },
  {
    date: "18/10/2019",
    name: "Kemba Walker",
    category: "security",
    position: "sso6",
    timein: "08:30",
    timeout: "17:30",
    remarks: "Present"
  },
  {
    date: "18/10/2019",
    name: "Nikola Jokic",
    category: "security",
    position: "sso7",
    timein: "08:30",
    timeout: "17:30",
    remarks: "Present"
  },
  {
    date: "18/10/2019",
    name: "Tray Young",
    category: "security",
    position: "sso8",
    timein: "08:30",
    timeout: "17:30",
    remarks: "Present"
  },
  {
    date: "18/10/2019",
    name: "Rudy Gobert",
    category: "cleaning",
    position: "cleaner1",
    timein: "08:30",
    timeout: "17:30",
    remarks: "Present"
  },
  {
    date: "18/10/2019",
    name: "Joel Embidd",
    category: "cleaning",
    position: "cleaner2",
    timein: "08:30",
    timeout: "17:30",
    remarks: "Present"
  },
  {
    date: "18/10/2019",
    name: "Spencer Dewindle",
    category: "cleaning",
    position: "cleaner2",
    timein: "08:30",
    timeout: "17:30",
    remarks: "Present"
  },
  {
    date: "18/10/2019",
    name: "Kyrie Irving",
    category: "cleaning",
    position: "cleaner3",
    timein: "08:30",
    timeout: "17:30",
    remarks: "Present"
  },
  {
    date: "18/10/2019",
    name: "Jordan White",
    category: "cleaning",
    position: "cleaner4",
    timein: "08:30",
    timeout: "17:30",
    remarks: "Present"
  },
  {
    date: "18/10/2019",
    name: "Karl Towns",
    category: "landscape",
    position: "supervisor",
    timein: "08:30",
    timeout: "17:30",
    remarks: "Present"
  },
  {
    date: "18/10/2019",
    name: "Kyle Lowry",
    category: "landscape",
    position: "gardener1",
    timein: "08:30",
    timeout: "17:30",
    remarks: "Present"
  },
  {
    date: "18/10/2019",
    name: "Myles Turner",
    category: "landscape",
    position: "gardener2",
    timein: "08:30",
    timeout: "17:30",
    remarks: "Present"
  },
  {
    date: "18/10/2019",
    name: "Brandon Ingram",
    category: "landscape",
    position: "gardener3",
    timein: "08:30",
    timeout: "17:30",
    remarks: "Present"
  }
]

export default ApplicationsData
