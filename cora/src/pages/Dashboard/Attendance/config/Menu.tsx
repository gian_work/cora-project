import Security from "../../../../components/Icons/Attendance/Security"
import Maintenance from "../../../../components/Icons/Attendance/Maintenance"
import Cleaner from "../../../../components/Icons/Attendance/Cleaner"
import Admin from "../../../../components/Icons/Attendance/Admin"

export const Menu = [
  {
    label: "Security",
    icon: Security,
    value: "security",
    color: "#21B6BF"
  },
  {
    label: "Maintenance",
    icon: Maintenance,
    value: "technician",
    color: "#50D757"
  },
  {
    label: "Cleaner",
    icon: Cleaner,
    value: "cleaning",
    color: "#FF808B"
  },
  {
    label: "Admin",
    icon: Admin,
    value: "office",
    color: "#2F80ED"
  }
]

export default Menu
