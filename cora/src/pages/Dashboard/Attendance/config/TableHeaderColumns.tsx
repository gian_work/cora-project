import React from "react"

const TableHeaderColumns = [
  {
    fixed: "right",
    disableFilters: true,
    columns: [
      {
        Header: "Action",
        Cell: () => {
          return (
            <div
              style={{
                color: "#09707B",
                fontWeight: 600,
                fontSize: "14px",
                cursor: "pointer"
              }}
            >
              TIME OUT
            </div>
          )
        },
        filterable: false
      }
    ]
  },
  {
    columns: [
      {
        Header: "Date",
        accessor: "date"
      },
      {
        Header: "Name",
        accessor: "name"
      },
      {
        Header: "Category",
        accessor: "category"
      },
      {
        Header: "Position",
        accessor: "position"
      },
      {
        Header: "Time in",
        accessor: "timein"
      },
      {
        Header: "Time out",
        accessor: "timeout"
      },
      {
        Header: "Remarks",
        accessor: "remarks"
      }
    ]
  }
]

export default TableHeaderColumns
