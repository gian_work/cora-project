import React, { useState } from "react"
import Box from "@material-ui/core/Box"

import FormControlLabel from "@material-ui/core/FormControlLabel"
import FormControl from "@material-ui/core/FormControl"
import Radio from "@material-ui/core/Radio"
import RadioGroup from "@material-ui/core/RadioGroup"
import Button from "@material-ui/core/Button"
import Select from "@material-ui/core/Select"
import MenuItem from "@material-ui/core/MenuItem"
import { makeStyles } from "@material-ui/core/styles"
import DateFnsUtils from "@date-io/date-fns"
import { MuiPickersUtilsProvider, TimePicker } from "@material-ui/pickers"

// component
import Drawer from "components/Drawer"
import Header from "components/Header"
import Spacer from "components/Spacer"
import Section from "components/Section"
import FormLabel from "components/Forms/Label"

const useStyles = makeStyles(() => ({
  root: {
    display: "flex",
    "& .MuiTextField-root": {
      flex: 1
    }
  },
  formControl: {
    width: "100%"
  },
  formControlButtons: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    width: "100%",
    padding: "0 20px"
  },
  formControlButton: {
    width: "49%"
  },
  radioLabel: {
    paddingBottom: "8px"
  }
}))

interface AddStaffProps {
  openUserForm: boolean
  setopenUserForm: Function
}

const AddStaff: React.FC<AddStaffProps> = ({ openUserForm, setopenUserForm }) => {
  const {
    root,
    formControl,

    formControlButtons,
    formControlButton
  } = useStyles()

  const [payment, setpaymentType] = useState("admin")
  const [name, setName] = React.useState("")
  const [timeIn, settimeIn] = useState(new Date())
  const [timeOut, settimeOut] = useState(new Date())

  const handleName = (event: React.ChangeEvent<{ value: unknown }>) => {
    setName(event.target.value as string)
  }
  const handlePayment = (event: React.ChangeEvent<{ value: unknown }>) => {
    setpaymentType(event.target.value as string)
  }

  const handleTime = (type: string, value: any) => {
    if (type === "timein") {
      settimeIn(value)
    } else {
      settimeOut(value)
    }
  }

  return (
    <Drawer openState={openUserForm} setopenState={setopenUserForm}>
      <Header title="Time-in Staff" subtitle="Attendance" subtitleAbove />
      <Box
        display="flex"
        flexDirection="column"
        flex="1"
        height="calc(100% - 103px)"
        maxWidth="350px"
      >
        <Box flex="1">
          <Section>
            <FormControl>
              <FormLabel label="Category" />
              <RadioGroup
                aria-label="credit type"
                name="creditType"
                value={payment}
                onChange={handlePayment}
              >
                <FormControlLabel
                  value="1"
                  control={<Radio color="primary" />}
                  label="Office"
                  labelPlacement="end"
                />
                <FormControlLabel
                  value="2"
                  control={<Radio color="primary" />}
                  label="Security"
                  labelPlacement="end"
                />
                <FormControlLabel
                  value="3"
                  control={<Radio color="primary" />}
                  label="Maintenance"
                  labelPlacement="end"
                />
                <FormControlLabel
                  value="4"
                  control={<Radio color="primary" />}
                  label="Cleaner"
                  labelPlacement="end"
                />
                <FormControlLabel
                  value="5"
                  control={<Radio color="primary" />}
                  label="Landscape"
                  labelPlacement="end"
                />
              </RadioGroup>
            </FormControl>
            <Spacer isDefault />
            <Box>
              <FormLabel label="Select Staff" />
              <FormControl variant="outlined" className={formControl}>
                <Select displayEmpty value={name} onChange={handleName}>
                  <MenuItem value="" disabled>
                    Staff Name
                  </MenuItem>
                  <MenuItem value="Damian Lillard">Damian Lillard</MenuItem>
                  <MenuItem value="Paul George">Paul George</MenuItem>
                  <MenuItem value="Bradley Beal">Bradley Beal</MenuItem>
                </Select>
              </FormControl>
            </Box>
            <Spacer isDefault />
          </Section>
          <Section>
            <Box>
              <FormLabel label="Login Details" />
              <Box display="flex" paddingTop="15px">
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                  <TimePicker
                    margin="normal"
                    label="Time in"
                    inputVariant="outlined"
                    value={timeIn}
                    onChange={(e) => handleTime("timein", e)}
                  />
                  <Spacer isDefault />
                  <TimePicker
                    margin="normal"
                    label="Time out"
                    inputVariant="outlined"
                    value={timeOut}
                    onChange={(e) => handleTime("timeout", e)}
                  />
                </MuiPickersUtilsProvider>
              </Box>
            </Box>
            <Spacer isDefault />
          </Section>
        </Box>
        <Box className={root} margin="20px 0">
          <FormControl className={formControlButtons}>
            <Button
              size="large"
              color="primary"
              className={formControlButton}
              onClick={() => null}
            >
              CANCEL
            </Button>
            <Spacer isDefault />
            <Button
              size="large"
              variant="contained"
              color="primary"
              className={formControlButton}
            >
              TIME-IN
            </Button>
          </FormControl>
        </Box>
      </Box>
    </Drawer>
  )
}

export default AddStaff
