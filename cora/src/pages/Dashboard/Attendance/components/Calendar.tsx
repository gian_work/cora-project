import * as React from "react"
import DatePicker from "react-datepicker"

const Calendar: React.FC = () => {
  return (
    <DatePicker
      inline
      selected={new Date()}
      onChange={() => null}
      calendarClassName="no-shadow"
    />
  )
}

export default Calendar
