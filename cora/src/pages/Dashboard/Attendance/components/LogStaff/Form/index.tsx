import React from "react"
import Box from "@material-ui/core/Box"
import Button from "@material-ui/core/Button"
import Radio from "@material-ui/core/Radio"
import RadioGroup from "@material-ui/core/RadioGroup"
import FormControlLabel from "@material-ui/core/FormControlLabel"
import { Formik } from "formik"
/** Components */
import Staffs from "components/Common/Staffs/new"
import FormWrapper from "components/Forms/FormWrapper"
import TimePicker from "components/Forms/TimePicker"

/** Validation */
import { createTimein } from "config/Dashboard/Attendance/validation"
/** Config */
import { RolesCategory } from "config/Common/Roles"
/** Styles */

import withContext from "context/Dashboard/Attendance/main/withContext"
import { CtxType } from "context/Dashboard/Attendance/main/Context"
import styles from "./styles"

const LogForm: React.FC<CtxType> = ({
  logDetails,
  setLogDetails,
  handleLogStaff,
  currentAction,
  showDrawer
}) => {
  const { container, section, label, labelGap, footer, wrapper } = styles()
  const ld = logDetails

  /** Methods */
  const handleFormChange = (
    name: string,
    e: React.ChangeEvent<HTMLInputElement> | any,
    setFieldValue: Function
  ) => {
    if (e?.target === undefined) {
      setFieldValue(name, e)
      setLogDetails &&
        setLogDetails({
          ...logDetails,
          [name]: e
        })
    } else {
      setFieldValue(name, e?.target?.value)
      setLogDetails &&
        setLogDetails({
          ...logDetails,
          [name]: e?.target?.value
        })
    }
  }

  const handleSubmitForm = (
    handleSubmit: Function,
    status: boolean,
    actionType: number
  ) => {
    handleSubmit()
    switch (actionType) {
      case 1:
        return handleLogStaff(1, 1)
      case 2:
        return handleLogStaff(2, 1)
      case 3:
        return handleLogStaff(1, 2)
      default:
        return handleLogStaff(1, 1)
    }

    return status
  }

  const actionText = (action: number) => {
    switch (action) {
      case 1:
        return "TIME-IN"
      case 2:
        return "TIME-OUT"
      case 3:
        return "UPDATE"
      default:
        return "TIME-IN"
    }
  }

  return (
    <Formik
      initialValues={{
        category: ld?.category,
        accountUID: ld?.accountUID,
        attendanceDateTime: ld?.attendanceDateTime
      }}
      onSubmit={(values, actions): void => {
        JSON.stringify(values, null, 2)
        actions.setSubmitting(false)
      }}
      validationSchema={createTimein}
    >
      {({
        touched,
        errors,
        isValid,
        handleBlur,
        handleSubmit,
        setFieldValue
      }): JSX.Element => {
        return (
          <form style={{ height: "100%" }}>
            <Box className={wrapper}>
              <Box flex="3">
                <FormWrapper>
                  <Box className={container}>
                    <Box className={section}>
                      <Box className={label}>Category</Box>
                      <RadioGroup
                        value={String(ld?.category)}
                        onChange={(e: React.ChangeEvent<HTMLInputElement>): void =>
                          handleFormChange("category", e, setFieldValue)}
                      >
                        {Object.keys(RolesCategory).map((role: any) => {
                          return (
                            <FormControlLabel
                              key={role}
                              value={role}
                              control={<Radio color="primary" />}
                              label={RolesCategory[role]}
                              labelPlacement="end"
                            />
                          )
                        })}
                      </RadioGroup>
                    </Box>

                    <Box className={section}>
                      <Box className={label}>Select Staff</Box>
                      <Box>
                        <Staffs
                          value={ld?.accountUID}
                          category={ld?.category}
                          handleChange={(e: any) =>
                            handleFormChange("accountUID", e, setFieldValue)}
                          onBlur={(e: any) => handleBlur(e)}
                          error={touched?.accountUID && Boolean(errors?.accountUID)}
                          helperText={
                            touched?.accountUID && errors?.accountUID?.toString()
                          }
                        />
                      </Box>
                    </Box>
                  </Box>

                  <Box>
                    <Box className={section}>
                      <Box className={labelGap} paddingBottom="10px">
                        Login Details
                      </Box>
                      <Box>
                        {currentAction === 1 && (
                          <TimePicker
                            label="Time in"
                            name="timeIn"
                            value={ld?.attendanceDateTime}
                            handleDateChange={(value: string): void =>
                              handleFormChange(
                                "attendanceDateTime",
                                value,
                                setFieldValue
                              )}
                            onBlur={(e: Event): void => handleBlur(e)}
                            error={
                              touched.attendanceDateTime &&
                              Boolean(errors.attendanceDateTime)
                            }
                            helperText={
                              touched.attendanceDateTime &&
                              errors.attendanceDateTime?.toString()
                            }
                          />
                        )}
                        {currentAction === 3 && (
                          <TimePicker
                            label="Time in"
                            name="timeIn"
                            value={ld?.attendanceDateTime}
                            handleDateChange={(value: string): void =>
                              handleFormChange(
                                "attendanceDateTime",
                                value,
                                setFieldValue
                              )}
                            onBlur={(e: Event): void => handleBlur(e)}
                            error={
                              touched.attendanceDateTime &&
                              Boolean(errors.attendanceDateTime)
                            }
                            helperText={
                              touched.attendanceDateTime &&
                              errors.attendanceDateTime?.toString()
                            }
                          />
                        )}
                        {currentAction === 2 && (
                          <TimePicker
                            label="Time out"
                            name="timeOut"
                            value={ld?.attendanceDateTimeout}
                            handleDateChange={(value: string): void =>
                              handleFormChange(
                                "attendanceDateTimeout",
                                value,
                                setFieldValue
                              )}
                            onBlur={(e: Event): void => handleBlur(e)}
                            error={
                              touched.attendanceDateTime &&
                              Boolean(errors.attendanceDateTime)
                            }
                            helperText={
                              touched.attendanceDateTime &&
                              errors.attendanceDateTime?.toString()
                            }
                          />
                        )}
                      </Box>
                    </Box>
                  </Box>
                </FormWrapper>
              </Box>
              <Box className={footer}>
                <Box>
                  <Button
                    color="primary"
                    fullWidth
                    onClick={() => showDrawer(1, false)}
                  >
                    CANCEL
                  </Button>
                </Box>
                <Box>
                  <Button
                    variant="contained"
                    color="primary"
                    fullWidth
                    onClick={() =>
                      handleSubmitForm(handleSubmit, isValid, currentAction)}
                  >
                    {actionText(currentAction)}
                  </Button>
                </Box>
              </Box>
            </Box>
          </form>
        )
      }}
    </Formik>
  )
}

export default withContext(LogForm)
