import React from "react"

import Provider from "context/Dashboard/Dashboard/main/Provider"
import View from "context/Dashboard/Dashboard/main/View"

export default () => (
  <Provider>
    <View />
  </Provider>
)
