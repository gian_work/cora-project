import React, { useContext, useEffect, useRef, ReactInstance } from "react"
import Box from "@material-ui/core/Box"
/** Layout */
import Spacer from "components/Spacer"

/** Components */
import { Context as DashboardContext } from "context/Dashboard/Dashboard/main/Context"
import Application from "./components/Application"
import VisitorManagement from "./components/VisitorManagement"
import NewAccountsRequest from "./components/NewAccountsRequest"
import PaymentReceivedToday from "./components/PaymentReceivedToday"
import NewFeedback from "./components/NewFeedback"
import Attendance from "./components/Attendance"
import PageTitle from "./components/PageTitle"
/** Context */

/* eslint-disable react/prefer-stateless-function */
class EmptyComponent extends React.Component {
  render() {
    return null
  }
}

const Dashboard: React.FC = () => {
  const spaceHeight = "23px"
  const ele: ReactInstance = new EmptyComponent({})
  const refReport = useRef<any>(ele)

  const { viewSettings } = useContext(DashboardContext)
  const { applications, vms } = viewSettings

  useEffect(() => {
    window.scrollTo(0, 0)
  }, [])

  function ViewApplication(): JSX.Element {
    if (!applications) {
      return <div />
    }
    return <Application />
  }

  function ViewVMS(): JSX.Element {
    if (!vms) {
      return <div />
    }
    return <VisitorManagement />
  }

  return (
    <>
      <Box>
        <PageTitle
          title="Dashboard"
          breadcrumbs="property management > dashboard"
          ele={ele}
          refElement={refReport}
        />
      </Box>
      <div ref={refReport} style={{ width: "100%" }}>
        <Box display="flex">
          <Box minWidth="70%" flex="1" paddingRight="23px">
            <Box>{ViewApplication()}</Box>
            <Spacer isDefault />
            <Box>{ViewVMS()}</Box>
          </Box>
          <Spacer isDefault />
          <Box minWidth="30%">
            <PaymentReceivedToday
              totalPaymentsReceivedToday="7,290"
              depositsThisWeek="18,224"
            />
            <Spacer height={spaceHeight} />
            <NewAccountsRequest />
            <Spacer height={spaceHeight} />
            <NewFeedback />
            <Spacer height={spaceHeight} />
            <Attendance total="35" />
          </Box>
        </Box>
      </div>

      {/* <ManageSettings openStatus={openStatus} setopenStatus={setopenStatus} /> */}
    </>
  )
}

export default Dashboard
