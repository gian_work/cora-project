const SummaryConfig: any = {
  facilities: {
    "title": "Facilities",
    "subtitle": "Total Facility Booking"
  },
  applications: {
    "title": "Application",
    "subtitle": "Total Application Requests"
  }
}

export const facilitiesMock = [
  {
    name: "BBQ Pits",
    total: 27
  },
  {
    name: "Squash Courts",
    total: 8
  },
  {
    name: "Basketball Courts",
    total: 10
  },
  {
    name: "Table Tennis",
    total: 5
  },
  {
    name: "Tennis Courts",
    total: 1
  },
  {
    name: "Gold Simulator Room",
    total: 4
  }
]

export const applicationsMock = [
  {
    name: "Moving in/out",
    total: 76
  },
  {
    name: "Pet Registration",
    total: 8
  },
  {
    name: "Bulky Delivery",
    total: 4
  },
  {
    name: "Access Card",
    total: 39
  },
  {
    name: "Aircon Cleaning",
    total: 5
  },
  {
    name: "Bike Tag",
    total: 3
  }
]

export default SummaryConfig
