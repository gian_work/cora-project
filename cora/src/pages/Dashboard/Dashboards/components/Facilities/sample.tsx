export const Today = [
  {
    blockUnitNo: "18#01-01",
    name: "Soh Sai Bor (Su Caibo)",
    facilityType: "Function Room 1",
    time: "6:00 pm"
  },
  {
    blockUnitNo: "18#01-02",
    name: "Soh Sai Bor (Su Caibo)",
    facilityType: "BBQ Pit 1",
    time: "6:30 pm"
  },
  {
    blockUnitNo: "18#01-03",
    name: "Manash Joshi",
    facilityType: "Function Room 2",
    time: "5:00 pm"
  },
  {
    blockUnitNo: "18#01-04",
    name: "Jin Shan Zi",
    facilityType: "KTV Room",
    time: "7:00 pm"
  },

  {
    blockUnitNo: "18#01-01",
    name: "Soh Sai Bor (Su Caibo)",
    facilityType: "Function Room 1",
    time: "6:00 pm"
  }
]

export const NextDay = [
  {
    blockUnitNo: "18#01-01",
    name: "Soh Sai Bor (Su Caibo)",
    facilityType: "Function Room 1",
    time: "6:00 pm"
  },
  {
    blockUnitNo: "18#01-04",
    name: "Jin Shan Zi",
    facilityType: "KTV Room",
    time: "7:00 pm"
  },
  {
    blockUnitNo: "18#01-03",
    name: "Manash Joshi",
    facilityType: "Function Room 2",
    time: "5:00 pm"
  },
  {
    blockUnitNo: "18#01-02",
    name: "Soh Sai Bor (Su Caibo)",
    facilityType: "BBQ Pit 1",
    time: "6:30 pm"
  },

  {
    blockUnitNo: "18#01-01",
    name: "Soh Sai Bor (Su Caibo)",
    facilityType: "Function Room 1",
    time: "6:00 pm"
  }
]
