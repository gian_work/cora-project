import React, { useState } from "react"
import { makeStyles, Theme } from "@material-ui/core/styles"
import Box from "@material-ui/core/Box"
import Card from "@material-ui/core/Card"
import Tabs from "@material-ui/core/Tabs"
import Tab from "@material-ui/core/Tab"
import Button from "@material-ui/core/Button"
import { Link } from "react-router-dom"
// import gql from "graphql-tag"
// import { useQuery } from "@apollo/react-hooks"

// components
import Table from "components/Table"

// sample config
import { Today, NextDay } from "./sample"
import TableHeaderColumns from "./config/TableHeaderColumns"

const useStyles = makeStyles((theme: Theme) => ({
  title: {
    color: theme.palette.body.secondary,
    fontSize: "16px",
    fontWeight: 600
  },
  container: {
    width: "24px",
    height: "24px",
    backgroundColor: theme.palette.body.pink,
    color: "#FFFFFF",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    marginLeft: "5px",
    borderRadius: "50%"
  }
}))

const Icon = (
  <svg
    width="16"
    height="20"
    viewBox="0 0 16 20"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M3 0V3H5V0H3ZM7 0V3H9V0H7ZM11 0V3H13V0H11ZM0 4V5C0 7.65632 1.66865 9.9054 4.07812 11.0938L0 20H2.17188L4.01562 16H11.9707L12.3652 16.8516C12.1283 17.1877 12.0008 17.5887 12 18C12 18.5304 12.2107 19.0391 12.5858 19.4142C12.9609 19.7893 13.4696 20 14 20C14.5304 20 15.0391 19.7893 15.4142 19.4142C15.7893 19.0391 16 18.5304 16 18C15.9996 17.501 15.8126 17.0201 15.4758 16.6519C15.139 16.2836 14.6767 16.0546 14.1797 16.0098L11.9043 11.1035C14.3235 9.91754 16 7.66297 16 5V4H0ZM2.24023 6H13.7598C13.1965 8.22071 10.9583 10 8 10C5.04168 10 2.80354 8.22071 2.24023 6ZM5.9707 11.7617C6.62276 11.9119 7.30093 12 8 12C8.69134 12 9.36232 11.9146 10.0078 11.7676L11.043 14H4.9375L5.9707 11.7617Z"
      fill="#464646"
    />
  </svg>
)

const Attendance: React.FC = () => {
  const { title, container } = useStyles()

  const [value, setValue] = useState(0)

  const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
    setValue(newValue)
  }

  const SampleData = value === 0 || value === 2 ? Today : NextDay

  const Badge = (label: string, count: number) => (
    <div
      style={{
        display: "flex",
        justifyContent: "flex-start"
      }}
    >
      <span>{label}</span>
      {" "}
      {count !== 0 && <span className={container}>{count}</span>}
    </div>
  )

  return (
    <>
      <Box>
        <Card>
          <Box
            display="flex"
            alignItems="center"
            justifyContent="space-between"
            padding="16px 23px 0"
            borderBottom="1px solid #F2F2F2"
          >
            <Box className={title} display="flex">
              <Box marginRight="20px">{Icon}</Box>
              Facilities
            </Box>
            <Box>
              <Tabs
                value={value}
                onChange={handleChange}
                indicatorColor="primary"
                textColor="primary"
              >
                <Tab label={Badge("Today", 0)} />
                <Tab label={Badge("Next Day", 0)} />
                <Tab label={Badge("Pending Approval", 12)} />
              </Tabs>
            </Box>
          </Box>
          <Box>
            <Table
              data={SampleData}
              columns={TableHeaderColumns}
              size={5}
              showPagination={false}
            />
            <Box display="flex" justifyContent="flex-end" padding="10px 10px">
              <Button
                color="primary"
                size="large"
                component={Link}
                to="/property-management/facilities"
              >
                <Box fontWeight="600">VIEW ALL</Box>
              </Button>
            </Box>
          </Box>
        </Card>
      </Box>
    </>
  )
}

export default Attendance
