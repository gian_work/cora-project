const TableHeaderColumns = [
  {
    Header: "",
    columns: [
      {
        Header: "Block/ Unit No.",
        accessor: "blockUnitNo"
      },
      {
        Header: "Name",
        accessor: "name"
      },
      {
        Header: "Facility Type",
        accessor: "facilityType"
      },
      {
        Header: "Time",
        accessor: "time"
      }
    ]
  }
]

export default TableHeaderColumns
