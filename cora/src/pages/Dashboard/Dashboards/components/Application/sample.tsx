export const Today = [
  {
    blockUnitNo: "18#01-01",
    name: "Tan Siang Hua",
    applicationType: "Aircon Servicing",
    deposit: "100.00"
  },
  {
    blockUnitNo: "18#01-02",
    name: "Soh Sai Bor (Su Caibo)",
    applicationType: "Renovation",
    deposit: "N/A"
  },
  {
    blockUnitNo: "18#01-03",
    name: "Manash Joshi",
    applicationType: "Moving In",
    deposit: "Paid"
  },
  {
    blockUnitNo: "18#01-04",
    name: "Jin Shan Zi",
    applicationType: "Moving Out",
    deposit: "Paid"
  },
  {
    blockUnitNo: "18#01-01",
    name: "Tan Siang Hua",
    applicationType: "Aircon Servicing",
    deposit: "100.00"
  },
  {
    blockUnitNo: "18#01-02",
    name: "Soh Sai Bor (Su Caibo)",
    applicationType: "Renovation",
    deposit: "N/A"
  },
  {
    blockUnitNo: "18#01-03",
    name: "Manash Joshi",
    applicationType: "Moving In",
    deposit: "Paid"
  },
  {
    blockUnitNo: "18#01-04",
    name: "Jin Shan Zi",
    applicationType: "Moving Out",
    deposit: "Paid"
  }
]

export const NextDay = [
  {
    blockUnitNo: "18#01-04",
    name: "Jin Shan Zi",
    applicationType: "Moving Out",
    deposit: "Paid"
  },
  {
    blockUnitNo: "18#01-03",
    name: "Manash Joshi",
    applicationType: "Moving In",
    deposit: "Paid"
  },
  {
    blockUnitNo: "18#01-02",
    name: "Soh Sai Bor (Su Caibo)",
    applicationType: "Renovation",
    deposit: "N/A"
  },
  {
    blockUnitNo: "18#01-01",
    name: "Tan Siang Hua",
    applicationType: "Aircon Servicing",
    deposit: "100.00"
  },
  {
    blockUnitNo: "18#01-03",
    name: "Manash Joshi",
    applicationType: "Moving In",
    deposit: "Paid"
  }
]
