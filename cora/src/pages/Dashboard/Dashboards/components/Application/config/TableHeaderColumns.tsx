const TableHeaderColumns = [
  {
    Header: "",
    columns: [
      {
        Header: "Block/ Unit No.",
        accessor: "blockUnitNo"
      },
      {
        Header: "Name",
        accessor: "name"
      },
      {
        Header: "Application Type",
        accessor: "applicationType"
      },
      {
        Header: "Deposit",
        accessor: "deposit"
      }
    ]
  }
]

export default TableHeaderColumns
