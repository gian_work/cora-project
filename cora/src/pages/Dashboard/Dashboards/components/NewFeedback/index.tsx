import React, { useContext } from "react"
import Card from "@material-ui/core/Card"
import Box from "@material-ui/core/Box"
import Fade from "@material-ui/core/Fade"
/** Components */
import Loader from "components/Loader"
/** Styles */
import { IconFeedback } from "components/Icons/Dashboard"
import { toNumeralFixed } from "utils/helpers"
import { Context } from "context/Dashboard/Dashboard/main/Context"
import styles from "./styles"
/**
 * Context
 */
/** Hook */
import { CountFeedbackHook } from "./hooks"
/** Utils */

const NewFeedback: React.FC = () => {
  const { viewAll, title, body, totalStyle, wrapper, totalWrapper } = styles()
  const { currentDate, dateRange } = useContext(Context)
  const { totalFeedback, isValidating } = CountFeedbackHook(dateRange)

  function ViewTotalNewFeedback() {
    if (isValidating) {
      return <Loader dark />
    }

    return (
      <Box className={totalWrapper}>
        <Fade in={!isValidating} timeout={1000}>
          <Box className={totalStyle}>{toNumeralFixed(totalFeedback)}</Box>
        </Fade>
      </Box>
    )
  }

  return (
    <Card>
      <Box className={wrapper}>
        <Box display="flex" padding="0 23px">
          <Box paddingRight="25px">{IconFeedback}</Box>
          <Box>
            <Box className={title}>New Feedback</Box>
            <Box className={body}>{`As of ${currentDate}`}</Box>
            <Box className={viewAll}>View All</Box>
          </Box>
        </Box>

        <ViewTotalNewFeedback />
      </Box>
    </Card>
  )
}

export default NewFeedback
