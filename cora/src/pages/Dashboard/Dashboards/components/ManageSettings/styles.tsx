import { makeStyles, Theme } from "@material-ui/core/styles"

const styles = makeStyles((theme: Theme) => ({
  root: {
    width: "100%",
    display: "flex",
    margin: 0,
    justifyContent: "space-between",
    "& > .MuiFormControlLabel-label": {
      color: theme.palette.body.main,
      fontSize: "16px"
    }
  },
  customizeTitle: {
    color: theme.palette.body.secondary,
    fontSize: "10px",
    marginBottom: "15px"
  },
  container: {
    padding: "20px 30px",
    borderBottom: "1px solid #F2F2F2"
  },
  buttonGroup: {
    display: "flex",
    justifyContent: "flex-end",
    padding: "50px 30px",
    "& > .MuiButton-root": {
      width: "130px"
    },
    "& > .MuiButton-contained": {
      width: "170px"
    }
  }
}))

export default styles
