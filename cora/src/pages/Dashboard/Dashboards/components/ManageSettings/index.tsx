import React, { useContext } from "react"
import Box from "@material-ui/core/Box"

/** Components */
import Drawer from "components/Drawer"
import Header from "components/Header"
import Switch from "components/Forms/Switch"
/** Context */
import { Context as DashboardContext } from "context/Dashboard/Dashboard/main/Context"
/** Styles */
import styles from "./styles"

interface ManageSettingsProps {
  openStatus: boolean
  setopenStatus: Function
}
export const ManageSettings: React.FC<ManageSettingsProps> = ({
  openStatus,
  setopenStatus
}) => {
  const { customizeTitle, container } = styles()

  const { viewSettings, setViewSettings } = useContext(DashboardContext)
  const { applications, vms } = viewSettings

  function handleChange(name: string, value: boolean) {
    setViewSettings &&
      setViewSettings((settings: any) => ({
        ...settings,
        [name]: value
      }))
  }

  return (
    <Drawer openState={openStatus} setopenState={setopenStatus}>
      <Header title="Widget Settings" subtitle="DASHBOARD SETTINGS" subtitleAbove />
      <Box padding="40px 30px" borderBottom="1px solid #F2F2F2">
        <Switch
          action={() => handleChange("applications", !applications)}
          label="Enable All Widgets"
          checked={applications}
        />
      </Box>
      {/* Customize */}
      <Box>
        <Box className={container}>
          <Box className={customizeTitle}>CUSTOMIZE</Box>
          <Switch
            action={() => handleChange("applications", !applications)}
            label="Applications"
            checked={applications}
          />
        </Box>
        <Box className={container}>
          <Switch
            action={() => handleChange("vms", !vms)}
            label="Visitor Management"
            checked={vms}
          />
        </Box>
        {/* <Box className={container}>
          <FormControlLabel
            className={root}
            control={
              <Switch
                checked={state.activitiesToday}
                onChange={handleChange("activitiesToday")}
                value="activitiesToday"
                color="primary"
              />
            }
            label="Activities Today"
            labelPlacement="start"
          />
        </Box>
        <Box className={container}>
          <FormControlLabel
            className={root}
            control={
              <Switch
                checked={state.eventsforthemonth}
                onChange={handleChange("eventsforthemonth")}
                value="eventsforthemonth"
                color="primary"
              />
            }
            label="Events for the month"
            labelPlacement="start"
          />
        </Box>
        <Box className={container}>
          <FormControlLabel
            className={root}
            control={
              <Switch
                checked={state.newaccountsrequest}
                onChange={handleChange("newaccountsrequest")}
                value="newaccountsrequest"
                color="primary"
              />
            }
            label="New Accounts Requests"
            labelPlacement="start"
          />
        </Box>
        <Box className={container}>
          <FormControlLabel
            className={root}
            control={
              <Switch
                checked={state.feedback}
                onChange={handleChange("feedback")}
                value="feedback"
                color="primary"
              />
            }
            label="Feedback"
            labelPlacement="start"
          />
        </Box>
        <Box className={container}>
          <FormControlLabel
            className={root}
            control={
              <Switch
                checked={state.facilities}
                onChange={handleChange("facilities")}
                value="facilities"
                color="primary"
              />
            }
            label="Facilities"
            labelPlacement="start"
          />
        </Box>
        <Box className={container}>
          <FormControlLabel
            className={root}
            control={
              <Switch
                checked={state.application}
                onChange={handleChange("application")}
                value="application"
                color="primary"
              />
            }
            label="Application"
            labelPlacement="start"
          />
        </Box>
        <Box className={container}>
          <FormControlLabel
            className={root}
            control={
              <Switch
                checked={state.attendance}
                onChange={handleChange("attendance")}
                value="attendance"
                color="primary"
              />
            }
            label="Attendance"
            labelPlacement="start"
          />
        </Box>
        <Box className={container}>
          <FormControlLabel
            className={root}
            control={
              <Switch
                checked={state.visitormanagement}
                onChange={handleChange("visitormanagement")}
                value="visitormanagement"
                color="primary"
              />
            }
            label="Visitor Management Managementdance"
            labelPlacement="start"
          />
        </Box>
       */}
      </Box>
      {/* <Box className={buttonGroup}>
        <Button size="large" color="primary" onClick={() => null}>
          Cancel
        </Button>
        <Button size="large" variant="contained" color="primary">
          Save
        </Button>
      </Box> */}
    </Drawer>
  )
}

export default ManageSettings
