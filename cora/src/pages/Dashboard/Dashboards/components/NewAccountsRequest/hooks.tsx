import useSWR from "swr"
import service from "services/Dashboard/Dashboard/"

export function CountAccountRequestHook({ startDate, endDate }: any) {
  const fetchCountAccountRequestHook = async () =>
    service.getCountAccountRequests({
      "start_date": startDate,
      "end_date": endDate
    })

  const { data, isValidating, error } = useSWR(
    "fetchCountAccountRequestHook",
    fetchCountAccountRequestHook,
    {
      revalidateOnFocus: true
    }
  )
  const totalAccountRequest = data?.data?._data?.total

  return {
    totalAccountRequest,
    isValidatingAccountRequest: isValidating,
    error
  }
}

export function CountChangeAddress({ startDate, endDate }: any) {
  const fetchCountChangeAddressRequestHook = async () =>
    service.getCountChangeAddress({
      "start_date": startDate,
      "end_date": endDate
    })

  const { data, isValidating, error } = useSWR(
    "fetchCountChangeAddressRequestHook",
    fetchCountChangeAddressRequestHook,
    {
      revalidateOnFocus: true
    }
  )
  const totalChangeAddress = data?.data?._data?.total

  return {
    totalChangeAddress,
    isValidatingChangeAddress: isValidating,
    error
  }
}
