import React, { useContext } from "react"
import Card from "@material-ui/core/Card"
import Box from "@material-ui/core/Box"
import Fade from "@material-ui/core/Fade"
/** Component */
import Loader from "components/Loader"
/** Icon */
import { IconAccountRequest } from "components/Icons/Dashboard/"
/** Styles */
import { Context } from "context/Dashboard/Dashboard/main/Context"
import styles from "./styles"
/** Context */
/** Hook */
import { CountAccountRequestHook, CountChangeAddress } from "./hooks"

const NewAccountsRequest: React.FC = () => {
  const { viewAll, title, body, totalStyle, wrapper, totalWrapper } = styles()
  const { currentDate, dateRange } = useContext(Context)
  const {
    totalAccountRequest,
    isValidatingAccountRequest
  } = CountAccountRequestHook(dateRange)
  const { totalChangeAddress, isValidatingChangeAddress } = CountChangeAddress(
    dateRange
  )

  function ViewTotalAccountRequest() {
    if (isValidatingAccountRequest) {
      return <Loader dark />
    }

    return (
      <Box className={totalWrapper}>
        <Fade in={!isValidatingAccountRequest} timeout={1000}>
          <Box className={totalStyle}>{totalAccountRequest}</Box>
        </Fade>
      </Box>
    )
  }

  function ViewTotalChangeAddress() {
    if (isValidatingChangeAddress) {
      return <Loader dark />
    }

    return (
      <Box className={totalWrapper}>
        <Fade in={!isValidatingChangeAddress} timeout={1000}>
          <Box className={totalStyle}>{totalChangeAddress || "N/A"}</Box>
        </Fade>
      </Box>
    )
  }

  return (
    <Card>
      <Box className={wrapper} borderBottom="1px solid #F2F2F2">
        <Box display="flex" padding="0 23px">
          <Box paddingRight="25px">{IconAccountRequest}</Box>
          <Box>
            <Box className={title}>New Accounts Requests</Box>
            <Box className={body}>{`As of ${currentDate}`}</Box>
            <Box className={viewAll}>View All</Box>
          </Box>
        </Box>

        <ViewTotalAccountRequest />
      </Box>

      <Box className={wrapper}>
        <Box display="flex" padding="0 23px">
          <Box marginRight="25px" width="20px" />
          <Box>
            <Box className={title}>Change Address Requests</Box>
            <Box className={body}>{`As of ${currentDate}`}</Box>
            <Box className={viewAll}>View All</Box>
          </Box>
        </Box>

        <ViewTotalChangeAddress />
      </Box>
    </Card>
  )
}

export default NewAccountsRequest
