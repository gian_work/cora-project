import * as React from "react"
import { makeStyles, Theme } from "@material-ui/core/styles"
import Card from "@material-ui/core/Card"
import Box from "@material-ui/core/Box"

const Icon = (
  <svg
    width="24"
    height="27"
    viewBox="0 0 24 27"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M4 0.333252V2.99992H2.66667C1.2 2.99992 0 4.19992 0 5.66658V24.3333C0 25.7999 1.2 26.9999 2.66667 26.9999H21.3333C22.8 26.9999 24 25.7999 24 24.3333V5.66658C24 4.19992 22.8 2.99992 21.3333 2.99992H20V0.333252H17.3333V2.99992H6.66667V0.333252H4ZM2.66667 5.66658H21.3333V8.33325H2.66667V5.66658ZM2.66667 10.9999H21.3333V24.3333H2.66667V10.9999ZM13.3333 16.3333V21.6666H18.6667V16.3333H13.3333Z"
      fill="#454B57"
    />
  </svg>
)

const useStyles = makeStyles((theme: Theme) => ({
  title: {
    color: theme.palette.body.secondary,
    fontSize: "16px",
    fontWeight: 600
  },
  totalStyle: {
    color: theme.palette.body.greenLight100,
    fontSize: "64px",
    fontWeight: 500
  },
  body: {
    color: theme.palette.body.dark,
    fontSize: "12px"
  },
  viewAll: {
    marginTop: "30px",
    fontSize: "14px",
    fontWeight: 600,
    color: theme.palette.primary.main,
    textTransform: "uppercase"
  }
}))

// interface
interface EventsProps {
  total: string
}

const Events: React.FC<EventsProps> = ({ total }) => {
  const { viewAll, title, body, totalStyle } = useStyles()

  return (
    <Card>
      <Box
        display="flex"
        alignItems="center"
        justifyContent="space-between"
        padding="28px 0"
      >
        <Box display="flex" padding="0 23px">
          <Box paddingRight="25px">{Icon}</Box>
          <Box>
            <Box className={title}>Events for the month</Box>
            <Box className={body}>As of January 17, 2020</Box>
            <Box className={viewAll}>View All</Box>
          </Box>
        </Box>

        <Box alignItems="center" justifyContent="space-between" padding="0 23px">
          <Box className={totalStyle}>{total}</Box>
        </Box>
      </Box>
    </Card>
  )
}

export default Events

// background: rgb(255,231,215);
// background: linear-gradient(108deg, rgba(255,231,215,1) 0%, rgba(255,176,123,1) 48%, rgba(240,140,71,1) 100%);
