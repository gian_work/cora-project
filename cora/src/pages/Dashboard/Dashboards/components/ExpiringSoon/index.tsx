import React, { useState } from "react"
import { makeStyles, Theme } from "@material-ui/core/styles"
import Card from "@material-ui/core/Card"
import Box from "@material-ui/core/Box"
import Tabs from "@material-ui/core/Tabs"
import Tab from "@material-ui/core/Tab"

const Icon = (
  <svg
    width="16"
    height="20"
    viewBox="0 0 16 20"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M8 0C7.172 0 6.5 0.672 6.5 1.5V2.19531C3.91318 2.86209 2 5.2048 2 8V14L0 16V17H6.26953C6.09344 17.3039 6.00048 17.6488 6 18C6 18.5304 6.21071 19.0391 6.58579 19.4142C6.96086 19.7893 7.46957 20 8 20C8.53043 20 9.03914 19.7893 9.41421 19.4142C9.78929 19.0391 10 18.5304 10 18C9.99893 17.6486 9.90529 17.3037 9.72852 17H16V16L14 14V8C14 5.2048 12.0868 2.86209 9.5 2.19531V1.5C9.5 0.672 8.828 0 8 0ZM8 4C10.206 4 12 5.794 12 8V14V14.8281L12.1719 15H3.82812L4 14.8281V14V8C4 5.794 5.794 4 8 4Z"
      fill="#464646"
    />
  </svg>
)

const useStyles = makeStyles((theme: Theme) => ({
  container: {
    minHeight: "335px"
  },
  title: {
    color: theme.palette.body.secondary,
    fontSize: "16px",
    fontWeight: 600
  },
  timelineTitle: {
    color: theme.palette.body.main,
    fontSize: "14px"
  },
  timelineSubtitle: {
    color: theme.palette.body.secondary,
    fontSize: "12px"
  },
  dot: {
    width: "8px",
    height: "8px",
    backgroundColor: theme.palette.body.pink,
    position: "absolute",
    top: "5px",
    left: "-25px",
    borderRadius: "50%"
  }
}))

const ExpiringSoon: React.FC = () => {
  const { container, dot, title, timelineTitle, timelineSubtitle } = useStyles()
  const [value, setValue] = useState(0)

  const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
    setValue(newValue)
  }

  return (
    <Card className={container}>
      <Box
        display="flex"
        alignItems="center"
        padding="16px 23px"
        borderBottom="1px solid #F2F2F2"
      >
        <Box padding="5px 15px 0 0">{Icon}</Box>
        <Box className={title}>Expiring Soon</Box>
      </Box>

      <Box borderBottom="1px solid #F2F2F2">
        <Tabs
          value={value}
          onChange={handleChange}
          indicatorColor="primary"
          textColor="primary"
          style={{ display: "flex", justifyContent: "space-between" }}
        >
          <Tab label="Contracts" style={{ flex: "1" }} />
          <Tab label="Permits" style={{ flex: "1" }} />
          <Tab label="Licence" style={{ flex: "1" }} />
        </Tabs>
      </Box>

      {value === 0 && (
        <Box
          borderLeft="3px solid #EDF0F1"
          padding="10px 20px 0 20px"
          margin="20px 30px 30px"
        >
          <Box
            display="flex"
            flexDirection="column"
            paddingLeft="15px"
            marginBottom="15px"
            position="relative"
          >
            <Box className={timelineTitle}>Cleaning Service Contracts</Box>
            <Box className={timelineSubtitle}>Due on February 29, 2020</Box>
            <Box className={dot} />
          </Box>

          <Box
            display="flex"
            flexDirection="column"
            paddingLeft="15px"
            marginBottom="15px"
            position="relative"
          >
            <Box className={timelineTitle}>Pest Control - Fogging</Box>
            <Box className={timelineSubtitle}>10:00 am - 1:00 pm</Box>
            <Box className={dot} />
          </Box>

          <Box
            display="flex"
            flexDirection="column"
            paddingLeft="15px"
            marginBottom="15px"
            position="relative"
          >
            <Box className={timelineTitle}>Landscape Service</Box>
            <Box className={timelineSubtitle}>Due on March 8, 2020</Box>
            <Box className={dot} />
          </Box>
        </Box>
      )}
      {/* Permits */}
      {value === 2 && (
        <Box
          borderLeft="3px solid #EDF0F1"
          padding="10px 20px 0 20px"
          margin="20px 30px 30px"
        >
          <Box
            display="flex"
            flexDirection="column"
            paddingLeft="15px"
            marginBottom="15px"
            position="relative"
          >
            <Box className={timelineTitle}>Elevator Operation</Box>
            <Box className={timelineSubtitle}>Due on February 14, 2020</Box>
            <Box className={dot} />
          </Box>

          <Box
            display="flex"
            flexDirection="column"
            paddingLeft="15px"
            marginBottom="15px"
            position="relative"
          >
            <Box className={timelineTitle}>Security Renewal</Box>
            <Box className={timelineSubtitle}>Due on February 27, 2020</Box>
            <Box className={dot} />
          </Box>

          <Box
            display="flex"
            flexDirection="column"
            paddingLeft="15px"
            marginBottom="15px"
            position="relative"
          >
            <Box className={timelineTitle}>Office Software Licenses</Box>
            <Box className={timelineSubtitle}>Due on March 9, 2020</Box>
            <Box className={dot} />
          </Box>
        </Box>
      )}
      {/* Licence */}
      {value === 1 && (
        <Box
          borderLeft="3px solid #EDF0F1"
          padding="10px 20px 0 20px"
          margin="20px 30px 30px"
        >
          <Box
            display="flex"
            flexDirection="column"
            paddingLeft="15px"
            marginBottom="15px"
            position="relative"
          >
            <Box className={timelineTitle}>Work Repair Permit - Lobby</Box>
            <Box className={timelineSubtitle}>Due on February 20, 2020</Box>
            <Box className={dot} />
          </Box>

          <Box
            display="flex"
            flexDirection="column"
            paddingLeft="15px"
            marginBottom="15px"
            position="relative"
          >
            <Box className={timelineTitle}>Electrical Maintenance</Box>
            <Box className={timelineSubtitle}>12:00 am - 1:00 am</Box>
            <Box className={dot} />
          </Box>

          <Box
            display="flex"
            flexDirection="column"
            paddingLeft="15px"
            marginBottom="15px"
            position="relative"
          >
            <Box className={timelineTitle}>Water Repair - Block A</Box>
            <Box className={timelineSubtitle}>Due on March 18, 2020</Box>
            <Box className={dot} />
          </Box>
        </Box>
      )}
    </Card>
  )
}

export default ExpiringSoon
