import React from "react"

const Security: React.FC = () => {
  return (
    <svg
      width="19"
      height="22"
      viewBox="0 0 19 22"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M9.5 0L0.875 4C0.875 4 0.875 8 0.875 10C0.875 17.83 7.04571 21.486 9.5 22C11.9543 21.486 18.125 17.83 18.125 10C18.125 8 18.125 4 18.125 4L9.5 0ZM16.2083 10C16.2083 16.134 11.6227 19.254 9.5 19.937C7.37729 19.254 2.79167 16.134 2.79167 10V5.3L9.5 2.189L16.2083 5.3V10Z"
        fill="#646464"
      />
    </svg>
  )
}

export default Security
