import React from "react"
import { makeStyles, Theme } from "@material-ui/core/styles"
import Box from "@material-ui/core/Box"
import Card from "@material-ui/core/Card"
import Tabs from "@material-ui/core/Tabs"
import Tab from "@material-ui/core/Tab"
import Button from "@material-ui/core/Button"
import { Link } from "react-router-dom"

// import gql from "graphql-tag"
// import { useQuery } from "@apollo/react-hooks"

// components
import Table from "components/Table"

// sample config
import { Today, NextDay } from "./sample"
import TableHeaderColumns from "./config/TableHeaderColumns"

const useStyles = makeStyles((theme: Theme) => ({
  title: {
    color: theme.palette.body.secondary,
    fontSize: "16px",
    fontWeight: 600
  },
  container: {
    width: "24px",
    height: "24px",
    backgroundColor: theme.palette.body.pink,
    color: "#FFFFFF",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    marginLeft: "5px",
    borderRadius: "50%"
  }
}))

const Icon = (
  <svg
    width="19"
    height="18"
    viewBox="0 0 19 18"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M2 0C0.897 0 0 0.897 0 2V18H4V16H2V2H12V7.55859C12.174 7.66159 12.343 7.77339 12.5 7.90039C12.935 7.54939 13.446 7.29548 14 7.14648V2C14 0.897 13.103 0 12 0H2ZM4 4V6H6V4H4ZM8 4V6H10V4H8ZM4 8V10H6V8H4ZM10 9C9.46957 9 8.96086 9.21071 8.58579 9.58579C8.21071 9.96086 8 10.4696 8 11C8 11.5304 8.21071 12.0391 8.58579 12.4142C8.96086 12.7893 9.46957 13 10 13C10.5304 13 11.0391 12.7893 11.4142 12.4142C11.7893 12.0391 12 11.5304 12 11C12 10.4696 11.7893 9.96086 11.4142 9.58579C11.0391 9.21071 10.5304 9 10 9ZM15 9C14.4696 9 13.9609 9.21071 13.5858 9.58579C13.2107 9.96086 13 10.4696 13 11C13 11.5304 13.2107 12.0391 13.5858 12.4142C13.9609 12.7893 14.4696 13 15 13C15.5304 13 16.0391 12.7893 16.4142 12.4142C16.7893 12.0391 17 11.5304 17 11C17 10.4696 16.7893 9.96086 16.4142 9.58579C16.0391 9.21071 15.5304 9 15 9ZM4 12V14H6V12H4ZM10 15C7.815 15 6 15.9088 6 17.2168V18H11H14H19V17.2168C19 15.9088 17.185 15 15 15C14.0549 15 13.1897 15.1777 12.5 15.4766C11.8103 15.1777 10.9451 15 10 15Z"
      fill="#454B57"
    />
  </svg>
)

const Application: React.FC = () => {
  const { title, container } = useStyles()

  const [value, setValue] = React.useState(0)

  const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
    setValue(newValue)
  }

  const SampleData = value === 0 || value === 2 ? Today : NextDay

  const Badge = (label: string, count: number) => (
    <div
      style={{
        display: "flex",
        justifyContent: "flex-start"
      }}
    >
      <span>{label}</span>
      {" "}
      {count !== 0 && <span className={container}>{count}</span>}
    </div>
  )

  return (
    <>
      <Box>
        <Card>
          <Box
            display="flex"
            alignItems="center"
            justifyContent="space-between"
            padding="16px 23px 0"
            borderBottom="1px solid #F2F2F2"
          >
            <Box className={title} display="flex">
              <Box marginRight="20px">{Icon}</Box>
              Visitor Management
            </Box>
            <Box>
              <Tabs
                value={value}
                onChange={handleChange}
                indicatorColor="primary"
                textColor="primary"
              >
                <Tab color="red" label={Badge("Today", 0)} />
                <Tab label={Badge("Next Day", 0)} />
                <Tab label={Badge("Pending Approval", 12)} />
              </Tabs>
            </Box>
          </Box>
          <Box>
            <Table
              data={SampleData}
              columns={TableHeaderColumns}
              size={5}
              showPagination={false}
            />
            <Box display="flex" justifyContent="flex-end" padding="10px 10px">
              <Button
                color="primary"
                size="large"
                component={Link}
                to="/property-management/visitor-management"
              >
                <Box fontWeight="600">VIEW ALL</Box>
              </Button>
            </Box>
          </Box>
        </Card>
      </Box>
    </>
  )
}

export default Application
