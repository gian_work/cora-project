const TableHeaderColumns = [
  {
    Header: "",
    columns: [
      {
        Header: "Block/ Unit No.",
        accessor: "blockUnitNo"
      },
      {
        Header: "Name",
        accessor: "name"
      },
      {
        Header: "Category",
        accessor: "cateogory"
      },
      {
        Header: "Time",
        accessor: "time"
      }
    ]
  }
]

export default TableHeaderColumns
