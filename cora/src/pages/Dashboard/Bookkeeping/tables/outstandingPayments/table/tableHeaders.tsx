import React from "react"
import ContextMenu from "components/ContextMenu"

/** Components */
import StatusBlock from "components/Table/components/StatusBlock"
import ContextBookKeeping from "pages/Dashboard/Bookkeeping/components/ContextMenu"

/** Utils */
import dateHelper from "utils/date"

/** Config */
import { paymentType, status, statusColor } from "config/Dashboard/Bookkeeping"

const TableHeaderColumns = [
  {
    fixed: "left",
    disableFilters: true,
    columns: [
      {
        width: 70,
        Cell: ({ row }: any) => {
          return (
            <ContextMenu>
              <ContextBookKeeping data={row._original} />
            </ContextMenu>
          )
        },
        filterable: false
      }
    ]
  },
  {
    columns: [
      {
        Header: "Status",
        id: "status",
        width: 170,
        // sortable: false,
        // filterable: false,
        accessor: (c: any) => {
          return status[c.payment_status]
        },
        Cell: ({ original }: any) => {
          return (
            <StatusBlock
              status={status[original.payment_status]}
              color={statusColor[original.payment_status]}
            />
          )
        }
      },
      {
        Header: "Receipt No.",
        id: "txn_id",
        accessor: "txn_id",
        width: 200
      },
      {
        Header: "Block/Unit No",
        id: "blockUnitNo",
        accessor: "unit.short_name",
        width: 150,
        Cell: ({ original }: any) => {
          return <div>{original.unit.short_name}</div>
        }
      },
      {
        Header: "Name",
        id: "payee_name",
        accessor: "payee_name",
        width: 200
      },
      {
        Header: "Type",
        id: "type",
        accessor: (c: any) => {
          return paymentType[c.payment_type]
        },
        Cell: ({ original }: any) => {
          return original.payment_type === undefined ? (
            "N/A"
          ) : (
            <div>{paymentType[original.payment_type]}</div>
          )
        },
        width: 150
      },
      {
        Header: "Category",
        id: "category",
        accessor: "category",
        width: 150
      },
      {
        Header: "Payment Description",
        id: "description",
        accessor: "description",
        width: 250
      },
      {
        Header: "Payment Date",
        id: "paymentDate",
        accessor: (c: any) => {
          return dateHelper.fromUnix(c._created)
        },
        Cell: ({ original }: any) => {
          return original._created === undefined
            ? "N/A"
            : dateHelper.fromUnix(original._created)
        },
        width: 150
      },
      {
        Header: "Amount",
        id: "amt",
        accessor: (c: any) => {
          return c.amt.toFixed(2)
        },
        Cell: ({ original }: any) => {
          return original.amt === undefined ? (
            "N/A"
          ) : (
            <div>{original.amt.toFixed(2)}</div>
          )
        }
      },
      {
        Header: "Tax",
        id: "tax",
        accessor: (c: any) => {
          return c.tax.toFixed(2)
        },
        Cell: ({ original }: any) => {
          return original.tax === undefined ? (
            "N/A"
          ) : (
            <div>{original.tax.toFixed(2)}</div>
          )
        }
      }
    ]
  }
]

export default TableHeaderColumns
