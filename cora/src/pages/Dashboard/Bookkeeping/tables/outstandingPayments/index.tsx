import React from "react"
import useSWR from "swr"
import Fade from "@material-ui/core/Fade"

/** Service */
import BookkeepingAPI from "services/Dashboard/Bookkeeping"

/** Components */
import Table from "components/Table"

/** Context */
import { compare } from "utils/helpers"
import tableHeaders from "./table/tableHeaders"

/** Utils */

const TablePayment: React.FC = () => {
  const fetchOutstandingPayments = async () =>
    BookkeepingAPI.getPayments({
      "payment_status": "1,2,3,4"
    })

  const { data, isValidating } = useSWR(
    "fetchOutstandingPayments",
    fetchOutstandingPayments
  )
  const itemsList = data?.data?._data
  const tableItems = itemsList && itemsList.sort(compare)

  return (
    <Fade in={!isValidating} timeout={1000}>
      <div>
        <Table data={tableItems} columns={tableHeaders} size={10} isFilterable />
      </div>
    </Fade>
  )
}

export default TablePayment
