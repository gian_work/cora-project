import React from "react"
import ContextMenu from "components/ContextMenu"

import ContextBookKeeping from "../components/ContextMenu"

const TableHeaderColumns = (setopenDetails: Function, openDetails: boolean) => [
  {
    fixed: "left",
    disableFilters: true,
    columns: [
      {
        width: 70,
        Cell: () => {
          return (
            <ContextMenu setopenDetails={setopenDetails} openDetails={openDetails}>
              <ContextBookKeeping />
            </ContextMenu>
          )
        },
        filterable: false
      }
    ]
  },
  {
    columns: [
      {
        Header: "Receipt No.",
        id: "receiptNumber",
        accessor: "receiptNumber",
        width: 200
      },
      {
        Header: "Block/Unit No.",
        id: "blockUnitNumber",
        accessor: "blockUnitNumber",
        width: 150
      },
      {
        Header: "Name",
        id: "name",
        accessor: "name",
        width: 200
      },
      {
        Header: "Type",
        id: "type",
        accessor: "type",
        width: 150
      },
      {
        Header: "Category",
        id: "category",
        accessor: "category",
        width: 150
      },
      {
        Header: "Payment Description",
        id: "paymentDescription",
        accessor: "paymentDescription",
        width: 250
      },
      {
        Header: "Payment Date",
        id: "paymentDate",
        accessor: "paymentDate",
        width: 150
      },
      {
        Header: "Amount",
        id: "amount",
        accessor: "amount",
        width: 150
      },
      {
        Header: "Tax",
        id: "tax",
        accessor: "tax",
        width: 150
      },
      {
        Header: "Collected By",
        id: "collectedBy",
        accessor: "collectedBy",
        width: 150
      },
      {
        Header: "Cash Amount",
        id: "amountCash",
        accessor: "amountCash",
        width: 150
      },
      {
        Header: "Amount (I-Banking)",
        id: "amountIbanking",
        accessor: "amountIbanking",
        width: 150
      },
      {
        Header: "Cheque Amount",
        id: "amountCheque",
        accessor: "amountCheque",
        width: 150
      },
      {
        Header: "Cheque Number",
        id: "chequeNo",
        accessor: "chequeNo",
        width: 150
      },
      {
        Header: "Bank",
        id: "bank",
        accessor: "bank",
        width: 150
      },
      {
        Header: "Deposit Status",
        id: "depositStatus",
        accessor: "depositStatus",
        width: 150
      },
      {
        Header: "Remarks",
        id: "remarks",
        accessor: "remarks",
        width: 150
      },
      {
        Header: "Date/Time",
        id: "date",
        accessor: "date",
        width: 150
      }
    ]
  }
]

export default TableHeaderColumns
