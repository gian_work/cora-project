import Deposits from "../../../../components/Icons/Deposits"
import ChequePayment from "../../../../components/Icons/ChequePayment"
import Banking from "../../../../components/Icons/Banking"
import Pending from "../../../../components/Icons/Pending"
import Payments from "../../../../components/Icons/Payments"
import Paid from "../../../../components/Icons/Paid"
import Cash from "../../../../components/Icons/Cash"

export const Menu = [
  {
    label: "Deposits",
    icon: Deposits,
    value: "deposit",
    color: "#8392A7"
  },
  {
    label: "Cheque Payment",
    icon: ChequePayment,
    value: "amount cheque",
    color: "#9EB7FF"
  },
  {
    label: "I-Banking",
    icon: Banking,
    value: "ibanking",
    color: "#006EFF"
  },
  {
    label: "Pending",
    icon: Pending,
    value: "pending",
    color: "#EAC91E"
  },
  {
    label: "Payments",
    icon: Payments,
    value: "payment",
    color: "#50D757"
  },
  {
    label: "Paid",
    icon: Paid,
    value: "paid",
    color: "#18A20C"
  },
  {
    label: "Cash",
    icon: Cash,
    value: "cash",
    color: "#21B6BF"
  }
]

export default Menu
