import React from "react"
import Box from "@material-ui/core/Box"
import Radio from "@material-ui/core/Radio"
import RadioGroup from "@material-ui/core/RadioGroup"
import FormControl from "@material-ui/core/FormControl"
import FormControlLabel from "@material-ui/core/FormControlLabel"
import { Formik } from "formik"

/** Components */
import FormWrapper from "components/Forms/FormWrapper"
import FormInput from "components/Forms/FormInput"
import Label from "components/Forms/Label"
import RefButton from "components/Forms/RefButton"

/** Config */
import validation from "config/Dashboard/Bookkeeping/validation"

/** Context */
import withContext from "context/Dashboard/Bookkeeping/forms/closeRefund/withContext"
import { CtxType } from "context/Dashboard/Bookkeeping/forms/closeRefund/Context"

/** Utils */
import { isFromAvailableBalance } from "utils/helpers"

/** Styles */
import { refSubmit } from "context/Dashboard/Bookkeeping/forms/closeRefund/View"
import styles from "./styles"

/** Interface */
interface ModePaymentProps extends CtxType {
  ref: any
}

const Details: React.FC<ModePaymentProps> = ({
  paymentDetails,
  setPaymentDetails
}) => {
  const { formControl } = styles()
  const pd = paymentDetails
  const { remarks, amt, refundTo, flags } = paymentDetails
  const fromBalance = +isFromAvailableBalance(flags) === 1
  const FormTitle = fromBalance ? "Closing Remarks" : "Refund Details"

  /** Methods */
  const handleFormChange = (
    setFieldValue: any,
    key: string,
    e: React.ChangeEvent<HTMLInputElement>
  ) => {
    return [
      setFieldValue(key, e?.target?.value),
      setPaymentDetails &&
        setPaymentDetails({
          ...paymentDetails,
          [key]: e?.target?.value
        })
    ]
  }

  return (
    <Box>
      <Formik
        initialValues={{
          description: pd?.description,
          remarks: pd?.remarks,
          paymentType: pd?.paymentType,
          category: pd?.category,
          amt: pd?.amt,
          tax: pd?.tax,
          refundTo: ""
        }}
        onSubmit={(values: any) => {
          return values
        }}
        validationSchema={validation.refundPayment}
      >
        {(formProps: any) => {
          const {
            touched,
            errors,
            handleBlur,
            handleSubmit,
            setFieldValue
          } = formProps
          return (
            <form>
              <Box display="flex" flexDirection="column" flex="1">
                <Box>
                  <FormWrapper title={FormTitle} width="40%">
                    <Box width="40%" margin="auto">
                      {!fromBalance && (
                        <>
                          <Box marginBottom="30px">
                            <FormInput
                              type="number"
                              name="amt"
                              label="Amount"
                              placeholder="$ 00.00"
                              value={amt}
                              onBlur={(e: Event) => handleBlur(e)}
                              error={touched.amt && Boolean(errors.amt)}
                              helperText={errors.amt && touched.amt && errors.amt}
                              handleOnChange={(
                                e: React.ChangeEvent<HTMLInputElement>
                              ) => handleFormChange(setFieldValue, "amt", e)}
                            />
                          </Box>

                          <Box marginBottom="30px">
                            <Label label="Refund To" />
                            <FormControl className={formControl}>
                              <RadioGroup
                                aria-label="refundTo"
                                name="refundTo"
                                value={refundTo}
                                onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                                  handleFormChange(setFieldValue, "refundTo", e)}
                                style={{ flexDirection: "row" }}
                              >
                                <FormControlLabel
                                  value="1"
                                  control={<Radio color="primary" />}
                                  label="Resident"
                                  labelPlacement="end"
                                />
                                <FormControlLabel
                                  value="2"
                                  control={<Radio color="primary" />}
                                  label="Available Balance"
                                  labelPlacement="end"
                                />
                              </RadioGroup>
                            </FormControl>
                          </Box>
                        </>
                      )}
                      <Box marginBottom="30px">
                        <FormInput
                          name="remarks"
                          label={fromBalance ? "" : "Remarks"}
                          placeholder="Remarks"
                          value={remarks}
                          multiline
                          rows={2}
                          handleOnChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                            handleFormChange(setFieldValue, "remarks", e)}
                          onBlur={(e: Event) => handleBlur(e)}
                          error={
                            touched.remarks &&
                            paymentDetails?.remarks === "" &&
                            Boolean(errors.remarks)
                          }
                          helperText={
                            errors.remarks &&
                            touched.remarks &&
                            paymentDetails?.remarks === "" &&
                            errors.remarks
                          }
                        />
                      </Box>

                      <RefButton action={handleSubmit} refValue={refSubmit} />
                    </Box>
                  </FormWrapper>
                </Box>
              </Box>
            </form>
          )
        }}
      </Formik>
    </Box>
  )
}
export default withContext(Details)
