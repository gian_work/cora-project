import React from "react"
import Box from "@material-ui/core/Box"
// import {Formik} from "formik"
// import * as Yup from "yup"

/** Components */
import FormWrapper from "components/Forms/FormWrapper"
import Label from "components/Forms/Label"

/** Context */
import withContext from "context/Dashboard/Bookkeeping/forms/closeRefund/withContext"
import { CtxType } from "context/Dashboard/Bookkeeping/forms/closeRefund/Context"

/** Config */
import { paymentType as paymentTypeConfig } from "config/Dashboard/Bookkeeping/index"

/** Styles */
// import {refSubmit} from "context/Dashboard/Bookkeeping/forms/closeRefund/View"
import styles from "./styles"

const PayerInfo: React.FC<CtxType> = ({ paymentDetails }) => {
  const { twoCol } = styles()
  const { unitName, payeeUID, description, paymentType, category } = paymentDetails

  /** Methods */
  // const handleFormChange = (
  //   setFieldValue: any,
  //   key: string,
  //   value: string | boolean,
  // ) => {
  //   return [
  //     setFieldValue(key, value),
  //     setPaymentDetails &&
  //       setPaymentDetails({
  //         ...paymentDetails,
  //         [key]: value,
  //       }),
  //   ]
  // }

  return (
    <Box>
      <Box>
        <FormWrapper title="Payer’s Information" width="60%">
          <Box className={twoCol} width="60%" paddingBottom="30px" margin="auto">
            <Box>
              <Label label="block/unit no." />
              <Box>{unitName}</Box>
            </Box>
            <Box>
              <Label label="name" />
              <Box>{payeeUID}</Box>
            </Box>
          </Box>
        </FormWrapper>
      </Box>

      <Box width="60%" margin="auto">
        <FormWrapper title="Payment Details">
          <Box className={twoCol} paddingBottom="30px">
            <Box>
              <Label label="payment category" />
              <Box>{category}</Box>
            </Box>

            <Box>
              <Label label="payment type" />
              <Box>{paymentTypeConfig[paymentType]}</Box>
            </Box>
          </Box>
        </FormWrapper>
        <Box>
          <Label label="payment description" />
          <Box>{description}</Box>
        </Box>
      </Box>

      {/* <Formik
        initialValues={{
          unitUID: "",
          payeeUID: "",
        }}
        onSubmit={(values: any) => {
          return values
        }}
        validationSchema={Yup.object().shape({
          unitUID: Yup.string().required("Required"),
          payeeUID: Yup.string().required("Required"),
        })}
      >
        {(formProps: any) => {
          const {
            touched,
            errors,
            handleBlur,
            handleSubmit,
            setFieldValue,
          } = formProps
          return (
            <div
              onClick={() => handleSubmit()}
              ref={refSubmit}
              role="button"
              tabIndex={-1}
            >
              &nbsp;
            </div>
          )
        }}
      </Formik> */}
    </Box>
  )
}
export default withContext(PayerInfo)
