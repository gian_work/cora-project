import { makeStyles } from "@material-ui/core/styles"

const styles = makeStyles(() => ({
  twoCol: {
    display: "flex",
    alignItems: "flex-start",
    "& > div": {
      width: "40%"
    }
  },
  threeCol: {
    display: "flex",
    alignItems: "flex-start",
    "& > div": {
      width: "calc(100% / 3)"
    }
  }
}))

export default styles
