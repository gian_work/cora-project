import { makeStyles } from "@material-ui/core/styles"

const styles = makeStyles(() => ({
  twoCol: {
    display: "flex",
    alignItems: "flex-start",
    "& > div": {
      width: "calc(100%/3)"
    }
  },
  twoColAttachment: {
    display: "flex",
    alignItems: "flex-start",
    justifyContent: "space-between",
    "& > div": {
      width: "49%"
    }
  }
}))

export default styles
