import React from "react"
import Box from "@material-ui/core/Box"
import useSWR from "swr"
import Cookie from "js-cookie"

/** Component */

/** API */
import BookkeepingAPI from "services/Dashboard/Bookkeeping"

/** Context */
import dateHelper from "utils/date"
import { toNumeral } from "utils/helpers"

/** Styles */
import styles from "../styles"

/** Icons */
import { IconPayments } from "../icons"

interface TotalPaymentsProps {
  dates: any
}

const TotalPayments: React.FC<TotalPaymentsProps> = ({ dates }) => {
  const { section, type, amount, iconContainer } = styles()

  const fetchTotalPaymentsPaymentStats = async () =>
    BookkeepingAPI.getTotalCollections({
      "condo_uid": Cookie.get("condoUID"),
      "start_date": `${dateHelper.toUnix(dates.first)}`,
      "end_date": `${dateHelper.toUnix(dates.last)}`,
      "payment_type": "1"
    })

  const { data } = useSWR(
    "fetchTotalPaymentsPaymentStats",
    fetchTotalPaymentsPaymentStats,
    {
      revalidateOnFocus: true
    }
  )

  const { amt, currency } = data?.data?._data !== undefined && data?.data?._data

  return (
    <Box className={section}>
      <Box display="flex" alignItems="center">
        <Box className={iconContainer} bgcolor="#0E61A2">
          {IconPayments}
        </Box>
        <Box className={type}>Payments</Box>
      </Box>
      <Box>
        <Box className={amount}>
          <small>{currency}</small> 
          {' '}
          {toNumeral(amt)}
        </Box>
      </Box>
    </Box>
  )
}
export default TotalPayments
