import React from "react"
import Box from "@material-ui/core/Box"
import useSWR from "swr"
import Cookie from "js-cookie"

/** Component */

/** API */
import BookkeepingAPI from "services/Dashboard/Bookkeeping"

/** Context */
import dateHelper from "utils/date"
import { toNumeral } from "utils/helpers"

/** Styles */
import styles from "../../styles"

/** Icons */
import { IconOthers } from "../../icons"

interface TotalPaymentsOthersProps {
  dates: any
}

const TotalPaymentsOthers: React.FC<TotalPaymentsOthersProps> = ({ dates }) => {
  const { section, iconContainer, type, amount } = styles()

  const fetchOthersStats = async () =>
    BookkeepingAPI.getTotalCollections({
      "condo_uid": Cookie.get("condoUID"),
      "start_date": `${dateHelper.toUnix(dates.first)}`,
      "end_date": `${dateHelper.toUnix(dates.last)}`,
      "payment_type": "1",
      "category": "Others"
    })

  const { data } = useSWR("fetchOthersStats", fetchOthersStats, {
    revalidateOnFocus: true
  })

  const { amt, currency } = data?.data?._data !== undefined && data?.data?._data

  return (
    <Box className={section} justifyContent="space-between" paddingBottom="15px">
      <Box display="flex" alignItems="center">
        <Box className={iconContainer} bgcolor="#0E61A2">
          {IconOthers}
        </Box>
        <Box className={type}>Others</Box>
      </Box>
      <Box>
        <Box className={amount}>
          <small>{currency}</small>
          {toNumeral(amt)}
        </Box>
      </Box>
    </Box>
  )
}
export default TotalPaymentsOthers
