import React from "react"
import Box from "@material-ui/core/Box"
import useSWR from "swr"
import Cookie from "js-cookie"

/** Component */

/** API */
import BookkeepingAPI from "services/Dashboard/Bookkeeping"

/** Context */
import dateHelper from "utils/date"
import { toNumeral } from "utils/helpers"

/** Styles */
import styles from "../../styles"

/** Icons */
import { IconFacilities } from "../../icons"

interface TotalPaymentsFacilitiesProps {
  dates: any
}

const TotalPaymentsFacilities: React.FC<TotalPaymentsFacilitiesProps> = ({
  dates
}) => {
  const { section, iconContainer, type, amount } = styles()

  const fetchFacilitiesStats = async () =>
    BookkeepingAPI.getTotalCollections({
      "condo_uid": Cookie.get("condoUID"),
      "start_date": `${dateHelper.toUnix(dates.first)}`,
      "end_date": `${dateHelper.toUnix(dates.last)}`,
      "payment_type": "1",
      "category": "Facilities"
    })

  const { data } = useSWR("fetchFacilitiesStats", fetchFacilitiesStats, {
    revalidateOnFocus: true
  })

  const { amt, currency } = data?.data?._data !== undefined && data?.data?._data

  return (
    <Box className={section} justifyContent="space-between" paddingBottom="15px">
      <Box display="flex" alignItems="center">
        <Box className={iconContainer} bgcolor="#007B83">
          {IconFacilities}
        </Box>
        <Box className={type}>Facilities</Box>
      </Box>
      <Box>
        <Box className={amount}>
          <small>{currency}</small>
          {toNumeral(amt)}
        </Box>
      </Box>
    </Box>
  )
}
export default TotalPaymentsFacilities
