import React from "react"
import Box from "@material-ui/core/Box"
import { Formik } from "formik"

/** Components */
import FormWrapper from "components/Forms/FormWrapper"
import Label from "components/Forms/Label"
import Attachment from "components/Attachment"
import FormInput from "components/Forms/FormInput"

/** Config */
import { paymentType as paymentTypeOptions } from "config/Dashboard/Bookkeeping"
import validation from "config/Dashboard/Bookkeeping/validation"

/** Utils */
import { toFixed } from "utils/helpers"

/** Context */
import withContext from "context/Dashboard/Bookkeeping/forms/rejectPayment/withContext"
import { CtxType } from "context/Dashboard/Bookkeeping/forms/rejectPayment/Context"

/** Ref */
import { refSubmit } from "context/Dashboard/Bookkeeping/forms/cancelPayment/View"

/** Styles */
import styles from "./styles"

const PaymentSummary: React.FC<CtxType> = ({
  paymentDetails,
  setPaymentDetails
}) => {
  const { twoCol } = styles()
  const {
    unitUID,
    payeeName,
    description,
    paymentType,
    category,
    amt,
    tax
  } = paymentDetails

  /** Methods */
  const handleFormChange = (
    setFieldValue: any,
    key: string,
    e: React.ChangeEvent<HTMLInputElement>
  ) => {
    return [
      setFieldValue(key, e?.target?.value),
      setPaymentDetails &&
        setPaymentDetails({
          ...paymentDetails,
          [key]: e?.target?.value
        })
    ]
  }

  return (
    <Box>
      <FormWrapper hasBottomBorder title="Payer’s Information" width="60%">
        <Box width="60%" margin="auto">
          <Box className={twoCol}>
            <Box>
              <Label label="block/unit no." />
              <Box>{unitUID}</Box>
            </Box>
            <Box>
              <Label label="name" />
              <Box>{payeeName}</Box>
            </Box>
          </Box>
        </Box>
      </FormWrapper>
      <FormWrapper title="Payment Details" width="60%">
        <Box width="60%" margin="auto">
          <Box className={twoCol} marginBottom="30px">
            <Box>
              <Label label="Payment Description" />
              <Box>{description}</Box>
            </Box>
          </Box>
          <Box className={twoCol} marginBottom="30px">
            <Box>
              <Label label="payment category" />
              <Box>{category}</Box>
            </Box>
            <Box>
              <Label label="payment type" />
              <Box>{paymentTypeOptions[paymentType]}</Box>
            </Box>
          </Box>
          <Box className={twoCol} marginBottom="30px">
            <Box>
              <Label label="amount" />
              <Box>{toFixed(amt)}</Box>
            </Box>
            <Box>
              <Label label="TAX" />
              <Box>{toFixed(tax)}</Box>
            </Box>
          </Box>

          <Box className={twoCol} marginBottom="30px">
            <Box>
              <Label label="mode of payment" />
              <Box>{category}</Box>
            </Box>
            <Box>
              <Label label="Bank Name" />
              <Box>{paymentTypeOptions[paymentType]}</Box>
            </Box>
            <Box>
              <Label label="cheque no." />
              <Box>{paymentTypeOptions[paymentType]}</Box>
            </Box>
          </Box>

          {paymentDetails?.photos !== null && (
            <Box marginBottom="30px">
              <Label label="Attachment" />
              <Box>
                {paymentDetails?.photos.map((photo: Record<string, any>) => (
                  <Attachment
                    key={photo?.file_name}
                    fileName={photo?.file_name}
                    url={photo?.url}
                    encoding={photo?.encoding}
                  />
                ))}
              </Box>
            </Box>
          )}

          <Box marginBottom="60px">
            <Formik
              initialValues={{
                remarks: "",
                ePaymentAgent: "",
                paymentReference: ""
              }}
              onSubmit={(values: any) => {
                return values
              }}
              validationSchema={validation.cancelPayment}
            >
              {(formProps: any) => {
                const {
                  touched,
                  errors,
                  handleBlur,
                  handleSubmit,
                  setFieldValue
                } = formProps
                return (
                  <form>
                    <Box>
                      <FormInput
                        name="remarks"
                        label="remarks"
                        placeholder="Remarks"
                        value={paymentDetails?.remarks}
                        multiline
                        rows={2}
                        handleOnChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                          handleFormChange(setFieldValue, "remarks", e)}
                        onBlur={(e: Event) => handleBlur(e)}
                        error={
                          touched.remarks &&
                          paymentDetails?.remarks === "" &&
                          Boolean(errors.remarks)
                        }
                        helperText={
                          errors.remarks &&
                          touched.remarks &&
                          paymentDetails?.remarks === "" &&
                          errors.remarks
                        }
                      />
                      <div
                        onClick={() => handleSubmit()}
                        ref={refSubmit}
                        role="button"
                        tabIndex={-1}
                        style={{ outline: "none" }}
                      >
                        &nbsp;
                      </div>
                    </Box>
                  </form>
                )
              }}
            </Formik>
          </Box>
        </Box>
      </FormWrapper>
    </Box>
  )
}
export default withContext(PaymentSummary)
