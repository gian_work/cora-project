import React from "react"

import DetailsPaymentProvider, {
  DetailsProviderProps
} from "context/Dashboard/Bookkeeping/categories/details/Provider"
import DetailsPaymentView from "context/Dashboard/Bookkeeping/categories/details/View"

const DetailsPayment: React.FC<DetailsProviderProps> = ({
  showPayment,
  setShowPayment,
  data
}): JSX.Element => (
  <DetailsPaymentProvider
    showPayment={showPayment}
    setShowPayment={setShowPayment}
    data={data}
  >
    <DetailsPaymentView />
  </DetailsPaymentProvider>
)

export default DetailsPayment
