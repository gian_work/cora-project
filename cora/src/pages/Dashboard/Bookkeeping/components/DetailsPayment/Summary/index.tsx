import React from "react"
import Box from "@material-ui/core/Box"

/** Components */
import FormWrapper from "components/Forms/FormWrapper"
import Label from "components/Forms/Label"

/** Config */
import { paymentType as paymentTypeOptions } from "config/Dashboard/Bookkeeping"

/** Utils */
import { toFixed } from "utils/helpers"

/** Styles */

/** Context */
import withContext from "context/Dashboard/Bookkeeping/categories/details/withContext"
import { CtxType } from "context/Dashboard/Bookkeeping/categories/details/Context"
import styles from "./styles"

const DetailsSummary: React.FC<CtxType> = ({ data }) => {
  const { twoCol } = styles()
  // console.log(data)

  // const {
  //   currency,
  //   unit,
  //   payee_name,
  //   description,
  //   category,
  //   payment_type,
  //   amt,
  //   tax,
  // } = data

  return (
    <Box>
      <FormWrapper title="Payer’s Information" width="60%">
        <Box width="60%" margin="auto">
          <Box className={twoCol}>
            <Box>
              <Label label="block/unit no." />
              <Box>{data?.unit?.short_name}</Box>
            </Box>
            <Box>
              <Label label="name" />
              <Box>{data?.payee_name}</Box>
            </Box>
          </Box>
        </Box>
      </FormWrapper>
      <FormWrapper title="Payment Details" width="60%">
        <Box width="60%" margin="auto">
          <Box className={twoCol} marginBottom="30px">
            <Box>
              <Label label="Payment Description" />
              <Box>{data?.description}</Box>
            </Box>
          </Box>
          <Box className={twoCol} marginBottom="30px">
            <Box>
              <Label label="payment category" />
              <Box>{data?.category}</Box>
            </Box>
            <Box>
              <Label label="payment type" />
              <Box>{paymentTypeOptions[data?.payment_type]}</Box>
            </Box>
          </Box>
          <Box className={twoCol} marginBottom="30px">
            <Box>
              <Label label="amount" />
              <Box>{toFixed(data?.amt, data?.currency)}</Box>
            </Box>
            <Box>
              <Label label="TAX" />
              <Box>{toFixed(data?.tax, data?.currency)}</Box>
            </Box>
          </Box>
        </Box>
      </FormWrapper>
    </Box>
  )
}
export default withContext(DetailsSummary)
