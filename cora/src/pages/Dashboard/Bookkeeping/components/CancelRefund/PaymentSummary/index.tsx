import React from "react"
import Box from "@material-ui/core/Box"
import { Formik } from "formik"

/** Components */
import FormWrapper from "components/Forms/FormWrapper"
import Label from "components/Forms/Label"
import FormInput from "components/Forms/FormInput"
import RefButton from "components/Forms/RefButton"

/** Config */
import { paymentType as paymentTypeOptions } from "config/Dashboard/Bookkeeping"
import validation from "config/Dashboard/Bookkeeping/validation"

/** Utils */
import { toFixed } from "utils/helpers"

/** Styles */

/** Context */
import withContext from "context/Dashboard/Bookkeeping/forms/cancelRefund/withContext"
import { CtxType } from "context/Dashboard/Bookkeeping/forms/cancelRefund/Context"
import { refSubmit } from "context/Dashboard/Bookkeeping/forms/closePayment/View"
import styles from "./styles"

const PaymentSummary: React.FC<CtxType> = ({
  setPaymentDetails,
  paymentDetails,
  data
}) => {
  const { twoCol } = styles()
  const {
    unitUID,
    payeeName,
    description,
    paymentType,
    category,
    amt,
    tax
  } = paymentDetails

  /** Methods */
  const handleFormChange = (
    setFieldValue: any,
    key: string,
    e: React.ChangeEvent<HTMLInputElement>
  ) => {
    return [
      setFieldValue(key, e?.target?.value),
      setPaymentDetails &&
        setPaymentDetails({
          ...paymentDetails,
          [key]: e?.target?.value
        })
    ]
  }

  return (
    <Box>
      <FormWrapper title="Payer’s Information" width="60%">
        <Box width="60%" margin="auto">
          <Box className={twoCol}>
            <Box>
              <Label label="block/unit no." />
              <Box>{unitUID}</Box>
            </Box>
            <Box>
              <Label label="name" />
              <Box>{payeeName}</Box>
            </Box>
          </Box>
        </Box>
      </FormWrapper>
      <FormWrapper title="Payment Details" width="60%">
        <Box width="60%" margin="auto">
          <Box className={twoCol} marginBottom="30px">
            <Box>
              <Label label="Payment Description" />
              <Box>{description}</Box>
            </Box>
          </Box>
          <Box className={twoCol} marginBottom="30px">
            <Box>
              <Label label="payment category" />
              <Box>{category}</Box>
            </Box>
            <Box>
              <Label label="payment type" />
              <Box>{paymentTypeOptions[paymentType]}</Box>
            </Box>
          </Box>
          <Box className={twoCol} marginBottom="30px">
            <Box>
              <Label label="amount" />
              <Box>{toFixed(amt, data?.currency)}</Box>
            </Box>
            <Box>
              <Label label="TAX" />
              <Box>{toFixed(tax, data?.currency)}</Box>
            </Box>
          </Box>

          <Box marginBottom="30px">
            <Formik
              initialValues={{
                remarks: "",
                ePaymentAgent: "",
                paymentReference: ""
              }}
              onSubmit={(values: any) => {
                return values
              }}
              validationSchema={validation.cancelRefund}
            >
              {(formProps: any) => {
                const {
                  touched,
                  errors,
                  handleBlur,
                  handleSubmit,
                  setFieldValue
                } = formProps
                return (
                  <form>
                    <FormInput
                      name="remarks"
                      label="remarks"
                      placeholder="Remarks"
                      value={paymentDetails?.remarks}
                      multiline
                      rows={2}
                      handleOnChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                        handleFormChange(setFieldValue, "remarks", e)}
                      onBlur={(e: Event) => handleBlur(e)}
                      error={
                        touched.remarks &&
                        paymentDetails?.remarks === "" &&
                        Boolean(errors.remarks)
                      }
                      helperText={
                        errors.remarks &&
                        touched.remarks &&
                        paymentDetails?.remarks === "" &&
                        errors.remarks
                      }
                    />
                    <RefButton action={handleSubmit} refValue={refSubmit} />
                  </form>
                )
              }}
            </Formik>
          </Box>
        </Box>
      </FormWrapper>
    </Box>
  )
}
export default withContext(PaymentSummary)
