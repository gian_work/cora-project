import React, { ReactInstance } from "react"
import Box from "@material-ui/core/Box"
// import Button from "@material-ui/core/Button"
import ReactTable from "react-table"
// import ReactToPrint from "react-to-print"

/** Components */
import Label from "components/Forms/Label"

/** Styles */

/** Context */
import withContext from "context/Dashboard/Bookkeeping/forms/cancelRefund/withContext"
import { CtxType } from "context/Dashboard/Bookkeeping/forms/cancelRefund/Context"

/** Config */
import { paymentType as paymentTypeOptions } from "config/Dashboard/Bookkeeping"

/** Utils */
import dateHelper from "utils/date"
import { toFixed } from "utils/helpers"

/** Refs */
// import {refPrintBtn} from "context/Dashboard/Bookkeeping/forms/cancelRefund/View"
import styles from "./styles"

/* eslint-disable react/prefer-stateless-function */
class EmptyComponent extends React.Component {
  render() {
    return null
  }
}

const Acknowledgement: React.FC<CtxType> = ({ paymentDetails }) => {
  const { value, table } = styles()
  const ele: ReactInstance = new EmptyComponent({})
  const refReport = React.useRef<any>(ele)

  const {
    receiptNo,
    payeeName,
    paymentDate,
    unitUID,
    remarks,
    paymentType,
    category,
    description,
    amt,
    tax
  } = paymentDetails

  return (
    <Box padding="0 10%">
      <div ref={refReport}>
        <Box display="flex" justifyContent="space-between" padding="30px">
          <Box>
            <Box marginBottom="20px">
              <Label label="Receipt no." />
              <Box className={value}>{receiptNo}</Box>
            </Box>
            <Box>
              <Label label="Name" />
              <Box className={value}>{payeeName}</Box>
            </Box>
          </Box>
          <Box>
            <Box marginBottom="20px">
              <Label label="date" />
              <Box className={value}>{dateHelper.fromUnix(paymentDate)}</Box>
            </Box>
            <Box>
              <Label label="condo" />
              <Box className={value}>The Alps</Box>
            </Box>
          </Box>
          <Box>
            <Box marginBottom="20px">
              <Label label="time" />
              <Box className={value}>{dateHelper.fromUnixTime(paymentDate)}</Box>
            </Box>
            <Box>
              <Label label="block/unit no." />
              <Box className={value}>{unitUID}</Box>
            </Box>
          </Box>
        </Box>

        <Box className={table}>
          <ReactTable
            showPagination={false}
            data={[
              {
                "type": `${paymentTypeOptions[paymentType]}`,
                "category": `${category}`,
                "paymentdescription": `${description}`,
                "amount": `${toFixed(amt)}`,
                "tax": `${toFixed(tax)}`
              }
            ]}
            columns={[
              {
                Header: "",
                columns: [
                  {
                    Header: "Type",
                    id: "type",
                    accessor: "type"
                  },
                  {
                    Header: "Category",
                    id: "category",
                    accessor: "category"
                  },
                  {
                    Header: "Payment Description",
                    id: "paymentdescription",
                    accessor: "paymentdescription",
                    width: 250
                  },
                  {
                    Header: "Amount",
                    id: "amount",
                    accessor: "amount"
                  },
                  {
                    Header: "Tax",
                    id: "tax",
                    accessor: "tax"
                  }
                ]
              }
            ]}
            defaultPageSize={1}
          />
        </Box>
        <Box padding="30px">
          <Label label="Remarks" />
          <Box className={value}>{remarks}</Box>
        </Box>

        <Box padding="30px" width="300px">
          <Label label="received by:" />
          <Box borderTop="1px solid #F2F2F2" marginTop="50px" textAlign="center">
            <Box marginTop="20px">Signature over printed name</Box>
          </Box>
        </Box>

        {/* <Box>
          <ReactToPrint
            trigger={() => <Button onClick={() => null} ref={refPrintBtn} />}
            content={() => (refReport.current ? refReport.current : ele)}
          />
        </Box> */}
      </div>
    </Box>
  )
}
export default withContext(Acknowledgement)
