import { makeStyles } from "@material-ui/core/styles"

const styles = makeStyles(() => ({
  twoCol: {
    display: "flex",
    alignItems: "flex-start",
    "& > div": {
      width: "40%"
    }
  }
}))

export default styles
