import React from "react"
import Box from "@material-ui/core/Box"

/** Components */
import FormWrapper from "components/Forms/FormWrapper"
import Label from "components/Forms/Label"

/** Config */
import { paymentType as paymentTypeOptions } from "config/Dashboard/Bookkeeping"

/** Utils */
import { toFixed } from "utils/helpers"

/** Styles */

/** Context */
import withContext from "context/Dashboard/Bookkeeping/forms/closePayment/withContext"
import { CtxType } from "context/Dashboard/Bookkeeping/forms/closePayment/Context"
import styles from "./styles"

const PaymentSummary: React.FC<CtxType> = ({ paymentDetails, data }) => {
  const { twoCol } = styles()
  const {
    unitUID,
    payeeName,
    description,
    paymentType,
    category,
    amt,
    tax
  } = paymentDetails

  return (
    <Box>
      <FormWrapper title="Payer’s Information" width="60%">
        <Box width="60%" margin="auto">
          <Box className={twoCol}>
            <Box>
              <Label label="block/unit no." />
              <Box>{unitUID}</Box>
            </Box>
            <Box>
              <Label label="name" />
              <Box>{payeeName}</Box>
            </Box>
          </Box>
        </Box>
      </FormWrapper>
      <FormWrapper title="Payment Details" width="60%">
        <Box width="60%" margin="auto">
          <Box className={twoCol} marginBottom="30px">
            <Box>
              <Label label="Payment Description" />
              <Box>{description}</Box>
            </Box>
          </Box>
          <Box className={twoCol} marginBottom="30px">
            <Box>
              <Label label="payment category" />
              <Box>{category}</Box>
            </Box>
            <Box>
              <Label label="payment type" />
              <Box>{paymentTypeOptions[paymentType]}</Box>
            </Box>
          </Box>
          <Box className={twoCol} marginBottom="30px">
            <Box>
              <Label label="amount" />
              <Box>{toFixed(amt, data?.currency)}</Box>
            </Box>
            <Box>
              <Label label="TAX" />
              <Box>{toFixed(tax, data?.currency)}</Box>
            </Box>
          </Box>
        </Box>
      </FormWrapper>
    </Box>
  )
}
export default withContext(PaymentSummary)
