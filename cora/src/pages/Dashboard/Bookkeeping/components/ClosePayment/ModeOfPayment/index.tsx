import React from "react"
import Box from "@material-ui/core/Box"
import FormControlLabel from "@material-ui/core/FormControlLabel"
import Radio from "@material-ui/core/Radio"
import RadioGroup from "@material-ui/core/RadioGroup"
import FormControl from "@material-ui/core/FormControl"
import { Formik } from "formik"

/** Components */
import FormWrapper from "components/Forms/FormWrapper"
import FormInput from "components/Forms/FormInput"
import Label from "components/Forms/Label"
import Attachment from "components/Attachment"

/** Config */
import validation from "config/Dashboard/Bookkeeping/validation"
import { paymentMethod } from "config/Dashboard/Bookkeeping"

/** Context */
import withContext from "context/Dashboard/Bookkeeping/forms/closePayment/withContext"
import { CtxType } from "context/Dashboard/Bookkeeping/forms/closePayment/Context"

/** Ref */
import { refSubmit } from "context/Dashboard/Bookkeeping/forms/closePayment/View"

/** Styles */
import styles from "./styles"

/** Interface */
interface ModePaymentProps extends CtxType {
  ref: any
}

const ModePayment: React.FC<ModePaymentProps> = ({
  paymentDetails,
  setPaymentDetails
}) => {
  const { formControl, twoCol } = styles()

  const ItemStatus = paymentDetails?.paymentStatus
  const paymentType = paymentDetails?.paymentType
  const FormTitle = ItemStatus === 2 ? "Payment Details" : "Mode of Payment"

  /** Validation */
  const { closePayment, closePaymentAgent } = validation
  const activeValidation = (mode: number) => {
    if (mode === 1) {
      return closePayment
    }
    return closePaymentAgent
  }

  /** Methods */
  const handleFormChange = (
    setFieldValue: any,
    key: string,
    e: React.ChangeEvent<HTMLInputElement>
  ) => {
    return [
      setFieldValue(key, e?.target?.value),
      setPaymentDetails &&
        setPaymentDetails({
          ...paymentDetails,
          [key]: e?.target?.value
        })
    ]
  }

  /** View */
  const ModeDetails = () => {
    if (ItemStatus !== 2 || paymentType === 3) {
      return null
    }

    return (
      <Box marginBottom="30px">
        <Label label="Mode of Payment" />
        <Box>{paymentMethod[+paymentDetails?.paymentMethod]}</Box>
      </Box>
    )
  }

  return (
    <Box>
      <Formik
        initialValues={{
          remarks: "",
          ePaymentAgent: "",
          paymentReference: ""
        }}
        onSubmit={(values: any) => {
          return values
        }}
        validationSchema={activeValidation(+paymentDetails?.paymentMethod)}
      >
        {(formProps: any) => {
          const {
            touched,
            errors,
            handleBlur,
            handleSubmit,
            setFieldValue
          } = formProps
          return (
            <form>
              <Box display="flex" flexDirection="column" flex="1">
                <Box>
                  <FormWrapper
                    title={paymentType === 3 ? "" : FormTitle}
                    width="60%"
                  >
                    <Box width="60%" margin="auto">
                      <ModeDetails />
                      {paymentDetails?.paymentStatus === 1 && paymentType !== 3 && (
                        <Box>
                          <Box marginBottom="30px">
                            <FormControl className={formControl}>
                              <RadioGroup
                                aria-label="mode of payment"
                                name="modePayment"
                                value={paymentDetails?.paymentMethod}
                                onChange={(e: any) =>
                                  handleFormChange(setFieldValue, "paymentMethod", e)}
                                style={{ flexDirection: "row" }}
                              >
                                <FormControlLabel
                                  value="1"
                                  control={<Radio color="primary" />}
                                  label="Cash"
                                  labelPlacement="end"
                                />
                                <FormControlLabel
                                  value="2"
                                  control={<Radio color="primary" />}
                                  label="Cheque"
                                  labelPlacement="end"
                                />
                                <FormControlLabel
                                  value="3"
                                  control={<Radio color="primary" />}
                                  label="E-transfer"
                                  labelPlacement="end"
                                />
                                <FormControlLabel
                                  value="4"
                                  control={<Radio color="primary" />}
                                  label="Available Balance"
                                  labelPlacement="end"
                                />
                              </RadioGroup>
                            </FormControl>
                          </Box>

                          <>
                            {paymentDetails?.paymentMethod === "2" && (
                              <Box className={twoCol} marginBottom="30px">
                                <Box>
                                  <FormInput
                                    name="ePaymentAgent"
                                    label="bank name"
                                    placeholder="Bank Name"
                                    value={paymentDetails?.ePaymentAgent}
                                    onBlur={(e: Event) => handleBlur(e)}
                                    error={
                                      touched.ePaymentAgent &&
                                      paymentDetails?.ePaymentAgent === "" &&
                                      Boolean(errors.ePaymentAgent)
                                    }
                                    helperText={
                                      errors.ePaymentAgent &&
                                      touched.ePaymentAgent &&
                                      paymentDetails?.ePaymentAgent === "" &&
                                      errors.ePaymentAgent
                                    }
                                    handleOnChange={(
                                      e: React.ChangeEvent<HTMLInputElement>
                                    ) =>
                                      handleFormChange(
                                        setFieldValue,
                                        "ePaymentAgent",
                                        e
                                      )}
                                  />
                                </Box>
                                <Box>
                                  <FormInput
                                    name="paymentReference"
                                    label="cheque no."
                                    placeholder="Cheque No."
                                    value={paymentDetails?.paymentReference}
                                    onBlur={(e: Event) => handleBlur(e)}
                                    error={
                                      errors.paymentReference &&
                                      touched.paymentReference &&
                                      paymentDetails?.paymentReference === "" &&
                                      Boolean(errors.paymentReference)
                                    }
                                    helperText={
                                      errors.paymentReference &&
                                      touched.paymentReference &&
                                      paymentDetails?.paymentReference === "" &&
                                      errors.paymentReference
                                    }
                                    handleOnChange={(
                                      e: React.ChangeEvent<HTMLInputElement>
                                    ) =>
                                      handleFormChange(
                                        setFieldValue,
                                        "paymentReference",
                                        e
                                      )}
                                  />
                                </Box>
                              </Box>
                            )}
                          </>

                          <>
                            {paymentDetails?.paymentMethod === "3" && (
                              <Box className={twoCol} marginBottom="30px">
                                <Box>
                                  <FormInput
                                    name="ePaymentAgent"
                                    label="Transfer Agent"
                                    placeholder="Transfer Agent"
                                    value={paymentDetails?.ePaymentAgent}
                                    onBlur={(e: Event) => handleBlur(e)}
                                    error={
                                      touched.ePaymentAgent &&
                                      Boolean(errors.ePaymentAgent)
                                    }
                                    helperText={
                                      errors.ePaymentAgent &&
                                      touched.ePaymentAgent &&
                                      errors.ePaymentAgent
                                    }
                                    handleOnChange={(
                                      e: React.ChangeEvent<HTMLInputElement>
                                    ) =>
                                      handleFormChange(
                                        setFieldValue,
                                        "ePaymentAgent",
                                        e
                                      )}
                                  />
                                </Box>
                                <Box>
                                  <FormInput
                                    name="paymentReference"
                                    label="Transfer ID"
                                    placeholder="Transfer ID"
                                    value={paymentDetails?.paymentReference}
                                    onBlur={(e: Event) => handleBlur(e)}
                                    error={
                                      touched.paymentReference &&
                                      Boolean(errors.paymentReference)
                                    }
                                    helperText={
                                      errors.paymentReference &&
                                      touched.paymentReference &&
                                      errors.paymentReference
                                    }
                                    handleOnChange={(
                                      e: React.ChangeEvent<HTMLInputElement>
                                    ) =>
                                      handleFormChange(
                                        setFieldValue,
                                        "paymentReference",
                                        e
                                      )}
                                  />
                                </Box>
                              </Box>
                            )}
                          </>
                        </Box>
                      )}

                      {paymentDetails?.photos !== null && (
                        <Box marginBottom="30px">
                          <Label label="Attachment" />
                          <Box>
                            {paymentDetails?.photos.map(
                              (photo: Record<string, any>) => (
                                <Attachment
                                  key={photo?.file_name}
                                  fileName={photo?.file_name}
                                  url={photo?.url}
                                  encoding={photo?.encoding}
                                />
                              )
                            )}
                          </Box>
                        </Box>
                      )}

                      <Box marginBottom="30px">
                        <FormInput
                          name="remarks"
                          label="remarks"
                          placeholder="Remarks"
                          value={paymentDetails?.remarks}
                          multiline
                          rows={2}
                          handleOnChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                            handleFormChange(setFieldValue, "remarks", e)}
                          onBlur={(e: Event) => handleBlur(e)}
                          error={
                            touched.remarks &&
                            paymentDetails?.remarks === "" &&
                            Boolean(errors.remarks)
                          }
                          helperText={
                            errors.remarks &&
                            touched.remarks &&
                            paymentDetails?.remarks === "" &&
                            errors.remarks
                          }
                        />
                      </Box>

                      <div
                        onClick={() => handleSubmit()}
                        ref={refSubmit}
                        role="button"
                        tabIndex={-1}
                        style={{ outline: "none" }}
                      >
                        &nbsp;
                      </div>
                    </Box>
                  </FormWrapper>
                </Box>
              </Box>
            </form>
          )
        }}
      </Formik>
    </Box>
  )
}
export default withContext(ModePayment)
