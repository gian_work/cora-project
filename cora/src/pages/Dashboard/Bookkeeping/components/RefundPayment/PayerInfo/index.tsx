import React from "react"
import Box from "@material-ui/core/Box"
import { Formik } from "formik"
import * as Yup from "yup"

/** Components */
import FormWrapper from "components/Forms/FormWrapper"
import BlockUnit from "components/Common/BlockUnit"
import Names from "components/Common/Names"

/** Context */
import withContext from "context/Dashboard/Bookkeeping/forms/closeRefund/withContext"
import { CtxType } from "context/Dashboard/Bookkeeping/forms/closeRefund/Context"

/** Styles */
import { refSubmit } from "context/Dashboard/Bookkeeping/forms/closeRefund/View"
// import styles from "./styles"

const PayerInfo: React.FC<CtxType> = ({ paymentDetails, setPaymentDetails }) => {
  // const {} = styles()
  const { unitUID, payeeUID } = paymentDetails

  /** Methods */
  const handleFormChange = (
    setFieldValue: any,
    key: string,
    value: string | boolean
  ) => {
    return [
      setFieldValue(key, value),
      setPaymentDetails &&
        setPaymentDetails({
          ...paymentDetails,
          [key]: value
        })
    ]
  }

  const handleBlockUnit = (setFieldValue: any, value: string | boolean) => {
    return [
      setFieldValue("unitUID", value),
      setPaymentDetails &&
        setPaymentDetails({
          ...paymentDetails,
          unitUID: value,
          payeeUID: ""
        })
    ]
  }

  return (
    <Box>
      <Formik
        initialValues={{
          unitUID: "",
          payeeUID: ""
        }}
        onSubmit={(values: any) => {
          return values
        }}
        validationSchema={Yup.object().shape({
          unitUID: Yup.string().required("Required"),
          payeeUID: Yup.string().required("Required")
        })}
      >
        {(formProps: any) => {
          const {
            touched,
            errors,
            handleBlur,
            handleSubmit,
            setFieldValue
          } = formProps
          return (
            <FormWrapper title="Payer’s Information" width="60%">
              <Box width="60%" margin="auto">
                <Box marginBottom="30px">
                  <BlockUnit
                    value={unitUID}
                    onChange={(e: any) =>
                      handleBlockUnit(setFieldValue, e.target.value)}
                    onBlur={(e: Event) => handleBlur(e)}
                    error={touched.unitUID && Boolean(errors.unitUID)}
                    helperText={errors.unitUID && touched.unitUID && errors.unitUID}
                  />
                </Box>
                <Box>
                  <Names
                    value={payeeUID}
                    onChange={(e: any) =>
                      handleFormChange(setFieldValue, "payeeUID", e.target.value)}
                    unitUID={unitUID}
                    onBlur={(e: Event) => handleBlur(e)}
                    error={touched.payeeUID && Boolean(errors.payeeUID)}
                    helperText={
                      errors.payeeUID && touched.payeeUID && errors.payeeUID
                    }
                  />
                </Box>
                <div
                  onClick={() => handleSubmit()}
                  ref={refSubmit}
                  role="button"
                  tabIndex={-1}
                >
                  &nbsp;
                </div>
              </Box>
            </FormWrapper>
          )
        }}
      </Formik>
    </Box>
  )
}
export default withContext(PayerInfo)
