import React from "react"
import Box from "@material-ui/core/Box"
import Select from "components/Forms/Select"
import Radio from "@material-ui/core/Radio"
import RadioGroup from "@material-ui/core/RadioGroup"
import FormControl from "@material-ui/core/FormControl"
import FormControlLabel from "@material-ui/core/FormControlLabel"
import { Formik } from "formik"

/** Components */
import FormWrapper from "components/Forms/FormWrapper"
import FormInput from "components/Forms/FormInput"
import Label from "components/Forms/Label"

/** Config */
import validation from "config/Dashboard/Bookkeeping/validation"
import {
  paymentCategory,
  paymentType as paymentTypeConfig
} from "config/Dashboard/Bookkeeping"

/** Context */
import withContext from "context/Dashboard/Bookkeeping/forms/closeRefund/withContext"
import { CtxType } from "context/Dashboard/Bookkeeping/forms/closeRefund/Context"

/** Styles */
import { refSubmit } from "context/Dashboard/Bookkeeping/forms/closeRefund/View"
import styles from "./styles"

/** Interface */
interface ModePaymentProps extends CtxType {
  ref: any
}

const Details: React.FC<ModePaymentProps> = ({
  paymentDetails,
  setPaymentDetails
}) => {
  const { formControl, twoCol } = styles()
  const pd = paymentDetails
  const {
    description,
    remarks,
    paymentType,
    category,
    amt,
    tax,
    refundTo
  } = paymentDetails

  /** Methods */
  const handleFormChange = (
    setFieldValue: any,
    key: string,
    e: React.ChangeEvent<HTMLInputElement>
  ) => {
    return [
      setFieldValue(key, e?.target?.value),
      setPaymentDetails &&
        setPaymentDetails({
          ...paymentDetails,
          [key]: e?.target?.value
        })
    ]
  }

  return (
    <Box>
      <Formik
        initialValues={{
          description: pd?.description,
          remarks: pd?.remarks,
          paymentType: pd?.paymentType,
          category: pd?.category,
          amt: pd?.amt,
          tax: pd?.tax,
          refundTo: ""
        }}
        onSubmit={(values: any) => {
          return values
        }}
        validationSchema={validation.refundPayment}
      >
        {(formProps: any) => {
          const {
            touched,
            errors,
            handleBlur,
            handleSubmit,
            setFieldValue
          } = formProps
          return (
            <form>
              <Box display="flex" flexDirection="column" flex="1">
                <Box>
                  <FormWrapper title="Refund Details" width="60%">
                    <Box width="60%" margin="auto">
                      <Box marginBottom="30px">
                        <FormInput
                          name="description"
                          label="payment description"
                          placeholder="Description"
                          value={description}
                          multiline
                          rows={2}
                          onBlur={(e: Event) => handleBlur(e)}
                          error={touched.description && Boolean(errors.description)}
                          helperText={
                            errors.description &&
                            touched.description &&
                            errors.description
                          }
                          handleOnChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                            handleFormChange(setFieldValue, "description", e)}
                        />
                      </Box>

                      <Box marginBottom="30px" className={twoCol}>
                        <Box>
                          <Select
                            label="Payment Category"
                            selectName="category"
                            placeholder="Select Payment Category"
                            value={category}
                            items={paymentCategory}
                            error={touched.category && Boolean(errors.category)}
                            helperText={
                              errors.category && touched.category && errors.category
                            }
                            onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                              handleFormChange(setFieldValue, "category", e)}
                          />
                        </Box>

                        <Box>
                          <Select
                            label="Payment Type"
                            idValue
                            selectName="paymentType"
                            placeholder="Select Payment Type"
                            value={paymentType}
                            items={paymentTypeConfig}
                            error={
                              touched.paymentType && Boolean(errors.paymentType)
                            }
                            helperText={
                              errors.paymentType &&
                              touched.paymentType &&
                              errors.paymentType
                            }
                            onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                              handleFormChange(setFieldValue, "paymentType", e)}
                          />
                        </Box>
                      </Box>

                      <Box marginBottom="30px" className={twoCol}>
                        <Box>
                          <FormInput
                            type="number"
                            name="amt"
                            label="Amount"
                            placeholder="$ 00.00"
                            value={amt}
                            onBlur={(e: Event) => handleBlur(e)}
                            error={touched.amt && Boolean(errors.amt)}
                            helperText={errors.amt && touched.amt && errors.amt}
                            handleOnChange={(
                              e: React.ChangeEvent<HTMLInputElement>
                            ) => handleFormChange(setFieldValue, "amt", e)}
                          />
                        </Box>

                        {pd?.paymentType !== 3 && (
                          <Box>
                            <FormInput
                              type="number"
                              name="tax"
                              label="Tax"
                              value={tax}
                              placeholder="$ 00.00"
                              onBlur={(e: Event) => handleBlur(e)}
                              error={touched.tax && Boolean(errors.tax)}
                              helperText={errors.tax && touched.tax && errors.tax}
                              handleOnChange={(
                                e: React.ChangeEvent<HTMLInputElement>
                              ) => handleFormChange(setFieldValue, "tax", e)}
                            />
                          </Box>
                        )}
                      </Box>

                      <Box marginBottom="30px">
                        <Label label="Refund To" />
                        <FormControl className={formControl}>
                          <RadioGroup
                            aria-label="refundTo"
                            name="refundTo"
                            value={refundTo}
                            onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                              handleFormChange(setFieldValue, "refundTo", e)}
                            style={{ flexDirection: "row" }}
                          >
                            <FormControlLabel
                              value="1"
                              control={<Radio color="primary" />}
                              label="Resident"
                              labelPlacement="end"
                            />
                            <FormControlLabel
                              value="2"
                              control={<Radio color="primary" />}
                              label="Available Balance"
                              labelPlacement="end"
                            />
                          </RadioGroup>
                        </FormControl>
                      </Box>

                      <Box marginBottom="30px">
                        <FormInput
                          name="remarks"
                          label="remarks"
                          placeholder="Remarks"
                          value={remarks}
                          multiline
                          rows={2}
                          handleOnChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                            handleFormChange(setFieldValue, "remarks", e)}
                          onBlur={(e: Event) => handleBlur(e)}
                          error={
                            touched.remarks &&
                            paymentDetails?.remarks === "" &&
                            Boolean(errors.remarks)
                          }
                          helperText={
                            errors.remarks &&
                            touched.remarks &&
                            paymentDetails?.remarks === "" &&
                            errors.remarks
                          }
                        />
                      </Box>

                      <div
                        onClick={() => handleSubmit()}
                        ref={refSubmit}
                        role="button"
                        tabIndex={-1}
                      >
                        &nbsp;
                      </div>
                    </Box>
                  </FormWrapper>
                </Box>
              </Box>
            </form>
          )
        }}
      </Formik>
    </Box>
  )
}
export default withContext(Details)
