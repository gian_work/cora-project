import React from "react"
import Collapse from "@material-ui/core/Collapse"
import List from "@material-ui/core/List"
import ListItem from "@material-ui/core/ListItem"
import Lock from "@material-ui/icons/Lock"
import LockOpenIcon from "@material-ui/icons/LockOpen"
import { mutate } from "swr"
import { toast } from "react-toastify"

/** Component */
import Dialog from "components/Dialog"

/** Services */
import BookkeepingAPI from "services/Dashboard/Bookkeeping"

/** Config */
import { tableName } from "config/Dashboard/Bookkeeping"

/** Context */
import withContextMain from "context/Dashboard/Bookkeeping/categories/main/withContext"
import withContext from "context/Dashboard/Bookkeeping/main/withContext"
import { CtxType } from "context/Dashboard/Bookkeeping/categories/main/Context"
import { CtxMenuContext } from "components/ContextMenu"

/** Utils */
import { binaryString, isRefundRequested } from "utils/helpers"

/** Styles */
import styles from "./styles"

/** Icons */
import { IconEdit, IconView, IconDown } from "./icons"

interface ContextProps extends CtxType {
  data: Record<string, any>
}

const ContextBookKeeping: React.FC<ContextProps> = ({
  contextActions,
  data,
  activeFilter
}) => {
  const { listItem, innerList, collapseControl, iconActive } = styles()
  const [open, setOpen] = React.useState(false)
  const [openDialog, setOpenDialog] = React.useState(false)
  /** Data Details */
  const paymentStatus = data?.payment_status
  const paymentType = data?.payment_type

  const { openContext } = React.useContext(CtxMenuContext)
  /** check flags */
  // const fromBalance = +isFromAvailableBalance(data.flags) === 1
  const isRefundLock = +binaryString(data.flags) === 1
  const isRequestedForRefund = +isRefundRequested(data.flags) === 1

  const {
    showClosePayment,
    showEditPayment,
    showCancelPayment,
    showRejectPayment,
    showDetails,
    showCancelRefund,
    showCloseRefund,
    showRejectRefund
  } = contextActions

  /** Notification */
  const notifyCreate = () =>
    toast("Successfully closed the payment.", {
      type: toast.TYPE.SUCCESS
    })

  /** Methods */
  const handleClick = () => {
    setOpen(!open)
  }

  const handleDialog = () => {
    setOpenDialog(true)
  }

  const handleUpdateDepositLock = async () => {
    const payload = {
      "_uid": data?._uid,
      "is_deposit_lock": !isRefundLock
    }

    try {
      const response = await BookkeepingAPI.updateDepositLock(payload).then(() => [
        notifyCreate(),
        openContext && openContext(false),
        mutate(tableName[activeFilter])
      ])
      return response
    } catch (e) {
      return e
    }
  }

  /** Views */

  const ViewDetails = (): JSX.Element => {
    return (
      <ListItem
        className={listItem}
        button
        onClick={() => {
          showDetails(data)
          openContext && openContext(false)
        }}
      >
        <i>{IconView}</i>
        View
      </ListItem>
    )
  }

  const ViewEdit = (): JSX.Element => {
    if (paymentStatus !== 1 || paymentType === 3) {
      return <div />
    }

    return (
      <ListItem
        className={listItem}
        button
        onClick={() => {
          showEditPayment(data)
          openContext && openContext(false)
        }}
      >
        <i>{IconEdit}</i>
        Edit
      </ListItem>
    )
  }

  const ViewPaymentTypePayment = (): JSX.Element => {
    if (paymentType !== 1) {
      return <div />
    }

    if (paymentStatus === 1) {
      return (
        <>
          <CancelPayment />
          <ClosePayment />
        </>
      )
    }

    if (paymentStatus === 2) {
      return (
        <>
          <CancelPayment />
          <ClosePayment />
          <RejectPayment />
        </>
      )
    }

    if (paymentStatus === 3) {
      return (
        <>
          <CancelPayment />
          {isRequestedForRefund ? <div /> : <CloseRefund />}
          <RejectPayment />
        </>
      )
    }

    return <div />
  }

  const ViewPaymentTypeDeposit = () => {
    if (paymentType !== 2) {
      return <div />
    }

    if (paymentStatus === 1) {
      return (
        <>
          <CancelPayment />
          <ClosePayment />
        </>
      )
    }

    if (paymentStatus === 2) {
      return (
        <>
          <CancelPayment />
          <ClosePayment />
          <RejectPayment />
        </>
      )
    }

    if (paymentStatus === 3) {
      return (
        <>
          <CancelPayment />
          {isRequestedForRefund || isRefundLock ? <div /> : <CloseRefund />}
          <RejectPayment />
        </>
      )
    }

    return <div />
  }

  const ViewPaymentTypeRefund = () => {
    if (paymentType !== 3) {
      return <div />
    }

    if (paymentStatus === 1) {
      return (
        <>
          <CancelRefund />
          <CloseRefund />
        </>
      )
    }

    if (paymentStatus === 2) {
      return (
        <>
          <CancelRefund />
          <CloseRefund />
          <RejectRefund />
        </>
      )
    }

    if (paymentStatus === 3) {
      return (
        <>
          <CancelRefund />
          {isRequestedForRefund ? <div /> : <CloseRefund />}
          <RejectRefund />
        </>
      )
    }

    return <div />
  }

  const ViewLockUnlock = (): JSX.Element => {
    if (paymentType !== 3) {
      if (paymentStatus === 3) {
        return (
          <>
            <LockRefund />
          </>
        )
      }
      /** Close Payment */
      /** Cancel Payment */
    }

    return <div />
  }

  const ClosePayment = (): JSX.Element => {
    return (
      <ListItem className={listItem} button onClick={() => showClosePayment(data)}>
        Close
      </ListItem>
    )
  }

  const CancelPayment = (): JSX.Element => {
    return (
      <ListItem className={listItem} button onClick={() => showCancelPayment(data)}>
        Cancel
      </ListItem>
    )
  }

  const RejectPayment = (): JSX.Element => {
    return (
      <ListItem className={listItem} button onClick={() => showRejectPayment(data)}>
        Reject
      </ListItem>
    )
  }

  const RejectRefund = (): JSX.Element => {
    return (
      <ListItem className={listItem} button onClick={() => showRejectRefund(data)}>
        Reject Refund
      </ListItem>
    )
  }

  const LockRefund = (): JSX.Element => {
    if (paymentType !== 2) {
      return <div />
    }

    return (
      <ListItem className={listItem} button onClick={() => handleDialog()}>
        <i>{isRefundLock ? <LockOpenIcon /> : <Lock />}</i>
        {isRefundLock ? "Unlock Refund" : "Lock Refund"}
      </ListItem>
    )
  }

  const CloseRefund = (): JSX.Element => {
    return (
      <ListItem className={listItem} button onClick={() => showCloseRefund(data)}>
        Close Refund
      </ListItem>
    )
  }

  const CancelRefund = (): JSX.Element => {
    return (
      <ListItem className={listItem} button onClick={() => showCancelRefund(data)}>
        Cancel Refund
      </ListItem>
    )
  }

  return (
    <>
      <List component="nav">
        <ViewDetails />
        {paymentStatus !== 6 && paymentStatus !== 5 && paymentStatus !== 4 && (
          <ListItem className={listItem} button onClick={handleClick}>
            <div className={collapseControl}>
              <div>
                <i />
                Mark as
              </div>
              <div>
                <i className={open ? iconActive : ""}>{IconDown}</i>
              </div>
            </div>
          </ListItem>
        )}
        <Collapse in={open} timeout="auto" unmountOnExit>
          <List className={innerList} component="div" disablePadding>
            <ViewPaymentTypePayment />
            <ViewPaymentTypeDeposit />
            <ViewPaymentTypeRefund />
            {/* <ViewRefund /> */}
          </List>
        </Collapse>
        <ViewLockUnlock />
        <ViewEdit />
      </List>
      <Dialog
        action={() => handleUpdateDepositLock()}
        isOpen={openDialog}
        setOpen={setOpenDialog}
        actionLabel="Confirm"
        title=""
        message="Are you sure you want to update the refund lock status?"
      />
    </>
  )
}

export default withContext(withContextMain(ContextBookKeeping))
