import React from "react"
import { Route } from "react-router-dom"

// DASHBOARDS
// import Banners from "./Banners"
// import Events from "./Events"
import Posts from "./Post"

interface AnnouncementsProps {
  match: {
    url: string
  }
  history: any
}

const Announcements: React.FC<AnnouncementsProps> = ({ match }) => {
  return (
    <>
      <Route path={`${match.url}/post`} component={Posts} />
      {/* <Route path={`${match.url}/banner`} component={Banners} />
      <Route path={`${match.url}/event`} component={Events} /> */}
    </>
  )
}

export default Announcements
