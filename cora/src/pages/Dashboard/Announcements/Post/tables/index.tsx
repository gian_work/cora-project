import React, { useContext } from "react"
import useSWR from "swr"
import Fade from "@material-ui/core/Fade"
import Cookie from "js-cookie"
/** Service */
import service from "services/Dashboard/Announcements/Post"
/** Components */
import Table from "components/Table"
import { toUnix } from "utils/date"
import { Context } from "context/Dashboard/Announcements/Posts/main/Context"
import tableHeaders from "./tableHeaders"

interface Props {
  showUpdate: Function
  activeTable: number
}

const TablePublicDocuments: React.FC<Props> = ({ showUpdate, activeTable }) => {
  const condoUID = Cookie.get("condoUID")
  const unixDateNow = toUnix(new Date().toString())

  const { setActiveData, setShowDocument } = useContext(Context)

  const fetchAnnouncementPost = async () =>
    service.getAnnouncementPost({
      "condo_uid": condoUID
    })

  const { data, isValidating } = useSWR(
    "fetchAnnouncementPost",
    fetchAnnouncementPost,
    {
      revalidateOnFocus: true
    }
  )

  const tableData = data?.data?._data

  const FilteredData = () => {
    const active: Array<{}> = []
    const expired: Array<{}> = []
    tableData !== undefined &&
      Object.keys(tableData).map((i: any) => {
        if (
          unixDateNow >= tableData[i]?.start_date &&
          unixDateNow <= tableData[i]?.end_date
        ) {
          return active.push(tableData[i])
        }
        return expired.push(tableData[i])
      })

    return {
      active,
      expired
    }
  }

  const TablePosts = (tab: number) => {
    switch (tab) {
      case 0:
        return tableData
      case 1:
        return FilteredData()?.active
      case 2:
        return FilteredData()?.expired
      default:
        return tableData
    }
  }

  const viewFile = (activeData: Record<string, any>) => {
    setActiveData && setActiveData(activeData)
    setShowDocument && setShowDocument(true)
  }

  const showUpdateForm = (activeData: Record<string, any>) => {
    showUpdate && showUpdate(activeData)
  }

  return (
    <Fade in={!isValidating} timeout={1000}>
      <div>
        <Table
          data={TablePosts(activeTable || 0) || []}
          columns={tableHeaders(viewFile, showUpdateForm)}
          minRows={2}
          isFilterable
          size={10}
        />
      </div>
    </Fade>
  )
}

export default TablePublicDocuments
