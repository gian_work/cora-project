import React from "react"
import Button from "@material-ui/core/Button"

import ContextMenu from "components/ContextMenu"
import FileLink from "components/Table/components/FileLink"

import dateHelper from "utils/date"
import ContextItems from "./ContextMenu"

const TableHeaderColumns = (viewFile: Function, showUpdate: Function) => [
  {
    fixed: "left",
    disableFilters: true,
    columns: [
      {
        width: 70,
        Cell: (row: any): JSX.Element => {
          return (
            <ContextMenu>
              <ContextItems data={row?.original} showUpdate={showUpdate} />
            </ContextMenu>
          )
        },
        filterable: false
      }
    ]
  },
  {
    Header: "",
    columns: [
      {
        Header: "Created by",
        id: "created_by",
        accessor: "created_by_account_name"
      },
      {
        Header: "Title",
        id: "title",
        accessor: "title"
      },
      // {
      //   Header: "Description",
      //   id: "description",
      //   accessor: (c: Record<string, any>) => {
      //     return c?.description
      //   },
      //   Cell: ({original}: Record<string, any>) => {
      //     return original?.description === true ? original?.description : "N/A"
      //   },
      // },
      {
        Header: "Status",
        id: "status",
        accessor: (original: Record<string, any>) => {
          return original?.status
        },
        Cell: ({ original }: Record<string, any>) => {
          return original?.status === 1 ? "Draft" : "Active"
        }
      },
      {
        Header: "Start Date",
        id: "start_date",
        width: 150,
        accessor: (original: Record<string, any>) => {
          return dateHelper.fromUnix(original?.start_date)
        },
        Cell: ({ original }: Record<string, any>) => {
          return original?.start_date === null
            ? "N/A"
            : dateHelper.fromUnix(original?.start_date)
        }
      },
      {
        Header: "End Date",
        id: "end_date",
        width: 150,
        accessor: (original: Record<string, any>) => {
          return dateHelper.fromUnix(original?.end_date)
        },
        Cell: ({ original }: Record<string, any>) => {
          return original?.end_date === null
            ? "N/A"
            : dateHelper.fromUnix(original?.end_date)
        }
      },
      {
        Header: "Attachment",
        id: "attachment",
        accessor: (original: Record<string, any>) => {
          return original?.attachment?.files === null
            ? "N/A"
            : original?.attachment?.files[0]?.file_name
        },
        Cell: ({ original }: any) => {
          return original?.attachment?.files === null ? (
            "N/A"
          ) : (
            <FileLink
              url={original?.attachment?.files[0]?.url}
              name={original?.attachment?.files[0]?.file_name}
            />
          )
        }
      }
    ]
  },
  {
    fixed: "right",
    disableFilters: true,
    columns: [
      {
        Cell: ({ original }: Record<string, any>): JSX.Element => {
          if (
            original?.attachment?.files === null ||
            original?.attachment?.files === undefined
          ) {
            return <div>N/A</div>
          }
          return (
            <Button color="primary" onClick={() => viewFile(original)}>
              View File
            </Button>
          )
        },
        filterable: false
      }
    ]
  }
]

export default TableHeaderColumns
