const data = [
  {
    serialNo: "1",
    description: "High Rise Littering",
    startDate: "7/25/2019",
    expiry: "N/A",
    attachment: "N/A"
  },
  {
    serialNo: "2",
    description: "No Smoking at Common Area",
    startDate: "8/16/2019",
    expiry: "N/A",
    attachment: "N/A"
  },
  {
    serialNo: "3",
    description: "Side Gate 2 - under repair",
    startDate: "12/20/2019",
    expiry: "N/A",
    attachment: "N/A"
  }
]

export default data
