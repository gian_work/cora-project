import React from "react"

import Provider from "context/Dashboard/Announcements/Posts/main/Provider"
import View from "context/Dashboard/Announcements/Posts/main/View"

export default () => (
  <Provider>
    <View />
  </Provider>
)
