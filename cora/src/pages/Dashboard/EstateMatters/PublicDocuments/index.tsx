import React from "react"

import Provider from "context/Dashboard/EstateMatters/PublicDocuments/main/Provider"
import View from "context/Dashboard/EstateMatters/PublicDocuments/main/View"

export default () => (
  <Provider>
    <View />
  </Provider>
)
