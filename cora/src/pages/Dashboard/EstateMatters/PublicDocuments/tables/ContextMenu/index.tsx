import React, { useContext, useState } from "react"
import List from "@material-ui/core/List"
import ListItem from "@material-ui/core/ListItem"
import { mutate } from "swr"

/** Components */
import Dialog from "components/Dialog"

/** Styles */
import { IconEdit, IconDelete } from "components/Icons/ContextMenu"

/** COntext */
import { CtxMenuContext } from "components/ContextMenu"
import { Context } from "context/Dashboard/EstateMatters/PublicDocuments/main/Context"
import styles from "./styles"

interface ContextNewAccountRequestsProps {
  data: Record<string, any>
  showUpdateForm: Function
}

const ContextPublicDocuments: React.FC<ContextNewAccountRequestsProps> = ({
  data,
  showUpdateForm
}) => {
  const { listItem } = styles()
  const { deleteDocument, activeDoc } = useContext(Context)
  const { openContext } = React.useContext(CtxMenuContext)
  const [openDialog, setOpenDialog] = useState(false)

  function handleDeleteDocument(uid: string) {
    openContext && openContext(false)
    deleteDocument && deleteDocument(uid)
    mutate(`fetchPublicDocuments-${activeDoc?._uid}`)
  }

  return (
    <>
      <List component="nav">
        <ListItem className={listItem} button onClick={() => showUpdateForm(data)}>
          <i>
            <IconEdit />
          </i>
          Edit
        </ListItem>
        <ListItem className={listItem} button onClick={() => setOpenDialog(true)}>
          <i>
            <IconDelete />
          </i>
          Delete
        </ListItem>
      </List>
      <Dialog
        action={() => handleDeleteDocument(data?._uid)}
        isOpen={openDialog}
        setOpen={setOpenDialog}
        actionLabel="Confirm"
        title=""
        message="Are you sure you want to update the status?"
      />
    </>
  )
}

export default ContextPublicDocuments
