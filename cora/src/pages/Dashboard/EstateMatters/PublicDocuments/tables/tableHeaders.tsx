import React from "react"
import Button from "@material-ui/core/Button"

/** Components */
import ContextMenu from "components/ContextMenu"
import FileLink from "components/Table/components/FileLink"

/** Utils */
import { fromUnix } from "utils/date"
import ContextItems from "./ContextMenu"

const tableHeaders = (viewFile: Function, showUpdateForm: Function) => {
  return [
    {
      fixed: "left",
      disableFilters: true,
      columns: [
        {
          width: 70,
          Cell: (row: any): JSX.Element => {
            return (
              <ContextMenu>
                <ContextItems data={row?.original} showUpdateForm={showUpdateForm} />
              </ContextMenu>
            )
          },
          filterable: false
        }
      ]
    },
    {
      Header: "",
      columns: [
        {
          Header: "S/No.",
          id: "estate_matter_reference_no",
          accessor: "estate_matter_reference_no"
        },
        {
          Header: "Description",
          id: "description",
          accessor: "description"
        },
        {
          Header: "Title",
          id: "title",
          accessor: "title"
        },
        {
          Header: "Start Date",
          id: "startDate",
          accessor: (c: any) => {
            return fromUnix(c.start_date)
          },
          Cell: ({ original }: any) => {
            return original.start_date === undefined
              ? "N/A"
              : fromUnix(original.start_date)
          }
        },
        {
          Header: "End Date",
          id: "endDate",
          accessor: (c: any) => {
            return fromUnix(c.end_date)
          },
          Cell: ({ original }: any) => {
            return original.end_date === undefined
              ? "N/A"
              : fromUnix(original.end_date)
          }
        },
        {
          Header: "Attachment",
          id: "attachment",
          accessor: "file.file_name",
          Cell: ({ original }: any) => {
            return original?.file?.url === undefined ||
              original?.file?.url === "" ? (
              "N/A"
            ) : (
              <FileLink url={original?.file?.url} name={original?.file?.file_name} />
            )
          }
        }
      ]
    },
    {
      fixed: "right",
      disableFilters: true,
      columns: [
        {
          Cell: ({ original }: any): JSX.Element => {
            if (original?.file?.url === undefined || original?.file?.url === "") {
              return <div>N/A</div>
            }
            return (
              <Button color="primary" onClick={() => viewFile(original)}>
                View File
              </Button>
            )
          },
          filterable: false
        }
      ]
    }
  ]
}

export default tableHeaders
