import React, { useContext } from "react"
import useSWR from "swr"
import Fade from "@material-ui/core/Fade"

/** Service */
import Service from "services/Dashboard/EstateMatters"

/** Components */
import Table from "components/Table"
import Loader from "components/Loader"
import { Context } from "context/Dashboard/EstateMatters/PublicDocuments/main/Context"
import { toUnix } from "utils/date"
import tableHeaders from "./tableHeaders"

interface Props {
  showUpdate: Function
}

const TablePublicDocuments: React.FC<Props> = ({ showUpdate }) => {
  const {
    activeDoc,
    setActiveData,
    setShowDocument,
    accountRequestsTab
  } = useContext(Context)
  const folderId = activeDoc?._uid

  const unixDateNow = toUnix(new Date().toString())

  const fetchPublicDocuments = async () =>
    Service.getEstateMatters({
      "estate_matter_folder_uid": folderId
    })

  const { data, isValidating } = useSWR(
    folderId ? `fetchPublicDocuments-${folderId}` : null,
    fetchPublicDocuments,
    {
      revalidateOnFocus: true
    }
  )

  const tableData = data?.data?._data

  const FilteredData = () => {
    const active: Array<{}> = []
    const expired: Array<{}> = []
    tableData !== undefined &&
      Object.keys(tableData).map((i: any) => {
        if (
          unixDateNow >= tableData[i]?.start_date &&
          unixDateNow <= tableData[i]?.end_date
        ) {
          return active.push(tableData[i])
        }
        return expired.push(tableData[i])
      })

    return {
      active,
      expired
    }
  }

  const TableDocs = (tab: number) => {
    switch (tab) {
      case 0:
        return tableData
      case 1:
        return FilteredData()?.active
      case 2:
        // return ExpiredDocument
        return FilteredData()?.expired
      default:
        return tableData
    }
  }

  // console.log(ExpiredDocument)

  const viewFile = (activeData: Record<string, any>) => {
    setActiveData && setActiveData(activeData)
    setShowDocument && setShowDocument(true)
  }

  const showUpdateForm = (activeData: Record<string, any>) => {
    setActiveData && setActiveData(activeData)
    showUpdate && showUpdate()
  }

  if (isValidating) {
    return <Loader dark />
  }

  return (
    <Fade in={!isValidating} timeout={1000}>
      <div>
        <Table
          data={TableDocs(accountRequestsTab || 0) || []}
          columns={tableHeaders(viewFile, showUpdateForm)}
          size={10}
          isFilterable
          minRows={2}
        />
      </div>
    </Fade>
  )
}

export default TablePublicDocuments
