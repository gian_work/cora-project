import React, { useContext, useState } from "react"
import Box from "@material-ui/core/Box"
import { Formik } from "formik"
/** Components */
import FormInput from "components/Forms/FormInput"
import RefButton from "components/Forms/RefButton"
import DatePicker from "components/Forms/DatePicker"
import FormWrapper from "components/Forms/FormWrapper"
import Spacer from "components/Spacer"
import Label from "components/Forms/Label"
import UploadBox from "components/UploadBox/new"

/** Context */
import { Context as MainContext } from "context/Dashboard/EstateMatters/PublicDocuments/forms/documentUpdate/Context"

/** Validation */
import { documentUpdateInfoValidation } from "config/Dashboard/EstateMatters/PublicDocuments/validation"

/** Ref */
import { refSubmit } from "context/Dashboard/EstateMatters/PublicDocuments/forms/documentUpdate/View"

const ResidentInfo: React.FC = () => {
  const {
    docDetails,
    setDocDetails,
    handleFileUpload,
    files,
    removeFile,
    fileAttaching,
    uploadError
  } = useContext(MainContext)
  const { title, description, startDate, endDate, remarks } = docDetails
  const [showForm, setShowForm] = useState(false)

  /** Methods */
  const handleFormChange = (
    name: string,
    e: React.ChangeEvent<HTMLInputElement> | any,
    setFieldValue: Function
  ) => {
    if (e?.target === undefined) {
      setFieldValue(name, e)
      setDocDetails &&
        setDocDetails({
          ...docDetails,
          [name]: e
        })
    } else {
      setFieldValue(name, e?.target?.value)
      setDocDetails &&
        setDocDetails({
          ...docDetails,
          [name]: e?.target?.value
        })
    }
  }

  return (
    <Box>
      <Formik
        initialValues={{
          title,
          description,
          startDate,
          endDate,
          remarks
        }}
        onSubmit={(values, actions): void => {
          JSON.stringify(values, null, 2)
          actions.setSubmitting(false)
        }}
        validationSchema={documentUpdateInfoValidation}
      >
        {({
          touched,
          errors,
          handleBlur,
          handleSubmit,
          setFieldValue
        }): JSX.Element => {
          return (
            <form>
              <Box display="flex" height="100%" padding="0 60px 0">
                <Box flex="1" paddingBottom="30px">
                  <FormWrapper title="Document Info">
                    <Box marginBottom="25px">
                      <FormInput
                        name="title"
                        value={title}
                        idValue="title"
                        label="Title"
                        placeholder="Title"
                        handleOnChange={(
                          e: React.ChangeEvent<HTMLInputElement>
                        ): void => handleFormChange("title", e, setFieldValue)}
                        onBlur={handleBlur}
                        error={touched.title && Boolean(errors.title)}
                        helperText={errors.title && touched.title && errors.title}
                      />
                    </Box>

                    <Box marginBottom="25px">
                      <FormInput
                        name="description"
                        value={description}
                        idValue="description"
                        label="Description"
                        placeholder="Description"
                        multiline
                        rows={3}
                        handleOnChange={(
                          e: React.ChangeEvent<HTMLInputElement>
                        ): void => handleFormChange("description", e, setFieldValue)}
                        onBlur={handleBlur}
                        error={touched.description && Boolean(errors.description)}
                        helperText={
                          errors.description &&
                          touched.description &&
                          errors.description
                        }
                      />
                    </Box>

                    <Box marginBottom="25px">
                      <Box marginBottom="15px">
                        <Label label="Expiry" />
                      </Box>
                      <Box display="flex" marginBottom="25px">
                        <Box flex="1">
                          <DatePicker
                            label="Start Date"
                            name="startDate"
                            format="MM/dd/yyyy"
                            value={startDate}
                            placeholder="mm/dd/yyyy"
                            shrinkLabel
                            handleDateChange={(value: string): void =>
                              handleFormChange("startDate", value, setFieldValue)}
                            onBlur={(e: Event): void => handleBlur(e)}
                            error={touched.startDate && Boolean(errors.startDate)}
                            helperText={
                              errors.startDate &&
                              touched.startDate &&
                              errors.startDate
                            }
                          />
                        </Box>
                        <Spacer isDefault />
                        <Box flex="1">
                          <DatePicker
                            label="End Date"
                            name="endDate"
                            format="MM/dd/yyyy"
                            value={endDate}
                            placeholder="mm/dd/yyyy"
                            minDate={startDate}
                            shrinkLabel
                            handleDateChange={(value: string): void =>
                              handleFormChange("endDate", value, setFieldValue)}
                            onBlur={(e: Event): void => handleBlur(e)}
                            error={touched.endDate && Boolean(errors.endDate)}
                            helperText={
                              errors.endDate && touched.endDate && errors.endDate
                            }
                          />
                        </Box>
                      </Box>
                    </Box>

                    <Box marginBottom="25px">
                      <FormInput
                        name="remarks"
                        value={remarks}
                        idValue="remarks"
                        label="Remarks"
                        placeholder="Remarks"
                        multiline
                        rows={3}
                        handleOnChange={(
                          e: React.ChangeEvent<HTMLInputElement>
                        ): void => handleFormChange("remarks", e, setFieldValue)}
                        onBlur={handleBlur}
                        error={touched.remarks && Boolean(errors.remarks)}
                        helperText={
                          errors.remarks && touched.remarks && errors.remarks
                        }
                      />
                    </Box>

                    <RefButton refValue={refSubmit} action={handleSubmit} />
                  </FormWrapper>
                </Box>
                <Box
                  flex="1"
                  padding="0 0 30px 60px"
                  marginLeft="60px"
                  borderLeft="1px solid #F2F2F2"
                >
                  <FormWrapper title="Attachment">
                    <Box padding="0 0 40px 0">
                      <UploadBox
                        hasTitle={false}
                        files={files}
                        attaching={fileAttaching}
                        title="Choose file to upload"
                        acceptedFile="application/pdf"
                        hasError={uploadError}
                        isShown={showForm}
                        hasDelete={showForm}
                        removePhoto={(event: any) => removeFile && removeFile(event)}
                        onDrop={(event: any) =>
                          handleFileUpload && handleFileUpload(event)}
                        disabled={files[0] !== undefined}
                      />

                      {showForm && files[0] !== undefined && (
                        <Box
                          display="flex"
                          justifyContent="flex-end"
                          fontSize="12px"
                          color="#777E86"
                        >
                          *Remove existing document before uploading a new document.
                        </Box>
                      )}

                      {files[0] === undefined && (
                        <Box
                          display="flex"
                          justifyContent="flex-end"
                          fontSize="12px"
                          padding="8px 0"
                          color="#FF0000"
                        >
                          *Please upload a document
                        </Box>
                      )}

                      {!showForm && (
                        <Box
                          color="#09707B"
                          fontSize="14px"
                          fontWeight="600"
                          padding="8px 0"
                          style={{ cursor: "pointer" }}
                          onClick={() => setShowForm(true)}
                        >
                          REPLACE ATTACHMENT
                        </Box>
                      )}
                    </Box>
                  </FormWrapper>
                </Box>
              </Box>
            </form>
          )
        }}
      </Formik>
    </Box>
  )
}

export default ResidentInfo
