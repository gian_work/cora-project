import React, { useContext } from "react"
import useSWR from "swr"
import Fade from "@material-ui/core/Fade"
/** Service */
import Service from "services/Dashboard/EstateMatters"
/** Components */
import Table from "components/Table"
import { Context } from "context/Dashboard/EstateMatters/ConstructionManuals/main/Context"
import tableHeaders from "./tableHeaders"
/** Context */

const TableConstructionManuals: React.FC = () => {
  const { activeDoc } = useContext(Context)
  const folderId = activeDoc?._uid

  const fetchConstructionManuals = async () =>
    Service.getEstateMatters({
      "estate_matter_folder_uid": folderId
    })

  const { data, isValidating } = useSWR(
    folderId ? `fetchConstructionManuals-${folderId}` : null,
    fetchConstructionManuals
  )

  const tableData = data?.data?._data

  const viewFile = (activeData: Record<string, any>) => {
    return activeData
  }

  return (
    <Fade in={!isValidating} timeout={1000}>
      <div>
        <Table
          data={tableData || []}
          columns={tableHeaders(viewFile)}
          size={10}
          isFilterable
          minRows={2}
        />
      </div>
    </Fade>
  )
}

export default TableConstructionManuals
