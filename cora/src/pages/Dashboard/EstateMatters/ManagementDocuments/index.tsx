import React from "react"

import Provider from "context/Dashboard/EstateMatters/ManagementDocuments/main/Provider"
import View from "context/Dashboard/EstateMatters/ManagementDocuments/main/View"

export default () => (
  <Provider>
    <View />
  </Provider>
)
