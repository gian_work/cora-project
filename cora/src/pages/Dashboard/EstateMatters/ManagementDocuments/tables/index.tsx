import React, { useContext } from "react"
import useSWR from "swr"
import Fade from "@material-ui/core/Fade"

/** Service */
import Service from "services/Dashboard/EstateMatters"

/** Components */
import Table from "components/Table"
import { Context } from "context/Dashboard/EstateMatters/PublicDocuments/main/Context"
import { toUnix } from "utils/date"
import tableHeaders from "./tableHeaders"

/** Utils */

interface Props {
  showUpdate: Function
}

const TablePublicDocuments: React.FC<Props> = ({ showUpdate }) => {
  const {
    activeDoc,
    setActiveData,
    setShowDocument,
    accountRequestsTab
  } = useContext(Context)
  const folderId = activeDoc?._uid

  const unixDateNow = toUnix(new Date().toString())

  const fetchManagementDocuments = async () =>
    Service.getEstateMatters({
      "estate_matter_folder_uid": folderId
    })

  const { data, isValidating } = useSWR(
    folderId ? `fetchManagementDocuments-${folderId}` : null,
    fetchManagementDocuments,
    {
      revalidateOnFocus: true
    }
  )

  const tableData = data?.data?._data

  const ActiveDocument = tableData?.filter(
    (i: any) => unixDateNow >= i?.start_date && unixDateNow <= i?.end_date
  )

  const ExpiredDocument = tableData?.filter((i: any) => unixDateNow < i?.start_date)

  const TableDocs = (tab: number) => {
    switch (tab) {
      case 0:
        return tableData
      case 1:
        return ActiveDocument
      case 2:
        return ExpiredDocument
      default:
        return tableData
    }
  }

  const viewFile = (activeData: Record<string, any>) => {
    setActiveData && setActiveData(activeData)
    setShowDocument && setShowDocument(true)
  }

  const showUpdateForm = (activeData: Record<string, any>) => {
    setActiveData && setActiveData(activeData)
    showUpdate && showUpdate()
  }

  return (
    <Fade in={!isValidating} timeout={1000}>
      <div>
        <Table
          data={TableDocs(accountRequestsTab || 0) || []}
          columns={tableHeaders(viewFile, showUpdateForm)}
          size={10}
          isFilterable
          minRows={2}
        />
      </div>
    </Fade>
  )
}

export default TablePublicDocuments
