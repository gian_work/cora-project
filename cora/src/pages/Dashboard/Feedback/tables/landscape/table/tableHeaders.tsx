import React from "react"
import ContextMenu from "components/ContextMenu"
/** Components */
import StatusBlock from "components/Table/components/StatusBlock"
/** Config */
import ColorStatus from "utils/status"
import { feedbackConfig } from "config/Dashboard/Feedback"
import dateHelper from "utils/date"
import CtxMenuFeedback from "../../../components/ContextMenu"

interface DataProps {
  row: {
    _original: Record<string, string | number>
  }
}

const TableHeaderColumns: Array<{}> = [
  {
    fixed: "left",
    disableFilters: true,
    columns: [
      {
        width: 70,
        Cell: ({ row }: DataProps): JSX.Element => {
          return (
            <ContextMenu setopenDetails={(): null => null} openDetails>
              <CtxMenuFeedback rowData={row._original} />
            </ContextMenu>
          )
        },
        filterable: false
      }
    ]
  },
  {
    columns: [
      {
        Header: "Status",
        id: "status",
        width: 170,
        sortable: false,
        filterable: false,
        accessor: (c: any) => {
          return feedbackConfig.status[c.status]
        },
        Cell: ({ original }: any) => {
          const { status } = original
          return (
            <StatusBlock
              status={feedbackConfig.status[status]}
              color={ColorStatus.code(feedbackConfig.status[status])}
            />
          )
        }
      },
      {
        Header: "Urgent",
        id: "is_urgent",
        accessor: (c: any) => {
          return c.is_urgent ? "Yes" : "No"
        },
        width: 100,
        Cell: ({ original }: any) => {
          return <div>{original.is_urgent ? "Yes" : "No"}</div>
        }
      },
      {
        Header: "Date",
        id: "posted_date_ms",
        accessor: (c: any) => {
          return dateHelper.fromUnix(c.posted_date_ms)
        },
        width: 150,
        Cell: ({ original }: any) => {
          return <div>{dateHelper.fromUnix(original.posted_date_ms)}</div>
        }
      },
      {
        Header: "Block/Unit No.",
        id: "feedback_unit_short_name",
        accessor: "feedback_unit_short_name",
        width: 150,
        Cell: ({ original }: any) => {
          return (
            <div>
              {!original.by_admin ? original.feedback_unit_short_name : "By Admin"}
            </div>
          )
        }
      },
      {
        Header: "Name",
        id: "feedback_owner_account_name",
        accessor: "feedback_owner_account_name"
      },
      {
        Header: "Category",
        id: "feedback_type",
        accessor: (c: any) => {
          return feedbackConfig.type[c.feedback_type]
        },
        Cell: ({ original }: any) => {
          return <div>{feedbackConfig.type[original.feedback_type]}</div>
        }
      },
      {
        Header: "Assign To",
        id: "assigned_to_staff_account_name",
        accessor: (c: any) => {
          return c.assigned_to_staff_account_name === "null" ||
            c.assigned_to_staff_account_name === ""
            ? "N/A"
            : c.assigned_to_staff_account_name
        },
        Cell: ({ original }: any) => {
          return original.assigned_to_staff_account_name === "null" ||
            original.assigned_to_staff_account_name === ""
            ? "N/A"
            : original.assigned_to_staff_account_name
        }
      },
      {
        Header: "Description",
        id: "remarks",
        accessor: "remarks",
        Cell: ({ original }: any) => {
          return original.remarks === "null" || original.remarks === ""
            ? "N/A"
            : original.remarks
        }
      },
      {
        Header: "Aging (number of days)",
        id: "aging",
        accessor: "posted_date_ms",
        Cell: ({ original }: any) => {
          return dateHelper.diffDays(original.posted_date_ms)
        }
      }
    ]
  }
]

export default TableHeaderColumns
