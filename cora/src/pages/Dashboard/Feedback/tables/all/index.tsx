import React from "react"
import Fade from "@material-ui/core/Fade"

/** Components */
import Table from "components/Table"
/** Context */
import { Context } from "context/Dashboard/Feedback/categories/main/Context"
import tableHeaders from "./table/tableHeaders"

const TableAll: React.FC = () => {
  const { tabStatus, data, isValidating } = React.useContext(Context)
  const tableData = data?.data?._data
  const filteredData = tableData?.filter((i: any) => i.status === tabStatus)
  const activeData = tabStatus !== 0 ? filteredData : tableData

  return (
    <Fade in={!isValidating} timeout={1000}>
      <div>
        <Table
          data={activeData || []}
          columns={tableHeaders}
          size={10}
          isFilterable
        />
      </div>
    </Fade>
  )
}

export default TableAll
