import React from "react"
import Fade from "@material-ui/core/Fade"

/** Components */
import Table from "components/Table"

/** Config */
import { Context } from "context/Dashboard/Feedback/categories/main/Context"
import tableHeaders from "./table/tableHeaders"

/** Context */

const TableOthers: React.FC = () => {
  const { activeFilter, data, isValidating } = React.useContext(Context)

  const tableData = data?.data?._data
  const filteredAll = tableData?.filter((i: any) => i.feedback_type === 5)

  const filteredData = filteredAll?.filter((i: any) => i.status === activeFilter)

  const activeData = activeFilter === 0 ? filteredAll : filteredData

  return (
    <Fade in={!isValidating}>
      <div>
        <Table
          data={activeData || []}
          columns={tableHeaders}
          size={10}
          isFilterable
        />
      </div>
    </Fade>
  )
}

export default TableOthers
