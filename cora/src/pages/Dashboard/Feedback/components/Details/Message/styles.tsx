import { makeStyles, Theme } from "@material-ui/core/styles"

const styles = makeStyles((theme: Theme) => ({
  container: {
    padding: "30px 50px",
    backgroundColor: "#F2F2F2",
    "& .two-col": {
      display: "flex"
    },
    "& .two-col > div:first-child": {
      marginRight: "50px"
    },
    "& .actions": {
      display: "flex",
      width: "100%",
      justifyContent: "flex-end"
    }
  },
  containerAdmin: {
    backgroundColor: "#FAFAFA"
  },
  value: {
    fontSize: "14px",
    color: theme.palette.secondary.dark
  },
  field: {
    marginBottom: "20px"
  },
  photoList: {
    display: "flex",
    justifyContent: "space-between"
  },
  photoItem: {
    width: "194px",
    height: "194px"
  },
  noConvo: {
    padding: "30px 0",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    "& .description": {
      marginTop: "10px",
      color: theme.palette.primary.main,
      fontSize: "24px",
      fontWeight: "500"
    }
  }
}))

export default styles
