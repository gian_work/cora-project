import React from "react"
import FormControlLabel from "@material-ui/core/FormControlLabel"
import Box from "@material-ui/core/Box"
import Checkbox from "@material-ui/core/Checkbox"
import FormGroup from "@material-ui/core/FormGroup"
import { Formik } from "formik"
import * as Yup from "yup"
/** Components */
import FormWrapper from "components/Forms/FormWrapper"
import UploadBox from "components/UploadBox/new"
import FeedbackType from "components/Common/FeedbackType"
import StaffList from "components/Common/StaffList"
import FormInput from "components/Forms/FormInput"
import RefButton from "components/Forms/RefButton"
/** Context */
import withContext from "context/Dashboard/Feedback/forms/editFeedback/withContext"
import { CtxType } from "context/Dashboard/Feedback/forms/editFeedback/Context"
/** Ref */
import { refSubmit } from "context/Dashboard/Feedback/forms/editFeedback/View"
/** Styles */
import styles from "./styles"

const Feedback: React.FC<CtxType> = ({
  setFeedbackDetails,
  feedbackDetails,
  handleImageUpload,
  removePhoto,
  photos,
  attaching
}) => {
  const { generalInfo, spaceBottom } = styles()
  const { type, staffName, description } = feedbackDetails

  const handleFormChange = (
    setFieldValue: Function,
    key: string,
    e: React.ChangeEvent<HTMLInputElement>
  ): void => {
    setFieldValue(key, e?.target?.value)
    setFeedbackDetails &&
      setFeedbackDetails({
        ...feedbackDetails,
        [key]: e?.target?.value
      })
  }

  const handleFormValue = (
    setFieldValue: Function,
    key: string,
    value: string | boolean
  ): void => {
    setFieldValue(key, value)
    setFeedbackDetails &&
      setFeedbackDetails({
        ...feedbackDetails,
        [key]: value
      })
  }

  return (
    <Box className={generalInfo}>
      <Formik
        initialValues={{
          type,
          staffName,
          description
        }}
        onSubmit={(values, actions): void => {
          JSON.stringify(values, null, 2)
          actions.setSubmitting(false)
        }}
        validationSchema={Yup.object().shape({
          type: Yup.string().required("Required"),
          assignTo: Yup.string().required("Required"),
          description: Yup.string().required("Required"),
          role: Yup.string().required("Required"),
          staffName: Yup.string().required("Required")
        })}
      >
        {(formikProps: any) => {
          const {
            touched,
            errors,
            handleBlur,
            handleSubmit,
            setFieldValue
          } = formikProps
          return (
            <form>
              <Box display="flex">
                <Box
                  flex="1"
                  width="50%"
                  padding="0 40px 0"
                  borderRight="1px solid #F2F2F2"
                >
                  <FormWrapper title="Feedback">
                    <Box className={spaceBottom}>
                      <FeedbackType
                        value={type}
                        onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                          handleFormChange(setFieldValue, "type", e)}
                        onBlur={(e: Event) => handleBlur(e)}
                        error={touched.type && Boolean(errors.type)}
                        helperText={errors.type && touched.type && errors.type}
                      />
                    </Box>
                    <Box className={spaceBottom}>
                      <FormInput
                        label="Description"
                        placeholder="Description"
                        handleOnChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                          handleFormChange(setFieldValue, "description", e)}
                        name="description"
                        idValue="description"
                        multiline
                        rows={3}
                        onBlur={handleBlur}
                        error={touched.description && Boolean(errors.description)}
                        helperText={
                          errors.description &&
                          touched.description &&
                          errors.description
                        }
                        value={description}
                      />
                    </Box>
                    <Box>
                      <Box marginBottom="20px">
                        <StaffList
                          value={staffName}
                          label="Assign to Staff"
                          onChange={(value: string) =>
                            handleFormValue(setFieldValue, "staffName", value)}
                          onBlur={(e: Event) => handleBlur(e)}
                          error={touched.staffName && Boolean(errors.staffName)}
                          helperText={
                            errors.staffName && touched.staffName && errors.staffName
                          }
                        />
                      </Box>
                    </Box>
                    <Box className={spaceBottom}>
                      <FormGroup>
                        <FormControlLabel
                          control={
                            <Checkbox
                              checked={feedbackDetails?.isUrgent}
                              onChange={() =>
                                handleFormValue(
                                  setFieldValue,
                                  "isUrgent",
                                  !feedbackDetails?.isUrgent
                                )}
                              value={feedbackDetails?.isUrgent}
                              color="primary"
                            />
                          }
                          label="Set as urgent"
                        />
                      </FormGroup>
                    </Box>
                  </FormWrapper>
                </Box>
                <Box flex="1" width="50%" padding="0 40px 40px">
                  <FormWrapper title="Upload an image">
                    <Box>
                      <UploadBox
                        onDrop={handleImageUpload}
                        files={photos}
                        removePhoto={removePhoto}
                        attaching={attaching}
                      />
                    </Box>
                  </FormWrapper>
                </Box>
              </Box>
              <RefButton refValue={refSubmit} action={handleSubmit} />
            </form>
          )
        }}
      </Formik>
    </Box>
  )
}

export default withContext(Feedback)
