import { makeStyles } from "@material-ui/core/styles"

const styles = makeStyles(() => ({
  generalInfo: {
    margin: "0 auto",
    "& .section": {
      borderBottom: "1px solid #F2F2F2",
      marginBottom: "40px"
    }
  },
  twoCol: {
    display: "flex",
    justifyContent: "space-between",
    "& > div": {
      width: "48%"
    }
  },
  spaceBottom: {
    paddingBottom: "30px"
  }
}))

export default styles
