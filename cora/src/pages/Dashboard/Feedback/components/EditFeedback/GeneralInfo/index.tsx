import React from "react"
import FormControlLabel from "@material-ui/core/FormControlLabel"
import Box from "@material-ui/core/Box"
import Checkbox from "@material-ui/core/Checkbox"
import FormGroup from "@material-ui/core/FormGroup"
import { Formik } from "formik"
import * as Yup from "yup"
/** Components */
import BlockUnit from "components/Common/BlockUnit"
import FormWrapper from "components/Forms/FormWrapper"
import RefButton from "components/Forms/RefButton"
/** Context */
import withContext from "context/Dashboard/Feedback/forms/editFeedback/withContext"
import { CtxType } from "context/Dashboard/Feedback/forms/editFeedback/Context"
/** Ref */
import { refSubmit } from "context/Dashboard/Feedback/forms/editFeedback/View"
/** Styles */
import styles from "./styles"

const GeneralInfo: React.FC<CtxType> = ({ setFeedbackDetails, feedbackDetails }) => {
  const { generalInfo, spaceBottom } = styles()
  const { unitUid } = feedbackDetails

  const handleFormChange = (
    setFieldValue: Function,
    key: string,
    value: string | boolean
  ): void =>
    setFieldValue(key, value).then(
      () =>
        setFeedbackDetails &&
        setFeedbackDetails({
          ...feedbackDetails,
          [key]: value
        })
    )

  const handleBlockUnit = (
    setFieldValue: Function,
    value: string | boolean
  ): void => {
    setFieldValue("unitUid", value).then(
      () =>
        setFeedbackDetails &&
        setFeedbackDetails({
          ...feedbackDetails,
          unitUid: value
        })
    )
  }

  return (
    <div>
      <div className={generalInfo}>
        <Formik
          initialValues={{
            unitUid
          }}
          onSubmit={(values: any) => {
            return values
          }}
          validationSchema={Yup.object().shape({
            unitUid: Yup.string().required("Required")
          })}
        >
          {(formikProps: any) => {
            const {
              touched,
              errors,
              handleBlur,
              handleSubmit,
              setFieldValue
            } = formikProps
            return (
              <form>
                <FormWrapper title="General Info" width="40%">
                  <Box width="40%" margin="auto">
                    <Box margin="auto">
                      <Box className={spaceBottom}>
                        <FormGroup>
                          <FormControlLabel
                            control={
                              <Checkbox
                                checked={feedbackDetails.byAdmin}
                                onChange={() =>
                                  handleFormChange(
                                    setFieldValue,
                                    "byAdmin",
                                    !feedbackDetails.byAdmin
                                  )}
                                value={feedbackDetails.byAdmin}
                                color="primary"
                              />
                            }
                            label="Feedback by Admin"
                          />
                        </FormGroup>
                      </Box>
                    </Box>

                    <Box margin="auto">
                      {!feedbackDetails.byAdmin && (
                        <Box className={spaceBottom}>
                          <BlockUnit
                            value={unitUid}
                            onChange={(
                              e: React.ChangeEvent<HTMLInputElement>
                            ): void =>
                              handleBlockUnit(setFieldValue, e.target.value)}
                            onBlur={(e: Event) => handleBlur(e)}
                            error={touched.unitUid && Boolean(errors.unitUid)}
                            helperText={
                              errors.unitUid && touched.unitUid && errors.unitUid
                            }
                          />
                        </Box>
                      )}
                    </Box>
                  </Box>
                </FormWrapper>
                <RefButton refValue={refSubmit} action={handleSubmit} />
              </form>
            )
          }}
        </Formik>
      </div>
    </div>
  )
}

export default withContext(GeneralInfo)
