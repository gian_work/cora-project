import { makeStyles } from "@material-ui/core/styles"

const styles = makeStyles(() => ({
  generalInfo: {
    // width: "40%",
    margin: "0 auto",
    "& .section": {
      borderBottom: "1px solid #F2F2F2",
      marginBottom: "40px",
      // width: "40%",
      margin: "0 auto"
    }
  },
  userOptions: {
    display: "flex",
    flexDirection: "row"
  },
  spaceBottom: {
    paddingBottom: "30px"
  }
}))

export default styles
