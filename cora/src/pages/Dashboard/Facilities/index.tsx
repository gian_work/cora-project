import React, { useState, useEffect } from "react"
import Box from "@material-ui/core/Box"

// Layout
import WithHeader from "layout/WithHeader"
import WithTable from "layout/WithTable"

// components
import Table from "components/Table"
import TabsFilter from "components/Table/components/TabsFilter"
import TableHeader from "components/TableHeader"
import TableFilterMenu from "components/TableFilterMenu"
import PageHeaderTitle from "components/PageHeaderTitle"
import LinkButton from "components/LinkButton"
import FacilitiesCards from "./components/FacilitiesCards"
import AnotherBooking from "./components/AnotherBooking"
import AddBooking from "./components/AddBooking"
import RowDetails from "./components/RowDetails"

// config
import Menu, { MenuIcons } from "./config/Menu"
import TableHeaderColumns from "./config/TableHeaderColumns"
import SampleData from "./sample"

const Facilities: React.FC = () => {
  const [openUserForm, setopenUserForm] = useState(false)
  const [openDetails, setopenDetails] = useState(false)
  const [anotherBooking, setanotherBooking] = useState(false)
  const [tableData, settableData] = useState(SampleData)
  const [activeTable, setactiveTable] = useState("functionroom")
  const [activeTitle, setActiveTitle] = useState("Function Room")
  const [activeStatus, setactiveStatus] = useState("All")
  const [activeRow, setactiveRow] = useState({})

  useEffect(() => {
    window.scrollTo(0, 0)
  }, [])

  // Methods
  const filterData = (value: string, label: string) => {
    settableData(SampleData)
    setactiveTable(value)
    setActiveTitle(label)

    if (activeStatus !== "All") {
      settableData(
        SampleData.filter(
          (i) => i.facType === value && i.status === activeStatus && i
        )
      )
    } else {
      settableData(SampleData.filter((i) => i.facType === value && i))
    }
  }

  const filterActiveTable = (value: string) => {
    setactiveStatus(value)
    // TODO: updated when real data is available
    if (value === "All" && activeTable === "all") {
      settableData(SampleData)
    }

    if (value !== "All" && activeTable === "all") {
      settableData(SampleData.filter((i) => i.status === value && i))
    }

    if (value === "All" && activeTable !== "all") {
      settableData(SampleData.filter((i) => i.facType === activeTable && i))
    }

    if (value !== "All" && activeTable !== "all") {
      settableData(
        SampleData.filter(
          (i) => i.facType === activeTable && i.status === value && i
        )
      )
    }
  }

  const setActiveRow = (state: boolean, details: any) => {
    setopenDetails(state)
    setactiveRow(details)
  }

  const getTrProps = (state: any, rowInfo: any) => {
    if (rowInfo && rowInfo !== undefined && rowInfo.row) {
      return {
        style: {
          backgroundColor: rowInfo.row.status === "Pending" ? "red" : "green",
          color: rowInfo.row.status === "Pending" ? "red" : "green"
        }
      }
    }
    return {}
  }

  useEffect(() => {
    filterData(activeTable, activeTitle)
  }, [])

  return (
    <>
      <WithHeader>
        <Box display="flex" justifyContent="space-between">
          <PageHeaderTitle
            title="Facilities"
            breadcrumbs="property management / facilities"
          />
          <Box>
            <LinkButton name="FACILITIES SETTINGS" action={() => null} />
          </Box>
        </Box>
        <Box display="flex" marginBottom="15px">
          <TableFilterMenu
            menu={Menu}
            filterText={activeTable}
            filterData={filterData}
            activeTable={activeTable}
            filterAll={() => [
              setactiveTable("all"),
              filterData("all", "Applications")
            ]}
          />
        </Box>
        <Box margin="40px 0 30px">
          <FacilitiesCards type={activeTable} />
        </Box>
        <WithTable>
          <TableHeader
            addActionLabel="Add New Booking"
            hasSearch
            title={activeTitle}
            titleIcon={MenuIcons[activeTable]}
            urlLink="facilities/add-new-booking"
            isLink
          />
          <Box paddingLeft="90px" borderBottom="1px solid #F2F2F2">
            <TabsFilter
              options={["All", "Pending", "Cancelled", "Approved", "No Show"]}
              onFilter={filterActiveTable}
            />
          </Box>
          <Table
            data={tableData}
            columns={TableHeaderColumns(setActiveRow)}
            size={10}
            getTrProps={getTrProps}
            isFilterable
          />
        </WithTable>
      </WithHeader>
      <AddBooking
        openUserForm={openUserForm}
        setopenUserForm={setopenUserForm}
        setanotherBooking={setanotherBooking}
      />
      <AnotherBooking openState={anotherBooking} setopenState={setanotherBooking} />
      <RowDetails
        openState={openDetails}
        setopenState={setopenDetails}
        activeRow={activeRow}
      />
    </>
  )
}

export default Facilities
