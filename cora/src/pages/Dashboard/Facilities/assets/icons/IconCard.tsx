import * as React from "react"

// interface
interface IconsCardsProps {
  color: string
}

const IconsCards: React.FC<IconsCardsProps> = ({ color }) => {
  return (
    <svg
      width="20"
      height="20"
      viewBox="0 0 20 20"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M2 0C0.895 0 0 0.895 0 2V4V7C0 8.09306 0.906937 9 2 9H7C8.09306 9 9 8.09306 9 7V3V2C9 0.895 8.105 0 7 0H2ZM13 0C11.895 0 11 0.895 11 2V4V7C11 8.09306 11.9069 9 13 9H18C19.0931 9 20 8.09306 20 7V3V2C20 0.895 19.105 0 18 0H13ZM2 4H7V7H2V4ZM13 4H18V7H13V4ZM2 11C0.895 11 0 11.895 0 13V14V15V18C0 19.0931 0.906937 20 2 20H7C8.09306 20 9 19.0931 9 18V15V14V13C9 11.895 8.105 11 7 11H2ZM13 11C11.895 11 11 11.895 11 13V15V18C11 19.0931 11.9069 20 13 20H18C19.0931 20 20 19.0931 20 18V14V13C20 11.895 19.105 11 18 11H13ZM2 15H7V18H2V15ZM13 15H18V18H13V15Z"
        fill={color}
      />
    </svg>
  )
}

export default IconsCards
