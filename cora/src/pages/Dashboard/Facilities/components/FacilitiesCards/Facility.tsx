import React from "react"
import Box from "@material-ui/core/Box"
import Image from "material-ui-image"
import { makeStyles, Theme } from "@material-ui/core/styles"

const useStyles = makeStyles((theme: Theme) => ({
  container: {
    display: "flex",
    alignItems: "center",
    width: "353px",
    height: "127px",
    overflow: "hidden",
    borderRadius: "10px",
    backgroundColor: theme.palette.common.white
  },
  content: {
    padding: "10px 0"
  },
  imageContainer: {
    borderRadius: "10px",
    overflow: "hidden",
    minWidth: "124px",
    marginRight: "15px"
  },
  nameStyle: {
    color: theme.palette.body.main,
    fontSize: "16px",
    fontWeight: 500,
    lineHeight: "26px"
  },
  total: {
    color: theme.palette.body.secondary,
    fontSize: "14px",
    lineHeight: "20px"
  },
  status: {
    color: theme.palette.body.gray
  }
}))

interface FacilityProps {
  type: string
  data: Record<string, any>
}

const Facility: React.FC<FacilityProps> = ({ data }) => {
  const { container, imageContainer, nameStyle, status } = useStyles()
  const { name, pending, image } = data
  return (
    <Box className={container}>
      <Box className={imageContainer}>
        <Image src={image} aspectRatio={1 / 1} />
      </Box>
      <Box>
        <div>
          <div className={nameStyle}>{name}</div>
        </div>
        <div>
          <div className={status}>{`${pending} Pending`}</div>
        </div>
      </Box>
    </Box>
  )
}

export default Facility
