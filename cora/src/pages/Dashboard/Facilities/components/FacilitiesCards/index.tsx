import React from "react"
import { makeStyles, Theme } from "@material-ui/core/styles"

// component
import Facility from "./Facility"

// assets
import bbqpit01 from "../../assets/bbqpit01.jpg"
import bbqpit02 from "../../assets/bbqpit02.jpg"
import tabletennis01 from "../../assets/tabletennis01.jpg"
import tabletennis02 from "../../assets/tabletennis02.jpg"
import tenniscourt from "../../assets/tenniscourt01.jpg"
import golfsimulatorroom from "../../assets/golfsimulatorroom.jpg"
import diningroom from "../../assets/diningroom.jpg"
import functionroom01 from "../../assets/functionroom1.jpg"
import functionroom02 from "../../assets/functionroom2.jpg"
import basketballcourt01 from "../../assets/basketballcourt01.jpg"
import basketballcourt02 from "../../assets/basketballcourt02.jpg"
import squashcourt01 from "../../assets/squashcourt01.jpg"
import squashcourt02 from "../../assets/squashcourt02.jpg"
import meetingroom01 from "../../assets/meetingroom01.jpg"
import meetingroom02 from "../../assets/meetingroom02.jpg"

const useStyles = makeStyles((theme: Theme) => ({
  titleStyle: {
    fontSize: "16px",
    lineHeight: "24px",
    color: theme.palette.body.main
  },
  facilityContainer: {
    width: "100%",
    overflowX: "auto",
    paddingTop: "30px",
    display: "flex",
    "& > div": {
      marginRight: "30px",
      paddingBottom: "20px"
    }
  }
}))

// interface
interface FacilitiesCardsProps {
  type: string
}

const sampleData: { [index: string]: any } = {
  "bbqpit": [
    {
      "pending": 3,
      "bookings": 131,
      "name": "BBQ Pit 1",
      "image": bbqpit01
    },
    {
      "pending": 5,
      "bookings": 101,
      "name": "BBQ Pit 2",
      "image": bbqpit02
    }
  ],
  "tabletennis": [
    {
      "pending": 3,
      "bookings": 131,
      "name": "Table Tennis 1",
      "image": tabletennis01
    },
    {
      "pending": 5,
      "bookings": 101,
      "name": "Table Tennis 2",
      "image": tabletennis02
    }
  ],
  "tenniscourt": [
    {
      "pending": 3,
      "bookings": 131,
      "name": "Tennis Court 1",
      "image": tenniscourt
    }
  ],
  "golfsimulatorroom": [
    {
      "pending": 3,
      "bookings": 131,
      "name": "Golf Simulator Room 1",
      "image": golfsimulatorroom
    }
  ],
  "diningroom": [
    {
      "pending": 3,
      "bookings": 131,
      "name": "Dining Room",
      "image": diningroom
    }
  ],
  "functionroom": [
    {
      "pending": 3,
      "bookings": 131,
      "name": "Function Room 1",
      "image": functionroom01
    },
    {
      "pending": 3,
      "bookings": 131,
      "name": "Function Room 2",
      "image": functionroom02
    }
  ],
  "basketballcourt": [
    {
      "pending": 3,
      "bookings": 131,
      "name": "Basketball Court 1",
      "image": basketballcourt01
    },
    {
      "pending": 3,
      "bookings": 131,
      "name": "Basketball Court 2",
      "image": basketballcourt02
    }
  ],
  "squashcourt": [
    {
      "pending": 3,
      "bookings": 131,
      "name": "Squash Court 1",
      "image": squashcourt01
    },
    {
      "pending": 3,
      "bookings": 131,
      "name": "Squash Court 2",
      "image": squashcourt02
    }
  ],
  "meetingroom": [
    {
      "pending": 3,
      "bookings": 131,
      "name": "Meeting Room 1",
      "image": meetingroom01
    },
    {
      "pending": 3,
      "bookings": 131,
      "name": "Meeting Room 2",
      "image": meetingroom02
    }
  ]
}

const FacilitiesCards: React.FC<FacilitiesCardsProps> = ({ type }) => {
  const { titleStyle, facilityContainer } = useStyles()
  return (
    <div>
      <div className={titleStyle}>Available for Booking</div>
      <div className={facilityContainer}>
        {Object.keys(sampleData[type]).map((item: any) => {
          return (
            <div>
              <Facility type="bbqpit" data={sampleData[type][item]} />
            </div>
          )
        })}
      </div>
    </div>
  )
}

export default FacilitiesCards
