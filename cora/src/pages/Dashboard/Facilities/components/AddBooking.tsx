import React, { useState } from "react"
import FormControlLabel from "@material-ui/core/FormControlLabel"
import Button from "@material-ui/core/Button"
import Checkbox from "@material-ui/core/Checkbox"
import TextField from "@material-ui/core/TextField"
import Select from "@material-ui/core/Select"
import InputLabel from "@material-ui/core/InputLabel"
import MenuItem from "@material-ui/core/MenuItem"
import FormLabel from "@material-ui/core/FormLabel"
import FormControl from "@material-ui/core/FormControl"
import FormGroup from "@material-ui/core/FormGroup"
import { makeStyles } from "@material-ui/core/styles"

// component
import Box from "@material-ui/core/Box"
import Header from "../../../../components/Header"
import Drawer from "../../../../components/Drawer"

const TestData: {
  [index: string]: { label: string }
} = {
  "bbqpit": {
    label: "BBQ Pit"
  },
  "tabletennis": {
    label: "Table Tennis"
  },
  "squashcourt": {
    label: "Squash Court"
  },
  "tenniscourt": {
    label: "Tennis Court"
  },
  "golfsimulatorroom": {
    label: "Golf Simulator Room"
  },
  "meetingroom": {
    label: "Meeting Room"
  },
  "functionroom": {
    label: "Function Room"
  }
}

const IconArrow = (
  <svg
    width="14"
    height="14"
    viewBox="0 0 14 14"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M6.99967 0.333496C3.32567 0.333496 0.333008 3.32616 0.333008 7.00016C0.333008 10.6742 3.32567 13.6668 6.99967 13.6668C10.6737 13.6668 13.6663 10.6742 13.6663 7.00016C13.6663 3.32616 10.6737 0.333496 6.99967 0.333496ZM6.99967 1.66683C9.95309 1.66683 12.333 4.04675 12.333 7.00016C12.333 9.95358 9.95309 12.3335 6.99967 12.3335C4.04626 12.3335 1.66634 9.95358 1.66634 7.00016C1.66634 4.04675 4.04626 1.66683 6.99967 1.66683ZM7.97103 4.36214L7.02832 5.30485L8.05697 6.3335H3.66634V7.66683H8.05697L7.02832 8.69548L7.97103 9.63818L10.609 7.00016L7.97103 4.36214Z"
      fill="#006CC1"
    />
  </svg>
)

const useStyles = makeStyles(() => ({
  container: {
    height: "100%"
  },
  root: {
    display: "flex",
    "& .MuiTextField-root": {
      flex: 1
    }
  },
  formControl: {
    width: "100%"
  },
  title: {
    // color: theme.palette.blue.title,
    fontWeight: 700,
    fontSize: "16px",
    lineHeight: "24px",
    textTransform: "uppercase"
    // padding: "15px 30px"
  },
  boxMargin: {
    marginBottom: "20px"
  },
  inlineInput: {
    width: "48%"
  },
  formControlButtons: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    width: "100%"
  },
  formControlButton: {
    width: "49%"
  }
}))

interface AddBookingProps {
  openUserForm: boolean
  setopenUserForm: Function
  setanotherBooking: Function
}

const AddBooking: React.FC<AddBookingProps> = ({
  openUserForm,
  setopenUserForm,
  setanotherBooking
}) => {
  const classes = useStyles()

  const [bookForAdmin, setbookForAdmin] = useState(false)
  const [name, setName] = useState("")
  const [remarks, setremarks] = useState("")

  const [state, setState] = useState<Record<string, boolean>>({
    bbqpit: false,
    tabletennis: false,
    squashcourt: false,
    tenniscourt: false,
    goldsimulatorroom: false,
    meetingroom: false,
    functionroom: false
  })

  const handleChange = (value: string) => (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setState({ ...state, [value]: event.target.checked })
  }

  const handleName = (event: React.ChangeEvent<{ value: unknown }>) => {
    setName(event.target.value as string)
  }

  const handleRemarks = (event: React.ChangeEvent<{ value: unknown }>) => {
    setremarks(event.target.value as string)
  }

  const handleCancel = () => {
    return "test"
  }

  return (
    <>
      <Drawer openState={openUserForm} setopenState={setopenUserForm}>
        <Box bgcolor="white" className={classes.container}>
          <Header title="Add New Booking" />

          <Box margin="30px 0 0 0" padding="0 30px" flex="1">
            <Box>
              <FormControl component="fieldset" className={classes.formControl}>
                <FormLabel component="legend">Select Facility</FormLabel>
                {Object.keys(TestData).map((facility) => (
                  <FormGroup>
                    <FormControlLabel
                      control={
                        <Checkbox
                          checked={state[facility]}
                          onChange={handleChange(facility)}
                          value={facility}
                          color="primary"
                        />
                      }
                      label={TestData[facility].label}
                    />
                  </FormGroup>
                ))}
              </FormControl>
            </Box>
            <Box
              display="flex"
              justifyContent="flex-end"
              className={(classes.root, classes.boxMargin)}
            >
              <Button
                size="small"
                color="primary"
                onClick={() => setanotherBooking(true)}
              >
                <Box marginRight="10px" paddingTop="5px">
                  {IconArrow}
                </Box>
                <Box borderBottom="1px solid">Check Availability</Box>
              </Button>
            </Box>

            <Box
              flexDirection="column"
              flex="1"
              paddingTop="30px"
              borderTop="1px solid #F2F2F2"
            >
              <Box className={classes.boxMargin}>
                <FormControlLabel
                  control={
                    <Checkbox
                      checked={bookForAdmin}
                      onChange={() => setbookForAdmin(!bookForAdmin)}
                      value="checkedB"
                      color="primary"
                    />
                  }
                  label="Book for Admin"
                />
              </Box>
              <Box margin="20px 0 0 0" flex="1">
                <Box
                  display="flex"
                  justifyContent="space-between"
                  className={(classes.root, classes.boxMargin)}
                >
                  <TextField
                    label="Block #"
                    variant="outlined"
                    value=""
                    className={classes.inlineInput}
                  />
                  <TextField
                    label="Unit #"
                    variant="outlined"
                    value=""
                    className={classes.inlineInput}
                  />
                </Box>

                <Box className={(classes.root, classes.boxMargin)}>
                  <FormControl variant="outlined" className={classes.formControl}>
                    <InputLabel>Name</InputLabel>
                    <Select key="test" value={name} onChange={handleName}>
                      <MenuItem value="Damian Lillard">Damian Lillard</MenuItem>
                      <MenuItem value="Paul George">Paul George</MenuItem>
                      <MenuItem value="Bradley Beal">Bradley Beal</MenuItem>
                    </Select>
                  </FormControl>
                </Box>

                <Box className={classes.root}>
                  <TextField
                    multiline
                    rows={5}
                    name="Remarks"
                    label="Remarks"
                    autoComplete="off"
                    variant="outlined"
                    value={remarks}
                    onChange={handleRemarks}
                  />
                </Box>

                <Box className={classes.root} margin="20px 0">
                  <FormControl className={classes.formControlButtons}>
                    <Button
                      size="medium"
                      variant="outlined"
                      color="primary"
                      className={classes.formControlButton}
                      onClick={handleCancel}
                    >
                      Set as Pending
                    </Button>
                    <Button
                      size="medium"
                      variant="contained"
                      color="primary"
                      className={classes.formControlButton}
                    >
                      Book Now
                    </Button>
                  </FormControl>
                </Box>
              </Box>
            </Box>
          </Box>
        </Box>
      </Drawer>
    </>
  )
}

export default AddBooking
