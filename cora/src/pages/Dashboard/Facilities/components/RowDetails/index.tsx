import React from "react"

// components
import Drawer from "components/Drawer"
import Header from "components/Header"
import Section from "components/Section"
import Title from "components/Forms/Title"
import Label from "components/Forms/Label"
import Value from "components/Forms/Value"

// styles
import styles from "./styles"

interface RowDetailsProps {
  openState: boolean
  setopenState: Function
  activeRow: Record<string, any>
}

const RowDetails: React.FC<RowDetailsProps> = ({
  openState,
  setopenState,
  activeRow
}) => {
  const { space } = styles()
  const {
    applicationDate,
    blockUnitNo,
    facilityType,
    name,
    bookingDate,
    timings,
    depositDue,
    remarks,
    status
  } = activeRow
  return (
    <Drawer openState={openState} setopenState={setopenState}>
      <Header title={facilityType} subtitle="facilities" subtitleAbove />
      <Section>
        <div>
          <Label label="APPLICATION DATE" />
          <Value val={applicationDate} />
        </div>
      </Section>
      <Section>
        <Title title="Personal Details" />
        <div className={space}>
          <Label label="block/unit no." />
          <Value val={blockUnitNo} />
        </div>
        <div className={space}>
          <Label label="name" />
          <Value val={name} />
        </div>
      </Section>
      <Section>
        <Title title="Booking Details" />
        <div className={space}>
          <Label label="facility type" />
          <Value val={facilityType} />
        </div>
        <div className={space}>
          <Label label="booking date" />
          <Value val={bookingDate} />
        </div>
        <div className={space}>
          <Label label="timings" />
          <Value val={timings} />
        </div>
      </Section>
      <Section>
        <Title title="Payment Details" />
        <div className={space}>
          <Label label="deposit" />
          <Value val={depositDue} />
        </div>
        <div className={space}>
          <Label label="Mode of payment" />
          <Value val="Cash" />
        </div>
      </Section>
      <Section>
        <Title title="Status & Remarks" />
        <div className={space}>
          <Label label="status" />
          <Value val={status} />
        </div>
        <div className={space}>
          <Label label="remarks" />
          <Value val={remarks || "none"} />
        </div>
      </Section>
    </Drawer>
  )
}
export default RowDetails
