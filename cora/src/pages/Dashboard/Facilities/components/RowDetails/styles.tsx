import { makeStyles } from "@material-ui/core/styles"

const styles = makeStyles(() => ({
  space: {
    paddingBottom: "18px"
  }
}))

export default styles
