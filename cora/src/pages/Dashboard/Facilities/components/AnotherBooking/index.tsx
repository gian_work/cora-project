import React, { useState } from "react"
import { makeStyles, Theme } from "@material-ui/core/styles"
import Box from "@material-ui/core/Box"
import Button from "@material-ui/core/Button"
// import Image from "material-ui-image"
import DateFnsUtils from "@date-io/date-fns"
import { MuiPickersUtilsProvider, KeyboardDatePicker } from "@material-ui/pickers"

// component
import Drawer from "components/Drawer"
import BbqPits from "components/Icons/Facilities/BbqPits"
import TableTennis from "components/Icons/Facilities/TableTennis"

import bbqpit01 from "../../assets/bbqpit01.jpg"
import bbqpit02 from "../../assets/bbqpit02.jpg"
import tabletennis01 from "../../assets/tabletennis01.jpg"
import tabletennis02 from "../../assets/tabletennis02.jpg"

const BackIcon = (
  <svg
    width="13"
    height="10"
    viewBox="0 0 13 10"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M5.66667 0.286499L0.953125 5.00004L5.66667 9.71358L6.66667 8.71358L3.61979 5.66671H13V4.33337H3.61979L6.66667 1.2865L5.66667 0.286499Z"
      fill="#646464"
    />
  </svg>
)

const useStyles = makeStyles((theme: Theme) => ({
  section: {
    border: "1px solid rgba(0, 0, 0, 0.1)",
    display: "flex",
    justifyContent: "space-between",
    padding: "25px 45px"
  },
  titleContainer: {
    borderBottom: "1px solid #F2F2F2",
    padding: "20px 30px 35px"
  },
  title: {
    color: theme.palette.body.main,
    fontSize: "24px",
    lineHeight: "29px"
  },
  titleBack: {
    color: theme.palette.body.main,
    fontSize: "12px",
    paddingBottom: "8px",
    cursor: "pointer",
    "& i": {
      marginRight: "10px"
    }
  },
  header: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    padding: "20px 20px",
    borderBottom: "1px solid #F2F2F2"
  },
  headerIcon: {
    width: "44px",
    height: "44px",
    borderRadius: "50%",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "red",
    marginRight: "15px"
  },
  content: {
    display: "flex",
    justifyContent: "space-between",
    padding: "20px 30px",
    borderBottom: "1px solid #F2F2F2"
  },
  imageContainer: {
    width: "330px",
    height: "150px",
    borderRadius: "10px",
    overflow: "hidden",
    marginBottom: "20px",
    position: "relative"
  },
  timeOptions: {
    display: "flex",
    justifyContent: "space-between",
    flexWrap: "wrap",
    width: "330px"
  },
  buttonItem: {
    width: "105px",
    fontSize: "11px",
    padding: "7px 0",
    marginBottom: "7px"
  },
  facility: {
    color: theme.palette.common.white,
    fontSize: "12px",
    fontWeight: 600
  },
  facilityDetails: {
    color: theme.palette.common.white,
    fontSize: "9px"
  },
  facilityInfo: {
    width: "100%",
    height: "50px",
    backgroundColor: "rgba(0, 0, 0, 0.3)",
    position: "absolute",
    bottom: "0",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    padding: "0 0 0 20px"
  },
  //
  root: {
    display: "flex",
    "& .MuiTextField-root": {
      flex: 1
    }
  },
  formControl: {
    width: "100%"
  },
  formControlButtons: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    width: "100%"
  },
  formControlButton: {
    width: "49%"
  }
}))

const timesample = [
  {
    time: "06:00  -  08:00"
  },
  {
    time: "08:00  -  10:00"
  },
  {
    time: "10:00  -  12:00"
  },
  {
    time: "12:00  -  14:00"
  },
  {
    time: "14:00  -  16:00"
  },
  {
    time: "16:00  -  18:00"
  },
  {
    time: "18:00  -  20:00"
  },
  {
    time: "20:00  -  22:00"
  },
  {
    time: "22:00  -  24:00"
  }
]

interface AnotherBookingProps {
  openState: boolean
  setopenState: Function
}

const AnotherBooking: React.FC<AnotherBookingProps> = ({
  openState,
  setopenState
}) => {
  const {
    header,
    headerIcon,
    content,
    imageContainer,
    timeOptions,
    buttonItem,
    facility,
    facilityDetails,
    facilityInfo,
    formControlButtons,
    formControlButton,
    title,
    titleContainer,
    titleBack
  } = useStyles()

  const [selectedDate, setSelectedDate] = useState<Date | null>(new Date(Date.now()))

  const handleDateChange = (date: Date | null) => {
    setSelectedDate(date)
  }

  return (
    <Drawer
      openState={openState}
      setopenState={setopenState}
      width={768}
      closeBtnDark
    >
      <Box className={titleContainer}>
        <Box className={titleBack} onClick={() => setopenState(false)}>
          <i>{BackIcon}</i>
          Back
        </Box>
        <Box className={title}>Facilities</Box>
      </Box>
      {/* BBQ PITS */}
      <Box>
        <Box className={header}>
          <Box display="flex" alignItems="center">
            <Box className={headerIcon}>
              <BbqPits />
            </Box>
            <Box component="span">BBQ Pits</Box>
          </Box>
          <Box>
            <MuiPickersUtilsProvider utils={DateFnsUtils}>
              <KeyboardDatePicker
                inputVariant="outlined"
                format="MM/dd/yyyy"
                margin="normal"
                label="Date"
                value={selectedDate}
                onChange={handleDateChange}
                KeyboardButtonProps={{
                  "aria-label": "change date"
                }}
              />
            </MuiPickersUtilsProvider>
          </Box>
        </Box>

        <Box display="flex" className={content}>
          <Box>
            <Box display="flex" flexDirection="column" width="48%">
              <Box className={imageContainer}>
                <img src={bbqpit01} alt="BBQ Pit 1" />

                <Box className={facilityInfo}>
                  <Box className={facility}>BBQ Pit 5</Box>
                  <Box className={facilityDetails}>Capacity: 20 People</Box>
                </Box>
              </Box>

              <Box className={timeOptions}>
                {timesample.map((item) => (
                  <Button variant="outlined" color="primary" className={buttonItem}>
                    {item.time}
                  </Button>
                ))}
              </Box>
            </Box>
          </Box>

          <div style={{ width: "20px", height: "20px" }} />

          <Box>
            <Box display="flex" flexDirection="column" width="48%">
              <Box className={imageContainer}>
                <img src={bbqpit02} alt="BBQ Pit 2" />
                {/* <Image
                  aspectRatio={16 / 9}
                  src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22800%22%20height%3D%22400%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20800%20400%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_15ba800aa1d%20text%20%7B%20fill%3A%23555%3Bfont-weight%3Anormal%3Bfont-family%3AHelvetica%2C%20monospace%3Bfont-size%3A40pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_15ba800aa1d%22%3E%3Crect%20width%3D%22800%22%20height%3D%22400%22%20fill%3D%22%23777%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%22285.921875%22%20y%3D%22218.3%22%3EFirst%20slide%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E"
                /> */}
                <Box className={facilityInfo}>
                  <Box className={facility}>BBQ Pit 4</Box>
                  <Box className={facilityDetails}>Capacity: 30 People</Box>
                </Box>
              </Box>

              <Box className={timeOptions}>
                {timesample.map((item) => (
                  <Button variant="outlined" color="primary" className={buttonItem}>
                    {item.time}
                  </Button>
                ))}
              </Box>
            </Box>
          </Box>
        </Box>
      </Box>
      {/* End of BBQ PITS */}
      {/* Table Tennis */}
      <Box>
        <Box className={header}>
          <Box display="flex" alignItems="center">
            <Box
              className={headerIcon}
              style={{ backgroundColor: "rgb(80, 215, 87)" }}
            >
              <TableTennis />
            </Box>
            <Box component="span">Table Tennis</Box>
          </Box>
          <Box>
            <MuiPickersUtilsProvider utils={DateFnsUtils}>
              <KeyboardDatePicker
                inputVariant="outlined"
                format="MM/dd/yyyy"
                margin="normal"
                label="Date"
                value={selectedDate}
                onChange={handleDateChange}
                KeyboardButtonProps={{
                  "aria-label": "change date"
                }}
              />
            </MuiPickersUtilsProvider>
          </Box>
        </Box>

        <Box display="flex" className={content}>
          <Box>
            <Box display="flex" flexDirection="column" width="48%">
              <Box className={imageContainer}>
                <img src={tabletennis01} alt="Table Tennis 1" />
                {/* <Image
                  aspectRatio={16 / 9}
                  src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22800%22%20height%3D%22400%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20800%20400%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_15ba800aa1d%20text%20%7B%20fill%3A%23555%3Bfont-weight%3Anormal%3Bfont-family%3AHelvetica%2C%20monospace%3Bfont-size%3A40pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_15ba800aa1d%22%3E%3Crect%20width%3D%22800%22%20height%3D%22400%22%20fill%3D%22%23777%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%22285.921875%22%20y%3D%22218.3%22%3EFirst%20slide%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E"
                /> */}
                <Box className={facilityInfo}>
                  <Box className={facility}>Table Tennis 1</Box>
                  <Box className={facilityDetails}>Capacity: 4 People</Box>
                </Box>
              </Box>

              <Box className={timeOptions}>
                {timesample.map((item) => (
                  <Button variant="outlined" color="primary" className={buttonItem}>
                    {item.time}
                  </Button>
                ))}
              </Box>
            </Box>
          </Box>

          <div style={{ width: "20px", height: "20px" }} />

          <Box>
            <Box display="flex" flexDirection="column" width="48%">
              <Box className={imageContainer}>
                <img src={tabletennis02} alt="Table Tennis 2" />
                {/* <Image
                  aspectRatio={16 / 9}
                  src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22800%22%20height%3D%22400%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20800%20400%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_15ba800aa1d%20text%20%7B%20fill%3A%23555%3Bfont-weight%3Anormal%3Bfont-family%3AHelvetica%2C%20monospace%3Bfont-size%3A40pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_15ba800aa1d%22%3E%3Crect%20width%3D%22800%22%20height%3D%22400%22%20fill%3D%22%23777%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%22285.921875%22%20y%3D%22218.3%22%3EFirst%20slide%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E"
                /> */}
                <Box className={facilityInfo}>
                  <Box className={facility}>Table Tennis 2</Box>
                  <Box className={facilityDetails}>Capacity: 4 People</Box>
                </Box>
              </Box>

              <Box className={timeOptions}>
                {timesample.map((item) => (
                  <Button variant="outlined" color="primary" className={buttonItem}>
                    {item.time}
                  </Button>
                ))}
              </Box>
            </Box>
          </Box>
        </Box>
      </Box>
      {/* End of Table Tennis */}

      {/* CTA */}
      <Box className={content} margin="20px 0">
        <Box className={formControlButtons}>
          <Button
            size="medium"
            variant="outlined"
            color="primary"
            className={formControlButton}
            onClick={() => "test"}
          >
            Clear Selected
          </Button>
          <Button
            size="medium"
            variant="contained"
            color="primary"
            className={formControlButton}
          >
            Submit
          </Button>
        </Box>
      </Box>
    </Drawer>
  )
}

export default AnotherBooking
