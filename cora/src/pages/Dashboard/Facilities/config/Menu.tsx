import BbqPits from "components/Icons/Facilities/BbqPits"
import SquashCourt from "components/Icons/Facilities/SquashCourt"
import BasketballCourt from "components/Icons/Facilities/BasketballCourt"
import TableTennis from "components/Icons/Facilities/TableTennis"
import TennisCourt from "components/Icons/Facilities/TennisCourt"
import FunctionRoom from "components/Icons/Facilities/FunctionRoom"
import GolfSimulatorRoom from "components/Icons/Facilities/GolfSimulatorRoom"
import DiningRoom from "components/Icons/Facilities/DiningRoom"
import MeetingRoom from "components/Icons/Facilities/MeetingRoom"

export const Menu = [
  {
    label: "Function Room",
    icon: FunctionRoom,
    value: "functionroom",
    color: "#21B6BF"
  },
  {
    label: "BBQ Pits",
    icon: BbqPits,
    value: "bbqpit",
    color: "#8392A7"
  },
  {
    label: "Squash Courts",
    icon: SquashCourt,
    value: "squashcourt",
    color: "#9EB7FF"
  },
  {
    label: "Golf Simulator Room",
    icon: GolfSimulatorRoom,
    value: "golfsimulatorroom",
    color: "#FF808B"
  },
  {
    label: "Tennis Courts",
    icon: TennisCourt,
    value: "tenniscourt",
    color: "#50D757"
  },
  {
    label: "Table Tennis",
    icon: TableTennis,
    value: "tabletennis",
    color: "#18A20C"
  },
  {
    label: "Dining Room",
    icon: DiningRoom,
    value: "diningroom",
    color: "#EAC91E"
  },
  {
    label: "Meeting Room",
    icon: MeetingRoom,
    value: "meetingroom",
    color: "#204B82"
  },
  {
    label: "Basketball Courts",
    icon: BasketballCourt,
    value: "basketballcourt",
    color: "#EA671E"
  }
]

const MenuIcons: {
  [index: string]: {}
} = {
  "bbqpit": BbqPits,
  "squashcourt": SquashCourt,
  "basketballcourt": BasketballCourt,
  "tabletennis": TableTennis,
  "tenniscourt": TennisCourt,
  "functionroom": FunctionRoom,
  "golfsimulatorroom": GolfSimulatorRoom,
  "diningroom": DiningRoom,
  "meetingroom": MeetingRoom
}

export default Menu

export { MenuIcons }
