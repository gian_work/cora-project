import React from "react"

import StatusBlock from "components/Table/components/StatusBlock"
import ContextMenu from "components/ContextMenu"
import ColorStatus from "utils/status"
import ContextFacilities from "../components/ContextMenu"

const TableHeaderColumns = (setActiveRow: Function) => [
  {
    fixed: "left",
    disableFilters: true,
    columns: [
      {
        width: 70,
        Cell: ({ row }: any) => {
          return (
            <ContextMenu setopenDetails={() => null} openDetails>
              <ContextFacilities setDetailsActive={setActiveRow} activeRow={row} />
            </ContextMenu>
          )
        },
        filterable: false
      }
    ]
  },
  {
    columns: [
      {
        Header: "Status",
        id: "status",
        accessor: "status",
        sortable: false,
        filterable: false,
        width: 150,
        Cell: ({ original }: any) => (
          <StatusBlock
            status={original.status}
            color={ColorStatus.code(original.status)}
          />
        )
      },
      {
        Header: "Application Date",
        id: "applicationDate",
        accessor: "applicationDate",
        width: 200
      },
      {
        Header: "Block/Unit No.",
        id: "blockUnitNo",
        accessor: "blockUnitNo",
        width: 150
      },
      {
        Header: "Name",
        id: "name",
        accessor: "name",
        width: 200
      },
      {
        Header: "Facility Type",
        id: "facilityType",
        accessor: "type",
        width: 150
      },
      {
        Header: "Booking Date",
        id: "bookingDate",
        accessor: "bookingDate",
        width: 150
      },
      {
        Header: "Timings",
        id: "timings",
        accessor: "timings",
        width: 150
      },
      {
        Header: "Deposit Due",
        id: "depositDue",
        accessor: "depositDue",
        width: 150
      },
      {
        Header: "Usage Fee Due",
        id: "usageFeeDue",
        accessor: "usageFeeDue",
        width: 150
      },
      {
        Header: "Remarks",
        id: "remarks",
        accessor: "remarks",
        width: 150
      }
    ]
  }
]

export default TableHeaderColumns
