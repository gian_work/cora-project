/* eslint global-require: "warn"*/

import React, { useState } from "react"
import DateFnsUtils from "@date-io/date-fns"
import { MuiPickersUtilsProvider, KeyboardDatePicker } from "@material-ui/pickers"

// styles
import styles from "./styles"

// assets
import bbqpit01 from "../../../../../assets/bbqpit01.jpg"
import bbqpit02 from "../../../../../assets/bbqpit02.jpg"
import tabletennis01 from "../../../../../assets/tabletennis01.jpg"
import tabletennis02 from "../../../../../assets/tabletennis02.jpg"
import tenniscourt from "../../../../../assets/tenniscourt01.jpg"
import golfsimulatorroom from "../../../../../assets/golfsimulatorroom.jpg"
import diningroom from "../../../../../assets/diningroom.jpg"
import functionroom01 from "../../../../../assets/functionroom1.jpg"
import functionroom02 from "../../../../../assets/functionroom2.jpg"
import basketballcourt01 from "../../../../../assets/basketballcourt01.jpg"
import basketballcourt02 from "../../../../../assets/basketballcourt02.jpg"

const SampleTimes = [
  "7:00 - 8:00",
  "8:00 - 9:00",
  "9:00 - 10:00",
  "10:00 - 11:00",
  "11:00 - 12:00",
  "12:00 - 1:00",
  "1:00 - 2:00"
]

const facilityTypes: {
  [index: string]: { name: string; items: Record<string, any> }
} = {
  "bbqpit": {
    name: "BBQ Pit",
    items: [
      {
        name: "BBQ Pit 1",
        status: "open",
        location: "Blk-18-02",
        capacity: "60",
        image: bbqpit01,
        availableTime: ["7:00-8:00", "8:00-9:00", "9:00-10:00", "10:00-11:00"]
      },
      {
        name: "BBQ Pit 2",
        status: "open",
        location: "Blk-19-02",
        capacity: "40",
        image: bbqpit02,
        availableTime: ["7:00-8:00", "8:00-9:00", "9:00-10:00", "10:00-11:00"]
      }
    ]
  },
  "tabletennis": {
    name: "Table Tennis",
    items: [
      {
        name: "Table Tennis 1",
        status: "open",
        location: "Blk-18-04",
        capacity: "10",
        image: tabletennis01,
        availableTime: ["10:00-11:00", "11:00-12:00", "12:00-1:00", "1:00-12:00"]
      },
      {
        name: "Table Tennis 2",
        status: "open",
        location: "Blk-18-05",
        capacity: "6",
        image: tabletennis02,
        availableTime: ["10:00-11:00", "11:00-12:00", "12:00-1:00", "1:00-12:00"]
      }
    ]
  },
  "tenniscourt": {
    name: "Tennis Court",
    items: [
      {
        name: "Tennis Court 1",
        status: "open",
        location: "Blk-18-04",
        capacity: "10",
        image: tenniscourt,
        availableTime: ["10:00-11:00", "11:00-12:00", "12:00-1:00", "1:00-12:00"]
      }
    ]
  },
  "golfsimulatorroom": {
    name: "Golf Simulator Room",
    items: [
      {
        name: "Golf Simulator Room 1",
        status: "open",
        location: "Blk-18-04",
        capacity: "10",
        image: golfsimulatorroom,
        availableTime: ["10:00-11:00", "11:00-12:00", "12:00-1:00", "1:00-12:00"]
      }
    ]
  },
  "diningroom": {
    name: "Dining Room",
    items: [
      {
        name: "Dining Room 1",
        status: "open",
        location: "Blk-18-04",
        capacity: "10",
        image: diningroom,
        availableTime: ["10:00-11:00", "11:00-12:00", "12:00-1:00", "1:00-12:00"]
      }
    ]
  },
  "functionroom": {
    name: "Function Room",
    items: [
      {
        name: "Function Room 1",
        status: "open",
        location: "Blk-18-04",
        capacity: "10",
        image: functionroom01,
        availableTime: ["10:00-11:00", "11:00-12:00", "12:00-1:00", "1:00-12:00"]
      },
      {
        name: "Function Room 2",
        status: "open",
        location: "Blk-18-04",
        capacity: "10",
        image: functionroom02,
        availableTime: ["10:00-11:00", "11:00-12:00", "12:00-1:00", "1:00-12:00"]
      }
    ]
  },
  "basketballcourt": {
    name: "Basketball Court",
    items: [
      {
        name: "Basketball Court 1",
        status: "open",
        location: "Blk-18-04",
        capacity: "10",
        image: basketballcourt01,
        availableTime: ["10:00-11:00", "11:00-12:00", "12:00-1:00", "1:00-12:00"]
      },
      {
        name: "Basketball Court 2",
        status: "open",
        location: "Blk-18-04",
        capacity: "10",
        image: basketballcourt02,
        availableTime: ["10:00-11:00", "11:00-12:00", "12:00-1:00", "1:00-12:00"]
      }
    ]
  }
}

interface FacilityItemProps {
  type: string
  item: any
}

const FacilityItem: React.FC<FacilityItemProps> = ({ item }) => {
  const { card, cardImage, cardInfo, cardTimeSlots } = styles()
  const { capacity, image, location, name, status } = item

  return (
    <div className={card}>
      <div className={cardImage}>
        <img src={image} alt="facility " />
      </div>
      <div className={cardInfo}>
        <div className="name">{name}</div>
        <div className="status">{status}</div>
        <div className="infoFooter">
          <div>
            Location:
            {location}
          </div>
          <div>
            Capacity:
            {`${capacity} pax`}
          </div>
        </div>
      </div>
      <div className={cardTimeSlots}>
        <div className="title">Available Time Slots</div>
        <div className="schedules">
          {SampleTimes.map((sampleItem) => (
            <div className="schedule">{sampleItem}</div>
          ))}
        </div>
      </div>
    </div>
  )
}

interface FacilityListProps {
  type: string
}

const FacilityList: React.FC<FacilityListProps> = ({ type }) => {
  const { container, headerBlock, facilityBlock } = styles()
  const [selectedDate, setSelectedDate] = useState<Date | null>(new Date(Date.now()))

  const handleDateChange = (date: Date | null) => {
    setSelectedDate(date)
  }

  return (
    <div>
      {facilityTypes[type] !== undefined ? (
        <div className={facilityBlock}>
          <div className={headerBlock}>
            <div className="name">{facilityTypes[type].name}</div>
            <div>
              <MuiPickersUtilsProvider utils={DateFnsUtils}>
                <KeyboardDatePicker
                  inputVariant="outlined"
                  format="MM/dd/yyyy"
                  margin="normal"
                  label="Date"
                  value={selectedDate}
                  onChange={handleDateChange}
                  KeyboardButtonProps={{
                    "aria-label": "change date"
                  }}
                />
              </MuiPickersUtilsProvider>
            </div>
          </div>
          <div className={container}>
            {Object.keys(facilityTypes[type].items).map((item) => {
              return (
                <FacilityItem type={type} item={facilityTypes[type].items[item]} />
              )
            })}
          </div>
        </div>
      ) : null}
    </div>
  )
}
export default FacilityList
