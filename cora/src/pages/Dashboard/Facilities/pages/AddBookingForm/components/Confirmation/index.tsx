import React from "react"

import styles from "./styles"

// component
import FacilityItem from "../common/FacilityItem"
import bbqpit01 from "../../../../assets/bbqpit01.jpg"
import tabletennis01 from "../../../../assets/tabletennis01.jpg"

const Confirmation: React.FC = () => {
  const {
    container,
    facilitySection,
    labelStyle,
    valueStyle,
    spaceBottom
  } = styles()

  return (
    <div className={container}>
      <div>
        <div>
          <div className={facilitySection}>
            <FacilityItem
              type="bbqpit"
              hasReceipt
              data={{
                name: "BBQ Pit 1",
                image: bbqpit01,
                receiptNo: "0987654-1"
              }}
            />
          </div>
          <div className={facilitySection}>
            <FacilityItem
              type="bbqpit"
              hasReceipt
              data={{
                name: "Table Tennis 1",
                image: tabletennis01,
                receiptNo: "2233456-1"
              }}
            />
          </div>
          <div className={facilitySection}>
            <div
              style={{
                display: "flex",
                alignItems: "center"
              }}
            >
              <div style={{ width: "253px", marginRight: "30px" }}>Total</div>
              <div className={valueStyle}>$ 00.00</div>
            </div>
          </div>
        </div>
      </div>
      <div className="bookingForm">
        <div>
          <div className={spaceBottom}>
            <div className="title">Booking Details</div>
          </div>

          <div className={spaceBottom}>
            <div className={labelStyle}>Personal Details</div>
            <div className={valueStyle}>Marcus Dorwart</div>
          </div>

          <div className={spaceBottom}>
            <div className={labelStyle}>Block/Unit No.</div>
            <div className={valueStyle}>18#01-01</div>
          </div>

          <div className={spaceBottom}>
            <div className={labelStyle}>Remarks</div>
            <div className={valueStyle}>This is for the homeowners party. </div>
          </div>
        </div>

        <div className={spaceBottom} />
        {/* Payment */}
        <div>
          <div className={spaceBottom}>
            <div className="title">Payment Details</div>
          </div>

          <div className={spaceBottom}>
            <div className={labelStyle}>payment description</div>
            <div className={valueStyle}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit.
              {" "}
            </div>
          </div>
          <div className={spaceBottom}>
            <div className={labelStyle}>mode of payment.</div>
            <div className={valueStyle}>Cash</div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Confirmation
