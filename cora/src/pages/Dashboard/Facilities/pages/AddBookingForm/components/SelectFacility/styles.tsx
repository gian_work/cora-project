import { makeStyles, Theme } from "@material-ui/core/styles"

const styles = makeStyles((theme: Theme) => ({
  container: {
    padding: "24px 50px"
  },
  menus: {
    display: "flex",
    flexWrap: "wrap",
    alignItems: "center",
    padding: "15px 0 30px  0"
  },
  buttonContainer: {
    position: "relative",
    background: "rgba(33,33,33,0.08)",
    border: "1px solid rgba(0, 0, 0, 0.12)",
    color: theme.palette.body.main,
    borderRadius: "16px",
    height: "32px",
    display: "flex",
    alignItems: "center",
    cursor: "pointer",
    width: "auto",
    margin: "0 15px 10px 0",
    outline: "none",
    "&.active": {
      background: theme.palette.body.orange,
      color: theme.palette.common.white
    }
  },
  name: {
    fontWeight: 500,
    fontSize: "13px",
    lineHeight: "20px",
    // color: theme.palette.common.white,
    flex: 1,
    padding: "0 16px",
    textAlign: "center",
    "&.active": {
      color: theme.palette.common.white
    }
  },
  mediumWidth: {
    minWidth: "140px"
  },
  smallWidth: {
    minWidth: "95px"
  },
  largeWidth: {
    minWidth: "180px"
  }
}))

export default styles
