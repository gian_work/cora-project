import React from "react"

// styles
import styles from "../BookingDetails/styles"

interface FacilityItemProps {
  type: string
  data: Record<string, any>
  hasReceipt?: boolean
}

const FacilityItem: React.FC<FacilityItemProps> = ({ data, hasReceipt }) => {
  const {
    facilityContainer,
    facilityImage,
    facilityDetails,
    labelStyle,
    valueStyle,
    twoCol
  } = styles()

  const { name, image, receiptNo } = data

  return (
    <div className={facilityContainer}>
      <div className={facilityImage}>
        <img src={image} alt="Facility" />
      </div>
      <div className={facilityDetails}>
        <div style={{ marginBottom: "15px" }}>
          <div className={labelStyle}>facility</div>
          <div className={valueStyle}>{name}</div>
        </div>
        <div className={twoCol}>
          <div>
            <div className={labelStyle}>date</div>
            <div className={valueStyle}>01/17/2020</div>
          </div>
          <div>
            <div className={labelStyle}>time</div>
            <div className={valueStyle}>17:00-22:00</div>
          </div>
        </div>
        <div className={hasReceipt ? "" : twoCol} style={{ marginTop: "20px" }}>
          <div>
            {hasReceipt && (
              <div className={labelStyle}>{`receipt no. ${receiptNo}`}</div>
            )}
            <div className={labelStyle}>deposit</div>
            <div className={valueStyle}>$ 00.00</div>
          </div>
          {hasReceipt && <div style={{ width: "20px", height: "20px" }} />}
          <div>
            {hasReceipt && (
              <div className={labelStyle}>{`receipt no. ${receiptNo}`}</div>
            )}
            <div className={labelStyle}>usage fee</div>
            <div className={valueStyle}>$ 00.00</div>
          </div>
        </div>
      </div>
    </div>
  )
}
export default FacilityItem
