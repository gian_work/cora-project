import React from "react"
import TextField from "@material-ui/core/TextField"
import FormControlLabel from "@material-ui/core/FormControlLabel"
import Select from "@material-ui/core/Select"
import MenuItem from "@material-ui/core/MenuItem"
import FormControl from "@material-ui/core/FormControl"
import Box from "@material-ui/core/Box"
import Radio from "@material-ui/core/Radio"
import RadioGroup from "@material-ui/core/RadioGroup"

import styles from "./styles"

// component
import FacilityItem from "../common/FacilityItem"

// asset
import bbqpit01 from "../../../../assets/bbqpit01.jpg"
import tabletennis01 from "../../../../assets/tabletennis01.jpg"

const BookingDetails: React.FC = () => {
  const { container, facilitySection, labelStyle, spaceBottom } = styles()
  const [name, setName] = React.useState("")
  const [remarks, setremarks] = React.useState("")
  const [modePayment, setmodePayment] = React.useState("1")

  const Spacer = () => <div style={{ width: "20px", height: "20px" }} />

  const handleModePayment = (event: React.ChangeEvent<{ value: unknown }>) => {
    setmodePayment(event.target.value as string)
  }

  const handleName = (event: React.ChangeEvent<{ value: unknown }>) => {
    setName(event.target.value as string)
  }

  const handleRemarks = (event: React.ChangeEvent<{ value: unknown }>) => {
    setremarks(event.target.value as string)
  }

  return (
    <div className={container}>
      <div>
        <div>
          <div className={facilitySection}>
            <FacilityItem
              hasReceipt={false}
              type="bbqpit"
              data={{
                name: "BBQ Pit 1",
                image: bbqpit01,
                receiptNo: "0987654-1"
              }}
            />
          </div>
          <div className={facilitySection}>
            <FacilityItem
              hasReceipt={false}
              type="tabletennis"
              data={{
                name: "Table Tennis 1",
                image: tabletennis01,
                receiptNo: "2233456-1"
              }}
            />
          </div>
        </div>
      </div>
      <div className="bookingForm">
        <div className={spaceBottom}>
          <div className="title">Payment Details</div>
        </div>
        <Box>
          <Box className={labelStyle}>Payment Description</Box>
          <TextField
            multiline
            rows={2}
            placeholder="Details"
            fullWidth
            variant="outlined"
          />
        </Box>
        <Spacer />
        <Box>
          <FormControl variant="outlined">
            <Select displayEmpty value={name} onChange={handleName}>
              <MenuItem value="" disabled>
                Payment Category
              </MenuItem>
              <MenuItem value="Payment Category 1">Payment Category 1</MenuItem>
              <MenuItem value="Payment Category 2">Payment Category 2</MenuItem>
              <MenuItem value="Payment Category 3">Payment Category 3</MenuItem>
            </Select>
          </FormControl>
        </Box>
        <Spacer />
        <Box>
          <FormControl variant="outlined">
            <Select displayEmpty value={name} onChange={handleName}>
              <MenuItem value="" disabled>
                Payment Type
              </MenuItem>
              <MenuItem value="Payment Category 1">Payment Type 1</MenuItem>
              <MenuItem value="Payment Category 2">Payment Type 2</MenuItem>
              <MenuItem value="Payment Category 3">Payment Type 3</MenuItem>
            </Select>
          </FormControl>
        </Box>
        <Spacer />
        <Box>
          <Box className={labelStyle}>Amount</Box>
          <TextField placeholder="$ 00.00" fullWidth variant="outlined" />
        </Box>
        <Spacer />
        <Box>
          <Box className={labelStyle}>Tax</Box>
          <TextField placeholder="$ 00.00" fullWidth variant="outlined" />
        </Box>
        <Spacer />
        <Box>
          <Box className={labelStyle}>mode of payment</Box>
          <FormControl>
            <RadioGroup
              aria-label="credit type"
              name="creditType"
              value={modePayment}
              onChange={handleModePayment}
              style={{ flexDirection: "row" }}
            >
              <FormControlLabel
                value="1"
                control={<Radio color="primary" />}
                label="Cash"
                labelPlacement="end"
              />
              <FormControlLabel
                value="2"
                control={<Radio color="primary" />}
                label="Cheque"
                labelPlacement="end"
              />
              <FormControlLabel
                value="3"
                control={<Radio color="primary" />}
                label="E-transfer"
                labelPlacement="end"
              />
            </RadioGroup>
          </FormControl>
        </Box>
        {/* Cheque */}
        <>
          {modePayment === "2" && (
            <Box>
              <Spacer />
              <Box>
                <Box>
                  <Box className={labelStyle}>bank name</Box>
                  <TextField placeholder="Bank Name" fullWidth variant="outlined" />
                </Box>
                <Spacer />
                <Box>
                  <Box className={labelStyle}>cheque no.</Box>
                  <TextField
                    placeholder="123-456-7890"
                    fullWidth
                    variant="outlined"
                  />
                </Box>
              </Box>
              <Spacer />
            </Box>
          )}
        </>
        {/* E-transfer */}
        <>
          {modePayment === "3" && (
            <Box>
              <Spacer />
              <Box>
                <Box>
                  <Box className={labelStyle}>transfer agent</Box>
                  <TextField placeholder="Agent Name" fullWidth variant="outlined" />
                </Box>
                <Spacer />
                <Box>
                  <Box className={labelStyle}>transfer id</Box>
                  <TextField
                    placeholder="123-456-7890"
                    fullWidth
                    variant="outlined"
                  />
                </Box>
              </Box>
              <Spacer />
            </Box>
          )}
        </>
        <Spacer />
        <Box>
          <Box className={labelStyle}>Remarks</Box>
          <TextField
            multiline
            rows={3}
            name="Remarks"
            label="Remarks"
            autoComplete="off"
            variant="outlined"
            value={remarks}
            onChange={handleRemarks}
          />
        </Box>
      </div>
    </div>
  )
}

export default BookingDetails
