import { makeStyles, Theme } from "@material-ui/core/styles"

const styles = makeStyles((theme: Theme) => ({
  container: {
    padding: "0 0 30px 0",
    display: "flex",
    overflow: "auto"
  },
  facilityBlock: {
    borderTop: "1px solid #F2F2F2",
    padding: "30px 0"
  },
  headerBlock: {
    display: "flex",
    alignItems: "flex-start",
    justifyContent: "space-between",
    padding: "0px 0 20px",
    "& > .name": {
      fontSize: "24px",
      lineHeight: "32px",
      fontWeight: "bold",
      color: theme.palette.body.main
    }
  },
  card: {
    maxWidth: "315px",
    // maxHeight: "382px",
    width: "315px",
    // height: "382px",
    backgroundColor: theme.palette.common.white,
    borderRadius: "10px",
    border: "1px solid #EDF0F1",
    overflow: "hidden",
    marginRight: "25px"
  },
  cardImage: {
    maxHeight: "140px",
    overflow: "hidden",
    "& > img": {
      width: "100%"
    }
  },
  cardInfo: {
    borderBottom: "1px solid #F2F2F2",
    padding: "5px 15px 10px",
    "& > .name": {
      color: theme.palette.body.main,
      fontSize: "20px",
      lineHeight: "32px",
      fontWeight: "bold"
    },
    "& > .status": {
      color: theme.palette.body.secondary,
      fontSize: "14px",
      textTransform: "capitalize"
    },
    "& > .infoFooter": {
      display: "flex",
      alignItems: "center",
      justifyContent: "space-between",
      color: theme.palette.body.secondary,
      fontSize: "14px",
      paddingTop: "10px"
    }
  },
  cardTimeSlots: {
    padding: "15px 15px 0",
    "& > .title": {
      color: theme.palette.body.main,
      fontSize: "12px"
    },
    "& > .schedules": {
      width: "100%",
      overflow: "auto",
      display: "flex",
      flexWrap: "wrap",
      margin: "12px 0 10px"
    },
    "& .schedule": {
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      minWidth: "84px",
      minHeight: "32px",
      background: "rgba(119, 126, 134, 0.08)",
      border: "1px solid rgba(69, 75, 87, 0.12)",
      boxSizing: "border-box",
      borderRadius: "16px",
      fontSize: "12px",
      margin: "0 8px 10px 0",
      cursor: "pointer"
    },
    "& > .reserve": {
      color: theme.palette.primary.main,
      fontSize: "14px",
      fontWeight: 600,
      letterSpacing: "0.0125em",
      textTransform: "uppercase",
      cursor: "pointer",
      outline: "none"
    }
  }
}))

export default styles
