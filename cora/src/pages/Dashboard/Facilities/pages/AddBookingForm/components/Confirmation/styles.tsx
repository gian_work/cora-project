import { makeStyles, Theme } from "@material-ui/core/styles"

const styles = makeStyles((theme: Theme) => ({
  container: {
    display: "flex",
    padding: "24px 40px 0 0",
    "& > .bookingForm": {
      borderLeft: "1px solid #F2F2F2",
      padding: "0 0 0 50px",
      minWidth: "367px",
      display: "flex",
      flexDirection: "column"
    },
    "& .title": {
      fontSize: "20px",
      fontWeight: "bolder",
      color: theme.palette.body.main
    }
  },
  labelStyle: {
    fontSize: "10px",
    marginBottom: "8px",
    color: theme.palette.body.secondary,
    textTransform: "uppercase"
  },
  valueStyle: {
    fontSize: "16px",
    color: theme.palette.body.main
  },
  spaceBottom: {
    marginBottom: "25px"
  },
  facilityContainer: {
    display: "flex"
  },
  facilityImage: {
    minWidth: "253px",
    minHeight: "193px",
    width: "253px",
    height: "193px",
    borderRadius: "10px",
    overflow: "hidden",
    marginRight: "30px"
  },
  facilityDetails: {
    paddingRight: "50px",
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-between",
    width: "273px"
  },
  facilitySection: {
    padding: "40px 20px 40px 60px",
    // marginBottom: "40px",
    borderBottom: "1px solid #F2F2F2"
  },
  twoCol: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between"
  }
}))

export default styles
