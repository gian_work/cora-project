import React, { useState } from "react"
import Checkbox from "@material-ui/core/Checkbox"
import TextField from "@material-ui/core/TextField"
import FormControlLabel from "@material-ui/core/FormControlLabel"
import Select from "@material-ui/core/Select"
import MenuItem from "@material-ui/core/MenuItem"
import FormControl from "@material-ui/core/FormControl"

import styles from "./styles"

// component
import FacilityItem from "../common/FacilityItem"

// asset
import bbqpit01 from "../../../../assets/bbqpit01.jpg"
import tabletennis01 from "../../../../assets/tabletennis01.jpg"

const BookingDetails: React.FC = () => {
  const { container, facilitySection, labelStyle, spaceBottom } = styles()
  const [bookForAdmin, setbookForAdmin] = useState(false)
  const [name, setName] = React.useState("")
  const [remarks, setremarks] = React.useState("")

  const handleName = (event: React.ChangeEvent<{ value: unknown }>) => {
    setName(event.target.value as string)
  }

  const handleRemarks = (event: React.ChangeEvent<{ value: unknown }>) => {
    setremarks(event.target.value as string)
  }
  return (
    <div className={container}>
      <div>
        <div>
          <div className={facilitySection}>
            <FacilityItem
              hasReceipt={false}
              type="bbqpit"
              data={{
                name: "BBQ Pit 1",
                image: bbqpit01,
                receiptNo: "0987654-1"
              }}
            />
          </div>
          <div className={facilitySection}>
            <FacilityItem
              hasReceipt={false}
              type="tabletennis"
              data={{
                name: "Table Tennis 1",
                image: tabletennis01,
                receiptNo: "2233456-1"
              }}
            />
          </div>
        </div>
      </div>
      <div className="bookingForm">
        <div className={spaceBottom}>
          <div className="title">Booking Details</div>
        </div>
        <div className={spaceBottom}>
          <FormControlLabel
            control={
              <Checkbox
                checked={bookForAdmin}
                onChange={() => setbookForAdmin(!bookForAdmin)}
                value="checkedB"
                color="primary"
              />
            }
            label="Book for Admin"
          />
        </div>

        <div className={spaceBottom}>
          <div className={labelStyle}>Personal Details</div>
          <FormControl variant="outlined">
            <Select displayEmpty value={name} onChange={handleName}>
              <MenuItem value="" disabled>
                Select Name
              </MenuItem>
              <MenuItem value="Payment type 1">Resident 1</MenuItem>
              <MenuItem value="Payment type 2">Resident 2</MenuItem>
              <MenuItem value="Payment type 3">Resident 3</MenuItem>
            </Select>
          </FormControl>
        </div>

        <div className={spaceBottom}>
          <div className={labelStyle}>Block/Unit No.</div>
          <TextField placeholder="00#00-00" fullWidth variant="outlined" />
        </div>

        <div className={spaceBottom}>
          <div className={labelStyle}>Remarks</div>
          <TextField
            multiline
            rows={5}
            name="Remarks"
            autoComplete="off"
            variant="outlined"
            value={remarks}
            onChange={handleRemarks}
          />
        </div>
      </div>
    </div>
  )
}

export default BookingDetails
