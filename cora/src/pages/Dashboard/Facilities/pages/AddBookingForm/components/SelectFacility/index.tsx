import React, { useContext } from "react"
import styles from "./styles"

// config
import Menu from "../../../../config/Menu"

// context
import { AddBookingContext } from "../.."

// component
import FacilityList from "./FacilityList"

const SelectFacility: React.FC = () => {
  const { activeFacilities, setactiveFacilities } = useContext(AddBookingContext)
  const { buttonContainer, container, menus, name } = styles()

  const handleFilter = (item: string) => {
    return activeFacilities && activeFacilities.includes(item)
      ? setactiveFacilities &&
          setactiveFacilities(activeFacilities.filter((value) => value !== item))
      : setactiveFacilities && setactiveFacilities([...activeFacilities, item])
  }

  return (
    <div className={container}>
      {/* Filter Menu */}
      <div className={menus}>
        {Menu.map((item: Record<string, any>) => {
          return (
            <div
              role="button"
              tabIndex={0}
              className={`${buttonContainer} ${
                activeFacilities && activeFacilities.includes(item.value)
                  ? "active"
                  : "inactive"
              }`}
              onClick={() => handleFilter(item.value)}
            >
              <span className={name}>{item.label}</span>
            </div>
          )
        })}
      </div>
      {/* Facility */}
      <div>
        {activeFacilities?.map((item) => (
          <FacilityList type={item} />
        ))}
      </div>
    </div>
  )
}

export default SelectFacility
