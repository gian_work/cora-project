const ApplicationsData = [
  {
    "applicationDate": "18/09/2019",
    "blockUnitNo": "21/#00-00",
    "name": "SOH SAI BOR (SU CAIBO)",
    "type": "FUNCTION ROOM 1",
    "bookingDate": "18/09/2019",
    "timings": "17:00 - 22:00",
    "status": "Pending",
    "depositDue": "US$ 100.00",
    "usageFeeDue": "US$ 20.00",
    "remarks": "NA",
    "facType": "functionroom"
  },
  {
    "applicationDate": "19/09/2019",
    "blockUnitNo": "21/#00-00",
    "name": "SOH SAI BOR (SU CAIBO)",
    "type": "BBQ PIT 1",
    "bookingDate": "19/09/2019",
    "timings": "17:00 - 22:01",
    "status": "Pending",
    "depositDue": "US$ 100.00",
    "usageFeeDue": "US$ 10.00",
    "remarks": "NA",
    "facType": "bbqpit"
  },
  {
    "applicationDate": "20/09/2019",
    "blockUnitNo": "23/#00-00",
    "name": "MANAS JOSHI",
    "type": "FUNCTION ROOM 2",
    "bookingDate": "20/09/2019",
    "timings": "17:00 - 22:02",
    "status": "Pending",
    "depositDue": "US$ 100.00",
    "usageFeeDue": "NA",
    "remarks": "NA",
    "facType": "functionroom"
  },
  {
    "applicationDate": "21/09/2019",
    "blockUnitNo": "25/#00-00",
    "name": "JIN SHAN ZI",
    "type": "FUNCTION ROOM",
    "bookingDate": "21/09/2019",
    "timings": "17:00 - 22:03",
    "status": "Pending",
    "depositDue": "US$ 100.00",
    "usageFeeDue": "NA",
    "remarks": "NA",
    "facType": "functionroom"
  },
  {
    "applicationDate": "22/09/2019",
    "blockUnitNo": "25/#00-00",
    "name": "JIN SHAN ZI",
    "type": "FUNCTION ROOM",
    "bookingDate": "22/09/2019",
    "timings": "17:00 - 22:04",
    "status": "Cancelled",
    "depositDue": "NA",
    "usageFeeDue": "NA",
    "remarks": "NA",
    "facType": "functionroom"
  },
  {
    "applicationDate": "23/09/2019",
    "blockUnitNo": "27/#00-00",
    "name": "BALACHANDRAN MURALI KRISHNAN",
    "type": "FUNCTION ROOM",
    "bookingDate": "23/09/2019",
    "timings": "17:00 - 22:05",
    "status": "Cancelled",
    "depositDue": "NA",
    "usageFeeDue": "NA",
    "remarks": "NA",
    "facType": "functionroom"
  },
  {
    "applicationDate": "24/09/2019",
    "blockUnitNo": "29/#00-00",
    "name": "JAYAKRISHNAN BALACHANDRAN",
    "type": "FUNCTION ROOM",
    "bookingDate": "24/09/2019",
    "timings": "17:00 - 22:06",
    "status": "Cancelled",
    "depositDue": "NA",
    "usageFeeDue": "NA",
    "remarks": "NA",
    "facType": "functionroom"
  },
  {
    "applicationDate": "25/09/2019",
    "blockUnitNo": "31/#00-00",
    "name": "ALBERT LEONG KUM HOONG",
    "type": "FUNCTION ROOM",
    "bookingDate": "25/09/2019",
    "timings": "17:00 - 22:07",
    "status": "Cancelled",
    "depositDue": "NA",
    "usageFeeDue": "NA",
    "remarks": "NA",
    "facType": "functionroom"
  },
  {
    "applicationDate": "26/09/2019",
    "blockUnitNo": "31/#00-00",
    "name": "ALBERT LEONG KUM HOONG",
    "type": "FUNCTION ROOM",
    "bookingDate": "26/09/2019",
    "timings": "17:00 - 22:08",
    "status": "Cancelled",
    "depositDue": "NA",
    "usageFeeDue": "NA",
    "remarks": "NA",
    "facType": "functionroom"
  },
  {
    "applicationDate": "27/09/2019",
    "blockUnitNo": "33/#00-00",
    "name": "JOANNA TAN SHIN YI",
    "type": "KTV ROOM",
    "bookingDate": "27/09/2019",
    "timings": "17:00 - 22:09",
    "status": "Pending",
    "depositDue": "PAID",
    "usageFeeDue": "US$ 20.00",
    "remarks": "NA",
    "facType": "ktvroom"
  },
  {
    "applicationDate": "28/09/2019",
    "blockUnitNo": "33/#00-00",
    "name": "JOANNA TAN SHIN YI",
    "type": "FUNCTION ROOM",
    "bookingDate": "28/09/2019",
    "timings": "17:00 - 22:10",
    "status": "Pending",
    "depositDue": "PAID",
    "usageFeeDue": "US$ 5.00",
    "remarks": "NA",
    "facType": "functionroom"
  },
  {
    "applicationDate": "29/09/2019",
    "blockUnitNo": "33/#00-00",
    "name": "JOHNATHON LIN TING, LAM TENG",
    "type": "FUNCTION ROOM",
    "bookingDate": "29/09/2019",
    "timings": "17:00 - 22:11",
    "status": "Pending",
    "depositDue": "PAID",
    "usageFeeDue": "PAID",
    "remarks": "NA",
    "facType": "functionroom"
  },
  {
    "applicationDate": "30/09/2019",
    "blockUnitNo": "35/#00-00",
    "name": "ANG CHI YUEN",
    "type": "KTV ROOM",
    "bookingDate": "30/09/2019",
    "timings": "17:00 - 22:12",
    "status": "Pending",
    "depositDue": "PAID",
    "usageFeeDue": "PAID",
    "remarks": "NA",
    "facType": "ktvroom"
  },
  {
    "applicationDate": "01/10/2019",
    "blockUnitNo": "35/#00-00",
    "name": "ANG CHI YUEN",
    "type": "FUNCTION ROOM",
    "bookingDate": "01/10/2019",
    "timings": "17:00 - 22:13",
    "status": "Approved",
    "depositDue": "NA",
    "usageFeeDue": "NA",
    "remarks": "NA",
    "facType": "functionroom"
  },
  {
    "applicationDate": "02/10/2019",
    "blockUnitNo": "35/#00-00",
    "name": "ANG CHI YUEN",
    "type": "FUNCTION ROOM",
    "bookingDate": "02/10/2019",
    "timings": "17:00 - 22:14",
    "status": "Approved",
    "depositDue": "NA",
    "usageFeeDue": "NA",
    "remarks": "NA",
    "facType": "functionroom"
  },
  {
    "applicationDate": "03/10/2019",
    "blockUnitNo": "37/#00-00",
    "name": "TAN SIANG HUA",
    "type": "FUNCTION ROOM",
    "bookingDate": "03/10/2019",
    "timings": "17:00 - 22:15",
    "status": "Approved",
    "depositDue": "NA",
    "usageFeeDue": "NA",
    "remarks": "NA",
    "facType": "functionroom"
  },
  {
    "applicationDate": "04/10/2019",
    "blockUnitNo": "37/#00-00",
    "name": "TAN SIANG HUA",
    "type": "KTV ROOM",
    "bookingDate": "04/10/2019",
    "timings": "17:00 - 22:16",
    "status": "Approved",
    "depositDue": "NA",
    "usageFeeDue": "NA",
    "remarks": "NA",
    "facType": "ktvroom"
  },
  {
    "applicationDate": "05/10/2019",
    "blockUnitNo": "39/#00-00",
    "name": "UCCHINO FABRIZIO CARMELO",
    "type": "FUNCTION ROOM",
    "bookingDate": "05/10/2019",
    "timings": "17:00 - 22:17",
    "status": "Approved",
    "depositDue": "NA",
    "usageFeeDue": "NA",
    "remarks": "NA",
    "facType": "functionroom"
  },
  {
    "applicationDate": "06/10/2019",
    "blockUnitNo": "39/#00-00",
    "name": "BAPTISTE GRANGER",
    "type": "CAR LABEL",
    "bookingDate": "06/10/2019",
    "timings": "17:00 - 22:18",
    "status": "Approved",
    "depositDue": "NA",
    "usageFeeDue": "NA",
    "remarks": "NA",
    "facType": "carlabel"
  }
]

export default ApplicationsData
