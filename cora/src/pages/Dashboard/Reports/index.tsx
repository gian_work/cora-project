import React from "react"

import Provider from "context/Dashboard/Reports/main/Provider"
import View from "context/Dashboard/Reports/main/View"

export default () => (
  <Provider>
    <View />
  </Provider>
)
