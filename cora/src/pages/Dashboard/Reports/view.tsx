import React from "react"
import Box from "@material-ui/core/Box"
import { makeStyles } from "@material-ui/core/styles"

/** Components */
import Spacer from "components/Spacer"
import Attendance from "./components/Attendance"
import Application from "./components/Application"
import RegisteredItem from "./components/RegisteredItem"
import TotalRegisteredUnits from "./components/TotalRegisteredUnits"
import TotalRegisteredUsers from "./components/TotalRegisteredUsers"
import TotalTenantedUnits from "./components/TotalTenantedUnits"
import OutstandingPayments from "./components/OutstandingPayments"
import VisitorManagement from "./components/VisitorManagement"

import TotalAmountCollected from "./components/TotalAmountCollected"
import Feedback from "./components/Feedback"

const useStyles = makeStyles(() => ({
  colContainer: {
    width: "33.33%"
  }
}))

interface Props {
  printRef: any
}

const Reports: React.FC<Props> = ({ printRef }) => {
  const { colContainer } = useStyles()

  const spaceHeight = "23px"
  return (
    <>
      {/* <Box
        display="flex"
        alignItems="flex-start"
        justifyContent="space-between"
        paddingBottom="20px"
      >
        <PageHeaderTitle
          title="Report"
          breadcrumbs="property management / Report"
        />
        <CTA ele={ele} refElement={refReport} />
      </Box> */}
      <div ref={printRef} style={{ width: "100%" }}>
        <Box display="flex">
          <Box display="flex" flex="2">
            <Box flex="1" paddingRight="23px">
              <TotalRegisteredUnits total="866" percentage="86.6%" />
              <Spacer height={spaceHeight} />
              <RegisteredItem
                title="Registered Vehicles"
                body="As of January 17, 2020"
                total="350"
              />
              <Spacer height={spaceHeight} />
              <RegisteredItem
                title="Registered Service Providers"
                body="As of January 17, 2020"
                total="75"
              />
              <Spacer height={spaceHeight} />
              <OutstandingPayments total="135,095.09" />
              <Spacer height={spaceHeight} />
              <VisitorManagement total="154" />
            </Box>
            <Box flex="1" paddingRight="23px">
              <TotalTenantedUnits total="385" percentage="38.5%" />
              <Spacer height={spaceHeight} />
              <TotalAmountCollected total="1,876,298.58 " />
              <Spacer height={spaceHeight} />
              <Application total="498" />
            </Box>
          </Box>
          <Box flex="1">
            <TotalRegisteredUsers total="1,357" />
            <Spacer height={spaceHeight} />
            <Attendance total="35" />
          </Box>
        </Box>
        <Spacer height={spaceHeight} />
        <Box display="flex">
          <Box paddingRight="23px" className={colContainer} />
          <Box className={colContainer} />
        </Box>
        <Spacer height={spaceHeight} />
        <Box>
          <Feedback />
        </Box>
      </div>
    </>
  )
}

export default Reports
