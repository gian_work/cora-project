const SummaryConfig: any = {
  facilities: {
    "title": "Facilities",
    "subtitle": "Total Facility Booking"
  },
  applications: {
    "title": "Application",
    "subtitle": "Total Application Requests"
  }
}

export const attendanceMock = [
  {
    name: "BBQ Pits",
    total: 27
  }
]

export const applicationsMock = [
  {
    name: "Moving in/out",
    total: 76
  },
  {
    name: "Pet Registration",
    total: 8
  },
  {
    name: "Bulky Delivery",
    total: 4
  },
  {
    name: "Access Card",
    total: 39
  },
  {
    name: "Aircon Cleaning",
    total: 5
  },
  {
    name: "Bike Tag",
    total: 3
  }
]

export default SummaryConfig
