import React from "react"
import Chart from "react-apexcharts"

// styles
import styles from "./styles"

const lineSeries = [
  {
    name: "Cleanliness",
    data: [15, 30, 6, 6]
  },
  {
    name: "Security",
    data: [17, 32, 8, 8]
  },
  {
    name: "Defects",
    data: [19, 34, 10, 10]
  },
  {
    name: "Landscape",
    data: [25, 10, 30, 40]
  },
  {
    name: "Others",
    data: [35, 14, 10, 20]
  }
]
const lineOptions = {
  chart: {
    height: 350,
    type: "line",
    zoom: {
      enabled: false
    },
    toolbar: {
      show: false
    }
  },
  dataLabels: {
    enabled: false
  },
  stroke: {
    curve: "straight",
    width: 1.8
  },
  markers: {
    shape: "circle",
    size: 5
  },
  colors: ["#09707B", "#A56300", "#D8B469", "#0E61A2", "#454B57"],
  legend: {
    show: true,
    position: "top"
  },
  grid: {
    row: {
      colors: ["#f3f3f3", "transparent"], // takes an array which will be repeated on columns
      opacity: 0.5
    }
  },
  xaxis: {
    categories: ["1st Week", "2nd Week", "3rd Week", "4th Week"]
  }
}

const FeedbackResponse: React.FC = () => {
  const { title, tfrInfo } = styles()
  return (
    <div>
      <div className={tfrInfo}>
        <div className={title}>Feedback Response Time per category</div>
      </div>
      <Chart
        options={lineOptions}
        series={lineSeries}
        type="line"
        height={350}
        width="100%"
      />
    </div>
  )
}
export default FeedbackResponse
