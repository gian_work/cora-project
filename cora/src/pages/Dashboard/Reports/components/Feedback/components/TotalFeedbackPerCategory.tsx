import React from "react"
import Chart from "react-apexcharts"

// styles
import styles from "./styles"

const chartOptions = {
  options: {
    labels: ["Cleanliness", "Security", "Defects", "Landscape", "Others"],
    legend: {
      position: "top"
    },
    plotOptions: {
      pie: {
        donut: {
          size: "30%"
        }
      }
    },
    colors: ["#09707B", "#A56300", "#D8B469", "#0E61A2", "#454B57"]
  }
}

const chartSeries = [27, 53, 78, 47, 29]

const TotalFeedbackPerCategory: React.FC = () => {
  const { title, tfrInfo } = styles()
  return (
    <div>
      <div className={tfrInfo}>
        <div className={title}>Total Feedback per Category</div>
      </div>
      <div style={{ width: "100%", display: "flex", justifyContent: "center" }}>
        <Chart
          options={chartOptions.options}
          series={chartSeries}
          type="donut"
          width="400px"
        />
      </div>
    </div>
  )
}
export default TotalFeedbackPerCategory
