import * as React from "react"
import { makeStyles, Theme } from "@material-ui/core/styles"
import Card from "@material-ui/core/Card"
import Box from "@material-ui/core/Box"
import Chart from "react-apexcharts"

import CardTitle from "components/CardTitle"

const Icon = (
  <svg
    width="20"
    height="21"
    viewBox="0 0 20 21"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M4 0V2H18V12H20V2C20 0.895 19.105 0 18 0H4ZM2 4C0.9 4 0.00976562 4.9 0.00976562 6L0 21L4 17H14C15.1 17 16 16.1 16 15V6C16 4.9 15.1 4 14 4H2ZM2 6H14V15H4H3.17188L2.58594 15.5859L2 16.1719V6Z"
      fill="#454B57"
    />
  </svg>
)

const chartOptions = {
  options: {
    labels: ["Cleanliness", "Security", "Defects", "Landscape", "Others"],
    plotOptions: {
      pie: {
        donut: {
          size: "30%"
        }
      }
    },
    colors: ["#62CB77", "#EC4B19", "#057686", "#204B82", "#FFCD2C"]
    // fill: {
    //   colors: ["#62CB77", "#EC4B19", "#057686", "#204B82", "#FFCD2C", "#006CC1"]
    // }
  }
}
const chartSeries = [44, 55, 41, 17, 15]

const lineSeries = [
  {
    name: "Cleanliness",
    data: [15, 30, 6, 6]
  },
  {
    name: "Security",
    data: [17, 32, 8, 8]
  },
  {
    name: "Defects",
    data: [19, 34, 10, 10]
  },
  {
    name: "Landscape",
    data: [25, 10, 30, 40]
  },
  {
    name: "Others",
    data: [35, 14, 10, 20]
  }
]
const lineOptions = {
  chart: {
    height: 350,
    type: "line",
    zoom: {
      enabled: false
    },
    toolbar: {
      show: false
    }
  },
  dataLabels: {
    enabled: false
  },
  stroke: {
    curve: "straight",
    width: 1.8
  },
  markers: {
    shape: "circle",
    size: 5
  },
  colors: ["#62CB77", "#EC4B19", "#057686", "#204B82", "#FFCD2C"],
  legend: {
    show: true,
    position: "top"
  },
  grid: {
    row: {
      colors: ["#f3f3f3", "transparent"], // takes an array which will be repeated on columns
      opacity: 0.5
    }
  },
  xaxis: {
    categories: ["1st Week", "2nd Week", "3rd Week", "4th Week"]
  }
}

const useStyles = makeStyles((theme: Theme) => ({
  totalStyle: {
    color: "#006157",
    fontSize: "48px",
    fontWeight: 500
  },
  body: {
    color: theme.palette.body.secondary,
    fontSize: "14px",
    paddingTop: "20px"
  }
}))

// interface
interface TotalRegisteredUnitsProps {
  total: string
}

const TotalRegisteredUnits: React.FC<TotalRegisteredUnitsProps> = ({ total }) => {
  const { body, totalStyle } = useStyles()
  return (
    <Card>
      <Box
        display="flex"
        alignItems="center"
        justifyContent="space-between"
        padding="16px 23px"
        borderBottom="1px solid #F2F2F2"
      >
        <CardTitle title="Total Feedback Received" icon={Icon} />
      </Box>

      <Box
        display="flex"
        flexDirection="column"
        justifyContent="space-between"
        padding="16px 23px"
      >
        <Box className={totalStyle}>{total}</Box>
      </Box>

      <Box
        alignItems="center"
        justifyContent="space-between"
        padding="26px 30px"
        borderTop="1px solid #EDF0F1"
      >
        <Box className={body} marginBottom="20px">
          Feedback Response Time
        </Box>
        <Chart
          options={lineOptions}
          series={lineSeries}
          type="line"
          height={350}
          width="100%"
        />
      </Box>

      <Box
        alignItems="center"
        justifyContent="space-between"
        padding="26px 30px"
        borderTop="1px solid #EDF0F1"
      >
        <Box className={body}>Feedback Category</Box>
        <Chart
          options={chartOptions.options}
          series={chartSeries}
          type="donut"
          width="100%"
        />
      </Box>
    </Card>
  )
}

export default TotalRegisteredUnits
