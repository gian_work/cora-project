import * as React from "react"
import { makeStyles, Theme } from "@material-ui/core/styles"
import Card from "@material-ui/core/Card"
import Box from "@material-ui/core/Box"

import CardTitle from "components/CardTitle"

const Icon = (
  <svg
    width="16"
    height="19"
    viewBox="0 0 16 19"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M4 0V1H0V3H1V19H15V3H16V1H12V0H4ZM3 3H13V17H3V3ZM5 5V7H7V5H5ZM9 5V7H11V5H9ZM5 9V11H7V9H5ZM9 9V11H11V9H9ZM5 13V15H7V13H5ZM9 13V15H11V13H9Z"
      fill="#454B57"
    />
  </svg>
)

const useStyles = makeStyles((theme: Theme) => ({
  title: {
    color: theme.palette.primary.main,
    fontSize: "16px",
    fontWeight: 500
  },
  total: {
    color: theme.palette.body.greenLight,
    fontSize: "48px",
    fontWeight: 500
  },
  body: {
    color: theme.palette.body.secondary,
    fontSize: "14px"
  },
  percentage: {
    color: theme.palette.body.greenLight,
    fontSize: "14px"
  },
  bar: {
    height: "18px",
    borderRadius: "20px",
    backgroundColor: "#F2F2F2"
  },
  barHighlight: {
    height: "18px",
    width: "82%",
    background: theme.palette.body.greenLight,
    borderRadius: "20px"
  }
}))

// interface
interface TotalRegisteredUnitsProps {
  total: string
  percentage: string
}

const TotalRegisteredUnits: React.FC<TotalRegisteredUnitsProps> = ({
  total,
  percentage
}) => {
  const classes = useStyles()
  return (
    <Card>
      <Box
        display="flex"
        alignItems="center"
        justifyContent="space-between"
        padding="16px 23px"
        borderBottom="1px solid #F2F2F2"
      >
        <CardTitle title="Total Registered Units" icon={Icon} />
      </Box>

      <Box
        display="flex"
        alignItems="center"
        justifyContent="space-between"
        padding="16px 23px"
      >
        <Box className={classes.total}>{total}</Box>
        <Box>
          <Box className={classes.percentage}>{percentage}</Box>
          <Box className={classes.body}>of total units</Box>
        </Box>
      </Box>

      <Box className={classes.bar} margin="0 23px">
        <Box className={classes.barHighlight} />
      </Box>

      <Box
        display="flex"
        alignItems="center"
        justifyContent="space-between"
        padding="16px 23px"
      >
        <Box className={classes.body} />
        <Box className={classes.body}>Total Units: 1000 </Box>
      </Box>
    </Card>
  )
}

export default TotalRegisteredUnits
