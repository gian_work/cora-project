import React from "react"
import Box from "@material-ui/core/Box"
import { makeStyles, Theme } from "@material-ui/core/styles"
import ReactToPrint from "react-to-print"

/** Components */
import DatePicker from "components/Forms/DatePicker"

/** Assets */
import IconPrint from "../../icons"

const useStyles = makeStyles((theme: Theme) => ({
  container: {
    display: "flex",
    justifyContent: "flex-end"
  },
  printButton: {
    color: theme.palette.primary.main,
    fontWeight: 500,
    fontSize: "14px",
    cursor: "pointer"
  }
}))

// interface
interface CTAProps {
  refElement: any
  ele: any
  activeDate: any
  setActiveDate: Function
}

const CTA: React.FC<CTAProps> = ({ refElement, ele, activeDate, setActiveDate }) => {
  const { container, printButton } = useStyles()
  return (
    <Box className={container}>
      <Box>
        <DatePicker
          label=""
          name="activeDate"
          format="MMM dd yyyy"
          value={activeDate}
          placeholder="mm/dd/yyyy"
          handleDateChange={(value: string): void => setActiveDate(value)}
          inlineStyles={{
            minHeight: "40px",
            backgroundColor: "#FFFFFF",
            minWidth: "215px",
            width: "215px"
          }}
        />
      </Box>
      <ReactToPrint
        trigger={() => (
          <Box display="flex" alignItems="center" marginLeft="15px">
            {IconPrint}
            <Box marginLeft="15px" className={printButton}>
              PRINT REPORT
            </Box>
          </Box>
        )}
        content={() => (refElement.current ? refElement.current : ele)}
      />
    </Box>
  )
}

export default CTA
