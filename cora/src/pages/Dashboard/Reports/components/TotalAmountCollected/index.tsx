import * as React from "react"
import { makeStyles, Theme } from "@material-ui/core/styles"
import Card from "@material-ui/core/Card"
import Box from "@material-ui/core/Box"

import CardTitle from "components/CardTitle"
import FilterWithInfo from "components/FilterButtons/FilterWithInfo"

const Icon = (
  <svg
    width="21"
    height="22"
    viewBox="0 0 21 22"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M10.5003 0.0292969C9.53169 0.0292969 8.56371 0.3525 7.79753 1H4.77279L5.86963 7.22852C4.08039 8.91712 0.916992 12.5262 0.916992 17C0.916992 19.002 1.50018 20.5926 2.6521 21.7266L2.92912 22H18.0715L18.3486 21.7266C19.4995 20.5916 20.0837 19.002 20.0837 17C20.0837 12.5305 16.9222 8.9208 15.1329 7.23047L16.2297 1H13.2031C12.4369 0.3525 11.469 0.0292969 10.5003 0.0292969ZM10.5003 2.02344C11.1093 2.02344 11.7176 2.25334 12.1699 2.71484L12.4507 3H13.9275L13.3997 6H7.60099L7.07316 3H8.54997L8.83073 2.71484C9.28306 2.25334 9.8913 2.02344 10.5003 2.02344ZM7.95288 8H13.0496C14.0846 8.861 18.167 12.539 18.167 17C18.167 18.283 17.8759 19.269 17.2798 20H11.002V19.0879C11.2981 19.0489 12.9692 18.7627 12.9692 16.6387C12.9692 13.7137 9.94533 14.2735 9.94629 12.4785C9.94629 11.4505 10.424 11.418 10.6051 11.418C10.8371 11.418 11.3295 11.6142 11.3295 12.8672H12.9823C12.9823 10.4692 11.4109 10.097 11.1311 10.041V8.71289H10.2495V10.0176C9.95339 10.0566 8.29354 10.3817 8.29354 12.4707C8.29354 15.5187 11.3183 14.6164 11.3183 16.6504C11.3183 17.6684 10.6868 17.6934 10.5583 17.6934C10.4414 17.6934 9.67114 17.7572 9.67114 16.1992H8.01839C8.01839 18.8382 9.85285 19.0489 10.126 19.0879V20H3.71899C3.12387 19.269 2.83366 18.283 2.83366 17C2.83366 12.552 6.91788 8.863 7.95288 8Z"
      fill="#454B57"
    />
  </svg>
)

const IconDeposit = () => (
  <svg
    width="20"
    height="19"
    viewBox="0 0 20 19"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M10.0001 0C7.23912 0 5.00012 2.239 5.00012 5C5.00012 7.761 7.23912 10 10.0001 10C12.7611 10 15.0001 7.761 15.0001 5C15.0001 2.239 12.7611 0 10.0001 0ZM10.0001 2C11.6571 2 13.0001 3.343 13.0001 5C13.0001 6.657 11.6571 8 10.0001 8C8.34312 8 7.00012 6.657 7.00012 5C7.00012 3.343 8.34312 2 10.0001 2ZM10.4083 3L8.50989 3.77734V4.81445L9.30286 4.50781V7H10.6622V3H10.4083ZM0.00012207 6V17C0.00012207 18.103 0.897122 19 2.00012 19H18.0001C19.1031 19 20.0001 18.103 20.0001 17V6H18.0001V17H2.00012V6H0.00012207ZM6.00012 12L10.0001 16L14.0001 12H6.00012Z"
      fill="white"
    />
  </svg>
)

const IconPayments = () => (
  <svg
    width="22"
    height="16"
    viewBox="0 0 22 16"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M2.00012 0C0.907059 0 0.00012207 0.906937 0.00012207 2V14C0.00012207 15.0931 0.907059 16 2.00012 16H20.0001C21.0932 16 22.0001 15.0931 22.0001 14V2C22.0001 0.906937 21.0932 0 20.0001 0H2.00012ZM2.00012 2H14.715V3.0293C14.376 3.0723 12.4767 3.42875 12.4767 5.71875C12.4767 9.05975 15.9357 8.07078 15.9357 10.3008C15.9357 11.4168 15.2155 11.4434 15.0685 11.4434C14.9345 11.4434 14.0529 11.5136 14.0529 9.80664H12.1622C12.1622 12.6986 14.2623 12.9277 14.5743 12.9707V14H2.00012V2ZM15.7228 2H20.0001V14H15.5763V12.9707C15.9153 12.9277 17.8263 12.6141 17.8263 10.2871C17.8253 7.08111 14.3654 7.69356 14.3654 5.72656C14.3654 4.59956 14.9872 4.56445 15.1212 4.56445C15.3662 4.56445 15.9493 4.7813 15.9493 6.1543H17.84C17.84 3.5263 16.0438 3.11764 15.7228 3.05664V2ZM4.00012 5V7H10.0001V5H4.00012ZM4.00012 9V11H9.00012V9H4.00012Z"
      fill="white"
    />
  </svg>
)

const useStyles = makeStyles((theme: Theme) => ({
  timelineTitle: {
    color: theme.palette.body.main,
    fontSize: "14px"
  },
  timelineSubtitle: {
    color: theme.palette.body.secondary,
    fontSize: "12px"
  },
  dot: {
    width: "8px",
    height: "8px",
    backgroundColor: "#D14339",
    position: "absolute",
    top: "5px",
    left: "-25px",
    borderRadius: "50%"
  },
  totalStyle: {
    color: theme.palette.body.green,
    fontSize: "48px",
    fontWeight: 500,
    "& span": {
      fontSize: "10px",
      paddingRight: "5px"
    }
  },
  revenue: {
    color: theme.palette.body.dark,
    fontSize: "14px"
  }
}))

// interface
interface RemindersProps {
  total: string
}

const TotalAmountCollected: React.FC<RemindersProps> = ({ total }) => {
  const { totalStyle, revenue } = useStyles()

  return (
    <Card>
      <Box height="365px">
        <Box
          display="flex"
          alignItems="center"
          padding="16px 23px"
          borderBottom="1px solid #F2F2F2"
        >
          <CardTitle title="Total Amount Collected" icon={Icon} />
        </Box>

        <Box
          display="flex"
          flexDirection="column"
          justifyContent="space-between"
          padding="10px 23px 16px 80px"
          borderBottom="1px solid #F2F2F2"
        >
          <Box className={totalStyle}>
            <span>PHP</span>
            {total}
          </Box>
          <Box className={revenue}>Revenue Streams</Box>
        </Box>

        <Box display="flex" flexDirection="column" height="50%">
          <Box
            display="flex"
            // alignItems="center"
            // justifyContent="center"
            paddingLeft="25%"
            flex="1"
            borderBottom="1px solid #F2F2F2"
            height="50%"
          >
            <FilterWithInfo
              currency="PHP"
              name="939,149"
              info="Deposits"
              color="#007B83"
              icon={IconDeposit}
              type="deposits"
              action={() => null}
              size="small"
              center
              style={{ minHeight: "auto", padding: 0, margin: 0 }}
            />
          </Box>
          <Box
            display="flex"
            // alignItems="center"
            // justifyContent="center"
            paddingLeft="25%"
            flex="1"
            height="50%"
          >
            <FilterWithInfo
              currency="PHP"
              name="937,149.58"
              info="Payments"
              color="#D8B469"
              icon={IconPayments}
              type="payments"
              action={() => null}
              size="small"
              center
              style={{ minHeight: "auto", padding: 0, margin: 0 }}
            />
          </Box>
        </Box>
      </Box>
    </Card>
  )
}

export default TotalAmountCollected
