import * as React from "react"
import { makeStyles, Theme } from "@material-ui/core/styles"
import Card from "@material-ui/core/Card"
import Box from "@material-ui/core/Box"

import CardTitle from "components/CardTitle"

const useStyles = makeStyles((theme: Theme) => ({
  container: {
    backgroundColor: theme.palette.common.white,
    minHeight: "101px"
  },
  title: {
    color: theme.palette.primary.main,
    fontSize: "16px",
    fontWeight: 500
  },
  totalStyle: {
    color: theme.palette.body.gold,
    fontSize: "48px",
    fontWeight: 500
  },
  bodyStyle: {
    color: theme.palette.body.dark,
    fontSize: "14px"
  }
}))

// interface
interface RegisteredItemProps {
  title: string
  body: string
  total: string
}

const RegisteredItem: React.FC<RegisteredItemProps> = ({ title, body, total }) => {
  const { container, bodyStyle, totalStyle } = useStyles()
  return (
    <Card className={container}>
      <Box
        display="flex"
        alignItems="center"
        justifyContent="space-between"
        padding="16px 23px"
        borderBottom="1px solid #F2F2F2"
      >
        <Box flex="1">
          <CardTitle title={title} />
          <Box className={bodyStyle}>{body}</Box>
        </Box>

        <Box
          display="flex"
          flex="1"
          justifyContent="flex-end"
          className={totalStyle}
        >
          {total}
        </Box>
      </Box>
    </Card>
  )
}

export default RegisteredItem
