import * as React from "react"
import { makeStyles, Theme } from "@material-ui/core/styles"
import Card from "@material-ui/core/Card"
import Box from "@material-ui/core/Box"

import CardTitle from "components/CardTitle"

const Icon = (
  <svg
    width="22"
    height="19"
    viewBox="0 0 22 19"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M11 0.0996094L0 10H3V19H19V10H22L11 0.0996094ZM11 2.79102L17 8.19141V9V17H5V8.19141L11 2.79102ZM11 7C10.4696 7 9.96086 7.21071 9.58579 7.58579C9.21071 7.96086 9 8.46957 9 9C9 9.53043 9.21071 10.0391 9.58579 10.4142C9.96086 10.7893 10.4696 11 11 11C11.5304 11 12.0391 10.7893 12.4142 10.4142C12.7893 10.0391 13 9.53043 13 9C13 8.46957 12.7893 7.96086 12.4142 7.58579C12.0391 7.21071 11.5304 7 11 7ZM11 12.25C9.665 12.25 8 12.9708 8 14.3008V15H14V14.3008C14 12.9708 12.335 12.25 11 12.25Z"
      fill="#454B57"
    />
  </svg>
)

const useStyles = makeStyles((theme: Theme) => ({
  title: {
    color: theme.palette.primary.main,
    fontSize: "16px",
    fontWeight: 500
  },
  total: {
    color: theme.palette.body.greenLight,
    fontSize: "48px",
    fontWeight: 500
  },
  body: {
    color: theme.palette.body.secondary,
    fontSize: "14px"
  },
  percentage: {
    color: theme.palette.body.greenLight,
    fontSize: "14px"
  },
  bar: {
    height: "18px",
    borderRadius: "20px",
    backgroundColor: "#F2F2F2"
  },
  barHighlight: {
    height: "18px",
    width: "19.1%",
    background: theme.palette.body.greenLight,
    borderRadius: "20px"
  }
}))

// interface
interface TotalTenantedUnitsProps {
  total: string
  percentage: string
}

const TotalTenantedUnits: React.FC<TotalTenantedUnitsProps> = ({
  total,
  percentage
}) => {
  const classes = useStyles()
  return (
    <Card>
      <Box
        display="flex"
        alignItems="center"
        justifyContent="space-between"
        padding="16px 23px"
        borderBottom="1px solid #F2F2F2"
      >
        <CardTitle title="Total Tenanted Units" icon={Icon} />
      </Box>

      <Box
        display="flex"
        alignItems="center"
        justifyContent="space-between"
        padding="16px 23px"
      >
        <Box className={classes.total}>{total}</Box>
        <Box>
          <Box className={classes.percentage}>{percentage}</Box>
          <Box className={classes.body}>of total units</Box>
        </Box>
      </Box>

      <Box className={classes.bar} margin="0 23px">
        <Box className={classes.barHighlight} />
      </Box>

      <Box
        display="flex"
        alignItems="center"
        justifyContent="space-between"
        padding="26px 23px"
      >
        <Box className={classes.body} />
        <Box className={classes.body} />
      </Box>
    </Card>
  )
}

export default TotalTenantedUnits
