import React from "react"
import Card from "@material-ui/core/Card"
import Box from "@material-ui/core/Box"
import { makeStyles, Theme } from "@material-ui/core/styles"
import Chart from "react-apexcharts"

const styles = makeStyles((theme: Theme) => ({
  titleStyle: {
    color: theme.palette.secondary.dark,
    fontSize: "16px"
  },
  infoStyle: {
    color: theme.palette.body.secondary,
    fontSize: "12px"
  },
  total: {
    color: theme.palette.body.gray,
    fontSize: "30px"
  },
  value: { color: theme.palette.body.gray, fontSize: "60px" },
  body: {
    color: theme.palette.secondary.dark,
    fontSize: "14px"
  }
}))

const chartOptions = {
  options: {
    labels: ["Used", "Unbooked", "Cancelled", "No Show"],
    legend: {
      position: "top",
      height: "50px"
    },
    plotOptions: {
      pie: {
        donut: {
          size: "30%"
        }
      }
    },
    colors: ["#007B83", "#A56300", "#004E8B", "#D8B469"]
  }
}

const chartSeries = [96, 120, 12, 12]

interface FacilityProps {
  name: string
}

const Facility: React.FC<FacilityProps> = ({ name }) => {
  const { titleStyle, infoStyle, total, value, body } = styles()
  return (
    <Card>
      <Box display="flex">
        <Box borderRight="1px solid #F2F2F2">
          <Box padding="15px 30px">
            <Box className={titleStyle}>{name}</Box>
            <Box className={infoStyle}>January 17, 2020 - February 17, 2020</Box>
          </Box>
          <Box padding="0 30px 10px">
            <span className={value}>120</span>
            <span className={total}>/240</span>
          </Box>
          <Box>
            <Box
              display="flex"
              justifyContent="space-between"
              borderTop="1px solid #EDF0F1"
              padding="15px 30px"
            >
              <Box className={body}>Used</Box>
              <Box className={body}>96</Box>
            </Box>
            <Box
              display="flex"
              justifyContent="space-between"
              borderTop="1px solid #EDF0F1"
              padding="15px 30px"
            >
              <Box className={body}>Unbooked</Box>
              <Box className={body}>120</Box>
            </Box>
            <Box
              display="flex"
              justifyContent="space-between"
              borderTop="1px solid #EDF0F1"
              padding="15px 30px"
            >
              <Box className={body}>Cancelled</Box>
              <Box className={body}>12</Box>
            </Box>
            <Box
              display="flex"
              justifyContent="space-between"
              borderTop="1px solid #EDF0F1"
              padding="15px 30px"
            >
              <Box className={body}>No show</Box>
              <Box className={body}>12</Box>
            </Box>
          </Box>
        </Box>
        <Box padding="20px 0" display="flex" justifyContent="center">
          <Chart
            options={chartOptions.options}
            series={chartSeries}
            type="donut"
            width="400px"
          />
        </Box>
      </Box>
    </Card>
  )
}
export default Facility
