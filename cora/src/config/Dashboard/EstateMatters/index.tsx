const EstateMattersConfig: any = {
  filterDocs: {
    0: "All",
    1: "Active",
    2: "Expired"
  }
}

export const { filterDocs } = EstateMattersConfig
