const ApplicationsConfig: any = {
  status: {
    1: "Pending",
    2: "Approved",
    3: "Rejected"
  },
  statusColor: {
    1: "#FBA11A",
    2: "#166CBB",
    3: "#DE4625"
  },
  tableName: {
    0: "fetchApplicationsMovingInOut",
    1: "fetchApplicationsVehicle"
  },
  tableType: {
    0: "movinginout",
    1: "vehicleregistration"
  }
}

export const { status, statusColor, tableName, tableType } = ApplicationsConfig
