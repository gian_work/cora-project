import * as Yup from "yup"

const validation: Record<any, {}> = {
  rejectApplication: Yup.object().shape({
    remarks: Yup.string().required("Required")
  }),
  addApplicationDetails: Yup.object().shape({
    serialNo: Yup.string().required("Required"),
    remarks: Yup.string().required("Required"),
    startDate: Yup.string().required("Required"),
    endDate: Yup.string().required("Required")
  }),
  addApplicationsResidentInfo: Yup.object().shape({
    unitUID: Yup.string().required("Required"),
    applicantUID: Yup.string().required("Required")
  }),
  addApplicationsVehicle: Yup.object().shape({
    vehicleType: Yup.string().required("Required"),
    vehicleNo: Yup.string().required("Required"),
    carLabel: Yup.string().required("Required"),
    model: Yup.string().required("Required"),
    IUNo: Yup.string().required("Required"),
    serialNo: Yup.string().required("Required"),
    remarks: Yup.string().required("Required"),
    startDate: Yup.string().required("Required"),
    endDate: Yup.string().required("Required")
  }),
  addApplicationsMovingInOut: Yup.object().shape({
    name: Yup.string().required("Required"),
    email: Yup.string()
      .email("Invalid email")
      .required("Required"),
    eta: Yup.string().required("Required"),
    mobile: Yup.string().required("Required"),
    purpose: Yup.string().required("Required"),
    remarks: Yup.string().required("Required"),
    numberOfPersons: Yup.string().required("Required"),
    contactPerson: Yup.string().required("Required"),
    multiplePersons: Yup.string().required("Required"),
    startDate: Yup.string().required("Required"),
    endDate: Yup.string().required("Required")
  })
}

export function ApplicationCheck(args: Record<string, any>): boolean {
  const argsLength = Object.values(args).length
  if (argsLength === 0) {
    return false
  }

  return !Object.values(args).includes("")
}

export const {
  addApplicationsResidentInfo,
  addApplicationsVehicle,
  addApplicationsMovingInOut,
  rejectApplication,
  addApplicationDetails
} = validation
