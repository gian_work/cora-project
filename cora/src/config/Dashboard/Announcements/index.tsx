const AnnouncementsConfig: any = {
  FilterTabs: {
    0: "All",
    1: "Active",
    2: "Expired"
  }
}

export const { FilterTabs } = AnnouncementsConfig
