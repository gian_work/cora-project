import { mutate } from "swr"

const AnnouncementsMutate: any = {
  FetchAnnouncementPosts: () => mutate("fetchAnnouncementPost")
}

export const { FetchAnnouncementPosts } = AnnouncementsMutate
