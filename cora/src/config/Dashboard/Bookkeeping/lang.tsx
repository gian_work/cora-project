const LANG: any = {
  en: {
    MAIN: {
      ROOT: "PROPERTY MANAGMENT",
      TITLE: "Bookkeeping",
      SETTINGS: "BOOKKEEPING SETTINGS",
      NEW_PAYMENT: "New Payment"
    }
  }
}

export default LANG
