import * as Yup from "yup"

const validation: Record<any, {}> = {
  createTimein: Yup.object().shape({
    accountUID: Yup.string().required("Required")
  })
}

export const { createTimein } = validation
