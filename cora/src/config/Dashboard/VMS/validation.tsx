import * as Yup from "yup"

export const vmsValidation: Record<number, {}> = {
  1: Yup.object().shape({
    name: Yup.string().required("Required"),
    eta: Yup.string().required("Required"),
    remarks: Yup.string().required("Required"),
    email: Yup.string().email("Invalid email")
  }),
  2: Yup.object().shape({
    name: Yup.string().required("Required"),
    remarks: Yup.string().required("Required"),
    email: Yup.string().email("Invalid email"),
    startDate: Yup.string().required("Required"),
    endDate: Yup.string().required("Required")
  }),
  3: Yup.object().shape({
    name: Yup.string().required("Required"),
    eta: Yup.string().required("Required"),
    remarks: Yup.string().required("Required"),
    purpose: Yup.string().required("Required"),
    email: Yup.string().email("Invalid email"),
    startDate: Yup.string().required("Required"),
    endDate: Yup.string().required("Required")
  })
}

export const vmsResidentInfo: Record<string, {}> = {
  "default": Yup.object().shape({
    unitUid: Yup.string().required("Required"),
    contactPerson: Yup.string().required("Required")
  })
}
