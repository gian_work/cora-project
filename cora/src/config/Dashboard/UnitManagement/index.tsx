const UnitManagementConfig: any = {
  filterAccountRequests: {
    0: "New",
    1: "Change Address Requests"
  },
  filterOwners: {
    0: "New",
    1: "Change Address Requests",
    2: "Registered",
    3: "Archived"
  },
  filterResidents: {
    0: "New",
    1: "Change Address Requests",
    2: "Registered",
    3: "Archived"
  },
  filterVehicles: {
    0: "New",
    1: "Change Address Requests",
    2: "Registered",
    3: "Archived"
  }
}

export const {
  filterAccountRequests,
  filterOwners,
  filterResidents,
  filterVehicles
} = UnitManagementConfig
