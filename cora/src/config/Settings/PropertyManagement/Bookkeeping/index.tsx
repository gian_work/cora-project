const BookkeepingSettingsConfig: any = {
  tabs: {
    0: "Type",
    1: "Category",
    2: "Mode of Payment"
  }
}

export const { tabs } = BookkeepingSettingsConfig
