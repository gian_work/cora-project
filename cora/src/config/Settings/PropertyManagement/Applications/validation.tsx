import * as Yup from "yup"

export const addApplication: Record<any, any> = Yup.object().shape({
  name: Yup.string().required("Required"),
  daysAdvanceBooking: Yup.string().required("Required"),
  daysCancelBooking: Yup.string().required("Required")
})

export const addTimeSlots: Record<any, any> = Yup.object().shape({
  weekdayStartTime: Yup.string().required("Required"),
  weekdayEndTime: Yup.string().required("Required"),
  saturdayStartTime: Yup.string().required("Required"),
  saturdayEndTime: Yup.string().required("Required"),
  holidayStartTime: Yup.string().required("Required"),
  holidayEndTime: Yup.string().required("Required")
})

const validation = {
  addApplication,
  addTimeSlots
}

export default validation
