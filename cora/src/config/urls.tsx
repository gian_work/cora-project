const dashboard = "/property-management"

export default {
  pm: {
    um: `${dashboard}/unit-management`,
    vms: `${dashboard}/visitor-management`
  }
}
