const path = require("path")

module.exports = {
  parser: "babel-eslint",
  parserOptions: {
    sourceType: "module",
    allowImportExportEverywhere: false,
    codeFrame: false
  },
  extends: ["airbnb", "prettier"],
  settings: {
    "import/resolver": {
      node: {
        extensions: [".js", ".jsx", ".ts", ".tsx"],
        moduleDirectory: ["node_modules", "src/"]
      }
    }
  },
  env: {
    browser: true,
    jest: true
  },
  rules: {
    "no-plusplus": "off",
    "max-len": [
      "warn",
      {
        ignoreTemplateLiterals: true,
        ignoreStrings: true
      }
    ],
    "import/no-unresolved": ["warn"],
    "import/prefer-default-export": ["off"],
    "import/no-dynamic-require": ["off"],
    "prefer-promise-reject-errors": ["off"],
    "no-underscore-dangle": ["off"],
    "react/jsx-filename-extension": ["off"],
    "react/jsx-props-no-spreading": 1,
    "react/prop-types": ["off"],
    "react/prefer-stateless-function": ["warn"],
    "no-return-assign": ["off"],
    "no-console": 2,
    "no-unused-expressions": ["warn"],
    "no-bitwise": ["off"],
    "semi": [2, "never"],
    "jsx-a11y/click-events-have-key-events": "off",
    "spaced-comment": [
      "error",
      "always",
      {
        markers: ["/"]
      }
    ],
    "no-param-reassign": 0,
    "react/jsx-wrap-multilines": ["off"],
    "@typescript-eslint/no-explicit-any": ["warn"],
    "import/extensions": [
      1,
      "ignorePackages",
      {
        js: "never",
        jsx: "never",
        ts: "never",
        tsx: "never"
      }
    ]
  },
  ignorePatterns: ["src/__tests__", "src/types", "src/services", "src/redux"],
  overrides: [
    {
      files: "**/*.+(ts|tsx)",
      parser: "@typescript-eslint/parser",
      parserOptions: {
        project: "./tsconfig.json"
      },
      plugins: ["@typescript-eslint/eslint-plugin"],
      extends: [
        "plugin:@typescript-eslint/eslint-recommended",
        "plugin:@typescript-eslint/recommended",
        "eslint-config-prettier/@typescript-eslint"
      ],
      rules: {
        "no-unused-vars": [
          "error",
          {
            vars: "all",
            args: "after-used",
            ignoreRestSiblings: false
          }
        ]
      }
    },
    {
      files: ["**/__tests__/**"],
      settings: {
        "import/resolver": {
          jest: {
            jestConfigFile: path.join(__dirname, "./jest.config.js")
          }
        }
      }
    }
  ]
}
