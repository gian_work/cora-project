const fetch = require('node-fetch')

const url = "https://earthquake.usgs.gov/fdsnws/event/1/query?format=geojson&starttime=2014-01-01&endtime=2014-01-02"
fetch(url)
  .then(function (response) {
    return response.json();
  })
  .then(function (quakedata) {
    // console.log(quakedata.features[0]);
    const quake = quakedata.features[0]
    const date = new Date(quake.properties.time)
    const year = date.getFullYear()
    const month = date.getMonth()

    const customData = {
      magnitude: quake.properties.mag,
      location: quake.properties.place,
      when: 'test date string',
      time: 'test time',
      id: quake.id
    }

    console.log(customData)
  });